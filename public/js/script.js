$.fn.serializeObject = function() {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

var Basket = {
    add: function(items, callback, onError)
    {
        $.ajax({
            url: '/basket/add/',
            method: 'POST',
            data: {
                items: items
            },
            dataType: 'json',
            cache: false,
        }).success(function(response){
            Basket.fire('add', response);
            if (callback)
                callback(response);
        }).error(function(){
            if (onError)
                onError();
        });
    },

    remove: function(id, callback, onError)
    {
        $.ajax({
            url: '/basket/remove/',
            method: 'POST',
            data: {
                id: id
            },
            dataType: 'json',
            cache: false,
        }).success(function(response){
            Basket.fire('remove', response);
            if (callback)
                callback(response);
        }).error(function(){
            if (onError)
                onError();
        });
    },

    update: function(data, callback, onError)
    {
        $.ajax({
            url: '/basket/update/',
            method: 'POST',
            data: data,
            dataType: 'json',
            cache: false,
        }).success(function(data){
            Basket.fire('update', data);
            if (callback)
                callback(data);
        }).error(function(){
            if (onError)
                onError();
        });
    },

    checkout: function(data, callback, onError) {
        $.ajax({
            url: '/basket/checkout/',
            method: 'POST',
            data: data,
            dataType: 'json',
            cache: false,
        }).success(function (data) {
            if (callback)
                callback(data);
        }).error(function () {
            if (onError)
                onError();
        });
    },

    listeners: [],

    on: function(eventType, callback)
    {
        this.listeners.push([eventType, callback]);
    },

    fire: function(eventType, data)
    {
        _.each(this.listeners, function (listener) {
            if (listener[0].indexOf(eventType) > -1) {
                listener[1](data);
            }
        });
    }
}

var Favorites = {
    add: function (id, callback, onError) {
        $.ajax({
            url: '/favorite/add/',
            method: 'POST',
            data: {
                id: id
            },
            dataType: 'json',
            cache: false,
        }).success(function (response) {
            Favorites.fire('add', response);
            if (callback)
                callback(response);
        }).error(function () {
            if (onError)
                onError();
        });
    },
    remove: function (id, callback, onError) {
        $.ajax({
            url: '/favorite/remove/',
            method: 'POST',
            data: {
                id: id
            },
            dataType: 'json',
            cache: false,
        }).success(function (response) {
            Favorites.fire('remove', response);
            if (callback)
                callback(response);
        }).error(function () {
            if (onError)
                onError();
        });
    },

    listeners: [],

    on: function(eventType, callback)
    {
        this.listeners.push([eventType, callback]);
    },

    fire: function(eventType, data)
    {
        _.each(this.listeners, function (listener) {
            if (listener[0].indexOf(eventType) > -1) {
                listener[1](data);
            }
        });
    }
}


var Fitting = {
    add: function (id, callback, onError) {
        $.ajax({
            url: '/fitting/add/',
            method: 'POST',
            data: {
                id: id
            },
            dataType: 'json',
            cache: false,
        }).success(function (response) {
            if (callback)
                callback(response);
        }).error(function () {
            if (onError)
                onError();
        });
    },
    remove: function (id, callback, onError) {
        $.ajax({
            url: '/fitting/remove/',
            method: 'POST',
            data: {
                id: id
            },
            dataType: 'json',
            cache: false,
        }).success(function (response) {
            if (callback)
                callback(response);
        }).error(function () {
            if (onError)
                onError();
        });
    },

    listeners: [],

    on: function(eventType, callback)
    {
        this.listeners.push([eventType, callback]);
    },

    fire: function(eventType, data)
    {
        _.each(this.listeners, function (listener) {
            if (listener[0].indexOf(eventType) > -1) {
                listener[1](data);
            }
        });
    }
}

function add2Basket(productId, count, modifiers, callback)
{
    var items;
    modifiers = modifiers || null;
    if (_.isArray(productId)) {
        items = productId;
    }
    else {
        items = [{
            id : productId,
            count: count = count || 1,
            modifiers: modifiers
        }]
    }
    //var ids = _.pluck(items, 'id');
    loading();

    Basket.add(items, function(result){
        if (callback)
            callback(result);

        loaded();

        var html = $(template('added-to-basket', {basket: result.basket, added: result.added} ));
        html.modal('show');
    }, function(){
        loaded();
        alert('Ошибка');
    })
}

function removeBasketItem(id, callback) {
    Basket.remove(id, function(result){
        if (callback)
            callback(result);
    });
}

function updateBasket(id, count, callback)
{
//        loading();

    var data = {
        items:
        [{
            id: id,
            count: count
        }]
    };

    Basket.update(data, function(result){
        callback(result);
//            loaded();
    });
}

$debouncedUpdate = _.debounce(updateBasket, 200);

function quickOrder(productId)
{

}


var loadingDiv = $('<div class="loading" style="position:fixed;width:64px;height:64px;top:50%;left:50%;margin-left:-32px;margin-top:-32px;background: rgba(0,0,0, .0);z-index:666;"></div>');
function loading()
{
    $('body').prepend(loadingDiv);
}

function loaded()
{
    loadingDiv.detach();
}
function round(number, precision)
{
    var dec = Math.pow(10, precision);

    return Math.round(parseFloat(number * dec)) / dec;
}

function template(id, vars){
    vars = vars || {};
    return _.template($('#' + id + '-template').html())(vars);
}

function moneyFormat($n)
{
    return number_format($n, 0, '.', ' ');
}

function number_format(number, decimals, dec_point, thousands_sep ) {	// Format a number with grouped thousands
    //
    // +   original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +	 bugfix by: Michael White (http://crestidg.com)

    var i, j, kw, kd, km;

    // input sanitation & defaults
    if( isNaN(decimals = Math.abs(decimals)) ){
        decimals = 2;
    }
    if( dec_point == undefined ){
        dec_point = ",";
    }
    if( thousands_sep == undefined ){
        thousands_sep = ".";
    }

    i = parseInt(number = (+number || 0).toFixed(decimals)) + "";

    if( (j = i.length) > 3 ){
        j = j % 3;
    } else{
        j = 0;
    }

    km = (j ? i.substr(0, j) + thousands_sep : "");
    kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
    //kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).slice(2) : "");
    kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : "");


    return km + kw + kd;
}

function plural (number, one, two, five, append) {
    var input = number;
    number = Math.abs(number);
    number %= 100;
    var result = five;
    if (number >= 5 && number <= 20) {
        result = five;
    }
    number %= 10;
    if (number == 1) {
        result = one;
    }
    if (number >= 2 && number <= 4) {
        result = two;
    }
    return append ? (input + ' ' + result) : result;
}

jQuery.expr.filters.offscreen = function(el) {
    var rect = el.getBoundingClientRect();

    return (
        (rect.bottom < 0 || rect.top > window.innerHeight)
        || (rect.left < 0 || rect.right > window.innerWidth)
    );
};

;(function (factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery'], factory);
    } else if (typeof exports === 'object') {
        // CommonJS / nodejs module
        module.exports = factory(require('jquery'));
    } else {
        // Browser globals
        factory(jQuery);
    }
}(function ($) {
    // Namespace all events.
    var eventNamespace = 'waitForImages';

    // CSS properties which contain references to images.
    $.waitForImages = {
        hasImageProperties: [
            'backgroundImage',
            'listStyleImage',
            'borderImage',
            'borderCornerImage',
            'cursor'
        ],
        hasImageAttributes: ['srcset']
    };

    // Custom selector to find all `img` elements with a valid `src` attribute.
    $.expr[':']['has-src'] = function (obj) {
        // Ensure we are dealing with an `img` element with a valid
        // `src` attribute.
        return $(obj).is('img[src][src!=""]');
    };

    // Custom selector to find images which are not already cached by the
    // browser.
    $.expr[':'].uncached = function (obj) {
        // Ensure we are dealing with an `img` element with a valid
        // `src` attribute.
        if (!$(obj).is(':has-src')) {
            return false;
        }

        return !obj.complete;
    };

    $.fn.waitForImages = function () {

        var allImgsLength = 0;
        var allImgsLoaded = 0;
        var deferred = $.Deferred();

        var finishedCallback;
        var eachCallback;
        var waitForAll;

        // Handle options object (if passed).
        if ($.isPlainObject(arguments[0])) {

            waitForAll = arguments[0].waitForAll;
            eachCallback = arguments[0].each;
            finishedCallback = arguments[0].finished;

        } else {

            // Handle if using deferred object and only one param was passed in.
            if (arguments.length === 1 && $.type(arguments[0]) === 'boolean') {
                waitForAll = arguments[0];
            } else {
                finishedCallback = arguments[0];
                eachCallback = arguments[1];
                waitForAll = arguments[2];
            }

        }

        // Handle missing callbacks.
        finishedCallback = finishedCallback || $.noop;
        eachCallback = eachCallback || $.noop;

        // Convert waitForAll to Boolean
        waitForAll = !! waitForAll;

        // Ensure callbacks are functions.
        if (!$.isFunction(finishedCallback) || !$.isFunction(eachCallback)) {
            throw new TypeError('An invalid callback was supplied.');
        }

        this.each(function () {
            // Build a list of all imgs, dependent on what images will
            // be considered.
            var obj = $(this);
            var allImgs = [];
            // CSS properties which may contain an image.
            var hasImgProperties = $.waitForImages.hasImageProperties || [];
            // Element attributes which may contain an image.
            var hasImageAttributes = $.waitForImages.hasImageAttributes || [];
            // To match `url()` references.
            // Spec: http://www.w3.org/TR/CSS2/syndata.html#value-def-uri
            var matchUrl = /url\(\s*(['"]?)(.*?)\1\s*\)/g;

            if (waitForAll) {

                // Get all elements (including the original), as any one of
                // them could have a background image.
                obj.find('*').addBack().each(function () {
                    var element = $(this);

                    // If an `img` element, add it. But keep iterating in
                    // case it has a background image too.
                    if (element.is('img:has-src') &&
                        !element.is('[srcset]')) {
                        allImgs.push({
                            src: element.attr('src'),
                            element: element[0]
                        });
                    }

                    $.each(hasImgProperties, function (i, property) {
                        var propertyValue = element.css(property);
                        var match;

                        // If it doesn't contain this property, skip.
                        if (!propertyValue) {
                            return true;
                        }

                        // Get all url() of this element.
                        while (match = matchUrl.exec(propertyValue)) {
                            allImgs.push({
                                src: match[2],
                                element: element[0]
                            });
                        }
                    });

                    $.each(hasImageAttributes, function (i, attribute) {
                        var attributeValue = element.attr(attribute);
                        var attributeValues;

                        // If it doesn't contain this property, skip.
                        if (!attributeValue) {
                            return true;
                        }

                        allImgs.push({
                            src: element.attr('src'),
                            srcset: element.attr('srcset'),
                            element: element[0]
                        });
                    });
                });
            } else {
                // For images only, the task is simpler.
                obj.find('img:has-src')
                    .each(function () {
                        allImgs.push({
                            src: this.src,
                            element: this
                        });
                    });
            }

            allImgsLength = allImgs.length;
            allImgsLoaded = 0;

            // If no images found, don't bother.
            if (allImgsLength === 0) {
                finishedCallback.call(obj[0]);
                deferred.resolveWith(obj[0]);
            }

            $.each(allImgs, function (i, img) {

                var image = new Image();
                var events =
                    'load.' + eventNamespace + ' error.' + eventNamespace;

                // Handle the image loading and error with the same callback.
                $(image).one(events, function me (event) {
                    // If an error occurred with loading the image, set the
                    // third argument accordingly.
                    var eachArguments = [
                        allImgsLoaded,
                        allImgsLength,
                        event.type == 'load'
                    ];
                    allImgsLoaded++;

                    eachCallback.apply(img.element, eachArguments);
                    deferred.notifyWith(img.element, eachArguments);

                    // Unbind the event listeners. I use this in addition to
                    // `one` as one of those events won't be called (either
                    // 'load' or 'error' will be called).
                    $(this).off(events, me);

                    if (allImgsLoaded == allImgsLength) {
                        finishedCallback.call(obj[0]);
                        deferred.resolveWith(obj[0]);
                        return false;
                    }

                });

                if (img.srcset) {
                    image.srcset = img.srcset;
                }
                image.src = img.src;
            });
        });

        return deferred.promise();

    };
}));

function slideTo(selector, offset)
{
    offset || 0;
    return $('html, body').animate({
        scrollTop: $(selector).offset().top + offset
    }, 300);
}

function showModalSimple(id)
{
    var content = template(id);
    var modal = template('modal', {content: content})

    $(modal).appendTo('body').modal('show');
}

function showProductAvailabilityModal(id)
{
    $('<div></div>').load('/product/' + id + '/availability/', function(){
        var modal = template('modal', {content: $(this).html()})

        $(modal).appendTo('body').modal('show');
    });
}

function selectFrame(lensId, el){
    var content = template('vibrat-opravy-dlya-linz-modal', {lensId: lensId});
    //                                    var div = $('#vibrat-opravy-dlya-linz-modal-template');
    var modal = template('modal', {content: content});
    $(modal).modal('show');

    //                                    $().get({
    //                                        url: '/product/select_frame',
    //                                        dataType: 'html',
    //                                        success: function()
    //                                        {
    //                                            $(result).modal('show');
    //                                        }
    //
    //                                    })
}

function configurateLenses(lensesId, basketItemId)
{
    if (basketItemId)
        var url = '/product/' + lensesId + '/configurate/?frame_id=' + basketItemId;
    else
        var url = '/product/' + lensesId + '/configurate/?a=b';

    url += '&cache=' + _.now();

    $('.modal').modal('hide');
    var modal = $(template('modal', {content: ''}));

    modal.find('.modal-content').html('<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Закрыть"><span aria-hidden="true">×</span></button><h3>Параметры линз</h3></div><div class="modal-body"></div>');
    modal.find('.modal-body').load(url, function(){
        modal.find('form').submit(function(e){
            e.preventDefault();

            $.ajax({
                method: 'POST',
                url: '/basket/add_lenses/',
                dataType: 'json',
                data: $(this).serializeObject(),
            }).success(function(result){
                modal.html('').modal('hide');
                var html = $(template('added-to-basket', {basket: result.basket, added: result.added} ));
                html.modal('show');
            });
        })
    })

    modal.modal('show').on('hidden.bs.modal', function(){
        modal.remove();
    });
}

function toggleFavorite(id, el)
{
    $(el).button('loading');

    if ($(el).data('favorite')) {
        Favorites.remove(id, function(){
            $(el).button('default');
            $(el).data('favorite', 0)
        });
    }
    else {
        Favorites.add(id, function(){
            $(el).button('success')
            $(el).data('favorite', 1)
        });
    }
}

function openCallbackWindow(el)
{
    kz.callRequestForm(el)
}

function addToFavorites(id, el)
{
    if ($(el).hasClass('opened'))
        return;

    $(el).button('loading');
    Favorites.add(id, function(){
        $(el).button('success').addClass('opened');
    });
}

function removeFromFavoritesCardClick(id, el){
    var $button = $(el).parents('.dropdown').find('a.dropdown-toggle');
    $button.button('loading');
    Favorites.remove(id, function(){
        $button.button('default').removeClass('opened');
    })
}

function addProductToFitting(productId, el)
{
    $(el).button('loading');

    var onErr = function(){
        $(el).button('reset');
        alert('Ошибка');
    };

    if ($(el).attr('data-active') == 1) {
        Fitting.remove(productId, function(result){
            if (result.success) {
                $(el).button('reset');
                $(el).attr('data-active', 0);
            }
            else {
                $(el).button('reset');
                flash(result.error, 'error');
            }
        }, onErr)
    }
    else {
        Fitting.add(productId, function(result){
            if (result.success) {
                $(el).button('reset');
                $(el).attr('data-active', 1);

                var html = $(template('added-to-basket', {basket: result.basket, added: result.added, fitting: true, fittings: result.fittings} ));
                html.modal('show');
            }
            else {
                $(el).button('reset');
                flash(result.error, 'error');
            }
        }, onErr)
    }
}

function setcookie(name, value, expires, path, domain, secure)
{
    document.cookie =    name + "=" + escape(value) +
        ((expires) ? "; expires=" + (new Date(expires)) : "") +
        ((path) ? "; path=" + path : "") +
        ((domain) ? "; domain=" + domain : "") +
        ((secure) ? "; secure" : "");
}


/**
 * Получить значение куки по имени name
 *
 */
function getcookie(name)
{
    var cookie = " " + document.cookie;
    var search = " " + name + "=";
    var setStr = null;
    var offset = 0;
    var end = 0;

    if (cookie.length > 0)
    {
        offset = cookie.indexOf(search);

        if (offset != -1)
        {
            offset += search.length;
            end = cookie.indexOf(";", offset)

            if (end == -1)
            {
                end = cookie.length;
            }

            setStr = unescape(cookie.substring(offset, end));
        }
    }

    return(setStr);
}