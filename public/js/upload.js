function template(id, vars){
    var template =  _.template($('#' + id + '-template').html());
    vars = vars || {};
    return template(vars);
}

function showUploadModal(callback, multiple)
{
    var multiple = multiple || false;
    var $el = $('<div class="container" style="margin-top:250px;"><div class="col-md-8 col-md-offset-2"></div></div>');

    $el.find('div').append($(template('upload-modal', {multiple: multiple})));

    var updateThumbs = function(input, callback) {
        if (input.files && input.files.length > 0) {
            for (i=0;i<=(input.files.length -1 );i++) {

                callback(URL.createObjectURL(input.files[i]));
            }
        }
    };

    var $gallery = $el.find('.image-uploader-thumbs');
    var $form = $el.find('form');
    var $button = $form.find('.start-upload');

    $el.find('[type="file"][name="upload[]"]').change(function(){
        $gallery.html('');

        updateThumbs(this, function(url){
            $img = $('<div style="width:80px;height:80px;float:left;margin-right:6px;"><img style="max-width:80px;max-height:80px" src="' + url + '"/></div>');

            $gallery.append($img);
        });
    });

    //$el.find('.submit-url').click(function(){
    //    console.log(123123);
    //    var url = $el.find('[type="text"][name="upload[]"]').val();
    //
    //    if (! url)
    //        return;
    //
    //    $img = $('<div style="width:80px;height:80px;float:left;margin-right:6px;"><img style="max-width:80px;max-height:80px" src="' + url + '"/></div>');
    //
    //    $gallery.append($img);
    //
    //    var url = $el.find('[type="text"][name="upload[]"]').val('');
    //});

    $el.find('.nav-tabs a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    })

    $form.submit(function(e){
        e.preventDefault();

        var $gallery = $el.find('.image-uploader-thumbs');

        var oldText = $button.text();
        $button.attr('disabled', 'disabled').addClass('disabled').text('Идет загрузка...');

        $form.ajaxSubmit(function(data){
            var after = function() {
                if (data.success) {
                    $form.clearForm();
                    $gallery.html('');
                }

                $button.removeAttr('disabled', 'disabled').removeClass('disabled').text(oldText);
            }

            if (data.success) {
                callback(data.result);
                $el.modal().close();
            }
            else {
                alert('Ошибка загрузки');
                after();
            }
        });
    });

    $el.modal().open();
}