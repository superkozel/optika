`<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwords must be at least six characters and match the confirmation.',
    'reset' => 'Ваш пароль был сброшен!',
    'sent' => 'На вашу почту отправлено письмо с кодом для сброса пароля!',
    'token' => 'Неверный код сброса пароля.',
    'user' => "Пользователь с таким email не найден.",

];
