<?php
/** @var DataGrid $grid */
?>
<table class="table table-condensed table-bordered">
    <thead>
    <tr>
        <? foreach ($grid->getColumns() as $key => $column):?>
            <?
            if (is_array($column)) {
                $title = array_get($column, 'title');
            }
            else {
                $title = $column;
            }
            ?>
            <th><?=$title?></th>
        <? endforeach;?>
    </tr>
    </thead>
    <tbody>
    <? foreach ($grid->getPaginator() as $model):?>
        <tr>
        <? foreach ($grid->getColumns() as $key => $column):?>
            <td>
            <?
            $type = null;
            if (is_numeric($key)) {
                if (is_array($column)){
                    $id = array_get($column, 'id');
                    $title = array_get($column, 'title');
                    $type = array_get($column, 'type');
                }
                else {
                    $title = $column;
                    $id = $column;
                }
            }
            else {
                $title = $column;
                $id = $key;
            }
            ?>

            <? if ($type == 'buttons'):?>
                <? $buttons = array_get($column, 'buttons', ['edit']);?>
                <? foreach ($buttons as $button):?>
                    <? if ($button == 'edit'):?>
                        <a href="<?=$model->getEditUrl()?>"><i class="fa fa-pencil"></i></a>
                    <? endif;?>
                    <? if ($button == 'delete'):?>
                        <a href="<?=$model->getDeleteUrl()?>"><i class="fa fa-close"></i></a>
                    <? endif;?>
                <? endforeach;?>
            <? elseif ($type == 'raw'):?>
                <?=$column['content']($model)?>
            <? elseif ($type == 'count'):?>
                <?=$model->{$id}->count()?>
            <? else:?>
                <? $value = $model->{$id};

                if (is_subclass_of($value, EloquentImage::class) || is_subclass_of($value, BaseImage::class)) {
                    $string = $value::img($value, 'mini');
                }
                else if ($value instanceof \Illuminate\Database\Eloquent\Collection) {
                    if ($value->first() instanceof ActiveRecord) {
                        $string = join(',', $value->pluck('name')->all());
                    }
                    else if (is_subclass_of($value->first(), EloquentImage::class)){
                        $img = $value->first();
                        $string = $img::img($img, 'mini');
                    }
                }
                else if ($value instanceof ActiveRecord) {
                    $string = Html::link($value->getEditUrl(), $value->getName());
                }
                else if ($value instanceof Eloquent) {
                    $string = $value->name;
                }
                else if ($value instanceof BaseEnum) {
                    $string = $value->getName();
                }
                else if ($id == 'name' && method_exists($model, 'getEditUrl')){
                    $string = Html::link($model->getEditUrl(), $value);
                }
                else{
                    $string = $value;
                }
                ?>
                <?=$string?>
            <? endif;?>
            </td>
        <? endforeach;?>
        </tr>
    <? endforeach;?>
    </tbody>
</table>
<?=view('front.parts.pagination', ['paginator' => $grid->getPaginator()])?>