<div style="max-width:500px">
    <?=Bootstrap3Form::model($user, compact('errors'))?>
    <?=Bootstrap3Form::errorSummary($errors)?>
    <?=Bootstrap3Form::textRow('name', 'Имя')?>
    <?=Bootstrap3Form::textRow('lastname', 'Фамилия')?>

    <?=Bootstrap3Form::selectRow('role_id', 'Роль', UserRole::getNames())?>

    <?=Bootstrap3Form::submit('Сохранить')?>
    <?=Bootstrap3Form::close()?>
</div>
