<a class="btn btn-primary" href="<?=action('AdminUserController@create')?>">Создать</a>
<?=DataGrid::create()
    ->setColumns([
        'id',
        [
            'type' => 'raw',
            'title' => 'Имя, фамилия',
            'content' => function($model) {
                return Html::link($model->getEditUrl(), $model->name . ' ' . $model->lastname);
            }
        ],
        'role' => 'роль',
        [
            'id' => 'email',
            'type' => 'raw',
            'title' => 'email',
            'content' => function($user) {
                return $user->email . ($user->isEmailConfirmed() ? (' <i class="fa fa-check" title="подтвержден"></i>') : '');
            }
        ],
        [
            'id' => 'phone',
            'type' => 'raw',
            'title' => 'Телефон',
            'content' => function(\App\User $user) {
                return $user->phone . ($user->isPhoneConfirmed() ? (' <i class="fa fa-check" title="подтвержден"></i>') : '');
            }
        ],
        'bonus_card_number' => 'номер карты',
        [
            'id' => 'orders',
            'type' => 'count',
            'title' => 'Количество заказов'
        ],
        [
            'type' => 'buttons'
        ],
    ])
    ->setData($users)
    ->render()
?>