<?=Bootstrap3Form::model($brand, compact('errors'))?>
<?=Bootstrap3Form::errorSummary($errors)?>
<?=Bootstrap3Form::textRow('name', 'Название')?>
<?=Bootstrap3Form::textRow('alt_name', 'Второе название')?>
<?=Bootstrap3Form::textareaRow('short_description', 'Короткое описание')?>
<?=Bootstrap3Form::textareaRow('description', 'Описание')?>
<?=Bootstrap3Form::formRow('image', 'Логотип', View::make('front.upload.widget', array('model' => $brand, 'property' => 'image'))->render())?>
<?=Bootstrap3Form::submit('Сохранить')?>
<?=Bootstrap3Form::close()?>
