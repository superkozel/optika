<a class="btn btn-primary" href="<?=$controller->getUrl('create')?>">Создать</a>
<?=DataGrid::create()
    ->setColumns([
        'id',
        'image' => 'логотип',
        'name' => 'название',
        'short_description' => 'короткое описание',
        [
            'type' => 'buttons'
        ],
    ])
    ->setData($brands)
    ->render()
?>