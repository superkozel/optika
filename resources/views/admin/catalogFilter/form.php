<?=Bootstrap3Form::model($catalogFilter, compact('errors'))?>
<?=Bootstrap3Form::errorSummary($errors)?>
<?=Bootstrap3Form::textRow('name', 'Название')?>
slug: <?=$catalogFilter->slug?><br/>
<?=Bootstrap3Form::selectRow('type_id', 'Тип фильтра', ['' => '-'] + CatalogFilterType::getNames())?>
<?=Bootstrap3Form::selectRow('value_type', 'Тип значения', ['' => '-'] + CatalogFilterValueType::getNames())?>

<? if ($catalogFilter->value_type->is(CatalogFilterValueType::ATTRIBUTE)):?>
    <?=Bootstrap3Form::selectRow('value_id', 'Атрибут', ['' => '-'] + Attribute::pluck('name', 'xml_id')->all())?>
<? elseif ($catalogFilter->value_type->is(CatalogFilterValueType::RELATION)):?>
    <? $product = new Product();$relations = $product->getRelationships();?>
    <?=Bootstrap3Form::selectRow('value_id', 'Связь', ['' => '-'] + $relations)?>
<? elseif ($catalogFilter->value_type->is(CatalogFilterValueType::COLUMN)):?>
    <? $product = new Product();$attrs = Schema::getColumnListing($product->getTable());?>
    <?=Bootstrap3Form::selectRow('value_id', 'Поле таблицы', ['' => '-'] + array_combine($attrs, $attrs))?>
<? else:?>
    <?=dd($catalogFilter->value_type)?>
    <?=dd($catalogFilter->name)?>
<? endif;?>

<?=Bootstrap3Form::textareaRow('description', 'Описание')?>
<?=Bootstrap3Form::selectRow('sort_type', 'Способ сортировки значений', CatalogFilterSortType::getNames())?>
<?=Bootstrap3Form::selectRow('sort_desc', 'Обратная сортировка', [0 => 'Нет', 1 => 'Да'])?>

<?=Bootstrap3Form::submit('Сохранить')?>
<?=Bootstrap3Form::close()?>
