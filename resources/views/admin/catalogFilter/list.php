<a class="btn btn-primary" href="<?=$controller->getUrl('create')?>">Создать</a>
<?=DataGrid::create()
    ->setColumns([
        'id',
        'name' => 'название',
        'type' => 'тип',
        'value_type' => 'тип значения',
        'value_id' => 'id поля значения',
        'options' => 'опции',
        'sort_type' => 'сортировка значений',
        [
            'type' => 'buttons'
        ],
    ])
    ->setData($catalogFilters)
    ->render()
?>