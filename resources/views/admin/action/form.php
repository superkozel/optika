<?=Bootstrap3Form::model($action, compact('errors'))?>

<?=Bootstrap3Form::errorSummary($errors)?>
<?=Bootstrap3Form::textRow('name', 'Название')?>
<?=Bootstrap3Form::textareaRow('preview_text', 'Предварительный текст')?>
<?=Bootstrap3Form::textareaRow('content', 'Полный текст')?>
<div class="input-group">
   <span class="input-group-btn">
     <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
       <i class="fa fa-picture-o"></i> Картинки
     </a>
   </span>
</div>
<img id="holder" style="margin-top:15px;max-height:100px;">
<script src="/vendor/laravel-filemanager/js/lfm.js"></script>
<script>
    $('#lfm').filemanager('images');
</script>

<?=Bootstrap3Form::dateRow('active_from', 'Начало акции', $action->active_from->format('Y-m-d'))?>
<?=Bootstrap3Form::dateRow('active_to', 'Последний день акции', $action->active_to->format('Y-m-d'))?>

<?=Bootstrap3Form::checkboxListRow('salons', 'Салоны акции', Salon::pluck('name', 'id')->all(), $action->salons()->pluck('id')->all())?>

<?=Bootstrap3Form::formRow('image', 'Картинка', View::make('front.upload.widget', array('model' => $action, 'property' => 'image'))->render())?>
<?=Bootstrap3Form::submit('Сохранить')?>
<?=Bootstrap3Form::close()?>
