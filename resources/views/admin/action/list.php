<?=DataGrid::create()
    ->setColumns([
        'id',
        'image',
        'name' => 'название',
        'active_from' => 'дата начала',
        'active_to' => 'дата конца',
        'preview_text' => 'короткое описание',
        [
            'type' => 'buttons'
        ],
    ])
    ->setData($actions)
    ->render()
?>