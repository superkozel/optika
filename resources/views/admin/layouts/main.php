<html>
<head>
	<?=Html::script('/js/vendor/jquery-1.11.3.min.js')?>
	<?=Html::script('/js/vendor/maskedinput.js')?>
	<?=Html::script('/js/vendor/underscore.js')?>
	<?=Html::script('/js/upload.js')?>
	<?=Html::script('http://malsup.github.io/min/jquery.form.min.js')?>

	<?=Html::script('/bootstrap/js/bootstrap.min.js')?>
	<?=Html::style('/bootstrap/css/bootstrap.min.css')?>
	<?=Html::style('/css/app.css')?>

	<?=Html::script('/js/vendor/modal.js')?>
	<?=Html::style('/css/modal.css')?>

	<script src="https://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js"></script>

	<script src="/vendor/select2/select2.min.js"></script>
	<link href="/vendor/select2/select2.css" rel="stylesheet"/>

	<link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
	<script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
	<?=Html::style('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css')?>

    <script src="/js/vendor/moment/moment-with-locales.min.js"></script>
    <script src="/js/vendor/datepicker/js/bootstrap-datetimepicker.min.js"></script>
    <link href="/js/vendor/datepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
    <script src="/vendor/laravel-filemanager/js/lfm.js"></script>
	<script>
		$.fn.editable.defaults.params = function (params) {
			params._token = '<?=csrf_token()?>';
			return params;
		};
        $("[data-toggle=popover]").popover();
	</script>
	<?
	$badge = function($count){
		if ($count)
			return '<span class="badge">' . $count . '</span>';
	}
	?>
	<style>
		body, body *{font-size:13px;}
	</style>
</head>
<body>
	<? if (Flash::has()):?>
		<? $flash = Flash::get();?>
		<?
		$class = $flash['level'];
		switch ($flash['level'])
		{
			case 'success':
				$icon = 'check';
				break;
			case 'error':
				$icon = 'remove';
				break;
			case 'warning':
				$class = 'danger';
				$icon = 'warning-sign';
				break;
			case 'info':
				$icon = 'info-sign';
				break;
		}
		?>
		<div id="flash" class="alert alert-<?=$class?>" style="position: fixed;right:12px;top:12px;font-size:24px;z-index:666;">
			<i class="fa fa-<?=$icon?>"></i> <?=$flash['text']?>
		</div>
		<script>
			$('#flash').delay(2000).fadeOut('slow');
		</script>
	<? endif;?>
	<header>

	</header>
	<div style="width:100%;height:30px;padding:6px;padding-left:25px;">
        <span class="btn btn-primary"><i class="fa fa-user"></i> <?=$user->name?> <?=$user->lastname?></span>
		<a class="btn btn-primary btn-sm" href="/">Вернуться на сайт</a>
	</div>
	<div style="float:left;width:200px;margin-left:20px;margin-top:30px;position:absolute;">
		<nav>
			<div class="panel panel-default">
				<div class="panel-body">
					<b>Контент</b><br/>
					<a href="<?=action('AdminBrandController@index')?>">Бренды</a><br/>
					<a href="<?=action('AdminSalonController@index')?>">Салоны</a><br/>
					<a href="<?=action('AdminActionController@index')?>">Акции</a><br/>
                    <a href="<?=action('AdminNewsController@index')?>">Новости</a><br/>
                    <br/>
                    <a href="<?=action('AdminFileController@files')?>">Файлы</a><br/>
                    <a href="<?=action('AdminFileController@images')?>">Картинки</a><br/>
                    <br/>
                    <a href="<?=action('AdminPageController@index')?>">Страницы</a><br/>
                    <br/>
                    <a href="<?=action('AdminBannerController@index')?>">Баннеры</a><br/>
				</div>
			</div>
            <? if ($user->isAdmin()):?>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <b>Каталог</b><br/>
                        <br/>
                        <a href="<?=action('AdminProductController@index')?>">Товары</a><br/>
                        <a href="<?=action('AdminProductTypeController@index')?>">Типы товаров</a><br/>
                        <br/>
                        <a href="<?=action('AdminAttributeController@index')?>">Атрибуты</a><br/>
                        <a href="<?=action('AdminAttributeSetController@index')?>">Наборы атрибутов</a><br/>
                        <br/>
                        <a href="<?=action('AdminCatalogCategoryController@index')?>">Категории</a><br/>
                        <br/>
                        <a href="<?=action('AdminCatalogFilterSetController@index')?>">Набор фильтров</a><br/>
                        <a href="<?=action('AdminCatalogFilterController@index')?>">Фильтры</a><br/>
                    </div>
                </div>
            <? endif;?>

            <div class="panel panel-default">
                <div class="panel-body">
                    <b>Интернет-магазин</b><br/>
                    <br/>
                    <a href="<?=action('AdminUserController@index')?>">Пользователи</a> <span class="label label-info"><?=\App\User::count()?></span><br/>
                    <a href="<?=action('AdminOrderController@index')?>">Заказы</a> <? if ($count = Order::where('status_id', OrderStatus::NEWS)->count()):?><span class="label label-danger">+<?=$count?></span><?endif;?><br/>
                </div>
            </div>

			<!--				<div class="panel panel-default">-->
			<!--					<div class="panel-body">-->
			<!--						<b>Салоны</b>-->
			<!--						<hr/>-->
			<!--						<a href="--><?//=action('AdminSalonController@index')?><!--">Салоны</a><br/>-->
			<!--						<a href="--><?//=action('AdminSalonController@index')?><!--">Услуги салонов</a><br/>-->
			<!--						<a href="--><?//=action('AdminSalonController@index')?><!--">Специализации салонов</a><br/>-->
			<!--					</div>-->
			<!--				</div>-->
		</nav>
	</div>
	<div style="float:left;width:100%;padding-left:220px;margin-top:30px;">
		<div class="container-fluid">
<!--			--><?//=view('front.layouts.parts.breadcrumbs')->render()?>
			<h1 class="page-header" style="margin-top:0px;font-size:21px;"><?=mb_ucfirst($h1)?></h1>
			<?=$content?>
		</div>
	</div>
</body>

<script type="text/html" id="upload-modal-template">
	<?=Form::open(array('url' => action('AdminUploadController@store'), 'files' => true));?>
	<?=Form::token()?>
	<div class="image-uploader panel panel-primary">
		<div class="panel-heading">
			<span class="h4 panel-title"><i class="fa fa-picture-o"></i> Загрузка изображений</span>
		</div>

		<div class="panel-body">
			<ul class="nav nav-tabs nav-justified" style="margin-bottom:15px">
				<li role="presentation" class="active"><a data-toggle="tab" role="tab" href="#upload-tab-disk">С диска</a></li>
				<li role="presentation"><a data-toggle="tab" role="tab"  href="#upload-tab-url">URL</a></li>
			</ul>

			<div class="tab-content">
				<div role="tabpanel" class="tab-pane fade active in" id="upload-tab-disk">
					<input type="file" name="upload[]"  <% if (multiple){%>multiple<% } %>/>
					<div class="image-uploader-thumbs" style="margin:24px 0px;"></div>
				</div>
				<div role="tabpanel" class="tab-pane fade" id="upload-tab-url">
					<?=Bootstrap3Form::text('url', null, ['placeholder' => 'http://'])?>
				</div>
			</div>
		</div>

		<div class="panel-footer">
			<button type="submit" class="btn btn-success pull-right start-upload" class="start-upload"><i class="fa fa-download"/> Загрузить</button>
			<div class="clearfix"></div>
		</div>
	</div>
	<?=Form::close();?>
</script>
</html>
