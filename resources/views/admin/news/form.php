<?=Bootstrap3Form::model($news, compact('errors'))?>
<?=Bootstrap3Form::errorSummary($errors)?>
<?=Bootstrap3Form::textRow('name', 'Название')?>
<?=Bootstrap3Form::textareaRow('preview_text', 'Предварительный текст')?>
<?=Bootstrap3Form::textareaRow('content', 'Полный текст')?>
<?//=Bootstrap3Form::select2Row('category_id', 'Категория', [])?>
<?=Bootstrap3Form::formRow('image', 'Картинка', View::make('front.upload.widget', array('model' => $news, 'property' => 'image'))->render())?>
<?=Bootstrap3Form::submit('Сохранить')?>
<?=Bootstrap3Form::close()?>
