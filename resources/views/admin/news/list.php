<a class="btn btn-primary" href="<?=$controller->getUrl('create')?>">Создать</a>
<?=DataGrid::create()
    ->setColumns([
        'id',
        'image' => 'картинка',
        'name' => 'название',
        'created_at' => 'дата публикации',
        'preview_text' => 'описание',
        'category' => 'категория',
        [
            'type' => 'buttons'
        ],
    ])
    ->setData($news)
    ->render()
?>