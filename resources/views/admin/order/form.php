<?php
/** @var Order $order */
?>
<div class="row">
    <div class="col-md-6">
        <?=Bootstrap3Form::model($order, compact('errors'))?>
        <?=Bootstrap3Form::errorSummary($errors)?>
        <?=Bootstrap3Form::textRow('name', 'Имя')?>
        <?=Bootstrap3Form::textRow('phone', 'Телефон')?>
        <?=Bootstrap3Form::textRow('email', 'email')?>
        <?=Bootstrap3Form::selectRow('status_id', 'Статус', OrderStatus::getNames())?>
        <?=Bootstrap3Form::selectRow('manager_id', 'Менеджер',
            ['' => 'Не назначен'] + \App\User::whereNotIn('role_id', [UserRole::CUSTOMER])->get()
            ->keyBy('id')
            ->map(function($v){return $v->name . ' ' . $v->lastname . '[' . $v->email . ']';})->all()
        )?>

        <?=Bootstrap3Form::dateRow('delivery_date', 'Дата доставки')?>
        <? if ($order->delivery_type->timeIntervals()):?>
            <?=Bootstrap3Form::selectRow('delivery_time', 'Время доставки', $order->delivery_type->timeIntervals())?>
        <? endif;?>
        <? if ($order->delivery_type->is(DeliveryType::SAMOVYVOZ)):?>
            <? $salons = Salon::orderBy('city_id')->get();
            $opts = [];
            foreach ($salons as $s) {
                $opts[$s->id] = $s->city->name . ' ' . $s->address;
            }
            ?>
            <?=Bootstrap3Form::selectRow('salon_id', 'Салон', $opts)?>
        <? else:?>
            <?=Bootstrap3Form::textareaRow('address', 'Адрес')?>
        <? endif;?>

        <div class="row">
            <div class="col-md-6">
                <?=Bootstrap3Form::textareaRow('comment', 'Комментарий покупателя')?>
            </div>
            <div class="col-md-6">
                <?=Bootstrap3Form::textareaRow('manager_comment', 'Комментарий менеджера')?>
            </div>
        </div>

        <?=Bootstrap3Form::submit('Сохранить')?>
        <?=Bootstrap3Form::close()?>
    </div>
    <div class="col-md-6">
        <h3>Оплата по заказу:</h3>
        <? foreach ($order->transactions()->where('status_id', \Enum\PaymentTransactionStatus::COMPLETE)->get() as $trans):?>
            #<?=$trans->id?> [Яндекс.Деньги.id=<?=$trans->transaction_id?>]<br/>
            <?=moneyFormat($trans->sum)?> руб.<br/>
            Оплачено <?=$trans->paid_at ? $trans->paid_at->format('d.m.Y H:i:s') : ''?>
        <? endforeach;?>
        <hr/>
        <?=view('front.order.parts.details', ['order' => $order])?>
        <hr/>
        <?=view('front.order.parts.items', ['order' => $order, 'items' => $order->items])?>
    </div>
</div>

</div>