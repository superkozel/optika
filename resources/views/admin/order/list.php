<div class="row">
<?=Bootstrap3Form::open(['method' => 'GET']);?>
    <div class="col-sm-2">
        <?=Bootstrap3Form::selectRow('status_id', 'Статус', ['-' => 'все'] + OrderStatus::getNames(), Input::get('status_id'), ['onchange' => "$(event.currentTarget).parents('form').submit()"])?>
    </div>
    <div class="col-sm-2">
        <?=Bootstrap3Form::selectRow('manager_id', 'Менеджер',
            ['-' => 'все'] + \App\User::whereNotIn('role_id', [UserRole::CUSTOMER])->get()
                ->keyBy('id')
                ->map(function($v){return $v->name . ' ' . $v->lastname . '[' . $v->email . ']';})->all(),
            Input::get('manager_id'), ['onchange' => "$(event.currentTarget).parents('form').submit()"])?>
    </div>
<?=Bootstrap3Form::close();?>
</div>
<?=DataGrid::create()
    ->setColumns([
        'id',
        'name' => 'покупатель',
        'created_at' => 'время',
        'phone' => 'телефон',
        'status' => 'статус',
        'payment_type' => 'способ оплаты',
        [
            'id' => 'payment_status',
            'type' => 'raw',
            'title' => 'Статус оплаты',
            'content' => function(Order $order){
                return $order->payment_required_sum > 0 ? ($order->getRemainingPayment() == 0 ? '<span style="color:green">оплачен</span>' : '<span style="color:red">ожидается оплата</span>') : 'оплата при получении';
            },
        ],
        [
            'id' => 'payment',
            'type' => 'raw',
            'title' => 'Оплата',
            'content' => function(Order $order){
                return $order->payment_required_sum > 0 ? (int)$order->paid_sum . '/' . $order->payment_required_sum . ' руб.' : '-';
            },
        ],
        'manager' => 'менеджер',
        'sum' => 'сумма',
        'email' => 'email',
        [
            'type' => 'raw',
            'content' => function(Order $order) {
                '<a href="accept">Принять</a>';
            }
        ],
        [
            'type' => 'buttons',
        ],
    ])
    ->setData($orders)
    ->render()
?>