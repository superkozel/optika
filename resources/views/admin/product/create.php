<?=Bootstrap3Form::model($product)?>
<?=Bootstrap3Form::errorSummary($errors)?>
<?=Bootstrap3Form::selectRow('type_id', 'Тип товара', ProductType::getNames())?>

<?=Bootstrap3Form::textRow('name', 'Название')?>
<?=Bootstrap3Form::textRow('model_name', 'Название модели')?>
<?=Bootstrap3Form::textRow('article', 'Артикул')?>
<?=Bootstrap3Form::textareaRow('description', 'Описание')?>
<?=Bootstrap3Form::textareaRow('short_description', 'Короткое описание')?>

<?=Bootstrap3Form::selectRow('brand_id', 'Бренд', Brand::select()->get()->pluck('id', 'name'))?>

<?=Bootstrap3Form::inputRow('number', 'price', 'Цена')?>
<?=Bootstrap3Form::inputRow('number', 'discount', 'Скидка')?>
<?=Bootstrap3Form::inputRow('number', 'available', 'В наличии количество')?>

<?=Bootstrap3Form::selectRow('new', 'Новинка', ['Нет', 'Да'])?>
<?=Bootstrap3Form::selectRow('hit', 'Популярное', ['Нет', 'Да'])?>
<?=Bootstrap3Form::selectRow('trend', 'Тренд', ['Нет', 'Да'])?>

<div class="form-group">
    <label for="photos[]">Фотографии</label>

    <?=View::make('front.upload.widget', array('model' => $product, 'property' => 'images'))?>
</div>

<div class="attributes">

</div>

<?=Bootstrap3Form::submit('Сохранить')?>

<?=Bootstrap3Form::close();?>
<script>
    $().ready(function(){
        $(['name=type_id']).change(function(){
            $.ajax({
                url: '<?=action('AdminProductController@getAttributes')?>',
                data: {
                    typeId: $(['name=type_id']).val(),
                },
                dataType: 'jsonp'
            }).
                success(function(){
               $('.attributes').html(data);
            })
        });
    })
</script>