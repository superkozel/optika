<a class="btn btn-primary" href="<?=$controller->getUrl('create')?>">Создать</a>
<?=DataGrid::create()
    ->setColumns([
        'id',
        'images' => 'фото',
        'article' => 'артикул',
        'name' => 'название',
        'type' => 'тип товара',
        'price' => 'цена',
        'discount' => 'скидка',
//        [
//            'type' => 'buttons'
//        ],
    ])
    ->setData($products)
    ->render()
?>