<?  foreach ($attributes as $attr):?>
    <? $name = 'attributes[' . $attr->id . ' ]';?>
    <? if ($attr->type_id == AttributeType::BOOL):?>
        <?=Bootstrap3Form::checkbox($name, $attr->name, 1, false)?>
    <? elseif ($attr->type_id == AttributeType::NUMBER):?>
        <?=Bootstrap3Form::inputRow('number', $name, $attr->name, $product->getPropertyValue($attr->id))?>
    <? endif;?>
<? endforeach;?>
