<a class="btn btn-primary" href="<?=$controller->getUrl('create')?>">Создать</a>
<?=DataGrid::create()
    ->setColumns([
        'id',
        'image' => 'картинка',
        'name' => 'название',
        'url' => 'адрес ссылки',
        'position' => '',
        'active_to' => 'дата окончания',
        [
            'type' => 'buttons'
        ],
    ])
    ->setData($banners)
    ->render()
?>