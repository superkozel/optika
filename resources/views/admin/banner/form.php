<?=Bootstrap3Form::model($banner, compact('errors'))?>
<?=Bootstrap3Form::errorSummary($errors)?>
<?=Bootstrap3Form::textRow('name', 'Название')?>
<?=Bootstrap3Form::textRow('url', 'Адрес ссылки')?>
<?=Bootstrap3Form::imageUploadRow('image', 'Картинка')?>

<?=Bootstrap3Form::selectRow('position_id', 'Позиция', BannerPosition::getNames())?>
<?=Bootstrap3Form::dateRow('active_to', 'Активен до', $banner->active_to ? $banner->active_to->format('Y-m-d') : '')?>

<?=Bootstrap3Form::submit('Сохранить')?>
<?=Bootstrap3Form::close()?>
