<?=Bootstrap3Form::model($catalogFilterSet)?>
<?=Bootstrap3Form::errorSummary($errors)?>

<?=Bootstrap3Form::textRow('name', 'Название')?>

<? foreach(CatalogFilter::all() as $filter):?>
<? endforeach;?>

<div class="row form-group" style="margin-bottom:12px;">
    <div class="col-md-12">
        <label for="">Фильтры</label>
        <table class="table table-bordered" id="filters-table" style="width:auto;">
            <thead>
                <tr>
                    <td>
                    </td>
                    <td>
                        Фильтр
                    </td>
                    <td>
                        Порядок сортировки
                    </td>
                </tr>
            </thead>
            <? $filters = $catalogFilterSet->filters()->orderBy('catalog_filter_catalog_filter_set.sort', 'DESC')->withPivot('sort')->get();?>
<!--            --><?// foreach($catalogFilterSet->filters()->orderBy('catalog_filter_catalog_filter_set.sort', 'DESC')->withPivot('sort')->get() as $filter):?>
            <? foreach(CatalogFilter::all() as $filter):?>
                <? $curFilter = $filters->where('id', $filter->id)->first()?>
                <tr>
                    <td>
                        <input type="checkbox" value="1" name="filters[<?=$filter->id?>][enabled]" <? if($curFilter):?>checked="checked"<?endif;?>>
                    </td>
                    <td>
                        <?=$filter->name?> (<?=$filter->id?>)
                    </td>
                    <td>
                        <?=Bootstrap3Form::number('filters[' . $filter->id . '][sort]', $curFilter ? $curFilter->pivot->sort : '')?>
                    </td>
                </tr>
            <? endforeach;?>
        </table>
<!--        <div class="row">-->
<!--            <div class="col-md-4">-->
<!--                --><?//=Bootstrap3Form::select('filters', CatalogFilter::all()->pluck('name', 'id'))?>
<!--            </div>-->
<!--            <div class="col-md-4">-->
<!--                <a onclick="addFilter(event.currentTarget)" class="btn btn-default">Добавить</a>-->
<!--            </div>-->
<!--        </div>-->
    </div>
</div>

<!--<script>-->
<!---->
<!--    function addFilter(e)-->
<!--    {-->
<!--        var $selected = $('[name=filters] option:selected');-->
<!---->
<!--        if ($selected.length == 0)-->
<!--            return;-->
<!---->
<!--        var html = _.template('filters-table-row', {-->
<!--            id: $selected.attr('value'),-->
<!--            name: $selected.text();-->
<!--        });-->
<!---->
<!--        html.appendTo('#filters-table')-->
<!---->
<!--        $('[name=filters]').reset();-->
<!--    }-->
<!---->
<!--    function removeFilter(el)-->
<!--    {-->
<!--        $(el).parents('tr').remove();-->
<!--    }-->
<!--</script>-->
<!--<script id="filters-table-row-template" type="text/html">-->
<!--    <tr>-->
<!--        <td><%=name%></td>-->
<!--        <td>--><?//=Bootstrap3Form::number('filters[<%=id%>][sort]', 0)?><!--</td>-->
<!--        <td>-->
<!--            <a onclick="removeFilter(event.currentTarget)"><i class="fa fa-close"></i></a>-->
<!--        </td>-->
<!--    </tr>-->
<!--</script>-->
<?=Bootstrap3Form::submit('Сохранить')?>

<?=Bootstrap3Form::close();?>
