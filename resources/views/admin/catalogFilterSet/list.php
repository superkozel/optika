<a class="btn btn-primary" href="<?=$controller->getUrl('create')?>">Создать</a>
<?=DataGrid::create()
    ->setColumns([
        'id',
        'name' => 'название',
        'filters' => 'фильтры',
//        [
//            'id' => 'filters',
//            'type' => 'raw',
//            'title' => 'фильтры',
//            'content' => function(CatalogFilterSet $set) {
//                $str = '';
//            }
//        ],
        [
            'type' => 'buttons'
        ],
    ])
    ->setData($catalogFilterSets)
    ->render();

?>