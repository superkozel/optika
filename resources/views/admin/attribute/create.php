<?=Bootstrap3Form::model($attribute, ['method' => "POST"])?>
<?=Bootstrap3Form::errorSummary($errors)?>
<?=Bootstrap3Form::textRow('name', 'Название')?>
<?=Bootstrap3Form::label('Slug')?>: <?=$attribute->code?>
<?=Bootstrap3Form::selectRow('type_id', 'Тип атрибута', AttributeType::getNames())?>

<script>
    $('[name="type_id"]').change(function(){
        $('[name="class"]').parent().hide();
        $('.options').hide();

        if ($(this).val() == <?=AttributeType::MODEL?>) {
           $('[name="class"]').parent().show();
        }

        if ($(this).val() == <?=AttributeType::SELECT?>) {
            $('.options').show();
        }
    });

    $().ready(function(){
        $('[name="type_id"]').change();
    })
</script>

<?=Bootstrap3Form::textRow('class', 'Класс')?>

<h3>Значения атрибута</h3>
<?=DataGrid::create()
    ->setColumns([
        'id',
        'xml_id' => 'uid',
        [
            'type' => 'raw',
            'id' => 'name',
            'title' => 'имя'
        ],
        'slug' => 'slug',
        [
            'type' => 'count',
            'id' => 'productValues',
            'title' => 'товаров'
        ],
        'pattern_m' => 'паттерн м.р.',
        'pattern_z' => 'паттерн ж.р.',
        'pattern_s' => 'паттерн с.р.',
    ])
    ->setData($attribute->options()->orderBy('name', 'ASC'))
    ->render()
?>
<!---->
<!--<div class="options">-->
<!--    <label>Опции</label>-->
<!--    <table class="table table-bordered table-condensed">-->
<!--        <thead>-->
<!--            <th>id</th>-->
<!--            <th>uid</th>-->
<!--            <th>значение</th>-->
<!--            <th>паттерн в м.р.</th>-->
<!--            <th>паттерн в ж.р.</th>-->
<!--            <th>паттерн в ср.р.</th>-->
<!--        </thead>-->
<!--        --><?// foreach($attribute->options as $option):?>
<!--            <tr>-->
<!--                <td>--><?//=$option->id?><!--</td>-->
<!--                <td>--><?//=$option->xml_id?><!--</td>-->
<!--                <td><input type="text" name="options[--><?//=$option->id?><!--][name]" placeholder="name" value="--><?//=$option->name?><!--"/></td>-->
<!--                <td><input type="text" name="options[--><?//=$option->id?><!--][pattern_m]" placeholder="Паттерн в мужском роде" value="--><?//=$option->pattern_m?><!--"/></td>-->
<!--                <td><input type="text" name="options[--><?//=$option->id?><!--][pattern_z]" placeholder="Паттерн в ж.р." value="--><?//=$option->pattern_z?><!--"/></td>-->
<!--                <td><input type="text" name="options[--><?//=$option->id?><!--][pattern_s]" placeholder="Паттерн в ср.р." value="--><?//=$option->pattern_s?><!--"/></td>-->
<!--            </tr>-->
<!--        --><?// endforeach;?>
<!--        <tr>-->
<!--            <td></td>-->
<!--            <td><input type="text" name="options[][name]" placeholder="name" /></td>-->
<!--            <td><input type="text" name="options[][pattern_m]" placeholder="Паттерн в мужском роде" /></td>-->
<!--            <td><input type="text" name="options[][pattern_z" placeholder="Паттерн в ж.р."/></td>-->
<!--            <td><input type="text" name="options[][pattern_s]" placeholder="Паттерн в ср.р."/></td>-->
<!--        </tr>-->
<!--        <tr>-->
<!--            <td></td>-->
<!--            <td><input type="text" name="options[][name]" placeholder="name" /></td>-->
<!--            <td><input type="text" name="options[][pattern_m]" placeholder="Паттерн в мужском роде" /></td>-->
<!--            <td><input type="text" name="options[][pattern_z" placeholder="Паттерн в ж.р."/></td>-->
<!--            <td><input type="text" name="options[][pattern_s]" placeholder="Паттерн в ср.р."/></td>-->
<!--        </tr>-->
<!--        <tr>-->
<!--            <td></td>-->
<!--            <td><input type="text" name="options[][name]" placeholder="name" /></td>-->
<!--            <td><input type="text" name="options[][pattern_m]" placeholder="Паттерн в мужском роде" /></td>-->
<!--            <td><input type="text" name="options[][pattern_z" placeholder="Паттерн в ж.р."/></td>-->
<!--            <td><input type="text" name="options[][pattern_s]" placeholder="Паттерн в ср.р."/></td>-->
<!--        </tr>-->
<!--    </table>-->
<!--    <a class="btn">Добавить</a>-->
<!--</div>-->

<?=Bootstrap3Form::submit('Сохранить')?>

<?=Bootstrap3Form::close();?>
