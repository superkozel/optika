<?=DataGrid::create()
    ->setColumns([
        'id',
        'name' => 'название',
        'code' => 'код',
        'type' => 'тип',
        'displayed' => 'отображается в списке',
        'required' => 'обязательный',
        'multiple' => 'множественный',
        'short_description' => 'короткое описание',

        'sort' => 'порядок',
        [
            'type' => 'buttons'
        ],
    ])
    ->setData($attributes)
    ->render()
?>