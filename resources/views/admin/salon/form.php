<div style="max-width:500px">
<?=Bootstrap3Form::model($salon, compact('errors'))?>
<?=Bootstrap3Form::errorSummary($errors)?>
<?=Bootstrap3Form::textRow('name', 'Название')?>
<?=Bootstrap3Form::textRow('address', 'Адрес')?>
<?=Bootstrap3Form::selectRow('city_id', 'Город', City::pluck('name', 'id'))?>


<?=Bootstrap3Form::label('Станция метро')?>
<div class="input-group">
    <?=Bootstrap3Form::select2('metro_selector', MetroStation::orderBy('name')->pluck('name'))?>
    <span class="input-group-btn">
    <?=Bootstrap3Form::button('Добавить')?>
    </span>
</div>
<div class="metro-list">
    <? foreach($salon->metros as $metro):?>
        <div><?=$metro->name?> <?=Bootstrap3Form::input('number', 'metro[34][range]', null, ['class' => 'input-xs'])?></div>
    <? endforeach;?>
</div>

<?=Bootstrap3Form::selectRow('specialization_id', 'Специализация', SalonSpecialization::getNames())?>
<?=Bootstrap3Form::textRow('coordinates', 'Координаты')?>
<?=Bootstrap3Form::textareaRow('mode', 'Режим работы')?>
<?=Bootstrap3Form::textareaRow('description', 'Описание')?>

<?=Bootstrap3Form::checkboxListRow('service_ids', 'Услуги', \ActiveRecord\SalonService::pluck('name', 'id'))?>

<?=Bootstrap3Form::formRow('image', 'Картинки', View::make('front.upload.widget', array('model' => $salon, 'property' => 'images'))->render())?>
<?=Bootstrap3Form::submit('Сохранить')?>
<?=Bootstrap3Form::close()?>
</div>
