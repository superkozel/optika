<a class="btn btn-primary" href="<?=action('AdminNewsController@create')?>">Создать</a>
<?=DataGrid::create()
    ->setColumns([
        'id',
        'images' => 'картинка',
        'name' => 'название',
        'city' => 'город',
        [
            'type' => 'buttons'
        ],
    ])
    ->setData($salons)
    ->render()
?>