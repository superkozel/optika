<?=Bootstrap3Form::model($catalogCategory, compact('errors'))?>
<?=Bootstrap3Form::errorSummary($errors)?>
<?=Bootstrap3Form::textRow('name', 'Название')?>
<?=Bootstrap3Form::selectRow('name_rod', 'Род названия(нужно для морфологии)', ['m', 's', 'z'])?>
<?php
$descendants = $catalogCategory->getDescendants()->add($catalogCategory);

$rootCategory = CatalogCategory::find(1);
$treeSelect = function($category, $level) use (&$treeSelect, $descendants) {
    if (in_array($category->id,
        $descendants->pluck('id', 'id')->all()))
        return [];

    $result = [];

    $result[$category->id] = str_repeat('--', $level) . ' ' .$category->name;

    foreach ($category->children as $child) {
        $result += $treeSelect($child, $level + 1);
    }

    return $result;
};

$categories = $treeSelect($rootCategory, 0);
?>
<?=Bootstrap3Form::selectRow('parent_id', 'Родительская категория', $categories)?>
<?=Bootstrap3Form::textareaRow('description', 'Описание')?>
<?=Bootstrap3Form::selectRow('filter_set_id', 'Набор фильтров', CatalogFilterSet::pluck('name', 'id'))?>

<h3>Seo</h3>
slug: <?=$catalogCategory->slug?>
<?=Bootstrap3Form::textRow('h1', 'h1')?>
<?=Bootstrap3Form::textRow('title', 'title')?>
<?=Bootstrap3Form::textRow('metadesc', 'meta description')?>
<?=Bootstrap3Form::textareaRow('seotext', 'SEO-текст')?>

<?=Bootstrap3Form::imageUploadRow('image', 'Картинка')?>

<?=Bootstrap3Form::submit('Сохранить')?>
<?=Bootstrap3Form::close()?>
