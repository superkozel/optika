<a class="btn btn-primary" href="<?=$controller->getUrl('create')?>">Создать</a>
<?=DataGrid::create()
    ->setColumns([
        'id',
        'image' => 'картинка',
        'name' => 'название',
        'parent' => 'родительская категория',
        'short_description' => 'короткое описание',
        'h1' => 'h1',
        'title' => 'title',
        'filterSet' => 'Фильтры',
        [
            'type' => 'buttons'
        ],
    ])
    ->setData($catalogCategories)
    ->render()
?>