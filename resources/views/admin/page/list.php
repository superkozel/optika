<a class="btn btn-primary" href="<?=$controller->getUrl('create')?>">Создать</a>
<?=DataGrid::create()
    ->setColumns([
        'id',
        'h1' => 'h1',
        'parent' => 'родитель',
        [
            'type' => 'buttons',
            'buttons' => ['edit']
        ],
    ])
    ->setData($pages)
    ->render()
?>