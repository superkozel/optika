<?=Bootstrap3Form::model($page, compact('errors'))?>
<?=Bootstrap3Form::errorSummary($errors)?>
<?=Bootstrap3Form::textRow('h1', 'h1')?>

<?=Bootstrap3Form::textareaRow('content', 'Контент', $page->path ? file_get_contents($page->getFullPath()) : '')?>

<h2>Seo</h2>
<?=Bootstrap3Form::textRow('title', 'title')?>
<?=Bootstrap3Form::textareaRow('metadesc', 'metadesc')?>
<?=Bootstrap3Form::textareaRow('keywords', 'keywords')?>

<h2>Технические параметры</h2>
<?=Bootstrap3Form::selectRow('layout', 'лэйоут', ['' => 'По умолчанию', 'page' => 'Страница сайта', 'empty' => 'Пустая страница'])?>
<?=Bootstrap3Form::selectRow('parent_id', 'родительская страница', ['' => '-'] + Page::where('parent_id', '!=', $page->id)->pluck('h1', 'id')->all())?>

<?=Bootstrap3Form::submit('Сохранить')?>
<?=Bootstrap3Form::close()?>
