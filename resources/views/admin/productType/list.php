<?=DataGrid::create()
    ->setColumns([
        'id',
        'name' => 'название',
        [
            'type' => 'raw',
            'title' => 'родитель',
            'content' => function(ProductType $type) {
                return $type->getParentId() ? $type->getParent()->getName() : null;
            }
        ],
        'attributeSet' => 'набор атрибутов',
    ])
    ->setData(ProductType::all())
    ->render()
?>