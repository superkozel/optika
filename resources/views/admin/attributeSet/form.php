<?=Bootstrap3Form::model($attributeSet)?>
<?=Bootstrap3Form::errorSummary($errors)?>

<?=Bootstrap3Form::textRow('name', 'Название')?>
<?=Bootstrap3Form::selectRow('product_type_id', 'Тип товара', ProductType::getNames())?>

<h3>Свойства в наборе</h3>
<?=view('admin.attribute.list', ['attributes' => $attributeSet->attrs()->orderBy('sort', 'DESC')])?>

<label for="">Свойства в наборе</label>
<? $ids = $attributeSet->attrs()->get()->pluck('id')->toArray()?>
<? foreach(Attribute::all() as $attr):?>
    <?=Bootstrap3Form::checkbox('attribute_ids[' . $attr->id . ']', $attr->name . '[' . $attr->id . ']', 1, in_array($attr->id, $ids))?>
<? endforeach;?>

<?=Bootstrap3Form::submit('Сохранить')?>

<?=Bootstrap3Form::close();?>
