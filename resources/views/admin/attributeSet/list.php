<a class="btn btn-primary" href="<?=$controller->getUrl('create')?>">Создать</a>
<?=DataGrid::create()
    ->setColumns([
        'id',
        'name' => 'название',
        'productType' => 'тип товара',
        [
            'type' => 'buttons'
        ],
    ])
    ->setData($attributeSets)
    ->render()
?>