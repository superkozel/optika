<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="owl-banners owl-carousel owl-theme">
                <? foreach(Banner::getByPosition('mainpage-mainbanner')->get() as $banner):?>
                    <a href="<?=$banner->getUrl()?>">
                        <?=$banner->img()?>
                    </a>
                <? endforeach;?>
            </div>

<!--            <div class="main_banner" style="margin-top:8px; margin-bottom:8px;">-->
<!--                <div class="row">-->
<!--                    --><?// foreach (Action::orderBy('active_from', 'DESC')
////                                    ->whereDate('active_to', '>=', date('Y-m-d'))
//                                    ->limit(4)
//                                    ->get() as $action):?>
<!--                        <div class="col-sm-3" style="padding-bottom;6px;">-->
<!--                            <div style="position:relative;overflow: hidden;" class="action-card">-->
<!--                                <a href="--><?//=$action->getUrl()?><!--">-->
<!--                                    --><?//=ActionImage::img($action->image, 'large', null, ['style' => 'width:100%'])?>
<!---->
<!--                                    <span class="action-card-desc" style="position:absolute;bottom:0px;top:0px;width:100%;height:100%;background-color: rgba(255,255,255,0.9);color:black;padding:12px;text-shadow:2px 0px 2px white;">-->
<!--                                        <div class="h3 notopmargin" style="text-decoration: underline;color:darkred">--><?//=$action->name?><!--</div>-->
<!--                                        --><?//=$action->preview_text?>
<!--                                    </span>-->
<!--                                </a>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    --><?// endforeach;?>
<!--                    <style>-->
<!--                        .action-card{margin-bottom:6px;}-->
<!--                        .action-card:hover {box-shadow:0px 0px 3px red;}-->
<!--                        .action-card-desc{display: none}-->
<!--                        .action-card:hover .action-card-desc{display: block;}-->
<!--                    </style>-->
<!--                </div>-->
<!--            </div>-->

<!--            <ul id="myTabs" class="nav nav-tabs" style="margin-bottom:18px;">-->
<!--                <li role="presentation" class="active"><a class="tab" href="#newTab"><i class="fa fa-flash"></i> Новинки 2017</a></li>-->
<!--                <li role="presentation"><a class="tab" href="#hitTab"><i class="fa fa-heart-o"></i> Популярное</a></li>-->
<!--                <li role="presentation"><a class="tab" href="#discountTab"><i class="fa fa-percent"></i> Со скидкой</a></li>-->
<!--            </ul>-->

<!--            <h2>Хиты</h2>-->
<!---->
<!--            <ul class="nav nav-tabs">-->
<!--                <li><a href="">Солнцезащитные очки</a></li>-->
<!--                <li><a href="">Оправы</a></li>-->
<!--                <li><a href="">Контактные линзы</a></li>-->
<!--            </ul>-->
<!---->
<!--            <div class="tab-content">-->
<!--                <div class="tab-pane active" id="sunglassesHits">-->
<!--                </div>-->
<!--                <div class="tab-pane" id="frameHits">-->
<!--                </div>-->
<!--                <div class="tab-pane" id="contactLensesHits">-->
<!--                </div>-->
<!--            </div>-->

        </div>
    </div>
</div>

<h2 class="h1" style="text-align: center;margin-bottom:30px;">Новинки <?=date('Y')?> года</h2>

<div style="padding-top:45px;padding-bottom:35px;
  background-color: #ecf8ff;
  border: solid 1px #a4c3d0;" class="full-width">
    <div class="container">
        <div class="container-fluid">

            <div class="" style="
    width: 100%;
    overflow: hidden;
    white-space: nowrap;">
                <? foreach(Banner::getByPosition('mainpage-novinki')->get() as $banner):?>
                    <a style="
    display: inline-block;margin-right:15px;" href="<?=$banner->getUrl()?>">
                        <?=$banner->img()?>
                    </a>
                <? endforeach;?>
            </div>
        </div>
    </div>
</div>


<h2 class="h1" style="text-align: center;margin-bottom:30px;">Наши салоны</h2>

<div style="margin-left:15px;margin-right:15px;">
    <div class="row" style="border-top:9px solid rgb(239, 246, 250);border-bottom: 9px solid rgb(239, 246, 250);">
        <div class="col-md-4 col-xs-6 col-xxs-12" style="padding-top:30px;padding-bottom:30px;min-height:100%;z-index: 1">
            <img alt="Салоны Оптика Фаворит" src="/dizayn/photo-salonov.png" style="max-width:100%;"/>
        </div>
        <div class="col-md-3 col-xs-6 col-xxs-12" style="padding-top:30px;padding-bottom:30px;min-height:100%;z-index: 1">
            <a style="display:block;font-size:18px;color:rgb(0, 106, 176);line-height: 26px;font-weight:bold;margin-bottom:24px;cursor: pointer;padding-left:54px;height:60px;" href="<?=actionts('SalonController@index')?>">
                <div style="position:absolute;left:15px;">
                    <div style="position:absolute;top:23px;left:6px;"><?=Salon::count()?></div>
                    <img src="/dizayn/33-salona.png" style="height:60px;"/>
                </div>
                <div style="display: table;height:60px;margin-left:6px;">
                    <div style="display: table-cell;height:60px;vertical-align: middle">
                        <?=plural(Salon::count(), 'Салон', 'Салона', 'Салонов')?> оптики в Москве и области:
                    </div>
                </div>
                <div class="clearfix"></div>
            </a>

            <div>
                <? foreach (City::withCount('salons')->orderBy('salons_count', 'desc')->get() as $city):?>
                    <a href="<?=$city->getUrl()?>" class="link" style="font-size:18px;color:rgb(0, 106, 176);line-height: 26px;display: inline-block;<? if ($city->name == 'Москва'):?>font-weight:bold;<?endif;?>"><?=$city->name?> ... <?=$city->salons_count?></a><br/>
                <? endforeach;?>
            </div>
        </div>
        <div class="col-md-5" style="padding-top:30px;padding-bottom:30px;border-left:9px solid rgb(239, 246, 250);border-right: 9px solid rgb(239, 246, 250);min-height:100%;">
            <div style="text-align: center;font-weight:bold;color:  rgb(0, 106, 176);font-size:36px;margin-bottom:24px;line-height: 1">Наши услуги</div>
            <? $icons = [
                'Проверка зрения' => ['/dizayn/proverka.png', \ActiveRecord\SalonService::find(8)->getUrl()],
                'Подбор очков' => ['/dizayn/podbor.png', \ActiveRecord\SalonService::find(8)->getUrl()],
                'Экспресс-диагностика' => ['/dizayn/express.png', \ActiveRecord\SalonService::find(8)->getUrl()],
                'Подбор линз МКЛ' => ['/dizayn/mkl.png'],
                'Ремонт очков' => ['/dizayn/remont.png', \ActiveRecord\SalonService::find(7)->getUrl()],
                'Изготовление очков' => ['/dizayn/izgotovlenie.png', \ActiveRecord\SalonService::find(6)->getUrl()],
            ]?>
            <div class="row">
            <? foreach (array_chunk($icons, 3, true) as $chunk):?>
                <div class="col-md-12 col-sm-6 col-xxs-12">
                    <? foreach ($chunk as $name => $serviceData):?>
                        <a <? if (! empty($serviceData[1])):?>href="<?=$serviceData[1]?>"<? endif;?> style="line-height:60px;height:60px;font-size:20px;color: rgb(46, 55, 59);display: block">
                            <div style="margin-left:33px;margin-right:33px;width:50px;text-align: center;float:left;">
                                <img src="<?=$serviceData[0]?>"/>
                            </div>

                            <?=$name?>
                        </a>
                    <? endforeach;?>
                </div>
            <? endforeach;?>
            </div>
        </div>
    </div>
</div>
<!--<div style="padding-top:36px;padding-bottom:36px;-->
<!--  background-image: radial-gradient(circle at 28% 19%, #ffffff, #f3f3f3);-->
<!--  box-shadow: inset 0 0 21px 0 rgba(0, 0, 0, 0.08);" class="full-width">-->
<!--    <div class="container">-->
<!--        <h2 class="nomargin" style="text-align: center;padding-bottom:36px;color:#ea1f1f">Почему покупатели <u>выбирают «Оптика Фаворит»?</u></h2>-->
<!--        <style>-->
<!--            .benefits {text-align: center;}-->
<!--            .benefits i{-->
<!--                background-color: red;color:white;border-radius:66px;font-size:30px;width:44px;line-height:44px;text-align: center;-->
<!--            }-->
<!--        </style>-->
<!--        <div class="row">-->
<!--            <div class="benefits col-sm-3">-->
<!--                <i class="fa fa-thumbs-up"></i>-->
<!--                <h3 style="">-->
<!--                    Примерка 4-х оправ<br/>-->
<!--                    на заказ-->
<!--                </h3>-->
<!--                <h4>Выберите то, что вам нравится-->
<!--                </h4>-->
<!--                <h4>Купите то, что вам подходит-->
<!--                </h4>-->
<!--            </div>-->
<!--            <div class="benefits col-sm-3">-->
<!--                <i class="fa fa-heart-o"></i>-->
<!--                <h3>Изготовление очков на заказ<br/>-->
<!--                    по рецепту</h3>-->
<!--                <h4>+ Срочное изготовление 45 минут.</h4>-->
<!--            </div>-->
<!--            <div class="benefits col-sm-3">-->
<!--                <i class="fa fa-truck"></i>-->
<!--                <h3>Доставка заказов на дом-->
<!--                и в салоны</h3>-->
<!--                <h4>--><?//=plural(Salon::count(), 'салон', 'салона', 'салонов', true)?><!-- оптики в Москве и области</h4>-->
<!--            </div>-->
<!--            <div class="benefits col-sm-3">-->
<!--                <i class="fa fa-handshake-o"></i>-->
<!--                <h3>1 год гарантии<br/>-->
<!--                    на всю продукцию</h3>-->
<!--                <h4>Только подлинная продукция</h4>-->
<!--                <h4>Заводская гарантия и сервис</h4>-->
<!--            </div>-->
<!--            <div class="clearfix"></div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<div class="container-fluid">
    <script>
        $('#myTabs a').click(function (e) {
            e.preventDefault()
            $(this).tab('show')
        })
    </script>

    <h2 style="text-align: center;">Хиты <?=date('Y')?> года</h2>

    <div class="tab-content" style="margin-top:50px;">
        <? $rowSize = 4;?>
        <div class="tab-pane active" id="newTab">
            <? foreach (Product::where('new', 1)->where('disabled', 0)->limit($rowSize)->get() as $product):?><div class="col-md-3 col-xs-4 nopadding col-inline">
                <?=view('front.product.card', ['product' => $product, 'rowSize' => $rowSize, 'simple' => 1])->render()?>
                </div><? endforeach;?>
        </div>
        <div class="tab-pane" id="hitTab">
            <? foreach (Product::where('hit', 1)->limit($rowSize)->get() as $product):?><?=view('front.product.card', ['product' => $product, 'rowSize' => $rowSize])->render()?><? endforeach;?>
        </div>
        <div class="tab-pane" id="discountTab">
            <? foreach (Product::where('old_price', '<', 'price')->limit($rowSize)->get() as $product):?><?=view('front.product.card', ['product' => $product, 'rowSize' => $rowSize])->render()?><? endforeach;?>
        </div>
    </div>

    <h2 style="text-align: center;">Новости</h2>

    <? $newsPageSize = 12; ?>
    <div class="news-carousel owl-carousel owl-theme">
        <? foreach(News::orderBy('created_at', 'DESC')->limit($newsPageSize)->get() as $new):?>
            <?=view('front.news.main-page-card', ['news' => $new])?>
        <? endforeach;?>
    </div>
</div>
<div style="padding-top:18px;padding-bottom:36px;">
    <h2 style="text-align: center;padding-bottom:36px;">Наши бренды</u></h2>
<!--    <div class="container-fluid">-->
<!--        <div class="row" style="max-height:300px;overflow: hidden;position: relative;">-->
<!--            --><?// foreach (Brand::limit(18)->get() as $brand):?>
<!--                <div class="col-md-2 col-sm-3 col-xs-4" style="margin-bottom:24px;">-->
<!--                    <a href="--><?//=\App\Http\Controllers\CatalogController::createUrlSimple(null, ['brand' => $brand->slug])?><!--">--><?//=BrandImage::img($brand->image, 'small', $brand->name, ['title' => $brand->name])?><!--</a>-->
<!--                </div>-->
<!--            --><?// endforeach;?>
<!--        </div>-->
<!--        <div style="text-align: center;margin-top:24px;font-size:24px;">-->
<!--            <a class="btn btn-default" href="--><?//=actionts("BrandController@index")?><!--" style="color:red;">Все бренды</a>-->
<!--        </div>-->
<!--    </div>-->

        <div class="brand-carousel owl-carousel owl-theme">
            <? foreach (Brand::all() as $brand):?>
                <a style="" href="<?=\App\Http\Controllers\CatalogController::createUrlSimple(null, ['brand' => $brand->slug])?>"><?=BrandImage::img($brand->image, 'small', $brand->name, ['title' => $brand->name])?></a>
            <? endforeach;?>
        </div>
        <script>
            $(document).ready(function(){
                $('.brand-carousel').owlCarousel({
//                    loop:true,
                    margin:35,
                    stagePadding:65,
                    navText: ['',''],
                    nav:true,
                    dots:false,
                    slideBy: 'page',
                    rewind:true,
                    lazyLoad: true,
                    responsive:{
                        0:{
                            items:2
                        },
                        600:{
                            items:4
                        },
                        1000:{
                            items:8
                        }
                    }
                });

                var newsSize = <?=$newsPageSize?>;
                $('.news-carousel').owlCarousel({
//                    loop:true,
                    margin:10,
                    stagePadding:85,
                    navText: ['',''],
                    nav:true,
                    dots:false,
                    slideBy: 'page',
                    lazyLoad: true,
                    rewind:false,
                    loop: false,
                    rewindNav: false,
                    navRewind: false,
                    responsive:{
                        0:{
                            items:1,
                            stagePadding:45,
                        },
                        600:{
                            stagePadding:45,
                            items:2
                        },
                        1000:{
                            items:3
                        }
                    }
                }).on('change.owl.carousel', function(e) {
                    if (e.namespace && e.property.name === 'position'
                        && (e.relatedTarget.relative(e.property.value) + e.page.size) === e.relatedTarget.items().length) {
                        $.ajax({
                            url: '<?=actionts('NewsController@ajax')?>',
                            dataType: 'json',
                            data: {
                                page_size : newsSize,
                                page: (e.relatedTarget.items().length / newsSize) + 1
                            },
                            success: function(response) {
                                if (response.content.length > 0) {
                                    for (i in response.content) {
                                        $('.news-carousel').trigger('add.owl.carousel', [response.content[i]])
                                    }
                                    $('.news-carousel').trigger('refresh.owl.carousel');
                                }
                            }
                        })
                    }
                })
                $('.owl-banners').owlCarousel({
//                    loop:true,
                    margin:0,
                    stagePadding:0,
                    navText: ['',''],
                    nav:true,
                    slideBy: 'page',
                    items:1,
                    lazyLoad: true,
                    rewind:true,
                    autoplay:true,
                    autoplayTimeout:3000,
                    autoplayHoverPause:true
                })
//                $('.novinki-carousel').owlCarousel({
////                    loop:true,
//                    margin:15,
//                    stagePadding:0,
//                    navText: ['',''],
//                    nav:true,
//                    slideBy: 'page',
//                    items:3
//                })
            });
        </script>
</div>