<?php
/** @var \App\User $user */
?>

<div style="float:right;text-align: center;">
    <a href="<?=Page::findBySlug('bonusnaya-programma')->getUrl()?>" class="link"><img src="http://www.i-love-english.ru/netcat_files/81/115/h_7252d1fb8c97f992030851fe9131b7a0"/>
    <br/>
    Памятка участника<br/>Бонусной программы</a>
</div>

<? if (! $user->bonus_program_active):?>
    <h3>
        <b>Принять участие в программе</b>
    </h3>
    <div class="form-box col-md-6">
        <?=Bootstrap3Form::open(['errors' => $errors, 'method' => 'POST', 'url' => actionts('UserController@bonusActivate')])?>
            <?=Bootstrap3Form::errorSummary($errors)?>
            <?=Bootstrap3Form::checkbox('accepted', 'Я прочитал и согласен с условиями <a target="_blank" class="link" href="' . Page::findBySlug('oferta')->getUrl() . '">договора-оферты</a>')?>

            <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Принять участие в программе</button>
        <?=Bootstrap3Form::close();?>
    </div>

    <div class="clearfix"></div>
    <a class="link" href="<?=Page::findBySlug('bonusnaya-programma')->getUrl()?>">Информация о программе</a>
    <div class="clearfix"></div>

<? elseif ($user->hasBonusCard()):?>
    Номер вашей карты: <?=$user->bonus_card_number?>
<? else:?>
<!--    <div class="alert-warning alert" style="display: inline-block">-->
<!--        <i class="fa fa-warning"></i> У вас еще нет бонусной карты. <br/>Получить карту можно в любом салоне.-->
<!--    </div>-->
<? endif?>
<h3 style="color:#3bbf7d;font-weight: bold;">Вы учавствуете в бонусной программе</h3>

<div>
    Общая сумма - <span style="font-size:1.4em;font-weight:bold;color:orange;"><?=plural($user->getTotalBonuses(), 'бонус', 'бонуса', 'бонусов', true)?></span> (1 Бонус = 1 Рубль)<br/>
</div>

<h3>Доступная сумма бонусов:</h3>

<? for ($i=1;$i<=rand(1,6);$i++):?>
    <div style="padding:12px 0px;">
        Кол-во бонусов: <b><?=rand(100,600)?></b>
        <div>
            Период начисления и сгорания:<br/>
            14.03.2016 по <span style="color:red">14.09.2017</span>
        </div>
    </div>
<? endfor;?>

<h3>Акция «Подари другу бонусы»</h3>

<div>
    Другу приходит смс уведомление о том, что ему подарили подарок. Если друг не зарегистрирован, то придет смс уведомление, что Вам подарили бонусы, что бы их активировать, нужно перейти по ссылке на сайт и зарегистрироваться.
</div>
<h3>Подарить другу свои бонусы.</h3>
<h4>Вы можете подарить до <a style="color:blue;text-decoration: underline" data-placement="bottom" data-toggle="popover" title="Как считаются даримые бонусы?" data-content="Вы можете подарить только заработанные бонусы(не подаренные)"><?=plural($user->getSendableBonusesAmount(), 'бонус', 'бонуса', 'бонусов', true)?></a></h4>
<div class="form-box col-md-6">
    <p>
        Укажите номер телефона друга и количество бонусов:
    </p>

    <?=Bootstrap3Form::open(['method' => 'POST', 'errors' => $errors, 'url' => actionts('UserController@sendBonuses')])?>
    <?=Bootstrap3Form::textRow('phone', 'Телефон друга')?>
    <?=Bootstrap3Form::inputRow('number', 'amount', 'Количество даримых бонусов')?>

    <button type="submit" class="btn btn-primary"><i class="fa fa-send"></i> Подарить бонусы</button>
</div>
