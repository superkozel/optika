<?php
/** @var \App\User $user */
?>
<h3>Мы рады вам, <?=$user->name?></h3>

<p>С помощью меню слева вы можете управлять вашим аккаунтом, отслеживать статус заказов, состояние бонусной карты</p>

<? if (BONUS_PROGRAM_ACTIVE):?>
    <hr/>
    <? if (! $user->bonus_program_active):?>
        <h4>Почему вы еще не участвуете в нашей бонусной программе?</h4>
        <p>
            Программа - это накопительная программа, позволяющая оплачивать до 30% заказа с помощью баллов, которые вы получаете за заказы, участие в социальной жизни.
        </p>
    <? else:?>
        <div class="row">
            <div class="col-sm-8">
                <h3 class="notopmargin" style="color:#3bbf7d;font-weight: bold;">Вы учавствуете в <a class="link" href="<?=actionts('UserController@bonus')?>">бонусной программе</a></h3>
                <div>
                    Общая сумма - <span style="font-size:1.4em;font-weight:bold;color:orange;"><?=plural($user->getTotalBonuses(), 'бонус', 'бонуса', 'бонусов', true)?></span> (1 Бонус = 1 Рубль)<br/>
                </div>
                Номер карты: <b style="font-size:1.5em;font-weight:bold;"><?=$user->bonus_card_number ?: 'карта не выдана на руки'?></b>
            </div>
            <div class="col-xs-4">
                <ul class="checkmarks">
                    <li>1 бонус = 1 рубль</li>
                    <li>Оплачивайте бонусами до 30% от стоимости заказа</li>
                    <li>Бонусы начисляется с небольшим промежутком после выполнения заказа</li>
                    <li>Бонусы с заказа действуют ограниченное время(до года)</li>
                </ul>
            </div>

    <!--        <div class="col-sm-4">-->
    <!--            <div class="bonus-card" style="background-color: red;color:white;border-radius:18px;height:220px;width:360px;">-->
    <!--                <div style="padding-top:24px;text-align: center;color:white;font-size:24px;font-weight:bold;">ОПТИКА ФАВОРИТ</div>-->
    <!--                <div style="padding:0px;text-align: center;color:white;">Карта лояльности</div>-->
    <!--            </div>-->
    <!--        </div>-->
        </div>
    <? endif;?>
<? endif;?>


<hr/>

<? if (count($user->orders) > 0):?>
    <h4><a href="<?=actionts('UserController@orders')?>">Мои заказы</a></h4>
    <div class="row">
        <div class="col-md-6">
            <? foreach($user->orders as $k => $order):?>
                <?=view('front.user.parts.order', compact('order'))?>
            <? endforeach;?>
        </div>
    </div>
<? endif;?>

<div style="padding:16px;background-color: whitesmoke">
    <h3 class="notopmargin">Обратная связь</h3>
    <?=view('front.forms.contactus')?>
</div>