<?
/** @var \App\User  $user */
?>
<?=Bootstrap3Form::model($user, ['method' => 'POST', 'errors' => $errors])?>
    <? if (! empty($login)):?>
        <?=Bootstrap3Form::hidden('login', $login)?>
    <? endif;?>
    <? if (! empty($error)):?>
        <span class="alert alert-danger">
                <i class="fa fa-close"> <?=$error?>
            </span>
    <? endif;?>

    <? $waitTo = $user->phone_confirmation_sent_at ? $user->phone_confirmation_sent_at->addSeconds(\App\User::SMS_PAUSE)->getTimestamp() : null;?>
    <? if ($user->phone_confirmation_sent_at):?>
        <p>На ваш телефон, указанный при регистрации или в анкете, было отправлено СМС с одноразовым кодом.</p>

        <div class="form-group<?=$errors->has('code') ? ' has-error' : '' ?>">
            <?=Bootstrap3Form::text('code', Input::old('code'), ['placeholder' => 'Код подтверждения', 'class' => 'input input-lg col-md-6', 'style' => 'text-align:center;width:100%;margin:0 auto;'])?>
            <script>
                $("[name=code]").mask("999999");
            </script>
            <? if ($errors->has('code')):?>
                <span class="help-block">
                    <strong><?=$errors->first('code')?></strong>
                </span>
            <? endif;?>
        </div>
        <div class="clearfix"></div>

        <div class="pull-right" style="margin-top:8px;">
            <?=Bootstrap3Form::submit('Проверить код', null, ['formaction' => ! empty($action) ? $action : Request::getRequestUri()]);?>
        </div>
        <div class="clearfix"></div>
    <? else:?>
        <p>На ваш телефон, указанный при регистрации или в анкете, будет отправлен СМС с одноразовым кодом.</p>
    <? endif?>

    <hr/>
    <button type="submit" formaction="<?=! empty($sendAction) ? $sendAction : actionts('UserController@sendSmsCode')?>" class="send-button btn btn-primary <? if ($waitTo && ($waitTo - time() > 0)):?>disabled<? endif;?>">
        <i class="fa fa-paper-plane"></i>&nbsp;<? if (! $user->phone_confirmation_code):?>Отправить СМС-код<? else:?>Отправить код повторно<? endif;?>
    </button>
    <div class="clearfix"></div>
    <div class="countdown">
        <? if ($waitTo > time()):?>
            Следующее СМС вы сможете отправить через <span class="cooldown"><?=plural($waitTo - time(), 'секунду', 'секунды', 'секунд', true)?></span>
        <? endif?>
    </div>
<?=Bootstrap3Form::close()?>
<script>
    var to = <?=$waitTo?>;
    var interval = setInterval(function(){
        var cooldown = Math.round(to - Date.now()/1000);
        if (cooldown > 0) {
            $('.cooldown').text(plural(cooldown, 'секунду', 'секунды', 'секунд', true));
        }
        else {
            clearInterval(interval);
            $('.send-button').removeClass('disabled');
            $('.countdown').hide();
        }
    }, 500);
</script>