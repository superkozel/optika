<div style="display: inline-block;border-bottom:2px dashed lightgray;padding:12px 0px;width:100%;">
    <div>
        <h4 style="margin:0px;">Заказ №<?=$order->id?> <div style="font-size:0.8em" class="pull-right"><?=$order->created_at->format('d.m.Y H:i:s')?></div></h4>
    </div>
    <div>
        <b>Статус</b>: <span class="label label-info"><?=$order->getStatus()->getName()?></span><br/>
        <b>Состав заказа</b>:<br/>
        <? foreach ($order->items as $k => $orderItem):?>
            <?=$k+1?>. <a href="<?=$orderItem->product->getUrl()?>"><?=$orderItem->name?></a> <?=$orderItem->count?> шт. - <?=$orderItem->sum?> руб.<br/>
        <? endforeach;?>

        <b>Сумма заказа</b>:  <?=moneyFormat($order->sum)?> руб.
        <? if ($order->bonus):?>
            + <?=$order->bonus?> бонусов | ~ <?=$order->bonuses_used?>
        <? endif;?>
    </div>
    <div>
        <div class="pull-right">
            <a class="btn btn-default btn-xs" href="<?=actionts('OrderController@view', ['id' => $order->id])?>"><i class="fa fa-eye"></i> Подробнее</a>
            <? if ($order->isEditable()):?>
                <a class="btn btn-primary btn-xs" href="<?=actionts('OrderController@load', ['id' => $order->id])?>"><i class="fa fa-pencil"></i> Изменить</a>
            <? else:?>
                <a class="btn btn-primary btn-xs" href="<?=actionts('OrderController@repeat', ['id' => $order->id])?>"><i class="fa fa-refresh"></i> Повторить заказ</a>
            <? endif;?>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<div class="clearfix"></div>