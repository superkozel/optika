<div class="col-md-6" style="background-color: #fafafa;padding:24px;position:relative">
    <?=Bootstrap3Form::model($user, ['method' => 'POST', 'errors' => $errors])?>
    <?=Bootstrap3Form::token();?>
    <?=Bootstrap3Form::textRow('name', 'Имя')?>
    <?=Bootstrap3Form::textRow('lastname', 'Фамилия')?>
    <? if (! $user->birthdate):?>
        <?=Bootstrap3Form::inputRow('date', 'birthdate', 'Дата рождения')?>
    <? endif;?>
    <?=Bootstrap3Form::textRow('email', 'Email')?>
    <? if ($user->email && ! $user->isEmailConfirmed()):?><a class="link" href="<?=actionts('UserController@confirmEmail')?>" style="position:absolute;margin-top:-16px;right:24px;color:red;">
        <i class="fa fa-warning"></i> Email не подтвержден</a><? endif;?>
    <?=Bootstrap3Form::textRow('phone', 'Номер телефона')?>
    <script type="text/javascript">
        $('[name=phone]').mask("8(999)999-99-99");
    </script>
    <? if ($user->phone && ! $user->isPhoneConfirmed()):?><a class="link" href="<?=actionts('UserController@confirmPhone')?>" style="position:absolute;margin-top:-16px;right:24px;color:red;">
        <i class="fa fa-warning"></i> Номер не подтвержден</i></a><? endif;?>
    <?=Bootstrap3Form::inputRow('password', 'password', 'Пароль(для подтверждения прав)')?>
    <?=Bootstrap3Form::submit('Сохранить')?>
    <?=Bootstrap3Form::close()?>
</div>