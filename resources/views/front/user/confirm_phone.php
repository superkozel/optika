<?
/** @var \App\User $user */
?>
<div class="col-md-6" style="background-color: #fafafa;padding:24px;position:relative">
    <? if (! $user->phone):?>
        <span class="alert alert-warning">
            <a href="<?=actionts('UserController@profile')?>">Заполните Телефон</a>
        </span>
    <? elseif ($user->phone_confirmed):?>
        <div class="alert alert-success">
            <i class="fa fa-check"></i> Телефон успешно подтвержден
        </div>
    <? else:?>
        <?=view('front.user.parts.sms_form', ['user' => $user,
            'action' => actionts('Auth\LoginController@login'),
            'sendAction' => actionts('UserController@sendSmsCode')
        ])?>
    <? endif;?>
</div>
<script>
    var interval = setInterval(function(){
        if (cooldown <= 0) {
            clearInterval(interval);
        }
        else {
            $('.cooldown').text();
        }
    })
</script>
