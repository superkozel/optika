<div class="col-md-6" style="background-color: #fafafa;padding:24px;position:relative">
<?=Bootstrap3Form::open()?>
<?=Bootstrap3Form::errorSummary($errors)?>
<? if ($user->password):?>
    <?=Bootstrap3Form::inputRow('password', 'old_password', 'Старый пароль')?>
<? else:?>
    <?=Bootstrap3Form::hidden('old_password', 'stub')?>
<? endif;?>
<?=Bootstrap3Form::textRow('new_password', 'Новый пароль')?>
<?=Bootstrap3Form::submit('Сохранить новый пароль')?>
<?=Bootstrap3Form::close()?>
</div>
