<ul id="orderTabs" class="nav nav-pills">
    <li role="presentation" class="active"><a href="#magazin">Заказы в интернет-магазины</a></li>
    <li role="presentation"><a href="#roznica">Розничные покупки</a></li>
</ul>
<script>
    $('#orderTabs a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    })
</script>
<div class="tab-content">
    <div class="tab-panel">
        <? if (count($orders)):?>
            <? foreach($orders as $k => $order):?>
                <?=view('front.user.parts.order', compact('order'))?>
            <? endforeach;?>
        <? else:?>
            <span class="alert alert-warning" style="display: inline-block">
            Заказов нет
        </span>
        <? endif;?>
    </div>
    <div id="roznica" class="tab-panel">
        Информация временно недоступна
    </div>
</div>

