<?
/** @var \App\User $user */
?>

<? if (! $user->email) :?>

<? elseif ($user->email == $user->email_confirmed):?>

<? else:?>

<? endif;?>
<div class="col-md-6" style="background-color: #fafafa;padding:24px;position:relative">
    <p>На ваше почту было отправлено письмо с ссылкой для подтверждения почты</p>
    <p>Пока вы не подтведрите вашу почту, мы не сможем <b>присылать вам полезную информацию</b>,
        о <b>проходящих акциях</b>, <b>спецпредложениях</b> и интересных <b>способах получить бонусы</b>.
    </p>
    <a href="<?=actionts('UserController@sendEmailConfirmation')?>" class="btn btn-primary"><i class="fa fa-envelope"></i> Отправить письмо повторно</a>
</div>
