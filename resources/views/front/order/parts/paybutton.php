<?
/** @var Order $order */
?>
<form style="display: inline" action="<?=config('yandexkassa.testmode') ? 'https://demomoney.yandex.ru/eshop.xml' : 'https://money.yandex.ru/eshop.xml'?>" method="POST">
    <input name="shopId" value="<?=config('yandexkassa.shopId')?>" type="hidden"/>
    <input name="scid" value="<?=config('yandexkassa.scid')?>" type="hidden"/>
    <input name="sum" value="<?=$order->getRemainingPayment()?>" type="hidden">
    <input name="customerNumber" value="<?=$order->user_id ? $order->user_id : '+7' . $order->phone?>" type="hidden"/>
    <input name="paymentType" value="<?=$order->payment_type->is(\Enum\PaymentType::KARTOY) ? 'AC' : ''?>" type="hidden"/>
    <input name="orderNumber" value="<?=$order->id?>" type="hidden"/>
    <input name="cps_phone" value="<?=$order->phone?>" type="hidden"/>
    <input name="cps_email" value="<?=$order->email?>" type="hidden"/>
    <input name="shopSuccessURL" value="<?=actionts('OrderController@view', ['id' => $order->id])?>" type="hidden"/>
    <input name="shopFailURL" value="<?=actionts('OrderController@view', ['id' => $order->id])?>" type="hidden"/>
    <input name="shopDefaultUrl" value="<?=actionts('OrderController@view', ['id' => $order->id])?>" type="hidden"/>

    <?
    $yandexKassaReceiptData = [
        "customerContact" => "+7" . $order->phone,
        "taxSystem" => 3,
    ];

    $items = [];
    foreach ($order->items as $item) {
        /** @var OrderItem $item */
        if ($item->hasModifier(ProductModifier::MAKING)) {
            $items[] = [
                'quantity' => '',
                'price' => [
                    'amount' => $item->price
                ],
                'tax' => 3,
                'text' => 'Готовые очки ' . $item->getProduct()->getName() . ' + линзы ' . $item->getLensLeft() . '/' $item->getLensRight()->getName();
            ]
        }
        else {
            $items[] = [
                'quantity' => $item->count,
                'price' => [
                    'amount' => $item->price
                ],
                'tax' => 3,
                'text' => $item->name;
            ]

        }
        if ($item->hasModifier(ProductModifier::TONING)) {
            $this->getModifierPrice(TONING, 1);
        }

        if ($making) {
            $items[] = 'Изготовление';
            return \ActiveRecord\SalonService::calculateIzgotovlenie($basketItem);
        }
    }

//    foreach ($order->getSuborders() as $suborder) {
//
//    }
    ?>
    <input name="ym_merchant_receipt" value="<?=json_encode($yandexKassaReceiptData)?>" type="hidden"/>

    <button type="submit" class="btn btn-success"><i class="fa fa-money"></i> Оплатить заказ</button>
</form>