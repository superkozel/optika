Статус: <b><?=$order->status->getName()?></b><br/>
Сумма заказа: <b><?=moneyFormat($order->sum)?></b><br/>
<? if (BONUS_PROGRAM_ACTIVE):?>
    Бонусов за заказ: <b><?=$order->bonus?></b><br/>
<? endif;?>
<? if ($order->comment):?>
    Комментарий к заказу: <?=$order->comment?>
<? endif?>
<? if ($order->reason):?>
    Причина отказа: <?=$order->reason?>
<? endif?>

<h3>Доставка</h3>
Способ доставки: <b><?=$order->deliveryType->getName()?></b><br/>
<? if ($order->deliveryType->is(DeliveryType::DOSTAVKA, DeliveryType::DOSTAVKA2)):?>
    Дата: <b><?=$order->delivery_date ?: 'не указано'?></b><br/>
    Время: <b><?=$order->delivery_time ?: 'не указано'?></b><br/>
    Адрес: <b><?=$order->address?></b><br/>
<? elseif ($order->deliveryType->is(DeliveryType::SAMOVYVOZ)):?>
    Дата: <b><?=$order->delivery_date ?: 'не указано'?></b><br/>
    Салон: <b><?=Html::link($order->salon->getUrl(), $order->salon->name)?></b><br/>
    Адрес: <b><?=$order->salon->address?></b><br/>
<? elseif ($order->deliveryType->is(DeliveryType::POCHTA)):?>
    Адрес: <?=$order->address?><br/>
<? endif;?>

<? if ($order->sum > 0):?>
    <h3>Оплата</h3>

    Статус оплаты:
    <? if ($order->paid_sum == $order->sum):?>
        <span style="color:green"><i class="fa fa-check"></i> полностью оплачен</span>
    <? elseif ($order->payment_required_sum && $order->payment_required_sum == $order->paid_sum):?>
        <span style="color:green"><i class="fa fa-check"></i> внесена предоплата</span>
    <? elseif (! $order->payment_allowed):?>
        <span style="color:orange"><i class="fa fa-close"></i> ожидается разрешение оплаты от менеджера</span>
    <? elseif ($order->payment_required_sum):?>
        <span style="color:red"><i class="fa fa-close"></i> не оплачен, требуется предоплата</span>
    <? else:?>
        <span style="color:orange"><i class="fa fa-close"></i> не оплачен, предоплата не требуется</span>
    <? endif;?><br/>
    Способ оплаты: <b><?=$order->paymentType->getName()?></b><br/>
    <? if ($order->payment_required_sum):?>
        <span style="color:red">Ожидается оплата: <b><?=moneyFormat($order->payment_required_sum)?> руб.</b></span><br/>
    <? endif;?>
    Оплачено: <b><?=moneyFormat($order->paid_sum)?> руб.</b><br/>

    <? if (BONUS_PROGRAM_ACTIVE):?>
        Оплата бонусами: <b><?=$order->bonuses_used ? $order->bonuses_used : '-'?></b><br/>
    <? endif;?>
<? endif;?>