<?
/** @var Order $order */
/** @var OrderItem[] $items */
?>
<table class="table table-condensed" width=100%>
    <cols>
        <col width="60"/>
        <col width=""/>
        <col width=""/>
        <col width="60"/>
        <col width=""/>
    </cols>
    <tr>
        <th></th>
        <th>Позиция</th>
        <th>Стоимость</th>
        <th>Кол-во</th>
        <th>Подытог</th>
    </tr>
    <? foreach ($items as $orderItem):?>
        <? $item = $orderItem->product;?>
        <tr data-id="<?=$item->id?>" class="tile">
            <td style="text-align:center;vertical-align:middle">
                <? if ($image = $item->getImage()):?>
                    <a style="position: relative;" href="<?=$item->getImage()->getUrl('large')?>" rel="lightbox[gal]">
                        <?=ProductImage::img($item->getImage(), 'mini', $item->name, ['style' => 'max-width:100%;cursor:zoom-in;'])?>
                    </a>
                <? else:?>
                    <?=ProductImage::img((new ProductImage()), 'mini', $item->name, ['style' => 'max-width:100%;'])?>
                <? endif;?>
            </td>
            <td style="vertical-align:middle">
                <a href="<?=$item->getUrl()?>" class="title"><?=$orderItem->name?></a><br/>
                <span class="subtitle"><?=$item->getType()->getName()?></span>
                <? if ($leftId = $orderItem->getModifier(ProductModifier::LENS_LEFT)):?>
                    <div>+ Левая линза: <?=Product::find($leftId)->name?></div>
                <? endif;?>
                <? if ($rightId = $orderItem->getModifier(ProductModifier::LENS_RIGHT)):?>
                    <div>+ Правая линза: <?=Product::find($rightId)->name?></div>
                <? endif;?>
                <? if ($orderItem->getModifier(ProductModifier::MAKING)):?>
                    <div>+ Изготовление</div>
                <? endif;?>
                <? if ($value = $orderItem->getModifier(ProductModifier::TONING)):?>
                    <div>+ Тонирование линз <? if ($value == 2):?>"Маска"<? endif;?></div>
                <? endif;?>
            </td>
            <td style="text-align:center;vertical-align:middle">
                <? if ($orderItem->getModifier("fitting")):?>
<!--                    <span style="text-decoration: line-through">--><?//=moneyFormat($orderItem->price_before)?><!-- руб.</span><br/>-->
                <? else:?>
                    <?=str_replace(' ', '&nbsp;', moneyFormat($orderItem->price))?> руб.
                <? endif;?>
            </td>

            <td class="tile-count" style="vertical-align:middle">
                <?=$orderItem->count?>
            </td>

            <td style="vertical-align:middle;text-align: right;" class="tile-price">
                <? if ($orderItem->getModifier("fitting")):?>
                    <span style="color:green">Бесплатная примерка</span>
                <? else:?>
                    <?=str_replace(' ', '&nbsp;', moneyFormat($orderItem->sum))?> руб.
                <? endif;?>
            </td>
        </tr>
    <? endforeach?>
    <? if ($order->bonuses_used):?>
        <tr>
            <td></td>
            <td>оплата бонусами</td>
            <td></td>
            <td><?=$order->bonuses_used?></td>
            <td>- <?=moneyFormat($order->bonuses_used)?> руб.</td>
        </tr>
    <? endif;?>
    <? if ($order->promocode):?>
        <tr>
            <td colspan="2" style="text-align:right;padding:24px 0px;line-height:1;">
                Промокод<br/>
                <b>"<?=$order->promocode?>"</b>
            </td>
            <th colspan="4" style="text-align:left;padding:24px 0px;padding-left:12px;font-size:1.1em;line-height:1em;" class="total-price">
                Скидка 10% на весь заказ
            </th>
        </tr>
    <? endif;?>
    <? if ($order->bonuses_used):?>
        <tr style="padding-bottom:0px">
            <td colspan="2" style="text-align:right;padding:24px 0px;line-height:1;">
                Оплата бонусами
            </td>
            <th colspan="4" style="text-align:left;padding:24px 0px;padding-left:12px;font-size:1em;line-height:1em;color:red;" class="bonuses-used">
               — <?=$order->bonuses_used?> руб.
            </th>
        </tr>
    <? endif;?>
    <tr>
        <td colspan="2" style="text-align:right;padding:24px 0px;line-height:1;vertical-align: middle">
            Итого<br/>
            без доставки
        </td>
        <th colspan="4" style="text-align:left;padding:24px 0px;padding-left:12px;font-size:1.5em;line-height:1em;vertical-align: middle" class="total-price">
            <?=moneyFormat($order->sum - $order->delivery_price)?> руб.
        </th>
    </tr>
    <tr>
        <td colspan="2" style="text-align:right;padding:24px 0px;line-height:1;vertical-align: middle">
            Стоимость доставки
        </td>
        <th colspan="4" style="text-align:left;padding:24px 0px;padding-left:12px;font-size:1.5em;line-height:1em;vertical-align: middle" class="total-price">
            <? if (! is_null($order->delivery_price)):?>
                <?=$order->getDeliveryType()->getPriceText($order)?>
            <? else:?>
                требует уточнения
            <? endif;?>
        </th>
    </tr>
    <tr style="background-color: #d1ecff">
        <td colspan="2" style="text-align:right;padding:24px 0px;line-height:1;vertical-align: middle">
            Общая сумма<br/>
            вашего заказа
        </td>
        <th colspan="4" style="text-align:left;padding:24px 0px;padding-left:12px;font-size:1.5em;line-height:1em;vertical-align: middle" class="payment_required_total">
            <?=moneyFormat($order->sum)?> руб.
        </th>
    </tr>
    <? if ($order->payment_required_sum > 0):?>
        <tr style="background-color: #d1ecff">
            <td colspan="2" style="text-align:right;padding:24px 0px;line-height:1;color:red;">
                Требуется предоплата
            </td>
            <th colspan="4" style="text-align:left;padding:24px 0px;padding-left:12px;font-size:1.5em;line-height:1em;" class="payment_required_total">
                <?=moneyFormat($order->payment_required_sum)?> руб.
            </th>
        </tr>
    <? endif;?>
</table>