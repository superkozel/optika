<?
/** @var Order $order */
?>
<div class="row">
    <div class="col-md-12" style="margin-bottom:24px;">
        <?=view('front.order.parts.details', ['order' => $order])?>

        <br/>
        <? if ($order->isPaymentRequired() && $order->payment_allowed):?>
            <?=view('front.order.parts.paybutton', ['order' => $order])?>
        <? endif;?>
        <? if ($order->status->is(OrderStatus::NEWS)):?>
            <a class="btn btn-primary" href="<?=actionts('OrderController@load', ['id' => $order->id, 'after' => 'checkout'])?>"><i class="fa fa-pencil"></i> Изменить данные заказа</a>

            <div class="pull-right">
                <a class="btn btn-danger" onclick="var reason = prompt('Пожалуйста, укажите причину отказа');$(event.currentTarget).attr('href', $(event.currentTarget).attr('href') + '?reason=' + reason)" href="<?=actionts('OrderController@cancel', ['id' => $order->id])?>"><i class="fa fa-trash"></i> Отменить заказ</a>
            </div>
        <? elseif ($order->status->is(OrderStatus::CANCELED)):?>
            <a class="btn btn-primary" href="<?=actionts('OrderController@renew', ['id' => $order->id])?>"><i class="fa fa-check"></i> Возобновить заказ</a>
        <? endif;?>
    </div>
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3>Ваш заказ</h3>
            </div>
            <?=view('front.order.parts.items', ['order' => $order, 'items' => $order->items])?>
            <div class="panel-footer">
                <a class="btn btn-primary" href="<?=actionts('OrderController@load', ['id' => $order->id])?>"><i class="fa fa-pencil"></i> Изменить содержимое заказа</a>
            </div>
        </div>
    </div>
</div>
