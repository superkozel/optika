<?
/** @var Order $order */
?>
<div class="row">
    <div class="col-md-6" style="margin-top:36px;">
        <h3 class="notopmargin" style="">Номер вашего заказа: <span style="color:blue;text-decoration: underline">#<?=$order->getNumber()?></span></h3>

        <p>
            Скоро с вами свяжется по телефону наш менеджер для подтверждения заказа.
        </p>
        <? if ($order->isPaymentRequired()):?>
            <? if ($order->paymentType->is(\Enum\PaymentType::ONLINE, \Enum\PaymentType::KARTOY)):?>
                <? $paymentSum = $order->sum?>
                <? $paymentRequired = true;?>
            <? else:?>
                <? $paymentSum = $order->payment_required_sum?>
                <? $paymentRequired = true;?>
                <h4><span style="color:red">Требуется предоплата на материалы и изготовление в размере <b><?=moneyFormat($order->payment_required_sum)?> руб.</b></span></h4>
            <? endif;?>

            <? if (! $order->payment_allowed):?>
                <div class="alert alert-warning">
                    <i class="fa fa-warning"></i> Оплата заказа возможна только после подтверждения заказа
                </div>
            <? else:?>
                <div class="pull-right">
                    <?=view('front.order.parts.paybutton', ['order' => $order])?>
                </div>
            <? endif;?>
        <? endif;?>

        <div class="clearfix"></div>
        <div style="margin-top:24px">
            <?=view('front.order.parts.details', ['order' => $order])?>
        </div>

        <div class="clearfix"></div>
        <div style="margin-top:50px">
            <?=view('front.order.parts.items', ['items' => $order->items, 'order' => $order])?>
        </div>
    </div>
</div>
