<div class="container-fluid">
    <h1><?=$h1?></h1>

    <? foreach ($news->chunk(2) as $chunk):?>
            <? foreach ($chunk as $new):?>
            <div class="row" style="margin-bottom:24px;">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-sm-2 col-xs-4">
                            <?=NewsImage::img($new->image, 'medium', null, ['style' => 'width:100%'])?>
                        </div>
                        <div class="col-sm-10 col-xs-8">
                            <a href="<?=actionts('NewsController@show', ['slug' => $new->slug])?>">
                                <h2 class="h3" style="margin-top:0px"><?=$new->name?></h2>
                            </a>
                            <div style="font-size:0.8em;color:gray"><?=$new->created_at?></div>

                            <p><?=$new->preview_text?></p>
                        </div>
                    </div>
                </div>
            </div>
            <? endforeach;?>
    <? endforeach;?>
    <?=View::make('front.parts.pagination', ['paginator' => $news])?>
</div>

