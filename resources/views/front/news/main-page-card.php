<div style="margin-top:36px;margin-bottom:36px;position:relative;padding-left:130px;height:120px;">
    <a href="<?=$news->getUrl()?>" style="box-shadow:0px 0px 6px lightgray;padding:10px;float:left;position:absolute;left:0px;">
        <?=NewsImage::img($news->image, 'small', null)?>
    </a>

    <a class="link" href="<?=$news->getUrl()?>" style="font-weight:bold;font-size:16px;"><?=$news->name?></a>

    <div style="position:absolute;color: rgb(46, 55, 59);left:130px;font-weight:bold;bottom:0px;opacity: .45;">
        <?=$news->created_at->format('d.m.Y')?>
    </div>
</div>
