<?=view('front.layouts.parts.header')->render()?>
<div class="content">
    <?=view('front.layouts.parts.breadcrumbs')?>
    <div class="container-fluid">
        <div class="row">
<!--            <div class="col-md-3">-->
<!--                <div class="panel panel-default">-->
<!--                    <div class="panel-body">-->
<!--                        <h4 style="margin-top:0px;">Покупатель</h4>-->
<!--                        --><?// if (Auth::user()):?>
<!---->
<!--                            --><?// if (BONUS_PROGRAM_ACTIVE):?>
<!--                            <a href="--><?//=actionts('UserController@bonus')?><!--"><i class="fa fa-credit-card"></i> Бонусная карта <span class="label label-success">--><?//=$user->getBonuses()?><!--</span></a><br/>-->
<!--                            --><?// endif;?>
<!--                            <a href="--><?//=actionts('BasketController@index')?><!--"><i class="fa fa-shopping-basket"></i> Корзина <span class="label label-danger">--><?//=$basket->getTotalCount()?><!--</span></a><br/>-->
<!--                            <a href="--><?//=actionts('FavoriteController@index')?><!--"><i class="fa fa-heart-o"></i> Избранное <span class="label label-info">--><?//=\ActiveRecord\Favorite::current()->count()?><!--</a><br/>-->
<!---->
<!--                            <hr/>-->
<!--                            <h4>Заказы</h4>-->
<!--                            --><?// $lastOrder = \App\User::lastOrder();?>
<!--                            --><?// if ($lastOrder):?>
<!--                                <a href="--><?//=actionts('OrderController@view', ['id' => $lastOrder->id])?><!--"><i class="fa fa-fire"></i> Последний заказ</a> <span class="label label-info">--><?//=$lastOrder->status->getName()?><!--</span><br/>-->
<!--                            --><?// endif;?>
<!--                            <a href="--><?//=actionts('UserController@orders')?><!--"><i class="fa fa-history"></i> История покупок</a><br/>-->
<!--                            <hr/>-->
<!---->
<!--                            <a href="--><?//=actionts('UserController@getProfile')?><!--"><i class="fa fa-vcard"></i> Изменить личные данные</a><br/>-->
<!--                            <a href="--><?//=actionts('UserController@getPassword')?><!--"><i class="fa fa-key"></i> Изменить пароль</a><br/>-->
<!--                            <br/>-->
<!--                            <a>-->
<!--                                <form method="POST" onclick="$(event.currentTarget).submit()" action="--><?//=actionts('Auth\LoginController@logout')?><!--">-->
<!--                                    <i class="fa fa-sign-out"></i> Выход-->
<!--                                    --><?//=Bootstrap3Form::token()?>
<!--                                </form>-->
<!--                            </a>-->
<!--                        --><?// else:?>
<!---->
<!--                            <a href="--><?//=actionts('BasketController@index')?><!--"><i class="fa fa-shopping-basket"></i> Корзина <span class="label label-danger">--><?//=$basket->getTotalCount()?><!--</span></a><br/>-->
<!--                            <a href="--><?//=actionts('FavoriteController@index')?><!--"><i class="fa fa-heart-o"></i> Избранное <span class="label label-info">--><?//=\ActiveRecord\Favorite::current()->count()?><!--</a><br/>-->
<!---->
<!--                            --><?// $lastOrder = \App\User::lastOrder();?>
<!--                            --><?// if ($lastOrder):?>
<!--                                <hr/>-->
<!--                                <h4>Заказы</h4>-->
<!--                                <a href="--><?//=actionts('OrderController@view', ['id' => $lastOrder->id])?><!--"><i class="fa fa-fire"></i> Последний заказ</a> <span class="label label-info">--><?//=$lastOrder->status->getName()?><!--</span><br/>-->
<!--                            --><?// endif;?>
<!--                            <hr/>-->
<!---->
<!--                            <a href="--><?//=actionts('Auth\RegisterController@register')?><!--"><i class="fa fa-sign-in"></i> Вход</a><br/>-->
<!--                            <a href="--><?//=actionts('Auth\RegisterController@register')?><!--"><i class="fa fa-product-hunt"></i> Регистрация</a><br/>-->
<!--                        --><?// endif;?>
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
            <div class="col-md-12">
                <h1><?=$h1?></h1>

                <?
                    $time = date('G');
                    if ($time < 7)
                        $hour = 'Доброй ночи';
                    else if ($time < 12)
                        $hour = 'Доброе утро';
                    else if ($time < 21)
                        $hour = 'Добрый вечер';
                    else
                        $hour = 'Доброй ночи';
                 ?>
                <div style="margin-top:35px;margin-bottom:65px;font-size:24px;font-weight:bold; color:rgb(46, 55, 59);text-align: center;"><?=$hour?>, <?=$user->name?></div>

                <? if (BONUS_PROGRAM_ACTIVE):?>
                    <? $tabs = [
                        'Дисконтная карта' => actionts('UserController@index'),
                        'История покупок' => actionts('UserController@orders'),
                        'Памятка участника бонусной программы' => Page::findBySlug('bonusnaya-programma')->getUrl(),
                    ]?>
                <? else:?>
                    <? $tabs = [
                        'Дисконтная карта' => actionts('UserController@index'),
                        'История покупок' => actionts('UserController@orders'),
                    ]?>
                <? endif;?>
                <div class="supertabs" style="width:100%">
                    <? foreach ($tabs as $tabName => $url):?>
                        <a href="<?=$url?>" class="active"><?=$tabName?></a>
                    <? endforeach;?>
                    <span class="stretch"></span>
                </div>
                <style>
                    .supertabs{
                        position: relative;
                        font-size:20px;
                        text-align: justify;
                        width:100%;
                    }
                    .supertabs a{
                        padding:10px 50px;
                        color:rgb(46, 55, 59);
                        display: inline-block;
                    }
                    .supertabs a.active{
                        color:rgb(46, 55, 59);
                        display: inline-block;
                        font-weight:bold;
                        color: rgb(0, 106, 176);
                        position: relative;
                    }

                    .supertabs a.active:after{
                        position:absolute;
                        content: "\f078";
                        margin-left:22px;
                        display: inline-block;
                        font: normal normal normal 14px/1 FontAwesome;
                        font-size: inherit;
                        text-rendering: auto;
                        -webkit-font-smoothing: antialiased;
                        margin-top:4px;
                    }
                    .supertabs a.active:before{
                        content: '';
                        position: absolute;
                        bottom:-9px;
                        z-index: 1;
                        left:0px;
                        height:9px;
                        border-radius: 9px;
                        background-color: rgb(0, 106, 176);
                        width:100%;
                    }

                    .supertabs:after{
                        content: '';
                        position: absolute;
                        bottom:19px;
                        left:0px;
                        height:9px;
                        border-radius: 9px;
                        background-color: rgb(238, 248, 255);
                        width:100%;
                    }
                    .stretch {
                        width: 100%;
                        display: inline-block;
                        font-size: 0;
                        line-height: 0
                    }
                </style>
                <?=$content?>
            </div>
        </div>
    </div>
</div>
<?=view('front.layouts.parts.jstemplates')->render()?>
<?=view('front.layouts.parts.footer')->render()?>