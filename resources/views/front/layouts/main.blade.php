<?=view('front.layouts.parts.header')->render()?>
    <div class="content">
        <?=view('front.layouts.parts.breadcrumbs')?>
        <?=$content?>
    </div>
<?=view('front.layouts.parts.jstemplates')->render()?>
<?=view('front.layouts.parts.footer')->render()?>