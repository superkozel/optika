<div class="visible-sm visible-xs">
    <a class="mobile-menu-opener" onclick="openMobileMenu()">
        <i class="fa fa-bars"></i>
    </a>
    <a class="mobile-menu-personal" href="<?=actionts('UserController@index')?>">
        <i class="fa fa-user"></i>
    </a>

    <style>
        body.noscroll{
            position:fixed;
            overflow:hidden;
        }
        .mobile-menu-personal{
            position: absolute;
            right: 12px;
            top: 12px;
            font-size: 28px;
            padding: 5px 10px;
            z-index: 1;
            line-height: 1;
            color: #ff0000;
            border-radius: 3px;
            border: 2px solid #76aed4;
            box-shadow: 0px 0px 2px #b7b5b5;
        }
        .mobile-menu-opener{
            position: absolute;
            left: 7px;
            top: 12px;
            font-size: 28px;
            padding: 4px 8px;
            z-index: 1;
            line-height: 1;
            color: #ff0000;
            border-radius: 3px;
            border: 2px solid #76aed4;
            box-shadow: 0px 0px 2px #b7b5b5;
        }

        .mobile-menu-personal{
            position: absolute;
            right: 12px;
            top: 12px;
            font-size: 28px;
            padding: 5px 10px;
            z-index: 1;
            line-height: 1;
            color: #ff0000;
            border-radius: 3px;
            border: 2px solid #76aed4;
            box-shadow: 0px 0px 2px #b7b5b5;
        }
        .mobile-menu-opener:hover,  .mobile-menu-personal:hover{
            opacity:0.7;
            color: white;
        }


        .mobile-menu {
            left: 0px;
            top: 0px;
            background-color: black;
            color: white;
            overflow: hidden;
            width: 320px;
            overflow-y: scroll;
            min-height: 100%;
            z-index: 20;
            position: fixed;
            height: 100%;
        }

        .mobile-menu-header{
            display: block;
            background-color: black;
            padding: 10px 20px;
            width:320px;
        }

        .mobile-menu-body ul a {
            color: white;
            line-height: 50px;
            font-size: 18px;
            padding-left: 20px;
            width: 100%;
            box-sizing: border-box;
            display: block;
        }
        .mobile-menu-body ul{
            margin-left:-40px;
            margin-bottom:0px
        }

        .mobile-menu-body a {
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -khtml-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            line-height: 50px;
        }

        .mobile-menu-body li {
            border-top: 1px solid #303030;
            width: 100%;
            position: relative;
        }
        .mobile-menu-footer {
            display: block;
            background-color: black;
            padding: 30px 0px;
            width: 320px;
            border-top: 1px solid #303030;
        }

        .mobile-menu-body li.have-children:after {
            display: block;
            content: '';
            background: url(/ui/images/menusep-icon.png) no-repeat;
            width: 9px;
            height: 14px;
            position: absolute;
            top: 0px;
            right: 0px;
            margin: 20px;
        }
        .mobile-menu-body li.have-children.opened:after, .menu-top li.have-children:hover:after {
            background-position: 0 -14px;
            width: 14px;
            height: 9px;
            top: 4px;
        }
        .mobile-menu-body ul ul a {
            padding-left: 40px;
        }
        .mobile-menu-body li.opened > a {
            background-color: #202020;
        }

        .mobile-menu-body ul li.active a {
            background-color: #e2223f;
        }

        #logo{max-width:100%;}
        @media screen and (max-width:992px) {
            /*#logo{max-height:45px;width:auto;}*/
            .b-logo{margin-left:50px;margin-right:50px;text-align:center;}

            .header-block{height:100px;}
                .header-block .header-contacts-row{padding-top:15px;}
        }
    </style>
    <div class="mobile-menu" style="display: none;">
        <div class="mobile-menu-header">
            <div style="opacity: 0.6;float:left;line-height:16px;padding-top:12px;width:248px;">
                Оптика Фаворит - <?=plural(Salon::count(), 'салон', 'салона', 'салонов', true)?> оптики в Москве и области
            </div>
            <a style="font-size: 30px;line-height: 55px;float:right;display:block;width:30px;text-align: right;" href="javascript:closeMobileMenu()">
                <img src="/ui/images/close-icon.png">
            </a>
            <div class="clearfix"></div>
        </div>
        <?
        $mobileMenu = [
            'Салоны' => actionts('SalonController@index', [], false),
            'Акции' => actionts('ActionController@index', [], false),
            'Услуги салонов' => [
                actionts('ServiceController@index', [], false),
                \ActiveRecord\SalonService::all()->mapWithKeys(function(\ActiveRecord\SalonService $service){
                    return [$service->name => $service->getUrl()];
                })
            ],
            'Информация' => [
                '/informaciya/',
                [
                    'Доставка' => Page::url(7),
                    'Способы оплаты' => Page::url(17),
                    'Гарантия' => Page::url(8),
                    'Обмен и возврат' => Page::url(18),
                ]
            ],
            'Солнцезащитные очки' => '/catalog/sunglasses/',
            'Оправы' => '/catalog/medical_frames/',
            'Линзы' => '/catalog/lenses/',
            'Контактные линзы' => '/catalog/contact_lenses/',
            'Готовые очки' => '/catalog/finished_glasses/',
            'Подарочные сертификаты' => '/catalog/gift_certificates/',
            'О компании' => '/o-nas/',
        ]
        ?>
        <div class="mobile-menu-body">
            <ul>
                <? foreach ($mobileMenu as $k => $v):?>
                    <? if (is_array($v)):?>
                        <? $active = strpos($_SERVER["REQUEST_URI"], $v[0]) !== false;?>
                        <li class="have-children <? if ($active):?>opened<?endif;?>"><a href="<?=$v[0]?>"><?=$k?></a>
                            <ul <? if (!$active):?>style="display:none;"<?endif;?>>
                                <? foreach($v[1] as $kk => $vv):?>
                                    <? $active2 = strpos($_SERVER["REQUEST_URI"], $vv) !== false;?>
                                    <li <? if ($active2):?>class="active"<?endif;?>><a href="<?=$vv?>"><?=$kk?></a></li>
                                <? endforeach;?>
                            </ul>
                        </li>
                    <? else:?>
                        <?php
//                                dump($_SERVER["REQUEST_URI"]);
//                                dump($v);
//                                dump(strpos($_SERVER["REQUEST_URI"], $v) !== false);
                        ?>
                        <li <? if (strpos($_SERVER["REQUEST_URI"], $v) !== false):?>class="active"<?endif;?>><a href="<?=$v?>"><?=$k?></a></li>
                    <? endif;?>
                <? endforeach;?>
            </ul>
        </div>
        <div class="mobile-menu-footer">
            <a href="tel:8 (495) 984-54-31" style="display:block;color:white;text-align: center;text-transform: uppercase;font-size:24px;"><?=Settings::get('phone')?></a>
            <div style="text-align: center;color: #515151;font-size: 11px;text-transform: uppercase;">Ежедневно с 9:00 до 18:00</div>
        </div>
    </div>

    <script>
        $.fn.fadeSlideRight = function(speed,fn) {
            var divWidth = $(this).width();
            var that = this;
            return $(this).css({display: 'block', 'width': '0px'}).animate({
                'width' : divWidth + 'px'
            },speed || 400, function() {
                $(that).css({width: 'auto'})
                $.isFunction(fn) && fn.call(this);
            });
        }

        $.fn.fadeSlideLeft = function(speed,fn) {
            var that = this;
            return $(this).animate({
                'width' : '0px'
            },speed || 400,function() {
                $(that).css({display: 'none', width: 'auto'})
                $.isFunction(fn) && fn.call(this);
            });
        }
        function openMobileMenu()
        {
            $('.mobile-menu').fadeSlideRight(350).addClass('mobile-open');
            $('body').addClass('noscroll');
        }

        function closeMobileMenu()
        {
            $('.mobile-menu').fadeSlideLeft(350).removeClass('mobile-open');
            $('body').removeClass('noscroll');
        }


        $('.mobile-menu-body li.have-children > a').click(function(e){
            e.stopPropagation();
            e.stopImmediatePropagation();
            e.preventDefault();

            var $li = $(this).parents('li');
            if (! $li.hasClass('opened')) {
                $li.addClass('opened');

                var sub = $li.find('ul').slideDown();
                $('.mobile-menu li').not($li).removeClass('opened').find('ul').slideUp();
            }
            else {
                $li.removeClass('opened').find('ul').slideUp();
            }

        });

        $('.mobile-menu li:not(.have-children)').click(function(e){
            window.location = $(this).children('a').attr('href');
        });
    </script>
</div>