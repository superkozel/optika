<?php
/** @var Basket $basket */
?>
</div>
</div>
<div onclick="slideTo('body')" id="scroller" class="b-top" style="display: none;"><i class="fa fa-chevron-up"></i></div>
<style>
    .b-top{width: 50px;
        height: 50px;
        background-color: #006ab0;
        text-align: center;
        padding: 10px 0;
        position: fixed;
        bottom: 10px;
        right: 10px;;
        cursor: pointer;
        display: none;
        color: #f5f5f5;
        font-size: 20px;
        z-index: 99;
    }
    @media screen and (max-width: 768px) {
        .b-top{
            bottom: 35px;
        }
    }
    #footer{
        color: #2e373b;
    }
    #footer *{
        color: #2e373b;
    }
    #footer a.link {
        color:#2e373b;
        text-decoration:underline;
        font-size: 16px;
        line-height:2.3;
    }
    #footer a:hover {
        text-decoration:none;
    }
    #footer .h4, #footer a.h4{
        font-weight: bold;
        font-size: 18px;
    }
</style>
<script>
    $(window).scroll(function () {
        if ($(this).scrollTop() > 0) {
            $('#scroller').fadeIn();
        } else {
            $('#scroller').fadeOut();
        }
    });

    Basket.on('add, remove, update', function(result){
        $('.mobile-footer').find('.basket-count').html(result.basket.count);
    });
    Favorites.on('add, remove, update', function(result){
        $('.mobile-footer').find('.favorite-count').html(result.count);
    });
</script>
<style>
    .mobile-footer a {
        display:block;
        font-size:12px;
        text-align: center;
        color:white;
        position:relative;;
    }
    .mobile-footer i{
        font-size:14px;
    }

    .mobile-footer-counter-inner{
        font-size:1em;
        border:2px solid rgba(255,255,255,0.9);
        background-color: red;
        color:white;
        width:18px;
        height:18px;
        line-height:15px;
        text-align: center;
        font-weight:bold;
        border-radius:12px;
        position:absolute;
        margin-left:16px;
    }

    .mobile-footer-counter{
        position:absolute;
        right:50%;
        top:-12px;
    }

    .mobile-footer-counter span{
        width:18px;
        position:absolute
        text-align: center;
    }

    .footer-callback{
        border-radius: 100px;border: solid 4px #9a9fa1;line-height:1;padding:4px 15px;display: inline-block;font-family: Helvetica;color: #2e383c;
        margin-bottom:12px;
    }

    .footer-callback:hover{
        border-color:#006ab0;
        color#006ab0:
    }
</style>
<div class="mobile-footer visible-xs" style="position:fixed;bottom:0px;left:0px;background-color: #006ab0;width:100%;padding:4px 0px;z-index:1;">
    <div class="row" stype="margin:0px;">
        <a href="<?=actionts('BasketController@index')?>" class="col-xs-3 nopadding">
            <i class="fa fa-shopping-basket"></i>
            Корзина
            <? if ($basket->getContentsCount() > 0):?>
                <div class="mobile-footer-counter">
                    <div class="mobile-footer-counter-inner basket-count">
                        <span><?=$basket->getContentsCount()?></span>
                    </div>
                </div>
            <? endif;?>
        </a>
        <a href="<?=actionts('FavoriteController@index')?>" class="col-xs-3 nopadding">
            <i class="fa fa-star"></i>
            Избранное
            <? if (($count = \ActiveRecord\Favorite::current()->count()) > 0):?>
                <div class="mobile-footer-counter">
                    <div class="mobile-footer-counter-inner favorite-count">
                        <span><?=$count?></span>
                    </div>
                </div>
            <? endif;?>
        </a>
        <a href="<?=actionts('SalonController@index')?>" class="col-xs-3 nopadding">
            <i class="fa fa-home"></i>
            Салоны
        </a>
        <a href="<?=actionts('ActionController@index')?>" class="col-xs-3 nopadding">
            <i class="fa fa-percent"></i>
            Акции
        </a>
    </div>
</div>
<footer id="footer" style="background-color: #f5f6f6;z-index:1;position:relative;">
    <div style="box-shadow: 0 1px 1px 0 #ffffff;border-bottom: solid 1px #cfd0d0;">
        <div class="container">
            <div class="col-sm-5 col-md-3 nopadding col-lg-3" style="text-align: left;">
                <img src="/dizayn/bitmap.png" style="max-width:100%;"/>
            </div>
            <div class="col-sm-7 nopadding">
                <div class="row nomargin">
                    <style>
                        #footer .footer-phone a{
                            font-family: Helvetica;
                            font-size: 20px;
                            text-align: left;
                            color: #2e383c;
                            line-height:55px;
                        }
                    </style>
                    <div class="col-md-4 nopadding col-sm-6 col-xs-4 col-xxs-6">
                        <div class="footer-phone">
                            <a href="tel:<?=Settings::get('phone')?>"><?=Settings::get('phone')?></a>
                        </div>
                    </div>
                    <div class="col-md-4 nopadding col-sm-6 col-xs-4 col-xxs-6">
                        <div class="footer-phone">
                            <a href="tel:<?=Settings::get('phone2')?>"><?=Settings::get('phone2')?></a>
                        </div>
                    </div>
                    <div class="col-md-4 nopadding hidden-md col-xs-4 col-xxs-12" style="padding-top:13px;text-align: center">
                        <a onclick="openCallbackWindow(event.currentTarget)" class="footer-callback">
                            заказать звонок
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-2 nopadding hidden-sm" style="margin-top:8px;text-align: right;">
                <a target="_blank" href="http://vk.com/club<?=Settings::get('vk')?>"><img src="/dizayn/vk-footer.png"/></a>
                <a target="_blank" href="https://www.instagram.com/<?=Settings::get('instagram')?>"><img src="/dizayn/instagram-footer.png"/></a>
                <a target="_blank" href="https://www.facebook.com/<?=Settings::get('facebook')?>"><img src="/dizayn/facebook-footer.png"/></a>
                <a target="_blank" href="https://twitter.com/<?=Settings::get('twitter')?>"><img src="/dizayn/twitter-footer.png"/></a>
            </div>
        </div>
    </div>
    <div class="container" >
        <div class="row" style="margin-bottom:24px;position:relative;">
            <div style="position: absolute;bottom:0px;right:24px;">
                <a class="h4" href="/sitemap/"><i class="fa fa-sitemap"></i> Карта сайта</a>
            </div>
            <div class="col-sm-4" style="margin-top:24px;">
                <b style="color:#2e373b;opacity: 0.65">© Оптика-Фаворит, 2001 - <?=date('Y')?>.г. ООО «ОчиАле»</b>
                <div style="margin:18px 0px">
                    <a href="https://itunes.apple.com/us/app/optika-favorit/id924843905"><img src="/dizayn/googleplay.png"/></a>
                    <a href="https://play.google.com/store/apps/details?id=ru.itcompass.myapplication"><img src="/dizayn/itunes.png"/></a>
                </div>
                Сеть специализированных магазинов оптики в Москве, Балашихе, Дзержинском, Долгопрудном, Железнодорожном, Жуковском, Ивантеевке, Красногорске, Лыткарино, Люберцах, Мытищах, Одинцово, Химках
                <br/><br/>
                <span>
                ООО «ОчиАле»
                г.Москва, ул.Вавилова, д.5, корп.3
                ОГРН 1137746935644,
                ИНН 7725805598
                </span>

                <br/>
                <br/>
                <span>
                +7 (495) 231-46-72
                Пн-Пт 09:00–18:00
                </span>
            </div>
            <div class="col-sm-8">
                <div class="row">
                    <div class="col-sm-4" style="margin-top:24px;">
                        <div class="h4 notopmargin">О компании</div>
                        <a class="link" href="<?=actionts('NewsController@index')?>">Новости</a><br/>
                        <a class="link" href="<?=Page::findBySlug('contacts')->getUrl()?>">Контакты</a><br/>
                        <a class="link" href="<?=actionts('SalonController@index')?>">Салоны</a><br/>
                        <a class="link" href="<?=Page::findBySlug('smi')->getUrl()?>">СМИ о нас</a><br/>
                        <a class="link" href="<?=Page::url(25)?>">Политика конфиденциальности</a><br/>
                    </div>
                    <div class="col-sm-4" style="margin-top:24px;">
                        <div class="h4 notopmargin">Салоны</div>

                        <a class="link" href="<?=actionts('SalonController@index')?>">Наши салоны</a><br/>
                        <a class="link" href="<?=actionts('ServiceController@index')?>">Услуги</a><br/>
                        <a class="link" href="<?=actionts('ActionController@index')?>">Акции</a><br/>
                    </div>
                    <div class="col-sm-4" style="margin-top:24px;">
                        <div class="h4 notopmargin">Каталог товаров</div>

                        <a class="link" href="<?=CatalogCategory::findBySlug('medical_frames')->getUrl()?>">Оправы</a><br/>
                        <a class="link" href="<?=CatalogCategory::findBySlug('contact_lenses')->getUrl()?>">Контактные линзы</a><br/>
                        <a class="link" href="<?=CatalogCategory::findBySlug('sunglasses')->getUrl()?>">Солнцезащитные очки</a><br/>
                        <a class="link" href="<?=CatalogCategory::findBySlug('finished_glasses')->getUrl()?>">Готовые очки</a><br/>
                        <a class="link" href="<?=CatalogCategory::findBySlug('lenses')->getUrl()?>">Линзы очковые</a><br/>
                        <a class="link" href="<?=CatalogCategory::findBySlug('gift_certificates')->getUrl()?>">Подарочные сертификаты</a><br/>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div style="background-color: #2e373b;">
        <div class="container" style="
  opacity: 0.19;
  font-family: Helvetica;
  font-size: 32px;
  font-weight: bold;
  text-align: center;
  color: #f5f6f6;">
<!--            <img style="max-width:100%;" src="/images/footer-warning.png"/>-->
            Имеются противопоказания, проконсультируйтесь со специалистом
        </div>
    </div>
</footer>

<? if (CALLBACK_ENABLED):?>
    <!-- Begin LeadBack code {literal} -->
    <script>
        var _emv = _emv || [];
        _emv['campaign'] = 'd0f82e1046ccbd26a95b3bf6';

        (function() {
            var em = document.createElement('script'); em.type = 'text/javascript'; em.async = true;
            em.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'leadback.ru/js/leadback.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(em, s);
        })();
    </script>
    <!-- End LeadBack code {/literal} -->
<? endif;?>
</body>
</html>