<script>
    var whereAt= (function(ev){
        if(window.pageXOffset!= undefined){
            return function(ev){
                return [ev.clientX+window.pageXOffset,
                    ev.clientY+window.pageYOffset];
            }
        }
        else return function(){
            var ev= window.event,
                d= document.documentElement, b= document.body;
            return [ev.clientX+d.scrollLeft+ b.scrollLeft,
                ev.clientY+d.scrollTop+ b.scrollTop];
        }
    })()
    var initialPosition = 0;
    var detect = function(ev) {
        initialPosition = whereAt(ev);
        $('body').off('mousemove', detect);
    };
    $('body').mouseenter(detect);

    $().ready(function(){
        var nav=$('.top-catalog-menu');
        var stickyNavTop=nav.offset().top + 60;

        var stickyNav=function(){
            var scrollTop=$(window).scrollTop();
            if(scrollTop>stickyNavTop && ! nav.hasClass('sticky')){
                nav.addClass('sticky');
                if (window.innerWidth >= 992) {
                    $('.top-menu-horizontal-link.has-children').removeClass('opened');
                }
            }
            if(scrollTop<stickyNavTop && nav.hasClass('sticky')){nav.removeClass('sticky');}
        };
        stickyNav();
        $(window).scroll(function(){stickyNav();});
        $('.top-menu-horizontal-link.has-children > a').click(function(event){
            var $el = $(this).parent();
            $('.top-menu-horizontal-link').not($el).removeClass('opened');
            if (! $el.hasClass('opened')) {
                $el.addClass('opened')
                event.preventDefault();
            }
        });
        $('.top-menu-horizontal-link.has-children > a').mouseenter(function(event){
            var that = this;

            if (initialPosition == 0) {
                initialPosition = whereAt(event);
            }

            var timeoutBeforeShow = 200;
            if (initialPosition) {//если курсор при открытии окна был на этом самом месте, показывать подменю нужно позже
                if (
                    initialPosition[0] > $(that).offset().left
                    && initialPosition[0] < $(that).offset().left + $(that).width()
                    && initialPosition[1] > $(that).offset().top
                    && initialPosition[1] < $(that).offset().top + $(that).height()
                ) {
                    timeoutBeforeShow = 1000;
                }
            }

            that.interval = setTimeout(function(){
                var $el = $(that).parent();
                $('.top-menu-horizontal-link.has-children').not($el).removeClass('opened');
                if (! $el.hasClass('opened')) {
                    $el.addClass('opened')
                }
            }, timeoutBeforeShow)
        });

        $('.top-menu-horizontal-link.has-children > a').mouseleave(function(){
            initialPosition = null;
            if (this.interval) {
                clearTimeout(this.interval);
                delete (this.interval);
            }
        });

        $('.top-menu-horizontal-link.has-children').mouseenter(function(){
            if (this.interval) {
                clearTimeout(this.interval);
                delete (this.interval);
            }
        });
        $('.top-menu-horizontal-link.has-children').mouseleave(function(){
            console.log(6666666666);
            var that = this;
            that.interval = setTimeout(function(){
                $(that).removeClass('opened');
            }, 300)
        });
    });
</script>
<style>
    .basket-block {
        color:#adcee4;
        height:60px;
        padding-top: 13px;
    }
    .basket-block:hover, .menu-favorite:hover {
        opacity:.8;
    }

    .menu-favorite{position:relative;}
    .menu-favorite  .menu-favorite-icon{width:34px;height:34px;text-align: center;border:3px solid #4b95c7;display: inline-block;margin-top:13px;vertical-align: top;margin-right:21px;border-radius:66px;}
    .basket-block .basket-count, .menu-favorite .favorite-count {
        font-weight:bold;
        color:#b3d2e7
    }

</style>
<style>
    .top-catalog-menu {
        width:100%;
        position: relative;
        z-index:10;
        width:100%;
    }

    .top-catalog-menu .menu-logo{
        display: none;
    }

    .top-catalog-menu .basket-block,
    .top-catalog-menu .menu-favorite
    {
        display: none;
    }

    .sub-menu{position:absolute;left:0px;width:100%;z-index:99;padding:0px 7px;}
    .sub-menu-inner{background-color: white;border:1px solid darkgrey;box-shadow: 0 0 17px 0 rgba(0, 0, 0, 0.5);padding:22px;border-top:none;padding-top:8px;}
    .sub-menu-header{font-weight:bold;color: #2e373b;;text-transform: uppercase;margin-bottom:14px;font-size:13px;margin-top:14px;}
    .sub-menu-close{position: absolute;right:16px;top:3px;color: #d73533;font-size:24px;}
    .sub-menu a.sub-menu-link{display: block;
        padding-left:12px;
        position:relative;
        font-family: Helvetica;
        font-size: 14px;
        line-height: 1.64;
        text-align: left;
        color: #2e373b;
    }
    .sub-menu a.sub-menu-link:before{
        content: ''; width: 6px;height:6px;
        background-color: #cbe1ef;
        border: solid 1px #006bb2;
        position:absolute;
        left:0px;
        top:50%;
        margin-top:-3px;
    }
    .sub-menu a.sub-menu-link[href]:hover{text-decoration: underline;}

    .sub-menu a.sub-menu-link[href]:hover:before{
        background-color: #f9e2e2;
        border: solid 1px #d73533;
    }

    .top-menu-horizontal-link .sub-menu{
        visibility: hidden;
    }

    .top-menu-horizontal-link {
        display: block;
        margin-right:2px;
    }

    .top-menu-horizontal-link > a {
        color: #2e373b;
        text-transform: uppercase;
        font-weight:bold;
        text-align:center;
        height:66px;
        padding-top:13px;
        display: block;
        position:relative;
    }

    .top-menu-horizontal-link > a > span {
        border-right: solid 2px #eef2f7;
        padding-left: 32px;
        line-height:40px;
        padding-right: 32px;
        display: block;
        margin-right:-2px;
    }

    .top-menu-horizontal-link.opened > a, .top-menu-horizontal-link:not(.top-menu-horizontal-toggle):hover > a {background-color: #d73533;color:lightyellow;}

    .top-menu-horizontal-link.opened > a {z-index:100}

    .top-menu-horizontal-link.opened .sub-menu{
        visibility: visible;
        transition-delay: 0s;
    }

    .top-menu-horizontal-link .sub-menu a.sub-menu-link:not([href]){
        color:darkgray;
        cursor:default;
    }
    .top-menu-horizontal-link.has-children.opened > a:after{
        content: "";
        display: block;
        position: absolute;
        bottom: -9px;
        left: 50%;
        width: 0;
        height: 0;
        margin-left: -10px;
        border-style: solid;
        border-width: 10px 10px 0;
        border-color: #d73533 transparent transparent transparent;
        z-index: 1011;
    }

    #search-form{position:relative;display: block;height:100%;vertical-align: top;font-size:16px;}
    #search-form .search-form-inner{position:relative;display: block;float:right;}
    #search-form input{width:100%;padding:0px 34px;border:3px solid #0069ae;position: relative;line-height:34px;height:34px;position:relative;border-radius: 66px;}

    #search-form.opened input:focus{box-shadow: none;outline: none;}
    #search-form:not(.opened) #searchCancelButton{display: none}
    #searchCancelButton{
        position: absolute;
        line-height: 34px;
        z-index: 2;
        height: 34px;
        width: 34px;
        right: 0px;
        text-align: center;
    }

    #search-form .search-icon {
        width:34px;
        text-align: center;
        z-index:2;
        font-size:15px;
        color:black;
        position:absolute;
        left:0px;
        top:0px;
    }

    .search-results{position: absolute;left:-90px;right:0px;
        width:auto;
        /*border:1px solid lightgray;*/
        z-index:18;box-sizing: content-box;
        border-radius: 6px 0px 6px 6px;
        background-color: white;
    }

    @media (min-width: 992px) and (max-width:1199px) {
        .top-menu-horizontal-link {
            display: inline-block;
        }

        .catalog-horizontal-links .top-menu-horizontal-link:first-child{margin-left:-15px;}
        .catalog-horizontal-links .top-menu-horizontal-link:last-child{margin-right:-15px;}

        .top-catalog-menu.sticky .top-menu-horizontal-link > a > span {
            padding-left: 16px;
            padding-right: 16px;
        }
    }

    .top-catalog-menu-collapse {
        display: inline;
    }

    @media (max-width:1199px) {

        #search-form{
            display: inline-block;
            padding-top:16px;
            position:absolute;
            right:20px;
        }


        #search-form input{
            display: block;
            width:34px;
            padding:0px;
        }

        #search-form:not(.opened) {
            color:transparent;
        }

        #search-form .search-icon {
            position: absolute;
            line-height:34px;
            pointer-events: none;
        }
        #search-form.opened input{
            width:360px;
            padding:0px 34px;
        }

        #search-form:not(.opened) input::-webkit-input-placeholder
        {
            color: transparent;
        }
        #search-form:not(.opened) input::-moz-placeholder
        {
            color: transparent;
        }
        #search-form:not(.opened) input:-moz-placeholder
        {
            color: transparent;
        }
        #search-form:not(.opened) input:-ms-input-placeholder
        {
            color: transparent;
        }
    }

    @media (min-width: 1200px) {
        .top-menu-horizontal-link {
            display: inline-block;
        }

        #search-form .search-icon {position: absolute;
            line-height:34px;pointer-events: none;}
        #search-form{
            float:right;
            padding-right:21px;padding-top:16px;
        }
        #search-form:not(.filled) #searchCancelButton{display: none}
    }

    .body{padding:0px;box-shadow: 0 -1px 31px 0 rgba(0, 0, 0, 0.38);margin-top:-33px;background-color: white;}
    @media (min-width: 992px) {
        .body{
            position:relative;
            padding-top:66px;
        }

        .top-catalog-menu:not(.sticky){
            margin-top:-66px;
        }

        .top-catalog-menu.sticky #search-form{
            padding-top:13px;
            display: inline-block;
            float:none;
        }

        .top-catalog-menu.sticky #search-form .search-form-inner{
            width:34px;
        }

        .top-catalog-menu.sticky #search-form input{
            padding:0px;
            background-color: #006ab0;
            font-size:16px;
            border-color:#4b95c7;
        }

        .top-catalog-menu.sticky #search-form .search-icon{
            color:white;
        }


        .top-catalog-menu.sticky  #search-form.opened{
            margin-left:0px;
            position:absolute;
            top:0px;
            right:0px;
            width:100%;
            padding-left:200px;
            padding-top:10px;
            z-index:1;
        }

        .top-catalog-menu.sticky #search-form.opened .search-form-inner{
            width:100%;
            -webkit-transition: width .5s ease-out;
            transition: width .5s ease-out;
        }

        .top-catalog-menu.sticky #search-form.opened input{
            width:100%;
            height:40px;
            padding-left:30px;
            background-color: #fbf0f0;
            border-color:#efc6c5;
        }

        .top-catalog-menu.sticky #search-form.opened .search-icon{
            color:black;
            line-height:40px;
        }

        .top-catalog-menu.sticky #search-form:not(.opened) input::-webkit-input-placeholder
        {
            color: transparent;
        }
        .top-catalog-menu.sticky #search-form:not(.opened) input::-moz-placeholder
        {
            color: transparent;
        }
        .top-catalog-menu.sticky #search-form:not(.opened) input:-moz-placeholder
        {
            color: transparent;
        }
        .top-catalog-menu.sticky #search-form:not(.opened) input:-ms-input-placeholder
        {
            color: transparent;
        }

        .top-catalog-menu.sticky {
            background-color: #006ab0;
            margin-top:0px;
            top:0px;
            position:fixed;
            height:60px;
            left:0px;
            box-shadow: 0 -1px 31px 0 rgba(0, 0, 0, 0.38);
        }

        .top-catalog-menu.sticky .menu-logo{
            display: block;
            float:left;
            margin-right:24px;
            padding-top:18px;
            height:60px;
        }

        .top-catalog-menu.sticky .top-menu-horizontal-link > a {
            color: #bfd9eb;
            font-size: 11px;
            padding-top:10px;
            height:60px;
        }
        .top-catalog-menu.sticky .top-menu-horizontal-link > a > span {
            border-right: solid 1px #1e7ab9;
            box-shadow: 1px 0 0 0 #0062a4;
        }
        .top-catalog-menu.sticky .top-menu-horizontal-link > a:last-child > span {
            border-right: none;
            box-shadow: none;
        }

        .top-catalog-menu.sticky .top-menu-horizontal-link.opened > a {
            color: white;
        }

        .top-catalog-menu.sticky .basket-block
        {
            display: inline-block;
        }

        .top-catalog-menu.sticky .menu-favorite
        {
            display: inline;
        }

        .top-catalog-menu.sticky .basket-block .basket-icon{
            color:white;
        }

        .top-catalog-menu.sticky  #searchCancelButton {
            line-height: 40px;
            height: 40px;
            width: 40px;
            right: 3px;
            color: red;
            font-size: 20px;
        }

        .top-catalog-menu.sticky #search-form.opened:not(.filled) #searchCancelButton{display: block}

        .top-catalog-menu.sticky .basket-block{display: inline-block;}
        .menu-favorite i {margin-top:5px;}
    }


    @media (max-width:991px) {

        .top-menu-horizontal-toggle:before {
            content:'';
            width: 12px;
            height: 6px;
            position:absolute;
            background-repeat: no-repeat;
            left:30px;
            margin-top:30px;
            opacity:.4;
            background-image: url('data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTIiIGhlaWdodD0iNiIgdmlld0JveD0iMCAwIDEyIDYiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHRpdGxlPlNoYXBlIDI8L3RpdGxlPjxwYXRoIGQ9Ik0uOCAwTDYgNC42IDExLjIgMGwuOC43TDYgNiAwIC43LjggMHoiIGZpbGw9IiMwMDAiIGZpbGwtcnVsZT0iZXZlbm9kZCIvPjwvc3ZnPg==')
        }

        .top-catalog-menu-inner.collapsed .top-menu-horizontal-toggle:before{
            -webkit-transform: rotate(180deg);
            -ms-transform: rotate(180deg);
            transform: rotate(180deg);
        }

        .search-results {
            left:0px;
        }

        .top-catalog-menu-inner.collapsed .top-catalog-menu-collapse {
            display: none;
        }

        .top-menu-horizontal-link {
            border-bottom: 1px solid beige;
            border-right: none;
        }

        .top-catalog-menu {
            margin-bottom: 24px;
        }

        #search-form {
            padding-right: 20px;
            padding-left: 20px;
            top: 0px;
            right: 0px;
            height:55px;
            padding-bottom:16px;
        }

        #search-form .search-form-inner input {
            display: inline;
        }

        #search-form.opened {
            width: 100%;
            background-color: white;
        }

        #search-form.opened .search-form-inner {
            width: 100%;
            text-align:right;
        }

        #search-form.opened .search-form-inner input {
            width: 100%;
        }
    }
</style>
<script>
    Basket.on('add, remove, update', function(result){
        $('.top-catalog-menu .basket-count').text(result.basket.count);
    });
    Favorites.on('add, remove, update', function(result){
        $('.top-catalog-menu .favorite-count').text(result.count);
        if (result.count) {
            $('.menu-favorite .menu-favorite-icon').button('active');
        }
        else {
            $('.menu-favorite .menu-favorite-icon').button('inactive');
        }
    });
</script>
<div class="top-catalog-menu">
    <div class="row">
        <div class="container">
            <div class="top-catalog-menu-inner collapsed" style="position:relative">
                <a href="/" class="menu-logo">
                    <img src="/dizayn/123456.png" style="height:19px;"/>
                </a>
                <div class="top-menu-horizontal-link top-menu-horizontal-toggle visible-xs visible-sm">
                    <a onclick="$('.top-catalog-menu-inner').toggleClass('collapsed')">
                        <span>Каталог</span>
                    </a>
                </div>
                <div class="top-catalog-menu-collapse">
                    <?
                    $medicalFramesCategory = CatalogCategory::findBySlug('medical_frames');
                    $contactLensesCategory = CatalogCategory::findBySlug('contact_lenses');
                    $sunglasssesCategory = CatalogCategory::findBySlug('sunglasses');
                    $menu = array(
                        'Солнцезащитные очки' => [CatalogCategory::findBySlug('sunglasses')->getUrl(),
                            [
                                [
                                    ['По форме', ['Кошачий глаз', 'Круглые', 'Квадратные']],
                                    ['Модные', ['2017 мужские', '2017 женские', 'Необычные', 'Хит сезона']],
                                ],
                                [
                                    ['Пол', [
                                        'Мужские' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['prinadlezhnost' => 'muzhskie']),
                                        'Женские' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['prinadlezhnost' => 'zhenskie']),
                                    ]],
                                    ['Возраст', [
                                        'Детские' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['prinadlezhnost' => 'detskie']),
                                        'Подростку' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['prinadlezhnost' => 'podrostkovye']),
                                        'Для взрослого' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['prinadlezhnost' => 'zhenskie,muzhskie,uniseks']),
                                    ]],
                                    ['По цене', [
                                        'До 5000 руб.' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['price' => '-5000']),
                                        'От 5000 до 10000 руб.' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['price' => '5000-10000']),
                                        'От 10000 до 20000 руб.' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['price' => '10000-20000']),
                                        'От 20000 руб.' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['price' => '20000-']),
                                    ]],

                                ],
                                [
                                    ['Популярные бренды', [
                                        'RayBan' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['brand' => 'ray_ban']),
                                        'Polaroid' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['brand' => 'polaroid']),
                                        'Lina Latini' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['brand' => 'lina_latini']),
                                        'Lucia Valdi' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['brand' => 'lucia_valdi']),
                                        'FreshLook' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['brand' => 'freshlook']),
                                        'Maxima' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['brand' => 'maxima']),
                                        'Zeiss' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['brand' => 'zeiss']),
                                    ]],
                                    ['Элитные бренды', [
                                        'DITA' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['brand' => 'ray_ban']),
                                        'Tom Brown' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['brand' => 'tom_brown']),
                                        'CHROME HEARTS' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['brand' => 'chrome_hearts']),
                                        'GUCCI' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['brand' => 'cucci']),
                                        'FENDI' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['brand' => 'fendi']),
                                        'PRADA' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['brand' => 'prada']),
                                    ]],
                                ],
                                [
                                    ['Аксессуары', ['Футляр', 'Салфетка', 'Средства ухода']]
                                ],
                            ]
                        ],
                        'Оправы' => [
                            CatalogCategory::findBySlug('medical_frames')->getUrl(),
                            [
                                [
                                    ['По форме', [
                                        'Овальные',
                                        'Кошачий глаз',
                                        'Круглые',
                                        'Квадратные',
                                        'Авиатор',
                                        'Бабочки',
                                        'Wayfarer',
                                        'Клубмастер']
                                    ],
                                    ['Стиль', ['Модные', 'Современные', 'Яркие', 'Классические']],
                                    [['Изготовление очков на заказ', '/uslugi/']],
            //                                    [['Ремонт очков', '/uslugi/']],
                                ],
                                [
                                    ['Пол', [
                                        'Мужские' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['prinadlezhnost' => 'muzhskie']),
                                        'Женские' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['prinadlezhnost' => 'zhenskie']),
                                    ]],
                                    ['Возраст', [
                                        'Детские' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['prinadlezhnost' => 'detskie']),
                                        'Подростку' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['prinadlezhnost' => 'podrostkovye']),
                                        'Для взрослого' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['prinadlezhnost' => 'zhenskie,muzhskie,uniseks']),
                                    ]],
                                    ['По цене', [
                                        'До 5000 руб.' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['price' => '-5000']),
                                        'От 5000 до 10000 руб.' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['price' => '5000-10000']),
                                        'От 10000 до 20000 руб.' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['price' => '10000-20000']),
                                        'От 20000 руб.' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['price' => '20000-']),
                                    ]],

                                ],
                                [
                                    ['Конструкция', [
                                        'Винтовые' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['kontrukciya' => 'vintovye']),
                                        'Лесочные' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['kontrukciya' => 'lesochnye']),
                                        'Монолинза' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['kontrukciya' => 'monolinza']),
                                        'Половинка' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['kontrukciya' => 'polovinka']),
                                        'Полнооправные' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['kontrukciya' => 'polnoopravnye']),
                                    ]],
                                    ['Материал', [
                                        'Металлические' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['material' => 'metall']),
                                        'Золотые' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['material' => 'zoloto']),
            //                                        'Минерал' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['material' => 'mineral']),
            //                                        'Мономер' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['material' => 'monomer']),
                                        'Пластик' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['material' => 'plastik']),
            //                                        'Полимер' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['material' => 'polimer']),
                                    ]]
                                ],
                                [
                                    ['Элитные бренды', [
                                        'DITA' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['brand' => 'dita']),
                                        'Tom Brown' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['brand' => 'tom_brown']),
                                        'CHROME HEARTS' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['brand' => 'chrome_hearts']),
                                        'GUCCI' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['brand' => 'cucci']),
                                        'FENDI' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['brand' => 'fendi']),
                                        'PRADA' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['brand' => 'prada']),
                                    ]],
                                    ['Модные'],
                                    ['Хиты 2017'],
                                ],
                            ]
                        ],
            //        'Готовые очки' => [CatalogCategory::findBySlug('finished_glasses')->getUrl(),
            //            [
            //                [
            //                    ['По форме', ['Кошачий глаз', 'Круглые', 'Квадратные']]
            //                ],
            //                [
            //                    ['Пол', ['Мужские', 'Женские']],
            //                    ['Возраст', ['Детские', 'Подростку', 'Для взрослого']]
            //                ],
            //                [
            //                    ['Конструкция', ['Винтовые', 'Лесочные', 'Монолинза', 'Половинка', 'Полнооправные']]
            //                ],
            //                [
            //                    ['Популярные бренды', ['Acuvue', 'AIR OPTIX', 'Clear', 'FreshLook', 'Maxima', 'Zeiss']],
            //                ],
            //            ]
            //        ],
                        'Линзы' => [CatalogCategory::findBySlug('lenses')->getUrl(),
                            [
                                [
                                    ['Поиск по рецепту', [
                                        ['Сфера', 'opticheskaya-sila-sfera', CatalogFilter::findBySlug('opticheskaya-sila-sfera')->getOptions(true)],
                                        ['Цилиндр', 'cilindr', CatalogFilter::findBySlug('cilindr')->getOptions(true)],
                                        ['Аддидация', 'addidaciya', CatalogFilter::findBySlug('addidaciya')->getOptions(true)]
                                    ], 'form', CatalogCategory::findBySlug('lenses')->getUrl()]
                                ],
                                [
                                    ['Тип линз', [
                                        'Традиционные',
                                        'Бифокальные',
                                        'Прогрессивные',
                                        'Офисные',
                                        'Специальные'
                                    ]],

                                    ['Дизайн поверхности', [
                                        'Сферический' => \App\Http\Controllers\CatalogController::createUrlSimple(CatalogCategory::findBySlug('lenses'), ['geometriya-linzy' => 'sfericheskaya']),
                                        'Асферический' => \App\Http\Controllers\CatalogController::createUrlSimple(CatalogCategory::findBySlug('lenses'), ['geometriya-linzy' => 'asfericheskaya']),
                                        'Би- Асферический' => \App\Http\Controllers\CatalogController::createUrlSimple(CatalogCategory::findBySlug('lenses'), ['geometriya-linzy' => 'dvojnaya-asferika']),
            //                                        'Лентикулярный' => \App\Http\Controllers\CatalogController::createUrlSimple(CatalogCategory::findBySlug('lenses'), ['geometriya-linzy' => 'asfericheskaya']),
                                    ]],
                                ],

                                [
                                    ['Особенности линз', [
                                        'Прозрачные',
                                        'Фотохромные',
                                        'Поляризационные',
                                        'Солнцезащитные',
                                        'Тонированыые',
                                        'Специальные'
                                    ]],
                                ],

                                [
                                    ['Материал', [
                                        'Полимер' => \App\Http\Controllers\CatalogController::createUrlSimple(CatalogCategory::findBySlug('lenses'), ['material' => 'polimer']),
                                        'Минерал' => \App\Http\Controllers\CatalogController::createUrlSimple(CatalogCategory::findBySlug('lenses'), ['material' => 'mineral']),
                                        'Пластик' => \App\Http\Controllers\CatalogController::createUrlSimple(CatalogCategory::findBySlug('lenses'), ['material' => 'plastik']),
                                        'Мономер' => \App\Http\Controllers\CatalogController::createUrlSimple(CatalogCategory::findBySlug('lenses'), ['material' => 'monomer']),
                                    ]],
                                    ['Покрытие', [
                                        'Базовое' => \App\Http\Controllers\CatalogController::createUrlSimple(CatalogCategory::findBySlug('lenses'), ['pokrytie-linzy' => 'bazovoe']),
                                        'Бизнес' => \App\Http\Controllers\CatalogController::createUrlSimple(CatalogCategory::findBySlug('lenses'), ['pokrytie-linzy' => 'biznes']),
                                        'Премиальное' => \App\Http\Controllers\CatalogController::createUrlSimple(CatalogCategory::findBySlug('lenses'), ['pokrytie-linzy' => 'premialnoe']),
                                    ]],
                                ]
                            ]
                        ],
                        'Контактные линзы' => [
                            CatalogCategory::findBySlug('contact_lenses')->getUrl(),
                            [
                                [
                                    ['Режим замены', [
                                        'Однодневные' => \App\Http\Controllers\CatalogController::createUrl(CatalogCategory::findBySlug('one_day_lenses')),
                                        'Двухнедельные',
                                        'Одномесячные',
                                        'Длительного ношения'
                                    ]],
                                    ['Тип линз', [
                                        'Торические' => \App\Http\Controllers\CatalogController::createUrl(CatalogCategory::findBySlug('toric_lenses')),
                                        'Цветные и оттеночные' => \App\Http\Controllers\CatalogController::createUrl(CatalogCategory::findBySlug('color')),
                                        'Цветные для тёмных глаз',
                                        'Crazy(с рисунком)' => \App\Http\Controllers\CatalogController::createUrl(CatalogCategory::findBySlug('toric_lenses')),
                                        'Спортивные'
                                    ]],
                                ],
                                [
                                    ['Популярные бренды', [
                                        'Acuvue' => \App\Http\Controllers\CatalogController::createUrlSimple($contactLensesCategory, ['brand' => 'acuvue']),
                                        'AIR OPTIX' => \App\Http\Controllers\CatalogController::createUrlSimple($contactLensesCategory, ['brand' => 'air_optix']),
                                        'Clear' => \App\Http\Controllers\CatalogController::createUrlSimple($contactLensesCategory, ['brand' => 'clear']),
                                        'FreshLook' => \App\Http\Controllers\CatalogController::createUrlSimple($contactLensesCategory, ['brand' => 'freshlook']),
                                        'Maxima' => \App\Http\Controllers\CatalogController::createUrlSimple($contactLensesCategory, ['brand' => 'maxima']),
                                        'Zeiss' => \App\Http\Controllers\CatalogController::createUrlSimple($contactLensesCategory, ['brand' => 'zeiss']),
                                    ]],
                                    ['Сопутствующие товары', [
                                        'Растворы и капли' => CatalogCategory::findBySlug('solutions_and_eye_drops')->getUrl(),
                                        'Футляры' => CatalogCategory::findBySlug('accessories')->getUrl(),
                                        'Щипцы' => CatalogCategory::findBySlug('accessories')->getUrl(),
                                        'Очки-тренажёры',
                                    ]]
                                ],
                                [
                                    [null, 'catalogmenu-kontaktnye-linzy', 'banner']
                                ],
                                [
                                    ['Поиск по рецепту', [
                                        ['Оптическая сила', 'opticheskaya-sila-sfera', CatalogFilter::findBySlug('opticheskaya-sila-sfera')->getOptions(true)],
                                        ['Радиус кривизны', 'cilindr', CatalogFilter::findBySlug('cilindr')->getOptions(true)],
                                        ['Аддидация', 'addidaciya', CatalogFilter::findBySlug('addidaciya')->getOptions(true)]
                                    ], 'form', $contactLensesCategory->getUrl()]
                                ],
                            ]
                        ],
                        'Подарочные карты' => [CatalogCategory::findBySlug('gift_certificates')->getUrl()],
                    );?>
                    <? foreach ($menu as $name => $href):?><div class="top-menu-horizontal-link <? if (! empty($href[1])):?>has-children<?endif?>">
                        <a href="<?=$href[0]?>">
                            <span><?=$name?></span>
                        </a>
                        <? if (! empty($href[1])):?>
                            <div class="sub-menu">
                                <div class="sub-menu-inner">
                                    <a class="sub-menu-close" onclick="$(event.currentTarget).parents('.top-menu-horizontal-link').removeClass('opened');">
                                        <i class="fa fa-close"></i>
                                    </a>
                                    <? foreach ($href[1] as $subMenuBlock):?>
                                        <div class="col-sm-3">
                                            <? foreach ($subMenuBlock as $k => $subMenuBlockBlock):?>
                                                <? if (! is_array($subMenuBlockBlock[0])):?>
                                                    <span class="sub-menu-header" style="display:block;"><?=$subMenuBlockBlock[0]?></span>
                                                <? else:?>
                                                    <a href="<?=$subMenuBlockBlock[0][1]?>" class="sub-menu-header" style="display:block;color:#982eb9;<? if ($k > 0):?>margin-top:8px;<?endif;?>`"><?=$subMenuBlockBlock[0][0]?></a>
                                                <? endif;?>

                                                <? if (! empty($subMenuBlockBlock[2])):?>
                                                    <? if ($subMenuBlockBlock[2] == 'form'):?>
                                                        <?=Bootstrap3Form::open(['url' => $subMenuBlockBlock[3], 'method' => 'GET']);?>
                                                        <? foreach ($subMenuBlockBlock[1] as $subMenuBlockEl):?>
                                                            <?=Bootstrap3Form::selectRow($subMenuBlockEl[1], $subMenuBlockEl[0], ['' => '-'] + $subMenuBlockEl[2]   )?>
                                                        <? endforeach;?>
                                                        <button class="btn btn-success" type="submit" value="1">Показать варианты</button>
                                                        <?=Bootstrap3Form::close();?>
                                                    <? elseif ($subMenuBlockBlock[2] == 'banner'):?>
                                                        <? if ($banner = Banner::getByPosition('catalogmenu-kontaktnye-linzy')->first()):?>
                                                            <div style="text-align: center">

                                                                <a href="<?=$banner->getUrl()?>">
                                                                    <?=$banner->img(null, ['style' => 'max-width:100%'])?>
                                                                </a>
                                                            </div>
                                                        <? endif;?>
                                                    <? endif;?>
                                                <? else:?>
                                                    <? if (! empty($subMenuBlockBlock[1])):?>
                                                        <? foreach ($subMenuBlockBlock[1] as $subMenuBlockEl => $subMenuBlockElVal):?>
                                                            <? if (is_numeric($subMenuBlockEl)):?>
                                                                <a class="sub-menu-link"><?=$subMenuBlockElVal?></a>
                                                            <? else:?>
                                                                <?=Html::link($subMenuBlockElVal, $subMenuBlockEl, ['class' => 'sub-menu-link'])?>
                                                            <? endif;?>
                                                        <? endforeach;?>
                                                    <? endif;?>
                                                <? endif;?>
                                            <? endforeach;?>
                                        </div>
                                    <? endforeach;?>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        <? endif;?>
                    </div><? endforeach;?></div><form id="search-form">
                    <div class="search-form-inner">
                        <i class="fa fa-search search-icon"></i>
                        <a id="searchCancelButton"><i class="fa fa-close"></i></a>
                        <input id="searchField" autocomplete="off"
                               title="Поиск в каталоге по артикулу, названию, типу или категории товара"
                               placeholder="Поиск в каталоге" type="text"
                               name="search"
                        >
                        <div class="search-results"></div>
                    </div>
                </form><a href="/favorite" class="menu-favorite">
                    <span class="menu-favorite-icon"
                          data-active-text='<i class="icon-heart"></i>'
                          data-inactive-text='<i class="icon-heart-o"></i>'
                        >
                        <i class="icon-heart<? if (\ActiveRecord\Favorite::current()->count() == 0):?>-o<? endif;?>"></i>
                    </span>
                    <span style="position:absolute;left:40px;top:50%;margin-top:-11px;" class="favorite-count" <? if (\ActiveRecord\Favorite::current()->count() == 0):?>style="display: none"<? endif;?>>
                        <?=\ActiveRecord\Favorite::current()->count()?>
                    </span>
                </a><a href="/basket" class="basket-block" >
                    <img src="/dizayn/basket-active.png"/>
                    <span class="basket-count" <? if ($basket->getTotalCount() == 0):?>style="display: none"<? endif;?>>
                        <?=$basket->getTotalCount()?>
                    </span>
                    <span class="hidden-md">
                        <span style="padding-left:8px;">Корзина</span>
                        <i class="fa fa-chevron-down"></i>
                    </span>
                </a>
            </div>
            <script>
                function search_request_ajax(keyword, callback)
                {
                    $.ajax({
                        type: 'GET',
                        dataType: 'html',
                        data: {
                            q: keyword
                        },
                        url: '<?=actionts('SearchController@suggest')?>',
                        success: function(result){
                            callback(result);
                        }
                    })
                }

                function enableSuggest($form) {
                    var $input = $form.find('input[type=text]');
                    var searchResult = $form.find('.search-results');
                    var doNotBlur = false;

                    $form.on('submit', function(){
                        var curli = $form.find('.search-results tr.active');

                        if (curli.length > 0) {
                            document.location.href = curli.data('href');
                        }

                        return false;
                    });

                    $input.on('focus', function(){
                        $form.addClass('opened');
                    });

    //                $input.on('blur', function(event){
    //                    if (doNotBlur) {
    //                        event.preventDefault();
    //                        return false;
    //                    }
    //
    //                    $form.removeClass('opened');
    //                    $form.find('#searchCancelButton').click();
    //                    console.log(3);
    //                });

                    $('#search-form').mousedown(function () {
                        doNotBlur = true;
                    });

                    $('#search-form').click(function () {
                        doNotBlur = false;
                    });

                    $form.find('.search-results').on('click', function(event){
        //                var x = window.scrollX, y = window.scrollY;
        //                window.scrollTo(x, y);

                        $input.focus();
                    })

                    $form.find('.search-icon').on('click', function(event){
                        $form.addClass('opened');
                        $input.focus();
                    })

                    $form.on('keypress, keydown', function(e){
                        if (e.keyCode == 40 || e.keyCode == 38)
                        {
                            e.preventDefault();
                        }
                    });
                    $form.on('keyup', function(e){
                        var keyword = "";

        //                                var $form = $(this);
                        var curli = $form.find('.search-results tr.active');
                        if (e.keyCode == 40 || e.keyCode == 38)
                        {
                            if (curli.length == 0)
                                return;

                            if (e.keyCode == 40)
                            {
                                var nextli = curli.next('[data-id]');
                                if (nextli.length == 0)
                                {
                                    var next_block = curli.closest('.search-block').next('.search-block');
                                    if (next_block.length == 0)
                                        nextli = searchResult.find('[data-id]:first');
                                    else
                                        nextli = next_block.find('[data-id]:first');
                                }

                            }
                            if (e.keyCode == 38)
                            {
                                var nextli = curli.prev('[data-id]');
                                if (nextli.length == 0)
                                {
                                    var prev_block = curli.closest('.search-block').prev('.search-block');
                                    if (prev_block.length == 0)
                                        nextli = searchResult.find('[data-id]:last');
                                    else
                                        nextli = prev_block.find('[data-id]:last');
                                }
                            }

                            curli.removeClass('active');
                            nextli.addClass('active');

                            if (nextli.offset().top  < $(document).scrollTop()) {
                                console.log('out-of-range top');
        //                                        $(document).scrollTop();
                                $('body').stop().animate({scrollTop:nextli.offset().top}, 150, 'swing');
                            }

                            if ((nextli.offset().top + nextli.height()) > ($(document).scrollTop() + $(window).height())){
                                console.log('out-of-range bottom');
                                $('body').stop().animate({scrollTop:(nextli.offset().top + nextli.height() - $(window).height())}, 150, 'swing');
        //                                        $(document).scrollTop((nextli.offset().top + nextli.height() - $(window).height()));
                            }
                            e.preventDefault();
                        }
                        else if (e.keyCode == 13 && curli.length > 0) {
                            if (curli.length == 0)
                                return false;

        //                                    console.log(curli.find('[data-href]').data('href'));
        //                                    document.location.href = curli.find('[data-href]').data('href');
                            return false;
                        }
                        else if (e.keyCode == 27) {
                            if ($input.val().length == 0) {
                                $input.blur();
                                $form.removeClass('opened');
                            }
                            else {
                                $input.val('');
                                searchResult.html('');
                                $form.removeClass('filled');
                            }
                        }
                        else
                        {
                            var keyword = $input.val();
                            if (keyword.length >  1) {
                                search_request_ajax(keyword, function(result){
                                    searchResult.html(result);

                                    searchResult.find('tr:first').addClass('active');
                                });
                            }
                        }

                        if ($input.val().length > 0) {
                            $form.addClass('filled');
                        }
                        else {
                            $form.removeClass('filled');
                        }

                        $form.delegate('[data-href]', 'click', function(){
                            document.location.href = $(this).data('href');
                        })
                    });

                    $form.find('#searchCancelButton').click(function(){
                        if ($input.val().length == 0) {
                            $input.blur();
                            $form.removeClass('opened');
                        }
                        else {
                            $input.val('');
                            searchResult.html('');
                            $form.removeClass('filled');
                        }
                    });

        //            $form.find('#searchField').on('blur', function(){
        //                $form.find('#searchCancelButton').click();
        //            });
                }

                enableSuggest($('#search-form'));
            </script>
        </div>
    </div>
</div>