<!DOCTYPE html>
<?
/** @var Basket $basket */
/** @var User $user */
?>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="csrf-token" content="<?=csrf_token()?>" />
    <? if (! empty($noindex)):?>
    <meta name="robots" content="noindex, nofollow" />
    <? endif;?>
    <title><?=$title?></title>

    <link rel="shortcut icon" type="image/x-icon" href="/favicon.png"/>
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic" rel="stylesheet"/>

    <?=Html::script('/js/vendor/jquery-1.11.3.min.js')?>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function logout()
        {
            $.ajax({
                url: '/logout',
                method: 'POST',
                data: {
                    "_token": "<?=csrf_token()?>"
                }
            }).success(function(){
                window.location.reload();
            })
        }

        $().ready(function(){
            $("[data-toggle=popover]").popover();
        })
    </script>

    <?=Html::script('/bootstrap/js/bootstrap.min.js')?>
    <?=Html::style('/bootstrap/css/bootstrap.min.css')?>
    <?=Html::style('/bootstrap/css/bootstrap-xxs.css')?>

    <?=Html::script('/js/underscore.js')?>
    <?=Html::script('/js/vendor/maskedinput.js')?>
    <?=Html::script('/js/script.js')?>

    <?=Html::style('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css')?>

    <?=Html::style('/fresco/fresco.css')?>
    <?=Html::script('/fresco/fresco.js')?>

    <?=Html::script('/js/vendor/owlcarousel/owl.carousel.js')?>
    <?=Html::style('/js/vendor/owlcarousel/assets/owl.carousel.css')?>
    <?=Html::style('/js/vendor/owlcarousel/assets/owl.theme.default.css')?>

    <?=Html::style('/css/dizayn/main.css')?>

    <? if (config('app.debug')):?>
        <script src="http://knopkazakaza/api.js?uid=2"></script>
    <? else:?>
        <script src="http://knopkazakaza.ru/api.js?uid=88"></script>
    <? endif;?>
</head>
<body style="background: url('/dizayn/logotipy-fon.jpg')">
<header>
    <div class="visible-lg" style="position: absolute;right:0px;top:0px;display: inline-block">
        <div id="google_translate_element"></div>
        <script type="text/javascript">
            function googleTranslateElementInit() {
                new google.translate.TranslateElement({pageLangugage: 'ru', includedLanguages: 'en,ja,zh-CN', layout: google.translate.TranslateElement.FloatPosition.TOP_RIGHT}, 'google_translate_element');
            }
        </script>
        <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
    </div>

    <div id="flash" style="position: fixed;top:4px;right:4px;font-size:1.5em;z-index: 9;"></div>
    <script>
        function flash(text, level) {
            var cls = level;
            var icon;
            if (level == 'success') {
                icon = 'check';
            }
            else if (level == 'error') {
                icon = 'remove';
                cls = 'danger';
            }
            else if (level == 'warning') {
                cls = 'warning';
                icon = 'warning-sign';
            }
            else if (level == 'info') {
                icon = 'info-sign';
            }
            var flash = $('<div class="alert alert-dismissable alert-' + cls + '"><i class="fa fa-' + icon + '"></i> ' + text + '</div>');
            $('#flash').append(flash);
            flash.delay(5000).fadeOut('slow', function(){flash.detach().remove()});
        }
    </script>
    <? if (Flash::has()):?>
        <? $flash = Flash::get();?>
        <script>
            flash('<?=$flash['text']?>', '<?=$flash['level']?>');
        </script>
    <? endif;?>

    <? if (Settings::get('bonus_program_active')):?>
        <? if (! Auth::user() || ! Auth::user()->bonus_program_active):?>
            <div style="height:32px;background-color: lightgreen;line-height:32px;display:block;" class="topad">
                <div class="container">
                    Учавствуйте в бонусной программе и получите 1000 бонусов на ваш первый заказ! #Акция
                    &nbsp;&nbsp;<a onclick="showModalSimple('register-bonus-program')" class="btn btn-success btn-xs">Получить 1000 бонусов</a>

                    <a onclick="setcookie('hide_bonus_ad'); $('.topad').hide();" class="pull-right">
                        <i class="fa fa-close"></i>
                    </a>
                </div>
            </div>
            <script>
                if (! getcookie('hide_bonus_ad')) {
                    setTimeout(function(){console.log($('.topad').slideDown().length);}, 0)
                }
            </script>
            <div style="display: none;" id="register-bonus-program-template">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Закрыть"><span aria-hidden="true">×</span></button>
                    Регистрация в бонусной программе
                </div>
                <div class="modal-body">
                    <?=Bootstrap3Form::open(['class' => 'form-horizontal', 'method' => "POST", 'url' => actionts('Auth\RegisterController@register')])?>
                    <?=csrf_field()?>

                    <? $options = ['inputDivOptions' => ['class' => 'col-md-6'], 'labelOptions' => ['class' => 'col-md-4']];?>

                    <?=Bootstrap3Form::hidden('simple', 1)?>
                    <?=Bootstrap3Form::textRow('phone', 'Телефон', null, $options)?>
                    <script type="text/javascript">
                        $('[name=phone]').mask("8(999)999-99-99");
                    </script>
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <?=Bootstrap3Form::checkbox('agreed', 'Прочитал и согласен с <a target="_blank" class="link" href="' . Page::findBySlug('bonusnaya-programma')->getUrl() . '">условиями участия в бонусной программе</a>', 1, $options)?>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Зарегистрироваться
                            </button>
                        </div>
                    </div>
                    <?=Bootstrap3Form::close()?>
                </div>
            </div>
        <? endif;?>
    <? endif;?>

    <style>
        .top-menu {
            color: black;
            box-shadow: 11px -1px 8px #989898;
            height:40px;
            background: white;
        }

        .full-width {
            width: 100vw;
            position: relative;
            left: 50%;
            right: 50%;
            margin-left: -50vw;
            margin-right: -50vw;
        }


        .header-basket{
            color: black;
            background-color: #f9f9f9;
            padding: 2px 12px;
            display: block;
            text-align: center;
            border-bottom: 2px solid red;
        }
        .header-basket span{font-size:15px;FONT-WEIGHT:BOLD;

            color:#ff5656;
            font-weight: bold;
        }
        .top-phone {
            text-align: left;
            font-family: Helvetica;
            color: #a9cce4;
            line-height:1;
        }
        .top-phone a {
            font-family: Helvetica;
            font-size: 20px;
            color: #c8e9ff;
            padding-bottom:6px;
            display: block;
        }
        .top-phone a:hover {
            color: white;
        }

        .top-callback{
            display: inline-block;
            font-family: Helvetica;
            font-size: 14px;
            text-align: left;
            color: #c8e9ff;
            border-radius: 100px;
            border: solid 4px #76aed4;padding:4px 15px;line-height:1;
        }
        .top-callback:hover{
            color:white;
            border-color:white;
        }

        .top-four-free{
            font-size: 16px;
            text-align: left;
            color: #c8e9ff;
            line-height:1.1;
        }

        .top-four-free-block:hover span{
            color:white;
            text-decoration: underline;
        }

        .top-line-links{margin:0px -12px;padding-right:24px;}

        .top-line-links > a, .top-line-links > span > a {padding:0px 12px;
            color:black;
            line-height:40px;
            height:40px;
            display: inline-block;
        }

        .top-line-links > a:hover, .top-line-links > span > a:hover {color:#006ab0;text-decoration: underline;}
        .top-line-links{float:left;}

        .top-line-social-links{
            position: absolute;
            margin-top: 4px;
        }
        .top-line-social-links > :hover{
            opacity:0.65;
        }
    </style>


    <div class="top-menu hidden-sm hidden-xs">
        <div class="container">
            <div class="top-line-links">
                <a href="<?=actionts('SalonController@index')?>">Салоны</a>
                <a href="<?=actionts('ActionController@index')?>">Акции</a>
<!--                <a href="--><?//=actionts('ServiceController@index')?><!--">Услуги</a>-->

                <span class="dropdown">
                    <a class="dropdown-toggle" id="topServiceMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        Услуги
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="topServiceMenu">
                        <? foreach (\ActiveRecord\SalonService::all() as $service):?>
                            <li><a href="<?=$service->getUrl()?>"><?=$service->name?></a></li>
                        <? endforeach;?>
                    </ul>
                </span>

                <span class="dropdown">
                    <a class="dropdown-toggle" id="informaciyaMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        Информация
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="informaciyaMenu">
                        <li><a href="<?=Page::url(7)?>">Доставка</a></li>
                        <li><a href="<?=Page::url(17)?>">Способы оплаты</a></li>
                        <li><a href="<?=Page::url(8)?>">Гарантия</a></li>
                        <li><a href="<?=Page::url(18)?>">Обмен и возврат</a></li>
                        <? if (BONUS_PROGRAM_ACTIVE):?>
                            <li><a href="<?=Page::url(9)?>">Бонусная программа</a></li>
                        <? endif;?>
                    </ul>
                </span>
                <span class="dropdown">
                <a class="dropdown-toggle" id="onasMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    О компании
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" aria-labelledby="onasMenu">
                    <li><a href="<?=Page::url(1)?>">О компании</a></li>
                    <li><a href="<?=Page::url(2)?>">СМИ о Нас</a></li>
                    <li><a href="<?=Page::url(3)?>">Мы в соцсетях</a></li>
                    <li><a href="<?=Page::url(4)?>">Вакансии</a></li>
                    <li><a href="<?=Page::url(5)?>">Контакты</a></li>
                </ul>
            </span>
                <!--            <a href="--><?//=Page::url(11)?><!--">Проверка зрения онлайн</a>-->
                <a href="<?=Page::url(5)?>">Контакты</a>
            </div>

            <span class="hidden-md top-line-social-links">
                <a target="_blank" href="http://vk.com/club<?=Settings::get('vk')?>" style="background: url('/dizayn/vk-head.jpg');height:30px;width:30px;background-size:100%;margin-right:4px;display: inline-block"></a>
                <a target="_blank" href="https://twitter.com/<?=Settings::get('twitter')?>" style="background: url('/dizayn/twitter-head.jpg');height:30px;width:30px;background-size:100%;margin-right:4px;display: inline-block"></a>
                <a target="_blank" href="https://www.facebook.com/<?=Settings::get('facebook')?>/" style="background: url('/dizayn/facebook-head.jpg');height:30px;width:30px;background-size:100%;margin-right:4px;display: inline-block"></a>
                <a target="_blank" href="https://www.instagram.com/<?=Settings::get('instagram')?>/" style="background: url('/dizayn/instagram-head.jpg');height:30px;width:30px;display: inline-block;background-size:cover;b">
                </a>
            </span>

<!--            <div class="col-sm-4 hidden-xs" style="text-align: right;">-->
<!--                --><?// $lastOrder = $basket->getContentsCount() == 0 ? \App\User::lastOrder() : null;?>
<!--                <a href="--><?//=$lastOrder ? actionts('OrderController@view', ['id' => $lastOrder->id]) : actionts('BasketController@index')?><!--" class="header-basket" style="width:100%;position:relative;">-->
<!--                    <div style="position:absolute;font-size:25px;left:8px">-->
<!--                        --><?// if ($lastOrder):?>
<!--                            <i class="fa fa-truck"></i>-->
<!--                        --><?// else:?>
<!--                            <i class="fa fa-shopping-basket"></i>-->
<!--                        --><?// endif;?>
<!--                    </div>-->
<!--                    <div style="float:left;text-align: left;padding-left:6px;margin-left:25px;">-->
<!--                        --><?// if (! $lastOrder):?>
<!--                            <span>Ваша корзина</span>-->
<!--                            <div style="line-height:1em;font-size:14px;">-->
<!--                                <span class="total-count">--><?//=plural($basket->getTotalCount(), 'товар', 'товара', 'товаров', true)?><!--</span>-->
<!--                                на <span class="total-price">--><?//=moneyFormat($basket->getTotalPrice())?><!--</span> рублей-->
<!--                            </div>-->
<!--                        --><?// else:?>
<!--                            <span>Ваш заказ #--><?//=$lastOrder->getNumber()?><!--</span>-->
<!--                            <div style="line-height:1em;font-size:14px;">-->
<!--                                статус: --><?//=$lastOrder->status->getName()?><!--, на сумму --><?//=moneyFormat($lastOrder->sum)?><!-- руб.-->
<!--                            </div>-->
<!--                        --><?// endif;?>
<!--                    </div>-->
<!--                    <div class="clearfix"></div>-->
<!--                </a>-->
<!--                <a href="--><?//=actionts('FavoriteController@index')?><!--" class="header-favorites"  style="display:block;width:50%;float:left;text-align: center;background-color: #ff4747;color:white;padding:4px 0px;">-->
<!--                    Избранное <span class="badge">--><?//=\ActiveRecord\Favorite::current()->count()?><!--</span>-->
<!--                </a>-->
<!--                <a href="--><?//=actionts('BasketController@index')?><!--" style="width:50%;float:left;text-align: center;background-color: #15bf90;color:white;padding:4px 0px;">-->
<!--                    Примерка <span class="badge">--><?//=count($basket->getFittings())?><!--</span>-->
<!--                </a>-->
<!--            </div>-->
            <script>
                Basket.on('add, remove, update', function(result){
                    $('.header-basket').find('.total-price').text(moneyFormat(result.basket.sum));
                    $('.header-basket').find('.total-count').html(plural(result.basket.count, 'товар', 'товара', 'товаров', true));
                });
                Favorites.on('add, remove, update', function(result){
                    $('.header-favorite .favorite-count').text(result.count);
                    if (result.count) {
                        $('.header-favorite a').button('active');
                    }
                    else {
                        $('.header-favorite a').button('inactive');
                    }
                });
            </script>

            <div class="pull-right">
                <? if (Route::has('login')):?>
                    <div class="top-right links">
                        <style>
                            .header-block{background-color: #006ab0;height:153px;}
                                .header-block .header-contacts-row{padding-top:45px;}
                                .header-personal{
                                    display: inline-block;
                                    padding:0px 8px;
                                    padding-left:45px;
                                }
                                .header-personal .header-login-icon{
                                    background-image: url('/dizayn/login-noactive.png');
                                    content: '';
                                    width:34px;
                                    height:34px;
                                    left:6px;
                                    float:left;
                                    margin-right:3px;
                                    position:absolute;
                                    margin-top:3px;
                                }
                                .header-personal:hover header-login-icon {
                                    background-image: url('/dizayn/login-active.png');
                                }

                                .header-personal .header-basket-icon{
                                    background-image: url('/dizayn/basket-noactive.png');
                                    content: '';
                                    width:34px;
                                    height:34px;
                                    left:6px;
                                    float:left;
                                    margin-right:3px;
                                    position:absolute;
                                    margin-top:3px;
                                }

                                .header-personal:hover header-basket-icon {
                                    background-image: url('/dizayn/basket-active.png');
                                }

                                .header-personal:hover{
                                    background-color: #006ab0;;
                                }
                                .header-personal:hover > a{
                                    color:white;
                                }
                                .header-personal > a {
                                    display: inline-block;
                                    height:40px;
                                    line-height:40px;
                                    color:black;
                                }

                                .header-personal .basket-count,
                                .header-favorite .favorite-count{
                                    opacity: 0.7;
                                    font-size: 16px;
                                    font-weight: bold;
                                    text-align: left;
                                    color: #006bb2;
                                }
                                .header-personal:hover .basket-count {
                                    color:white;
                                }

                                .header-favorite i{margin-top:4px}
                                .header-favorite {height:40px;display: inline-block;vertical-align: top;padding-top:3px;}
                                .header-favorite a{border-radius:66px;border:3px solid #c2c4c5;width:34px;height:34px;display: inline-block;text-align: center;}
                        </style>
                        <div class="dropdown header-personal">
                            <a class="dropdown-toggle" id="profileMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                <div class="header-login-icon"></div>
                                Личный кабинет
                                <i class="fa fa-chevron-down"></i>
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="profileMenu">
                                <? if (Auth::check()):?>
    <!--                                    <a class="dropdown-toggle" id="profileMenu" style="font-weight: bold;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">-->
    <!--                                        <i class="fa fa-user"></i>-->
    <!--                                        --><?//=$user->name?>
    <!--                                        <span class="caret"></span>-->
    <!--                                    </a>-->

                                    <li><a href="<?=url('/user/')?>">Личный кабинет</a></li>
                                    <? if ($user && $user->isAdmin()):?>
                                        <li><a href="<?=url('/admin/')?>">Панель управления</a></li>
                                    <? endif;?>
                                    <li><a href="<?=url('/user/orders/')?>">Заказы</a></li>
                                    <li><a href="<?=actionts('UserController@getProfile')?>">Настройки профиля</a></li>
                                      <? if (BONUS_PROGRAM_ACTIVE):?>
                                    <li><a href="<?=url('/user/bonus')?>">Бонусы <span class="label label-success"><?=$user->getBonuses()?></span></a></li>
                                      <? endif;?>
                                    <li role="separator" class="divider"></li>
                                    <li>
                                        <a href="javascript:logout()">
                                            <i class="fa fa-sign-out"></i> Выход
                                        </a>
                                    </li>
                                <? else:?>
                                    <li><a href="<?=url('/login/')?>">Вход</a></li>
                                    <li><a href="<?=url('/register/')?>">Регистрация</a></li>
                                    <? if (BONUS_PROGRAM_ACTIVE):?>
                                        <li><a style="color:#f7114f;font-weight:bold;" href="javascript:showModalSimple('register-bonus-program')">Оформить бонусную карту</a></li>
                                    <? endif;?>
                                <? endif;?>
                             </ul>
                        </div>
                        <div class="header-favorite">
                            <a href="/favorite"
                                data-active-text='<i class="icon-heart"></i>'
                                data-inactive-text='<i class="icon-heart-o"></i>'
                            >
                                <i class="icon-heart<? if (\ActiveRecord\Favorite::current()->count() == 0):?>-o<? endif;?>"></i>
                            </a>
                            <span class="favorite-count" <? if (\ActiveRecord\Favorite::current()->count() == 0):?>style="display: none"<? endif;?>>
                                <?=\ActiveRecord\Favorite::current()->count()?>
                            </span>
                        </div>
                        <div class="dropdown header-personal">
                            <a href="<?=actionts('BasketController@index')?>">
                                <div class="header-basket-icon"></div>
                                <span class="basket-count" <? if ($basket->getTotalCount() == 0):?>style="display: none"<? endif;?>>
                                    <?=$basket->getTotalCount()?>
                                </span>
                                Корзина
                                <i class="fa fa-chevron-down"></i>
                            </a>
                        </div>
                    </div>
                <? endif;?>
            </div>
        </div>
    </div>
    <div class="header-block">
        <div class="container" style="position:relative;">
            <?=view('front.layouts.parts.mobile-menu')?>
            <div class="row header-contacts-row">
                <div class="col-md-3 b-logo">
                    <a href="/">
                        <img src="/dizayn/123456.png" id="logo"/><br/>
                    </a>
    <!--                <div style="color:#d43127;font-weight:bold;line-height:1;">--><?//=plural(Salon::count(), 'салон', 'салона', 'салонов', true)?><!-- оптики в Москве и области</div>-->

                </div>
                <div class="col-sm-4 col-md-6 hidden-sm hidden-xs">
                    <div class="row">
                        <div class="col-lg-9 nopadding">
                            <div class="col-xs-6">
                                <div class="top-phone">
                                    <a href="tel:<?=Settings::get('phone')?>"><?=Settings::get('phone')?></a>
                                    <div>Интернет-магазин, пн-вс 9-18</div>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="top-phone">
                                    <a href="tel:<?=Settings::get('phone2')?>"><?=Settings::get('phone2')?></a>
                                    <div>Офис, пн-пт 9-18</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 nopadding" style="text-align:center;">
                            <a onclick="openCallbackWindow(event.currentTarget)" class="top-callback">
                                заказать звонок
                            </a>
                        </div>
                    </div>
                </div>
                <a onclick="showModalSimple('fitting-modal')" class="col-md-3 hidden-sm hidden-xs top-four-free-block" style="padding-right:97px;position:relative;display: block">
                    <span class="top-four-free" style="float:right;">
                        Бесплатная примерка<br/>
                        4-х пар очков!
                    </span>
                    <div style="position:absolute;right:7px;margin-top:-26px;">
                        <img src="/dizayn/primerka.png"/>
                    </div>
                </a>
            </div>
        </div>
    </div>
</header>
<div class="container">
    <div class="body">
<?=view('front.layouts.parts.menu')?>