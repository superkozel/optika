<div class="container">
    <h1><?=$h1?></h1>

    <? if (! empty($archive)):?>
        <h3 style="color:red;">Внимание! Данные акции закончились</h3>
        <h3 style="margin-bottom:60px;text-decoration: underline"><a href="<?=actionts('ActionController@index')?>">Перейти к текущим акциям</a></h3>
    <? endif;?>
    <? foreach ($actions->chunk(2) as $chunk):?>
        <div class="row" <?if (! empty($archive)):?>style="opacity:0.6"<?endif;?>>
            <? foreach ($chunk as $action):?>
            <?php
            /** @var Action $action */
            ?>
                <div class="col-md-6">
                    <div class="row" style="margin-bottom:24px;">
                        <a href="<?=actionts('ActionController@show', ['slug' => $action->slug])?>">
                            <div class="col-sm-6">
                                <?=ActionImage::img($action->image, 'small', null, ['style' => 'width:100%'])?>
                            </div>
                            <div class="col-sm-6">
                                <h2 class="h4" style="margin-top:0px;font-weight: bold;">
                                    <?=$action->name?>
                                    <? if ($user && $user->isAdmin()):?>
                                        <a href="<?=action('AdminActionController@edit', ['id' => $action->id])?>"><i class="fa fa-pencil"></i></a>
                                    <?endif;?>
                                </h2>

                                <? if (! $action->isActive()):?>
                                    <div style="font-size:1em;color:#ff2d5c;font-weight:bold;">
                                        <? ;?>
                                        Акция закончилась <?=formatDate($action->active_to->getTimestamp())?>
                                    </div>
                                <? else:?>
                                    <div style="font-size:1em;color:#ff2d5c;font-weight:bold;">
                                        С <?=formatDate($action->active_from->getTimestamp())?> по <?=formatDate($action->active_to->getTimestamp())?>
                                    </div>
                                <? endif;?>

                                <?=$action->preview_text?>
                            </div>
                        </a>
                    </div>
                </div>
            <? endforeach;?>
        </div>
    <? endforeach;?>

    <?=View::make('front.parts.pagination', ['paginator' => $actions])?>

<!--    <a class="link" href="--><?//=actionts('ActionController@archive')?><!--">Архив акций</a>-->
</div>