<div class="container">
    <h1><?=$h1?>
        <? if ($user && $user->isAdmin()):?><a href="<?=action('AdminActionController@edit', ['id' => $action->id])?>"><i class="fa fa-pencil"></i></a><?endif;?>
    </h1>

    <? if (! $action->isActive()):?>
        <h3 style="color:red;">Внимание! Данная акция закончились <?=$action->active_to->format('d.m.Y')?></h3>
        <h3 style="margin-bottom:60px;text-decoration: underline"><a href="<?=actionts('ActionController@index')?>">Перейти к текущим акциям</a></h3>
    <? else:?>
        <h3 style="color:red;">С <?=formatDate($action->active_from->getTimestamp())?> по <?=formatDate($action->active_to->getTimestamp())?></h3>
    <? endif;?>

<!--    --><?// if (! $action->isActive()):?>
<!--        <div style="display: inline-block;" class="alert alert-danger">-->
<!--            <i class="fa fa-warning"></i> Акция закончилась --><?//=$action->active_to->format('d.m.Y')?><!--</i>-->
<!--        </div>-->
<!--        <div class="clearfix"></div>-->
<!--    --><?// endif;?>

    <div <? if (! $action->isActive()):?>style="opacity: 0.6"<?endif;?>>
        <?=$action->content?>
    </div>
</div>