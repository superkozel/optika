<div class="row">
<? foreach ($brands as $brand):?>
    <div class="col-md-2" style="text-align: center;display: inline-block;">
        <a href="<?=\App\Http\Controllers\CatalogController::createUrlSimple(null, ['brand' => $brand->slug])?>">
            <?=BrandImage::img($brand->image, 'small', $brand->name)?>
            <br/>
            <h4><?=$brand->name?></h4>
            <span style="color:gray"><?=plural($brand->products_count, 'товар', 'товара', 'товаров', true)?></span>
        </a>
    </div>
<? endforeach;?>
</div>
