<?
/** @var SalonService[] $services */
?>
<div class="container">
    <ul class="checkmarks">
        <? foreach ($services as $service):?>
            <li>
                <a class="link" href="<?=actionts('ServiceController@view', ['id' => $service->slug])?>"><?=$service->getName()?></a> -
                <a class="link" href="<?=actionts('SalonController@index', ['id' => null, 'service' => $service->getId()])?>">
                    <?=plural($service->salons()->count(), 'салон', 'салона', 'салонов', true)?>
                </a></li>
        <? endforeach;?>
    </ul>
</div>