<?=$service->description?>
<? if ($service->file_path):?>
    <?=view('front.pages.' . $service->file_path);?>
<? endif;?>

<? if ($service->salons()->count() > 0):?>
    <h3>
        Салоны, в которых предоставляется услуга:
    </h3>

    <table class="table table-condensed">
        <? foreach ($service->salons()->get()->groupBy('city_id') as $group => $salons):?>
            <tr>
                <th colspan="3"><h4><?=City::find($group)->name?></h4></th>
            </tr>
            <? foreach ($salons as $salon):?>
                <tr>
                    <td><a href="<?=$salon->getUrl()?>"><?=$salon->name?></a></td>
                    <td><?=$salon->address?></td>
                    <td><a href="tel:<?=$salon->phone?>"><?=$salon->phone?></a></td>
                </tr>
            <? endforeach;?>
        <? endforeach;?>
    </table>
<? endif;?>
