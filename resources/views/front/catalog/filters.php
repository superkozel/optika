<? if ($ajax):?>
    <?
    $result = [];
    foreach ($filters as $k => $filter) {
        $result[$filter->id] = [];

        if ($filter->type_id == CatalogFilterType::CLIST) {
            $options = $filter->getOptionsWithCount($category, $filterValues);
            foreach ($options as $k => $v) {
                $result[$filter->id][$k] = [
                    'active' => ! empty($filterValues[$filter->id]),
                    'name' => $v['name'],
                    'count' => $v['count']
                ];
            }
        }
    }
    echo json_encode($result);
    ?>
<? else :?>
    <? foreach ($filters as $k => $filter):?>
        <div class="filter-block <? if ($k < 5):?>opened <?endif;?><? if (! empty($filterValues[$filter->id])):?>active<?endif;?>" style="position:relative;"
             data-filter-id="<?=$filter->id?>">
            <div class="filter-label" onclick="$(event.currentTarget).parents('.filter-block').toggleClass('opened')"><?=$filter->name?>
                <span class="filter-selected">
                    <? if ($filter->type->is(CatalogFilterType::CLIST, CatalogFilterType::COLOR)):?>
                    Выбрано: <span class="filter-selected-count">
                        <? if (! empty($filterValues[$filter->id])):?><?=count($filterValues[$filter->id])?><? else:?>0<? endif;?>
                    </span>
                    <? endif;?>
                    <a class="filter-selected-close" onclick="removeFilter(event)">&times;</a>
                </span>

                <? if ($filter->description):?>
                    <i class="fa fa-question-circle-o" data-toggle="popover" data-content="<?=$filter->description?>" data-dismissable="true" onclick="event.stopPropagation()"></i>
                <? endif;?>
            </div>

            <div class="filter-body">
                <? if ($filter->type_id == CatalogFilterType::CLIST):?>
                    <? $options = $filter->getOptionsWithCount($category, $filterValues);?>
                    <div class="filter-inner">
                        <? foreach ($options as $k => $v):?>
                            <div class="checkbox" data-option-id="<?=$k?>">
                                <label for="<?=$filter->slug?>_<?=$k?>">
                                    <input <? if (! empty($filterValues[$filter->id]) && in_array($k, $filterValues[$filter->id])):?>checked="checked"<?endif?> onchange="applyFilter(event.currentTarget)" name="<?=$filter->slug?>[]" id="<?=$filter->slug?>_<?=$k?>" type="checkbox" value="<?=$k?>"/> <?=$v['name']?>
                                    <span class="filter-option-count"><?=$v['count']?></span>
                                </label>
                            </div>
                        <? endforeach;?>
                    </div>
                    <? if (count($options) > 12):?>
                        <a style="margin-top:8px;" class="filter-expand btn btn-default btn-sm" onclick="$(event.currentTarget).parents('.filter-block').addClass('expanded')">
                            Еще
                        </a>
                    <? endif;?>
                <? endif;?>

                <? if ($filter->type_id == CatalogFilterType::DROPDOWN):?>
                    <? $options = $filter->getOptions();?>
                    <div class="filter-inner">
                        <?=Bootstrap3Form::select($filter->slug, ['' => ''] + $options, array_get($filterValues, $filter->id . '.0'), ['onchange' => 'applyFilter(event.currentTarget)'])?>
                    </div>
                <? endif;?>

                <? if ($filter->type_id == CatalogFilterType::RANGE):?>
                    <?
                    if (! empty($filterValues[$filter->id])) {
                        list($from, $to) = explode('-', $filterValues[$filter->id]);
                    }
                    else {
                        $from = null;
                        $to = null;
                    }
                    ?>
                    <div class="row">
                        <div class="col-xs-6" style="position:relative;">
                            <div style="position: absolute;left:22px;z-index:6;top:3px;">от</div>
                            <input <? if ($from):?>value="<?=$from?>"<?endif;?> name="<?=$filter->id?>[from]" onkeyup="applyFilter(event.currentTarget)" style="padding-left:35px;padding-right:3px;width:100%;" class="col-sm-12" type="number" placeholder="0"/>
                        </div>
                        <div class="col-xs-6" style="position:relative;">
                            <div style="position: absolute;left:22px;z-index:6;top:3px;">до</div>
                            <input <? if ($to):?>value="<?=$to?>"<?endif;?> name="<?=$filter->id?>[to]" onkeyup="applyFilter(event.currentTarget)" style="padding-left:35px;padding-right:3px;width:100%;" class="col-sm-12" type="number" placeholder="0"/>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                <? endif;?>
            </div>
        </div>
    <? endforeach;?>
<? endif;?>
