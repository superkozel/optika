<?
/** @var CatalogFilter[] $filters */
/** @var Product[] $products */
/** @var CatalogCategory $category */
?>
<div class="container-fluid">
    <h1 class="catalog-h1"><?=$h1?> <? if ($user && $user->isAdmin()):?><a style="right:0px;top:0px;" href="<?=$category->getEditUrl()?>"><i class="fa fa-pencil"></i></a><?endif;?></h1>
    <span class="total-item-count"><?=$products->total()?> <?=plural($products->total(), 'позиция', 'позиции', 'позиций')?></span>
</div>
<div class="container-fluid">
    <? if ($category->children->count() > 0):?>
        <div class="row" style="margin-left:-3px;margin-right:-3px;">
            <? foreach ($category->children as $subcategory):?>
                <div class="col-xs-6 col-sm-4 col-md-3 catalog-subcategory-block">
                    <a href="<?=$subcategory->getUrl()?>">
                        <?=CategoryImage::img($subcategory->image, 'mini', null, ['style' => 'position:absolute;left:8px;top:8px'])?>
                        <?=$subcategory->name?>
                        <span style="color:darkgrey;font-weight:bold;text-align: right;"><?=$subcategory->getProductCount()?></span>
                    </a>
                </div>
            <? endforeach;?>
        </div>
    <? endif;?>

    <? if (count($activeFilters) == 0):?>
        <p>
            <?=$category->description?>
        </p>
    <? endif;?>

    <style>
        .sorter {margin-bottom:24px;background-color: #eef8ff;padding:8px;display: inline-block;padding-left:36px;padding-right:36px;}
        .catalog-h1{display: inline-block;position:relative}
        .total-item-count{
            padding: 6px 12px;
            background-color: #ff4747;
            color: white;
            font-weight: bold;
            font-size: 0.9em;
            border-radius: 66px;
            vertical-align: super;
        }
        @media (max-width:767px) {
            .catalog-h1{font-size:21px;}
            .sorter{width:100%;padding:8px;}
            .filters{border:2px solid #cfd0d0}

            .catalog-filters-block.collapsed .filters{display: none}
        }
    </style>
    <div class="row nomargin">
        <div class="catalog-filters-block col-md-3 col-sm-4 nopadding collapsed" style="padding-right:7px;">
            <style>
                .filters-mobile-toggle{padding:6px 36px;text-align:center;border:1px solid lightgrey;display: block;cursor: pointer;font-size:20px;display: inline-block;border-radius:66px;cursor: pointer;}

                .filters-mobile-toggle:before {
                    content:'';
                    width: 12px;
                    height: 6px;
                    position:absolute;
                    background-repeat: no-repeat;
                    left: 24px;
                    margin-top: 13px;
                    opacity:.4;
                    background-image: url('data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTIiIGhlaWdodD0iNiIgdmlld0JveD0iMCAwIDEyIDYiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHRpdGxlPlNoYXBlIDI8L3RpdGxlPjxwYXRoIGQ9Ik0uOCAwTDYgNC42IDExLjIgMGwuOC43TDYgNiAwIC43LjggMHoiIGZpbGw9IiMwMDAiIGZpbGwtcnVsZT0iZXZlbm9kZCIvPjwvc3ZnPg==')
                }

                .top-catalog-menu-inner.collapsed .filters-mobile-toggle:before{
                    -webkit-transform: rotate(180deg);
                    -ms-transform: rotate(180deg);
                    transform: rotate(180deg);
                }

                .drop-all-filters{font-size: 16px;text-align: center;color: #676767;display: block;text-align: right;text-decoration: underline;border-bottom:2px solid #cfd0d0;
                 padding:12px 0px;}
                .drop-all-filters i{width:30px;height:30px;line-height:22px;text-align:center;border:4px solid #676767;margin-right:8px; color: #676767;border-radius:32px;}
                .drop-all-filters:hover{}
                .drop-all-filters:hover, .drop-all-filters:hover i{color:red;border-color:red;}
            </style>
            <a class="btn btn-default filters-mobile-toggle visible-xs" onclick="$('.catalog-filters-block').toggleClass('collapsed')" style="width:100%;line-height:30px;">
                Фильтры
<!--                <span class="selected-filter-count">Выбрано: 3</span>-->
            </a>
            <a class="drop-all-filters" onclick="dropFilters()" <? if (count($activeFilters) == 0):?>style="display: none;"<?endif;?>>
                <i class="fa fa-close"></i>Сбросить все фильтры
            </a>
            <script>
                var currentPage = <?=$page?>;
                var currentSort = <?=$sort?>;
                var totalPages = <?=$products->lastPage()?>;
                var categoryId = <?=$category->id?>;

                var currentExecution = null;

                function getFilterValues() {

                    var values = {};

                    $('.filter-block').removeClass('active').each(function(kk, vv){
                        var $el = $(vv);
                        var filterId = $el.data('filter-id');

                        var selCount = $el.find('input:checked').each(function(k, v){
                            if (! values[filterId])
                                values[filterId] = [];

                            values[filterId].push($(v).attr('value'));
                            $el.addClass('active');
                        }).length;

                        $el.find('.filter-selected-count').text(selCount);

                        $el.find('select').each(function(k, v) {
                            if ($(v).val()) {
                                values[filterId] = [$(v).val()];
                                $el.addClass('active');
                            }
                        });

                        var from = $el.find('input[name="' + filterId + '[from]"]').val();
                        var to = $el.find('input[name="' + filterId + '[to]"]').val();

                        if (from || to) {
                            values[filterId] = from + '-' + to;
                            $el.addClass('active');
                        }
                    });

                    return values;
                }
                function updateParts(parts, callback, append)
                {
                    if (currentExecution)
                        currentExecution.abort();

                    if (! append)
                        loading();

                    var values = getFilterValues();

                    if (_.size(values) == 0) {
                        $('.drop-all-filters').hide();
                    }
                    else {
                        $('.drop-all-filters').show();
                    }

                    currentExecution = $.ajax({
                        type: 'get',
                        url: '/catalog/ajax',
                        cache: false,
                        data: {
                            category_id: categoryId,
                            values: values,
                            page: currentPage,
                            sort: currentSort,
                            parts: parts
                        },
                        dataType: 'json'
                    }).success(function(result){
                        if (parts.indexOf('products') != -1) {
                            if (append){
                                $('.products').append(result.products);
                            }
                            else {
                                $('.products').html(result.products);

                                $('.products').waitForImages(function(){
                                    if ($('.products').is(':offscreen')) {
                                        slideTo('h1');
                                    }
                                });
                            }

                            totalPages = result.totalPages;
                            $('.total-item-count').text(result.total + ' '  + plural(result.total, 'позиция', 'позиции', 'позиций'));

                            toggleShowMore();
                        }

                        if (parts.indexOf('pagination') != -1) {
                            $('.pagination').html(result.pagination);
                        }

                        if (parts.indexOf('options') != -1) {
                            for (i in result.options) {
                                $filter = $('.filter-block[data-filter-id=' + i + ']');

                                $filter.find('.checkbox').each(function(k, v) {
                                    $v = $(v);
                                    var optionId = $v.data('option-id');

                                    var prCount = result.options[i][optionId] ? result.options[i][optionId].count : 0;

                                    if (prCount == 0) {
                                        $v.addClass('disabled').find("input").attr("disabled", true);
                                    }
                                    else {
                                        $v.removeClass('disabled').find("input").removeAttr("disabled");
                                    }

                                    $v.find('.filter-option-count').text(prCount);
                                });
                            }
                        }
                        else if (parts.indexOf('filters') != -1) {
                            $('.filters').html(result.filters);
                        }
                    }).complete(function(){
                        if (! append)
                            loaded();

                        if (callback)
                            callback();
                    });
                }

                var debounceFilter = _.debounce(updateParts, 400);

                function applyFilter(el)
                {
                    currentPage = 1;

                    debounceFilter(['products', 'options', 'pagination']);
                }

                function dropFilters()
                {
                    $(".filter-block").find('input[type=checkbox]').removeAttr('checked');
                    $(".filter-block").find('select, input[type=number], input[type=text]').val('');

                    applyFilter();
                }

                function removeFilter(e)
                {
                    e.stopPropagation();

                    $(e.currentTarget).parents(".filter-block").find('input[type=checkbox]').removeAttr('checked');
                    $(e.currentTarget).parents(".filter-block").find('select, input[type=number], input[type=text]').val('');

                    applyFilter();
                }

                function toggleShowMore()
                {
                    if (currentPage >= totalPages) {
                        $('.showmore').addClass('hidden');
                    }
                    else {
                        $('.showmore').removeClass('hidden');
                    }
                }

                function applySort(sort) {
                    currentSort = sort;
                    currentPage = 1;

                    $('.sorter').find('a').removeClass('active').filter('[data-sort-id=' + sort + ']').addClass('active');

                    debounceFilter(['products', 'pagination']);
                }

                function applyPage(page)
                {
                    currentPage = page;

                    debounceFilter(['products', 'pagination']);
                }

                function showMore()
                {
                    if ($('.showmore').filter('.loading').length > 0) {
                        return;
                    }

                    currentPage = currentPage + 1;

                    $('.showmore').addClass('loading');

                    debounceFilter(['products', 'pagination'], function(){
                        $('.showmore').removeClass('loading');
                    }, true);
                }

                $().ready(function(){
                    $('.pagination').on('click', 'a', function(e){
                        e.preventDefault();
                        applyPage($(this).data('page'));

                        slideTo('h1');

                        return false;
                    });
                })
            </script>

            <style>
                .filter-block{border-bottom:2px solid #cfd0d0;}
                .filter-block:hover{background-color: whitesmoke;}
                .filter{margin-bottom:8px;border-bottom:2px solid #cfd0d0}
                .filter-block .filter-label{padding: 16px 41px 16px 16px; cursor:pointer;font-weight:bold;text-transform: uppercase;
                    color: #373f43;font-size:13px;}

                .filter-block .filter-label:before {
                    content:'';
                    width: 12px;
                    height: 6px;
                    position:absolute;
                    background-repeat: no-repeat;
                    right:15px;
                    margin-top:7px;
                    opacity:.4;
                    background-image: url('data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTIiIGhlaWdodD0iNiIgdmlld0JveD0iMCAwIDEyIDYiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHRpdGxlPlNoYXBlIDI8L3RpdGxlPjxwYXRoIGQ9Ik0uOCAwTDYgNC42IDExLjIgMGwuOC43TDYgNiAwIC43LjggMHoiIGZpbGw9IiMwMDAiIGZpbGwtcnVsZT0iZXZlbm9kZCIvPjwvc3ZnPg==')
                }

                .filter-block.opened .filter-label:before{
                    -webkit-transform: rotate(180deg);
                    -ms-transform: rotate(180deg);
                    transform: rotate(180deg);
                }

                .filter-block.active:before{
                    position: absolute;
                    top: 16px;
                    left: 0;
                    width: 0;
                    height: 0;
                    border-top: 9px solid transparent;
                    border-left: 7px solid red;
                    border-bottom: 9px solid transparent;
                    background: 0 0;
                    content: '';
                }

                .filter-block .filter-selected{display: none}
                .filter-block.active .filter-selected{display: inline-block}


                .filter-selected{color:red;float:right;padding-right:15px;}
                a.filter-selected-close{
                    font-size: 2em;
                    position: absolute;
                    right: 40px;
                    top: 4px;
                    color: red;}

                .filter-block .checkbox, .filter-block .radio {
                    margin-top:0px;
                    margin-bottom:0px;
                    position:relative;
                }

                .filter-block .checkbox.disabled{cursor:default;color:lightgrey;}
                .filter-block label{width:100%;
                }
                .filter-option-count{color:darkgrey;font-weight: bold;font-size:0.9em;position:absolute;right:0px;}

                .filter-block input[type=checkbox]{margin-top:-1px;}

                .filter-block .filter-body{
                    padding:16px;
                    padding-top:0px;
                }

                .filter-block .filter-inner{
                    max-height: 300px;
                    overflow: hidden;
                    font-size:0.9em;
                    width: 100%;
                }

                .filter-block .filter-inner select{
                    font-size: 16px;
                    text-align-last:center;
                    border: solid 3px #cfd0d0;
                    line-height:40px;
                    height:40px;
                }

                .filter-block .filter-body{
                    display: none;
                }
                .filter-block.opened .filter-body{
                    display: block;
                }

                .filter-block.expanded .filter-inner{
                    max-height: 360px;
                    overflow-y: scroll;
                    padding-right:1px;
                }


                .filter-block.expanded .filter-expand{
                    display:none;
                }

                .sorter a.active{font-weight: bold;}

                .showmore{position:relative;text-align: center;}
                .showmore.hidden{display: none;}

                .showmore:hover .showmore-inner{background-color: whitesmoke;}
                .showmore-inner{padding:12px 36px;text-align:center;border:1px solid lightgrey;display: block;cursor: pointer;font-size:20px;display: inline-block;border-radius:66px;cursor: pointer;}
                .showmore.loading .showmore-inner{cursor:default;opacity: 0.3}

                .filters{border-bottom:0px;}

                .catalog-subcategory-block {padding:3px;display: table}
                .catalog-subcategory-block a{display: table-cell;vertical-align: middle;background-color: #eef8ff;padding:8px;margin-bottom:12px;position: relative;padding-left:72px;height:64px;width:100%}
            </style>

            <div class="filters">
                <?=$parts['filters']?>
            </div>

            <? $favorites = \ActiveRecord\Favorite::current()->get();?>
            <? if (count($favorites) > 0):?>
                <div class="hidden-xs hidden-sm" style="margin-top:48px;">
                    <div style="padding: 6px 12px;
    border-bottom: 1px solid #fdd0d0;
    background-color: #d73533;
    color: white;">
                        <h4><a class="link" style="color:white;" href="<?=actionts('FavoriteController@index')?>">Избранное</a></h4>
                    </div>

                    <? foreach ($favorites as $favorite):?>
                        <a  href="<?=$favorite->getUrl()?>" style="padding:0px;border-bottom:1px solid lightgrey;display: block">
                            <?=ProductImage::img($favorite->getImage(), 'mini')?>
                            <?=$favorite->name?>
                        </a>
                    <? endforeach;?>
                </div>
            <? endif;?>
        </div>

        <div class="col-md-9 col-sm-8 nopadding">
            <div class="sorter">
                <? $sorts = [
                    \App\Http\Controllers\CatalogController::SORT_PRICE => 'по цене',
                    \App\Http\Controllers\CatalogController::SORT_NEW => 'по популярности',
                    \App\Http\Controllers\CatalogController::SORT_POPULAR => 'по новизне',
                ]?>

                Сортировка: <? foreach($sorts as $k => $v):?>
                    <a style="margin:0px 6px;border-bottom:1px dashed #006ab0;" <?=$sort == $k ? 'class="active"' : ''?> data-sort-id="<?=$k?>" href="javascript:applySort(<?=$k?>)"><?=$v?></a>
                <? endforeach;?>
            </div>
            <div class="row nomargin">
                <div class="products">
                    <?=$parts['products']?>
                </div>
            </div>

            <div class="showmore <? if (! $products->hasMorePages()):?>hidden<? endif;?>" onclick="showMore()">
                <div class="showmore-inner">показать еще</div>
            </div>

            <div class="pagination hidden-sm hidden-xs">
                <?=$parts['pagination']?>
            </div>
        </div>
    </div>
</div>