<?
$modifiers = $product->getModifiers()->get();
$variantProperties = [Attribute::ID_SFERA, Attribute::ID_TSILINDR];

$variants = $product->variants()->get();

$spheras = [];
$radius = [];

$modifiersValuesTable = [];

$propertyValues = [];
$properties = [];

foreach($variantProperties as $propertyCode) {
    $attr = Attribute::findByCode($propertyCode);
    $propertyValues[$propertyCode] = $attr->getOptionList();
    $properties[$propertyCode] = $attr;
}
foreach ($variants as $v) {
    $vData = [
        'article' => $v->id,
        'count' => $v->available
    ];
    foreach ($variantProperties as $propertyCode) {
        $values = $v->getProperty(Attribute::findByCode($propertyCode));
        $vData[$propertyCode] = array_get($values, 0);
    }
    $modifiersValuesTable[$v->id] = $vData;
}

$productSpherasVariants = array_filter(array_pluck($modifiersValuesTable, Attribute::ID_SFERA), function($value){return ! is_null($value);});
$productRadiusesVariants = array_filter(array_pluck($modifiersValuesTable, Attribute::ID_TSILINDR), function($value){return ! is_null($value);});

$spheras = array_only($propertyValues[Attribute::ID_SFERA], $productSpherasVariants);
$radius = array_only($propertyValues[Attribute::ID_TSILINDR], $productRadiusesVariants);

    //dump($propertyValues);
    //dd($modifiersValuesTable);

uasort($spheras, function($a, $b){
    return parseFloat($a) > parseFloat($b);
});

uasort($radius, function($a, $b){
    return parseFloat($a) > parseFloat($b);
});

$max = null;
$count = 1;
if ($frameId) {
    $count = \App\User::getBasket()->getBasketItemById($frameId)->getCount();
    $max = $count;
}
?>
<form id="modifiers">
    <? if (! empty($frameId)):?>
        <input type="hidden" name="frame_id" value="<?=$frameId?>"/>
    <? endif;?>
    <?=Bootstrap3Form::inputRow('number', 'count', 'Количество ПАР линз', $count, ['style'=>'width:70px;', 'max' => null, 'min' => 1])?>
    <input type="hidden" name="identical" value="1"/>
    <input type="hidden" name="lens_id" value="<?=$product->id?>"/>
    <input type="hidden" name="lens_id_left" value="<?=$product->id?>"/>
    <input type="hidden" name="lens_id_right" value="<?=$product->id?>"/>
    <div class="btn-group">
        <a href="#modifiers-odinakovie" data-toggle="tab" type="button" class="btn btn-primary active" data-value="1">Одинаковые</a>
        <? if(count($variants) > 1):?>
            <a href="#modifiers-raznie" data-toggle="tab" type="button" class="btn btn-default" data-value="0">Различающиеся</a>
        <? endif;?>
    </div>
    <div class="tab-content" style="margin-top:12px;">
        <div class="tab-pane active" id="modifiers-odinakovie">
            <div class="row">
                <div class="col-xs-3">
                    <?=Bootstrap3Form::selectRow('sphera', 'Сфера', array_replace(['' => 'не выбрано'], $spheras), $product->getProperty($properties[Attribute::ID_SFERA]), ['onchange' => 'updateLensSelectForm()'])?>
                </div>
                <div class="col-xs-3">
                    <?=Bootstrap3Form::selectRow('radius', 'Радиус', array_replace(['' => 'не выбрано'], $radius), $product->getProperty($properties[Attribute::ID_TSILINDR]), ['onchange' => 'updateLensSelectForm()'])?>
                </div>
            </div>
        </div>
        <? if(count($variants) > 1):?>
        <div class="tab-pane" id="modifiers-raznie">
            <h4>ЛЕВЫЙ ГЛАЗ</h4>
            <div class="row">
                <div class="col-xs-3">
                    <?=Bootstrap3Form::selectRow('sphera_left', 'Сфера слева', array_replace(['' => 'не выбрано'], $spheras), $product->getProperty($properties[Attribute::ID_SFERA]), ['onchange' => 'updateLensSelectForm()'])?>
                </div>
                <div class="col-xs-3">
                    <?=Bootstrap3Form::selectRow('radius_left', 'Радиус слева', array_replace(['' => 'не выбрано'], $radius), $product->getProperty($properties[Attribute::ID_TSILINDR]), ['onchange' => 'updateLensSelectForm()'])?>
                </div>
            </div>
            <hr/>
            <h4>ПРАВЫЙ ГЛАЗ</h4>
            <div class="row">
                <div class="col-xs-3">
                    <?=Bootstrap3Form::selectRow('sphera_right', 'Сфера справа', array_replace(['' => 'не выбрано'], $spheras), $product->getProperty($properties[Attribute::ID_SFERA]), ['onchange' => 'updateLensSelectForm()'])?>
                </div>
                <div class="col-xs-3">
                    <?=Bootstrap3Form::selectRow('radius_right', 'Радиус справа', array_replace(['' => 'не выбрано'], $radius), $product->getProperty($properties[Attribute::ID_TSILINDR]), ['onchange' => 'updateLensSelectForm()'])?>
                </div>
            </div>
        </div>
        <? endif;?>
    </div>
    <div>
        <button type="submit" class="btn btn-success pull-right submit-lenses-confuguration"><i class="fa fa-check"></i> <? if ($frameId):?>Вставить в оправу<?else:?>В корзину<?endif;?></button>
    </div>
    <div class="clearfix"></div>
</form>
<script>
    var modifiersValuesTable = <?=json_encode($modifiersValuesTable)?>;

    $('.btn[data-toggle="tab"]').click(function(){
        var $el = $(this).parents('#modifiers');

        $el.find('.btn[data-toggle="tab"]').removeClass('active').removeClass('btn-primary').addClass('btn-default');
        $(this).addClass('active').addClass('btn-primary').removeClass('btn-default');

        $el.find('[name=identical]').val($(this).data('value'));

        updateSubmitButton(this);
    });

    var updateSubmitButton = function(element){
        var identical = parseInt($('[name=identical]').val());
        $('.submit-lenses-confuguration').prop('disabled', 'disabled');

        if ((identical && $('[name=lens_id]').val().length > 0) || (! identical && $('[name=lens_id_left]').val().length > 0 && $('[name=lens_id_right]').val().length > 0)) {
            $('.submit-lenses-confuguration').removeProp('disabled');
        }
    }

    function updateLensSelectForm(){
        <? foreach (['', '_left', '_right'] as $suffix):?>
        $('[name=lens_id<?=$suffix?>]').val('');
        var sphere = $('[name=sphera<?=$suffix?>]').val();
        var radius = $('[name=radius<?=$suffix?>]').val();
        console.log(sphere);
        console.log(radius);
        if (sphere) {
            var radiuses = [];
            _.each(modifiersValuesTable, function(v){
                if (parseInt(v.<?=Attribute::ID_SFERA?>) == sphere) {
                    radiuses.push(v.<?=Attribute::ID_TSILINDR?>);
                }
            });

            $('[name=radius<?=$suffix?>] option').each(function(k, v) {
                if (k == 0 || radiuses.indexOf($(v).attr('value')) > -1) {
                    $(v).removeProp('disabled').show();
                }
                else {
                    if ($(v).prop('selected')) {
                        $('[name=radius<?=$suffix?>]').val('');
                    }

                    $(v).prop('disabled', 'disabled').hide();
                }
            });

        }
        if (! sphere && ! radius) {
            $('[name=radius<?=$suffix?>] option').removeProp('disabled').show();
        }
        else {
            $('[name=lens_id_<?=$suffix?>]').val('');

            _.each(modifiersValuesTable,function(v){
                if (
                    (v.<?=Attribute::ID_SFERA?> == sphere || (! sphere && ! v.<?=Attribute::ID_SFERA?>))
                    && (v.<?=Attribute::ID_TSILINDR?> == radius  || (! radius && ! v.<?=Attribute::ID_TSILINDR?>))
                    ) {
                    $('[name=lens_id<?=$suffix?>]').val(v.article);
                }
            });
        }
        <? endforeach;?>

        updateSubmitButton();
    }

    updateLensSelectForm();
</script>
