<?
/** @var Product $product */
/** @var \App\User $user */
?>
<script src="/js/vendor/magnifier/js/jquery.magnify.js"></script>
<script src="/js/vendor/magnifier/js/jquery.magnify-mobile.js"></script>
<link rel="stylesheet" href="/js/vendor/magnifier/css/magnify.css">
<script>
    $().ready(function() {
        $('.product-image').magnify({
            zoom: 2,
        });
    });

    function addProductToCart(productId, el) {
        if (productId == <?=ProductType::KONTAKTNIE_LINZY?>) {
            configurateLenses(productId)
        }
        else {
            $(el).button('loading');

            add2Basket(productId, 1, null, function(){
                $(el).button('success');
            });
        }
    }

    function favoriteSalonClick(salonId, el) {
        if ($(el).hasClass('active')) {
            $.ajax({
                url: '/salons/' + salonId + '/unfavorite/',
            }).success(function(){
                $(el).removeClass('active').button('reset');
            });
        }
        else {
            $.ajax({
                url: '/salons/' + salonId + '/favorite/',
            }).success(function(){
                $(el).addClass('active').button('active');
            });
        }
    }

</script>
<style>
    .rotate {
        -moz-transform: translateX(-50%) translateY(-50%) rotate(-90deg);
        -webkit-transform: translateX(-50%) translateY(-50%) rotate(-90deg);
        transform:  translateX(-50%) translateY(-50%) rotate(-90deg);
        position: absolute;
        top: 50%;
        left: 50%;
        white-space: nowrap;
    }
    .product-price{
        font-family: Helvetica;
        font-size: 36px;
        font-weight: bold;
        text-align: right;
        color: #006ab0;
    }

    .product-price-before{
        text-decoration: line-through;
        color:#2e373b;
        font-size:20px;
    }

    .product-favorites-button{
        padding-top:8.5px;
        padding-bottom:8.5px;
    }
    .product-attributes-table>tbody>tr>td, .product-attributes-table>tbody>tr>th {
        border-top:9px solid  #eef8ff;
        font-size: 18px;
        line-height:24px;
    }
</style>
<div class="container-fluid" >
    <h1>
        <?=$h1?>
        <? if ($user && $user->isAdmin()):?><a href="<?=actionts('AdminProductController@edit', ['id' => $product->id])?>"><i class="fa fa-pencil"></i></a><? endif;?>
    </h1>

    <div class="row">
        <div class="col-md-5 col-sm-6" style="text-align: center;">
            <div class="product-image-outer" style="display: inline-block;position:relative;">
                <? if ($product->getDiscount()):?>
                    <div class="product-image-discount" style="position:absolute;right:-10px;top:0px;z-index: 2;margin-top:-3px;">
                        <div style="position:relative">
                            <img src="/dizayn/sale-yellow-flag.png"/>
                            <div style="position:absolute;font-size: 20px;font-weight: bold;width:100%;height:100%;text-align: center;line-height:20px;top:0px;margin-top:12px;color:white;">
                                -<?=$product->getDiscount()?>%
                            </div>
                        </div>
                    </div>
                <? elseif ($product->new):?>
                    <div class="product-image-new" style="position:absolute;right:-10px;top:0px;z-index: 2;margin-top:-3px;">
                        <img src="/dizayn/new-big.png"/>
                    </div>
                <? elseif ($product->hit):?>
                    <div class="product-image-new" style="position:absolute;right:-10px;top:0px;z-index: 2;margin-top:-3px;">
                        <img src="/dizayn/new-big.png"/>
                    </div>
                <? endif;?>
                <? $image = $product->images->first(); $full = $image ? $image->getUrl('full') : null;?>
                <?
                $name = $product->type->is(ProductType::LINZI, ProductType::KONTAKTNIE_LINZY) ? explode(' ', $product->name, 2)[1] : $product->name;
                ?>
                <? if (! $image):?>
                    <? $image = (new ProductImage())->setOwner($product);?>
                <? endif;?>
                <a href="<?=$image->getUrl('full')?>" class="zoom-in fresco" data-fresco-group="product" data-fresco-caption="<?=$name?> <?=$product->getSellingPrice()?> руб.">
                    <?=ProductImage::img($image, 'large', null, ['style' => 'max-width:100%;', 'class' => 'product-image', 'data-magnify-src' => $full])?>
                </a>
            </div>
        </div>

        <div class="col-md-7">
            <div class="row">
                <div class="col-md-6">
                    <? if ($product->article):?>
                        <span class="label label-info">артикул: <?=$product->article?></span>
                    <? endif;?>

                    <? if (! $product->available):?>
                        <i style="color:red" class="fa fa-close"></i> Товара нет в наличии
                        <? if ($product->brand_id):?>
                            <div class="alert alert-warning">
                                Товар из старой коллекции.
                                <br/>Рекомендуем обратить внимание на более новые коллекции <?=Html::link(\App\Http\Controllers\CatalogController::createUrl(null, ['brand' => $product->brand_id]), $product->brand->name)?>.
                            </div>
                        <? endif;?>
                    <? else:?>
                        <div style="margin-top:18px;width:100%;">
                            <? if ($product->getSellingPrice()):?>
                                <div>
                                    <div style="font-weight:bold;margin-bottom:18px;">
                                        Ваша цена: <? if ($product->getDiscount()):?>(-<?=$product->getDiscount()?>%)<?endif;?>
                                    </div>
                                    <span class="product-price"><?=moneyFormat($product->getSellingPrice())?> руб.</span>
                                </div>
                                <? if ($product->price > $product->getSellingPrice()):?>
                                    <div>
                                        <div style="font-weight:bold;margin-bottom:18px;">
                                            Цена для всех:
                                        </div>
                                        <span class="product-price-before"><?=moneyFormat($product->price)?> руб.</span>
                                    </div>
                                <? endif;?>
                            <? else:?>
                                <span style="color:darkgrey">Цена не указана, уточняйте цену и наличие по телефону</span>
                            <? endif;?>

                            <? if (BONUS_PROGRAM_ACTIVE):?>
                                <? if ($product->getBonuses()):?>
                                    <a href="javascript:showModalSimple('bonus-modal')" style="font-size:1.1em;color:#d73533;font-style:italic;cursor: pointer">
                                        <img src="/dizayn/bonusy-blue.png"/> <span style="text-decoration: underline;"><?=plural($product->getBonuses($user), 'бонус', 'бонуса', 'бонусов', true)?></span></a>
                                <? endif;?>
                            <? endif;?>
                        </div>

                        <? if ($product->getSellingPrice()):?>
                            <div style="margin-top:18px;margin-bottom:10px;">
                                <? if ($product->type->is(ProductType::LINZI)):?>
                                    <a style="margin-right:0px;margin-bottom:3px;" onclick="selectFrame(<?=$product->id?>, event.currentTarget);" class="btn btn-primary btn-round btn-lg"
                                       data-success-text="<i class='fa fa-check'></i> В корзине"
                                       data-loading-text="<i class='fa fa-circle-o-notch fa-spin' ></i> В корзине"
                                    >
                                        <i class="fa fa-plus"></i> Выбрать оправу и купить
                                    </a>
                                    <a onclick="configurateLenses(<?=$product->id?>, event.currentTarget);" class="btn btn-default btn-round btn-lg"
                                       data-success-text="<i class='fa fa-check'></i> В корзине"
                                       data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> В корзине"
                                    >
                                        Купить отдельно
                                    </a>
                                <? else:?>
                                    <a style="margin-right:0px;" onclick="<? if ($product->type->is(ProductType::KONTAKTNIE_LINZY)):?>configurateLenses<?else:?>addProductToCart<?endif;?>(<?=$product->id?>, event.currentTarget);" class="btn btn-primary btn-round btn-lg"
                                       data-success-text="<i class='fa fa-check'></i> В корзине"
                                       data-loading-text="<i class='fa fa-circle-o-notch fa-spin' ></i> В корзине"
                                    >
                                        <img src="/dizayn/basket-white.png"/> Купить
                                    </a>
                                <? endif?>

                                <?php
                                $fittingImage = "<img src='/dizayn/primerka-blue.png'
                                              srcset='/dizayn/primerka-blue@2x.png 2x,img/primerka-blue@3x.png 3x'
                                              class='primerka_blue'>";
                                ?>
                                <? if ($product->type->is(ProductType::OCHKI_MEDICINSKIE, ProductType::OPRAVA, ProductType::SOLNECHNIE_OCHKI)):?>
                                    <div style="display: inline-block;text-align: center;position:relative;margin-left:6px;">
                                        <a onclick="javascript:addProductToFitting(<?=$product->id?>, event.currentTarget);" class="btn btn-default btn-round btn-lg primerka-button"
                                            <? if ($basket->getBasketItem($product->id, ['fitting' => 1])):?>data-active="1"<?endif;?>
                                           data-loading-text="..."
                                        >
                                            <i class="primerka-button-active-icon fa fa-check"></i> <?=$fittingImage?>
                                        </a>
                                        <div class="button-subtext">примерка</div>
                                    </div>
                                <? endif;?>

                                <div style="display: inline-block;text-align: center;position:relative;margin-left:6px;">
                                    <a style="width:56px;text-align: center" data-favorite="<?=intval(\App\User::isFavorite($product))?>" onclick="toggleFavorite(<?=$product->id?>, event.currentTarget)" title="Избранное" class="product-favorites-button btn btn-round btn-default dropdown-toggle" data-favorite="<?=intval(\App\User::isFavorite($product))?>" type="button"
                                       data-loading-text="..."
                                       data-success-text="<i class='icon-heart'></i>"
                                       data-default-text="<i class='icon-heart-o'></i>"
                                    ><? if (\App\User::isFavorite($product)):?><i class='icon-heart'></i><? else:?><i class="icon-heart-o"></i><? endif;?></a>
                                    <a href="<?=actionts('FavoriteController@index')?>" class="button-subtext">избранное</a>
                                </div>
                            </div>
                            <div style="margin-top:20px">
                                <a style="width:230px;" class="btn btn-danger btn-round"
                                   onclick="kz.quickOrderForm(event.currentTarget)" data-item="<?=$product->name?>"
                                >
                                    быстрая покупка
                                    <img src="/dizayn/basket-white.png"/>
                                    <i class="fa fa-check"></i>
                                </a>
                            </div>
                        <? endif;?>
                    <? endif;?>
                </div>
                <div class="col-md-6">
                    <style>
                        .benefits{text-align: left;background-color: white;}
                        .benefits b{font-size:18px;}
                        .benefits div, .benefits > a{padding:10px;margin-bottom:1px;display: block;line-height:21px;color:#2e373b;}
                        .benefits > a:hover{text-decoration: underline}
                        .benefits div > a{text-decoration: underline;}
                        .benefits a:hover{text-decoration: none;}
                    </style>
                    <div class="clearfix"></div>
                    <div class="benefits">
                        <div>
                            <b>В наличии на складе <i style="color:limegreen" class="fa fa-check"></i></b>
                        </div>
                        <div>
                            <b>Наличие в салонах:</b> <a href="javascript:showModalSimple('nalichie-v-salonah')">в 30 салонах</a>
                        </div>
                        <div>
                            <b>Доставка:</b> <a href="<?=Page::url(7)?>">от 250 руб&nbsp;<img style="position:absolute;" src="/dizayn/question-red.png"/></a>
                        </div>
                        <? if ($product->type->is(ProductType::OPRAVA, ProductType::LINZI)):?>
                            <a href="javascript:showModalSimple('izgotovlenie')">
                                <b>Изготоволение очков по рецепту</b>
                            </a>
                            <a href="javascript:showModalSimple('izgotovlenie')">
                                <b>Срок изготовления 7-14 дней</b>
                            </a>
                        <? endif?>
                        <a href="javascript:showModalSimple('izgotovlenie')">
                            <b>Срок доставки:</b> 1 день
                        </a>

<!--                        <a href="javascript:showModalSimple('fitting-modal')">Бесплатная примерка 4-х оправ</a>-->
                        <div><b>Оплата:</b> наличные, карта, yandex</div>
                        <a href="<?=Page::findBySlug('garantiya')->getUrl()?>"><b>Гарантия: 1 год на все товары <img style="position:absolute;" src="/dizayn/group-31.png"/></b></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <? if ($product->getType()->is(ProductType::SOLNECHNIE_OCHKI, ProductType::OPRAVA)):?>
        <div style="padding: 24px 0px;position: relative;font-size:16px;color:#006ab0;font-weight:normal;border: solid 9px #eef8ff;">
            <div class="row">
                <div class="col-sm-6" style="text-align: center;">
                    <div style="position:relative;display: inline-block;">
                        <img style="max-width:100%" src="/dizayn/glasses-big-gray.jpg"
                             srcset="/dizayn/glasses-big-gray@2x.jpg 2x,/dizayn/glasses-big-gray@3x.jpg 3x"
                             class="glasses_big_gray">
                        <?
                        $displayData = [
                            ['SHIRINA_OPRAVY_V_MM', 18, -11, 82, -11],
                            ['VYSOTA_RAMKI', 19, 28, 19, 84],
                            ['RAZMER_RAMKI_', 59, 42, 92, 42],
                            ['RAZMER', 45, 58, 55, 58],
                        ]
                        ?>
                        <? foreach ($displayData as $i):?>
                            <? $vertical = ($i[2] == $i[4] ? false : true);
                            if ($vertical) {$width = '16px';$height = ($i[4] - $i[2]) . '%';}
                            else {$height = '16px'; $width = ($i[3] - $i[1]) . '%';}
                            ?>
                            <? $attr = Attribute::findByCode($i[0]);?>
                            <div style="position:absolute;left:<?=$i[1]?>%;top:<?=$i[2]?>%;width:<?=$width?>;height:<?=$height?>;">
                                <div <? if ($vertical):?>class="rotate"<?endif;?>><?=$attr->valueString($product->getProperty($attr));?> мм</div>
                            </div>
                        <? endforeach;?>
                    </div>
                </div>
                <div class="col-sm-6" style="text-align: center;">
                    <div style="position:relative;display: inline-block;">
                        <img style="max-width:100%" src="/dizayn/zaushina-gray.png"
                             srcset="/dizayn/zaushina-gray@2x.png 2x,/dizayn/zaushina-gray@3x.png 3x"
                             class="zaushina_gray">
                        <?
                        $displayData = [
                            ['DLINA_ZAUSHNIKA', 20, 98, 80, 98],
                        ]
                        ?>
                        <? foreach ($displayData as $i):?>
                            <? $vertical = ($i[2] == $i[4] ? false : true);?>
                            <? $attr = Attribute::findByCode($i[0]);?>
                            <div style="position:absolute;left:<?=$i[1]?>%;top:<?=$i[2]?>%;width:<?=$i[3] - $i[1]?>%;height:16px;">
                                <?=$attr->valueString($product->getProperty($attr));?> мм
                            </div>
                        <? endforeach;?>
                    </div>
                </div>
            </div>

            <div style="text-align: center">
                <a style="font-size:1.2em;border-bottom:1px dashed #006ab0;" href="javascript:showModalSimple('kak-vibrat-razmer')">
                    Как выбрать размер <img src="/dizayn/question-blue.png"/>
                </a>
            </div>
        </div>
    <? endif;?>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <div style="margin: 24px 0px;margin-top:0px;">
                <? $attributes = [
                    'Бренд' => 'RayBan',
                    'Страна' => 'США',
                    'Пол' => 'Женские',
                    'Материал оправы' => 'Метал',
                    'Стиль оправы' => 'Повседневный'
                ];?>
                <h3>Характеристики</h3>
                <table class="table table-condensed product-attributes-table" >
                    <? foreach ($product->getProperties() as $attrId => $values):?>
                        <? if (! empty($values) && ($attr = Attribute::find($attrId)) && $attr->displayed):?>
                            <tr>
                                <th><?=$attr->name?>  :</th>
                                <td><span><?=$attr->valueString($values)?></span></td>
                            </tr>
                        <? endif;?>
                    <? endforeach;?>
                </table>

<!--                <h3>Комплектация</h3>-->
<!---->
<!--                <p>-->
<!--                    Чехол, заушники-->
<!--                </p>-->

            </div>
        </div>

        <div class="col-md-6">
            <? if ($product->description || $product->short_description):?>
                <h3>Описание</h3>
                <div class="product-description" style="margin:24px 0px;font-size:18px;">
                    <?=$product->description ? $product->description : $product->short_description?>
                </div>
            <? endif;?>
        </div>
    </div>
    <h2 style="border-bottom:solid 9px #eef8ff;">Похожие товары</h2>
    <div style="padding:0px 50px;">
        <? foreach (Product::where('type_id', $product->type_id)->orderBy(DB::raw('RAND()'))->limit(4)->get() as $similarProduct):?><div class="col-md-3 col-xs-6 col-sm-4 nopadding">
                <?=View::make('front.product.card', ['product' => $similarProduct, 'rowSize' => 4])?>
            </div><? endforeach;?>
    </div>
</div>
<script type="text/html" id="kak-vibrat-razmer-template">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Закрыть"><span aria-hidden="true">×</span></button>
        <div class="h1">Как выбрать размер</div>
    </div>
    <div class="modal-body">
        <img src="/dizayn/kak-vibrat-razmer.jpg"/>
        <br/><br/>
        <p>Каждая модель очков или оправы представлена в одном или нескольких доступных размерах по ширине линзы.</p>

        <div class="alert alert-info">
            <img src="/dizayn/invalid-name.png" style="margin-top: -5px;"/> <b>На Заметку</b>: Чтобы узнать свой подходящий размер - достаточно измерить любые очки, которые вы носите или носили раньше.
        </div>
        <p style="text-align: center;font-weight: bold;color:#006ab0;font-size:18px;">Наш магазин помогает покупать только то, что вам действительно подходит!</p>
        <p>
            <b>Любую модель</b> из каталога <b>можно примерить в <a class="link" href="<?=actionts('SalonController@index')?>">наших салонах</a></b>.
            Вы можете заказать несколько оправ на выбор, и мы привезём в удобный для вас день в любой салон оправу для вас,
            где вы сможете выбрать. <b>Данная услуга абсолютно бесплатная.</b>
        </p>
    </div>
    <div class="modal-footer" style="text-align: center">
        <a class="btn btn-default col-xs-12 btn-inline"><i class="fa fa-check"></i> Продолжить покупки</a>
    </div>
</script>

<div type="text/html" id="nalichie-v-salonah-template" style="display:none;">
    <?=view('front.product.availability', ['product' => $product])?>
</div>