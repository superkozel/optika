<?
/** @var Product $product */

$name = $product->type->is(ProductType::LINZI, ProductType::KONTAKTNIE_LINZY) ? explode(' ', $product->name, 2)[1] : $product->name;
//if ($product->type->is(ProductType::OPRAVA)) {
//    $name = $product->model_name;
//}
?><div class="product-card">
    <? if ($product->getDiscount()):?>
        <div class="product-image-discount" style="position:absolute;right:-10px;top:-20px;z-index: 2;margin-top:-3px;">
            <div style="position:relative">
                <img src="/dizayn/sale-small.png" />
                <div style="position:absolute;font-size: 20px;font-weight: bold;width:100%;height:100%;text-align: center;line-height:20px;top:0px;margin-top:12px;color:white;">
                    -<?=$product->getDiscount()?>%
                </div>
            </div>
        </div>
    <? elseif ($product->new):?>
        <div class="product-image-new" style="position:absolute;right:-10px;top:-20px;z-index: 2;margin-top:-3px;">
            <img src="/dizayn/new-small.png"/>
        </div>
    <? elseif ($product->hit):?>
        <div class="product-image-new" style="position:absolute;right:-11px;top:-20px;z-index: 2;margin-top:-3px;">
            <img src="/dizayn/hit-small.png"/>
        </div>
    <? endif;?>

    <? $image = $product->images->first()?>
    <div style="display: block;position: relative">
        <? if ($image):?>
            <a href="<?=$image->getUrl('full')?>" class="zoom-in fresco" data-fresco-group="product" data-fresco-caption="<?=$name?> <?=$product->getSellingPrice()?> руб.">
                <i class="fa fa-search-plus"></i>
            </a>
        <? else:?>
            <? $image = (new ProductImage())->setOwner($product);?>
        <? endif?>
        <a href="<?=$product->getUrl()?>"><?=ProductImage::img($image, 'medium', null, ['style' => 'max-width:100%;width:100%;'])?></a>
    </div>

    <div style="text-align: center;position:relative;">
        <a class="product-name" href="<?=$product->getUrl()?>"><?=$name?></a>
        <? if (! empty($user) && $user->isAdmin()):?>
            <a style="position:absolute;right:-5px;top:0px;" href="<?=actionts('AdminProductController@edit', ['id' => $product->id])?>"><i class="fa fa-pencil"></i></a>
        <? endif;?>
    </div>
    <? if ($product->brand && $product->brand->image):?>
        <?=BrandImage::img($product->brand->image, 'mini', null, ['style'=>'position:absolute;right:6px;top:0px;pointer-events: none;'])?>
    <? endif;?>



    <? if (empty($simple)):?>
    <!--            <span style="color:grey">--><?//=$product->getType()->getName();?>
    <!--                --><?// if ($product->type->is(ProductType::OPRAVA)):?>
    <!--                    --><?//=$product->getPropertyStringByCode('SHIRINA_OPRAVY_V_MM')?><!--x--><?//=$product->getPropertyStringByCode('VYSOTA_RAMKI')?><!--x--><?//=$product->getPropertyStringByCode('RAZMER')?>
    <!--                --><?// endif;?>
    <!--            </span>-->

        <? if (! $product->isAvailable()):?>
            <i class="fa fa-close" style="color:red"></i> нет в наличии
        <? else:?>
            <div style="margin-top:8px;">
                <? if ($product->getSellingPrice()):?>
                    <!--                    <span style="color:grey;text-decoration: line-through">--><?//=number_format($product->getSellingPrice())?><!-- <i class="fa fa-rub"></i></span>-->
                    <? if ($product->discount):?>
                        <span class="product-price-before"><?=moneyFormat($product->price)?> руб</span>
                    <? endif;?>
                    <span class="product-price"><?=moneyFormat($product->getPotentialPrice())?> руб</span>
                <? else:?>
                    <span style="color:grey;">Цена не указана</span>
                <? endif;?>
                <div class="clearfix"></div>
            </div>

            <div style="float:left;">
                <a class="product-available" href="javascript:showProductAvailabilityModal(<?=$product->id?>)" style="margin-bottom:6px;display: inline-block;text-align: left;">
                    <img src="/dizayn/path-10.png" style="margin-right:7px;"/> в наличии
                </a>
                <br/>
                <a class="product-favorite" data-favorite="<?=intval(\App\User::isFavorite($product))?>"
                   onclick="toggleFavorite(<?=$product->id?>, event.currentTarget)"
                   title="Добавить в избранное"
                   data-loading-text="<i class='icon-heart-o'></i> ..."
                   data-success-text="<i class='icon-heart'></i> в избранном"
                   data-default-text="<i class='icon-heart-o'></i> в избранное"
                >
                    <? if (\App\User::isFavorite($product)):?>
                        <i class='icon-heart'></i> в избранном
                    <? else:?>
                        <i class="icon-heart-o"></i> в избранное
                    <? endif;?>
                </a>
            </div>

            <? if ($product->getSellingPrice()):?>
                <div class="pull-right">
                    <? if ($product->type->is(ProductType::LINZI)):?>
                    <a class="btn btn-default btn-round" href="javascript:selectFrame(<?=$product->id?>)"
                       style="position: relative;overflow: visible;margin-top:12px;">
                        <img src="/dizayn/basket-blue.png"/>&nbsp;Выбрать для оправы
                    <? else:?>
                    <a onclick="var $el = $(event.currentTarget);<?if($product->type->is(ProductType::KONTAKTNIE_LINZY)):?>configurateLenses(<?=$product->id?>)<?else:?>add2Basket(<?=$product->id?>, 1, null, function(result){ $el.addClass('active'); $el.find('.label').text(result.added[0].item.product_count).show()})<?endif;?>"
                       class="btn btn-default btn-round" style="position:relative;margin-top:12px;">
                        <img src="/dizayn/basket-blue.png"/>&nbsp;В корзину
                    <? endif;?>
                        <span style="position:absolute;<? if (! $count = \App\User::getBasket()->getItemCount($product)):?>display:none;<? endif;?>" class="label label-danger"><?=$count?></span>
                    </a>
                </div>

                <div style="margin-top:3px;">
                    <? if ($product->type->is(ProductType::OCHKI_MEDICINSKIE, ProductType::OPRAVA, ProductType::SOLNECHNIE_OCHKI)):?>
                        <?php
                        $fittingImage = "<img src='/dizayn/primerka-blue.png'
                                              srcset='/dizayn/primerka-blue@2x.png 2x,img/primerka-blue@3x.png 3x'
                                              class='primerka_blue'>";
                        ?>
                        <div style="display: inline-block;text-align: center;position:relative;margin-left:6px;float:right;">
                            <a onclick="javascript:addProductToFitting(<?=$product->id?>, event.currentTarget);" class="btn btn-default btn-round btn-lg primerka-button"
                               <? if (\App\User::getBasket()->getBasketItem($product->id, ['fitting' => 1])):?>data-active="1"<?endif;?>
                               data-loading-text="..."
                            >
                                <i class="primerka-button-active-icon fa fa-check"></i> <?=$fittingImage?> Примерка
                            </a>
                        </div>
                    <? endif;?>
                </div>
                <div class="clearfix"></div>
            <? endif;?>
            <div class="clearfix"></div>
        <? endif;?>
    <? endif;?>
    <style>

        .product-card {
            position: relative;
            border: 1px solid #f1f1f1;
            padding:21px;
            margin-left:-1px;
        }

        .product-card img {
            max-width: 100%;
        }

        .product-card .add-to-favorites:hover i{color:red;}

        .product-card .zoom-in{position: absolute;right:5px;bottom:5px;color:#adcfe5;;font-size:2.5em;display: none;}
        .product-card:hover {
            border-color:transparent;
            border-radius:0px;
            box-shadow: 0 0 8px 0 rgba(0, 0, 0, 0.44);
            z-index: 2;
            position: relative;
        }
        .product-card:hover .zoom-in{display:block;}
        .product-card:hover .zoom-in:hover{color:#d73533}

        .product-card .product-name{
            text-align: center;
            display: inline-block;
            font-size: 18px;
            font-weight: bold;
            color:#2e373b;
            height:25px;
            overflow: hidden;
        }
        .product-card .product-name{
            text-align: center;
            display: inline-block;
            font-size: 18px;
            font-weight: bold;
            color:#2e373b;
            height:25px;
            overflow: hidden;
        }
        .product-card .product-price{
            font-family: Helvetica;
            /*font-size: 24px;*/
            font-size: 30px;
            font-weight: bold;
            float:right;
            color: #006ab0;
            line-height:1;
        }
        .product-card .product-price-before{
            font-size: 24px;
            float: left;
            color: #d73533;text-decoration: line-through
        }
        .product-card .product-available{
            font-size: 12px;
            font-weight: 300;
            text-align: right;
            color: #006ab0;
        }

        .product-card .product-available{
            font-size: 13px;
            font-weight: 300;
            text-align: right;
            color: #006ab0;
        }
        .product-card{background-color: white;}
        .product-card .product-favorite{font-size:13px;}
        .product-card .product-favorite i{margin-right:2px}
    </style>
</div>