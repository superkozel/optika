<script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script type="text/javascript">
    var myMap;
    function showAvailabilityOnMap(el) {

        var mapEl = $(el).parents('.modal').find('.nalichie-map').show()[0];

        if ($(mapEl).find('ymaps').length > 0) {
            return;
        }

        shopsCoords = [
            <? foreach(Salon::all() as $salon):?>
            <?$coords = explode(',', $salon->coordinates);?>
            <? $metroData = ''?>
            <? foreach ($salon->metros()->withPivot('range')->get() as $metro):?>
            <? $metroData.='<div style="margin-left:20px" class="metro-station line-m' . intval($metro->line->number) . '">' . $metro->name . ($metro->pivot->range ? ('  <span style="font-style: italic;color:darkgrey">' . $metro->pivot->range . ' ' . plural($metro->pivot->range, 'метр', 'метра', 'метров') .'</span>') : '') . '</div>';?>
            <? endforeach;?>
            ['<?=$coords[0]?>','<?=$coords[1]?>','«Оптика Фаворит» на <?=$salon->name?>','<i class="fa fa-check" style="color:green"></i> В наличии: <?=rand(3,10)?><br/><?=$salon->address?><br/><?=$metroData?><br/><span style="color:red"><?=$salon->phone?></span><br/><br/><b>Режим работы</b><br/><?=str_replace("\n", '', $salon->mode)?><br/><?=SalonImage::img($salon->images->get(0), 'medium', null)?>'],
            <? endforeach;?>
        ];

        ymaps.ready(function () {
            myMap = new ymaps.Map(mapEl, {
                center: [55.7488816,37.6088263],
                zoom: 10,
                controls: ['typeSelector']
            });

            myMap.controls.add('zoomControl', {position: {top: 10, left: 10}, size: 'small'});
            myMap.controls.get('typeSelector').state.set('size','small');
            myMap.behaviors.disable('scrollZoom');

            shopsCollection = new ymaps.GeoObjectCollection(null, {
                iconLayout: 'default#image',
                iconImageHref: '/marker_s.png',
                iconImageSize: [26, 26],
                iconImageOffset: [-26, -26]
            });

            for (var i = 0; i < shopsCoords.length; i++) {
                placemark = new ymaps.Placemark([shopsCoords[i][0], shopsCoords[i][1]], {
                    balloonContentHeader: shopsCoords[i][2],
                    balloonContentBody: shopsCoords[i][3]
                });

                shopsCollection.add(placemark);
            }

            // Показываем маркеры на карте
            myMap.geoObjects.add(shopsCollection);

            // Карта автоматом показывает все маркеры
            myMap.setBounds(ymaps.geoQuery(shopsCollection).getBounds(), {zoomMargin: [50, 50, 50, 50]});
            myMap.setZoom(myMap.getZoom(),{checkZoomRange: true});

            /*
             Когда надо обновить маркеры на карте на основе фильтра:

             shopsCollection.removeAll();
             // новые маркеры добавляем в коллецию (for () {})
             myMap.geoObjects.add(shopsCollection);

             */
        });
    }
</script>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Закрыть"><span aria-hidden="true">×</span></button>
    <span class="h3">Наличие в салонах</span>
    <div class="pull-right">
        <a onclick="showAvailabilityOnMap(event.currentTarget)" class="btn btn-primary"><i class="fa fa-map-marker"></i>&nbsp;&nbsp;Показать на карте</a>
    </div>
</div>
<div class="modal-body">
    <div class="nalichie-map" style="width:100%;height:345px;margin-bottom:20px;display: none;"></div>
    <table class="table table-condensed table-bordered">
        <colgroup>
            <col width="210px"/>
            <? if ($user):?>
                <col width="10px"/>
            <? endif;?>
            <col width="*"/>
        </colgroup>
        <thead>
        <tr>
            <th>Салон</th>
            <? if ($user):?>
                <th></th>
            <? endif;?>
            <th>Наличие</th>
        </tr>
        </thead>
        <? $favorites = $user ? $user->favoriteSalons : null;?>
        <? foreach (Salon::all()->sort(function($a, $b) use ($favorites){return ! $favorites || $favorites->where('id', $a->id)->count() > 0;}) as $salon):?>
            <? $active = $favorites && $favorites->where('id', $salon->id)->count() > 0;?>
            <tr>
                <td style="text-align: right;"><a href="<?=$salon->getUrl()?>"><?=$salon->name?> <?=$salon->city->name?></a></td>
                <? if ($user):?>
                    <td style="text-align: center;">
                        <a <? if ($active):?>class="active"<?endif;?> title="Избранный салон" onclick="favoriteSalonClick(<?=$salon->id?>, event.currentTarget)"
                           data-active-text="<i class='fa fa-heart favorite-icon'></i>"
                           data-default-text="<i class='fa fa-heart-o favorite-icon'></i>"
                        >
                            <? if ($active):?>
                                <i class='fa fa-heart favorite-icon'></i>
                            <? else:?>
                                <i class='fa fa-heart-o favorite-icon'></i>
                            <? endif;?>
                        </a>
                    </td>
                <? endif;?>
                <td><i title="<?=rand(1,16)?> в наличии" style="color:#47a717;font-weight: bold;" class="fa fa-check"></i></td>
            </tr>
        <? endforeach;?>
    </table>
</div>
<div class="modal-footer">
    <div class="row">
        <div class="col-xs-6 " data-dismiss="modal">
            <a class="btn btn-default col-xs-12"><i class="fa fa-check"></i> Продолжить покупки</a>
        </div>
    </div>
</div>
<style>
    a .favorite-icon{color:black;}
    a .favorite-icon:hover{color:red;}
    a.active > .favorite-icon{color:red;}
</style>