<?
if (method_exists($model, $property)){
	$class = get_class($model->{$property}());
	$multiple = (strpos($class, 'One') === false);
}
else {
	$multiple = false;
}
?>
<div id="<?=$property?>">

</div>
<div class="clearfix"></div>

<a class="btn btn-primary" id="<?=$property?>-start-upload">Добавить...</a>

<script>
	$().ready(function(){
		$('#<?=$property?>-start-upload').click(function(){
			showUploadModal(function(uploads){
				<? if (! $multiple):?>
					$('#<?=$property?>').html('');
				<? endif;?>
				for (i in uploads) {
					$('#<?=$property?>').append(template('uploaded-<?=$property?>', uploads[i]));
				}
			}, <?=intval($multiple)?>)
		});
	});

	$().ready(function(){
		<? if (old($property)):?>
			<? if (is_array(old($property))):?>
				<? foreach(old($property) as $k => $v):?>
					<? if (mb_strpos($v, 'u') === 0):?>
						<? $upload = \Upload::find(mb_substr($v, 1));?>
						$('#<?=$property?>').append(template('uploaded-<?=$property?>', {
							id : '<?=$v?>',
							thumb : '<?=$upload->thumb()?>',
						}));
					<? else:?>
						<? $image = $model->{$property}()->find($v);?>
						$('#<?=$property?>').append(template('uploaded-<?=$property?>', {
							id : '<?=$v?>',
							thumb : '<?=$image->thumb()?>',
						}));
					<? endif;?>
				<? endforeach;?>
			<? else:?>
				<?$v = old($property)?>
				<? if (mb_strpos($v, 'u') === 0):?>
					<? $upload = \Upload::find(mb_substr($v, 1));?>
					$('#<?=$property?>').append(template('uploaded-<?=$property?>', {
						id : '<?=$v?>',
						thumb : '<?=$upload->thumb()?>',
					}));
					<? else:?>
					<? $image = $model->{$property}()->find($v);?>
					$('#<?=$property?>').append(template('uploaded-<?=$property?>', {
						id : '<?=$v?>',
						thumb : '<?=$image->thumb()?>',
					}));
				<? endif;?>
			<? endif;?>
		<? elseif ($model->id):?>
			<? if ($multiple):?>
				<? foreach($model->{$property} as $image):?>
					$('#<?=$property?>').append(template('uploaded-<?=$property?>', {
						id : '<?=$image->id?>',
						thumb : '<?=$image->thumb()?>',
					}));
				<? endforeach;?>
			<? elseif ($images = $model->{$property}):?>
				$('#<?=$property?>').append(template('uploaded-<?=$property?>', {
					id : '<?=! empty($images->id) ? $images->id : 666?>',
					thumb : '<?=$images->thumb()?>',
				}));
			<? endif;?>
		<? endif;?>
	});
</script>

<script type="text/html" id="uploaded-<?=$property?>-template">
	<div style="position: relative;float:left;margin-right:6px; margin-bottom:6px;">
		<a onclick="$(event.currentTarget).parent().remove();" style="cursor:pointer;position: absolute;right:0;top:0;color:red;"><i class="fa fa-close"></i></a>
		<img src="<%=thumb%>"/>
		<input type="hidden" name="<?=$multiple ? ($property . '[]') : $property ?>" value="<%=id%>"/>
	</div>
</script>