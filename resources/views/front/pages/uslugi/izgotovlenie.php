<p>
    В нашей мастерской быстро, точно и качественно изготавливаются заказы любой сложности. Срок изготовления до 3х дней. Использование высокоточного оборудования в сочетании с профессионализмом наших мастеров позволит изготовить для вас мужские, женские и детские очки любой сложности.
</p>

<p>
    На оправы, купленные в сторонней организации, в работу принимаются без гарантии», поэтому лучше приобретать оправу и изготавливать очки в одной организации.
</p>

<p>
    Лицензия _________________
</p>

<p>
    Делая покупку в нашем магазине, Вы всегда можете обратиться к нам за помощью или советом.
</p>

<h4>Будем всегда рады помочь!</h4>


<h3>Стоимость изготовления очков</h3>

<table border="0" cellspacing="0">
    <tbody><tr>
        <td align="center" height="20" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><b><font color="#000000">Тип оправы</font></b></td>
        <td align="center" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><b><font color="#000000">Стоимость материалов</font></b></td>
        <td align="center" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><b><font color="#000000">Цена услуги</font></b></td>
    </tr>
    <tr>
        <td align="center" height="20" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000">Оправа полноободковая и под "леску"</font></td>
        <td align="center" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000">до 1 500</font></td>
        <td align="center" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000">200</font></td>
    </tr>
    <tr>
        <td align="center" height="20" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000"><br></font></td>
        <td align="center" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000">до 3 000</font></td>
        <td align="center" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000">400</font></td>
    </tr>
    <tr>
        <td align="center" height="20" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000"><br></font></td>
        <td align="center" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000">до 4 500 </font></td>
        <td align="center" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000">600</font></td>
    </tr>
    <tr>
        <td align="center" height="20" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000"><br></font></td>
        <td align="center" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000">до 8 000</font></td>
        <td align="center" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000">1,000</font></td>
    </tr>
    <tr>
        <td align="center" height="20" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000"><br></font></td>
        <td align="center" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000">до 15 000</font></td>
        <td align="center" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000">1,200</font></td>
    </tr>
    <tr>
        <td align="center" height="20" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000"><br></font></td>
        <td align="center" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000">до 30 000</font></td>
        <td align="center" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000">2,000</font></td>
    </tr>
    <tr>
        <td align="center" height="20" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000"><br></font></td>
        <td align="center" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000">свыше 30 000</font></td>
        <td align="center" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000">3,000</font></td>
    </tr>
    <tr>
        <td align="left" height="20" valign="bottom"><font color="#000000"><br></font></td>
        <td align="left" valign="bottom"><font color="#000000"><br></font></td>
        <td align="left" valign="bottom"><font color="#000000"><br></font></td>
    </tr>
    <tr>
        <td align="center" height="40" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000">Оправа безободковая (на винтах и втулках)</font></td>
        <td align="center" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000">до 3 000</font></td>
        <td align="center" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000">650</font></td>
    </tr>
    <tr>
        <td align="center" height="20" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000"><br></font></td>
        <td align="center" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000">до 6 000</font></td>
        <td align="center" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000">950</font></td>
    </tr>
    <tr>
        <td align="center" height="20" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000"><br></font></td>
        <td align="center" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000">до 10 000</font></td>
        <td align="center" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000">1,500</font></td>
    </tr>
    <tr>
        <td align="center" height="20" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000"><br></font></td>
        <td align="center" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000">до 15 000</font></td>
        <td align="center" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000">2,000</font></td>
    </tr>
    <tr>
        <td align="center" height="20" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000"><br></font></td>
        <td align="center" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000">до 30 000</font></td>
        <td align="center" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000">2,500</font></td>
    </tr>
    <tr>
        <td align="center" height="20" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000"><br></font></td>
        <td align="center" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000">свыше 30 000</font></td>
        <td align="center" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000">3,500</font></td>
    </tr>
    <tr>
        <td align="left" height="20" valign="bottom"><font color="#000000"><br></font></td>
        <td align="left" valign="bottom"><font color="#000000"><br></font></td>
        <td align="left" valign="bottom"><font color="#000000"><br></font></td>
    </tr>
    <tr>
        <td align="center" height="20" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000">Стоимость материалов свыше 50 000</font></td>
        <td align="center" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000"><br></font></td>
        <td align="center" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000">4,000</font></td>
    </tr>
    <tr>
        <td align="center" height="20" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000">Стоимость материалов свыше 100 000</font></td>
        <td align="center" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000"><br></font></td>
        <td align="center" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000">5,000</font></td>
    </tr>
    <tr>
        <td align="left" height="20" valign="bottom"><font color="#000000"><br></font></td>
        <td align="left" valign="bottom"><font color="#000000"><br></font></td>
        <td align="left" valign="bottom"><font color="#000000"><br></font></td>
    </tr>
    <tr>
        <td align="center" height="20" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000">Дополнительные услуги:</font></td>
        <td align="center" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000"><br></font></td>
        <td align="center" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000"><br></font></td>
    </tr>
    <tr>
        <td align="center" height="20" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000">тонирование линз</font></td>
        <td align="center" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000"><br></font></td>
        <td align="center" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000">210</font></td>
    </tr>
    <tr>
        <td align="center" height="20" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000">Тонирование "маска"</font></td>
        <td align="center" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000"><br></font></td>
        <td align="center" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000">240</font></td>
    </tr>
    <tr>
        <td align="center" height="40" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000">Ультразвуковая чистка оправы покуаптеля при заказе очков</font></td>
        <td align="center" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000"><br></font></td>
        <td align="center" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;" valign="bottom"><font color="#000000">50</font></td>
    </tr>
    </tbody>
</table>