<p>«Оптика Фаворит» – это возможность купить очки и проверить зрение в более чем 30 современных&nbsp;<a href="http://www.optika-favorit.ru/salons/">салонах оптики</a>, расположенных в Москве и в Московской области (Балашиха, Дзержинский, Железнодорожный, Жуковский, Ивантеевка, Красногорск, Люберцы, Лыткарино, Мытищи, Одинцово, Химки).&nbsp;</p>

<img src="http://dev.optika-favorit.ru/upload/iblock/134/134a278c0f475577850ba10d8f7432f3.jpg" style="width:100%;"/>

<p>Наши салоны выдержаны в едином фирменном стиле эксклюзивного дизайна. &nbsp;&nbsp;</p>

<p>Оснащение наших светлых и просторных залов новым торговым и медицинским оборудованием является нашей отличительной чертой.</p>


<p>Продавцы-консультанты расскажут о современных тенденциях моды, характеристиках, достоинствах и отличительных особенностях товара, помогут подобрать&nbsp;<a href="http://www.optika-favorit.ru/catalog/sunglasses/">очки</a>,&nbsp;<a href="http://www.optika-favorit.ru/catalog/medical_frames/">оправы</a>&nbsp;и&nbsp;<a href="http://www.optika-favorit.ru/catalog/glasses_accessories/">аксессуары</a>.</p>

<div>
    <br>
</div>

<div>Использование высокоточного оборудования в сочетании с профессионализмом наших мастеров позволит изготовить для вас мужские, женские и детские очки любой сложности.</div>

<div>Только в салонах «Оптика Фаворит» настоящий выбор&nbsp;<a href="http://www.optika-favorit.ru/catalog/medical_frames/">оправ</a>,&nbsp;<a href="http://www.optika-favorit.ru/catalog/sunglasses/">солнцезащитных очков</a>,&nbsp;<a href="http://www.optika-favorit.ru/catalog/lenses/">очковых линз</a>&nbsp;и<a href="http://www.optika-favorit.ru/catalog/contact_lenses/"> средств контактной коррекции</a>, способный удовлетворить потребности покупателей любого возраста.</div>

<div>
    <br>
</div>

<div>Сотрудничество с мировыми&nbsp;<a href="http://www.optika-favorit.ru/brands/">брендами</a> уже говорит о многом. Нам доверяют:</div>

<div><a href="http://www.optika-favorit.ru/brands/chopard/">Chopard</a>,&nbsp;<a href="http://www.optika-favorit.ru/brands/escada/">Escada</a>,&nbsp;<a href="http://www.optika-favorit.ru/brands/john_galliano/">J. Galliano</a>,&nbsp;<a href="http://www.optika-favorit.ru/brands/tom_ford/">Tom Ford</a>, <a href="http://www.optika-favorit.ru/brands/dupont_s_t/">Dupont</a>,&nbsp;<a href="http://www.optika-favorit.ru/brands/porsche_design/">Porsche Design</a>,&nbsp;<a href="http://www.optika-favorit.ru/brands/chrome_hearts/">Chrome Hearts</a>,&nbsp;<a href="http://www.optika-favorit.ru/brands/givenchy/">Givenchy</a>,&nbsp;<a href="http://www.optika-favorit.ru/brands/dolce_gabbana/">D&amp;G</a>,&nbsp;<a href="http://www.optika-favorit.ru/brands/roberto_cavalli/">Roberto Cavalli</a>, &nbsp;<a href="http://www.optika-favorit.ru/brands/rodenstock/">Rodenstock</a>,&nbsp;<a href="http://www.optika-favorit.ru/brands/valentino/">Valentino</a>,&nbsp;<a href="http://www.optika-favorit.ru/brands/chloe/">Chloe</a>,&nbsp;<a href="http://www.optika-favorit.ru/brands/ray_ban/">Ray Ban</a>,&nbsp;<a href="http://www.optika-favorit.ru/brands/lacoste/">Lacoste</a>,&nbsp;<a href="http://www.optika-favorit.ru/brands/trussardi/">Trussardi</a>, <a href="http://www.optika-favorit.ru/brands/laura_biagiotti/">Laura Biagiotti</a>,&nbsp;<a href="http://www.optika-favorit.ru/brands/emanuel_ungaro/">Ungaro</a>,&nbsp;<a href="http://www.optika-favorit.ru/brands/puma/">Puma</a>,&nbsp;<a href="http://www.optika-favorit.ru/brands/esprit/">Esprit</a>, и др.&nbsp;</div>

<div>
    <br>
</div>

<div><a href="http://www.optika-favorit.ru/catalog/lenses/">Ассортимент очковых линз</a>&nbsp;представлен ведущими производителями мира: Zeiss, Nikon, Seiko, Hanmi Swiss.&nbsp;</div>

<div>
    <br>
</div>

<div>Используя разный материал и технологию обработки линз мы сможем удовлетворить потребности самого взыскательного клиента. Полимерные и минеральные линзы, различного дизайна (сферические и асферические), прозрачные и затемненные линзы, фотохромные, поляризационные, прогрессивные, бифокальные - наши возможности практически безграничны.</div>

<div>
    <br>
</div>

<div>Контактная коррекция представлена линзами мировых производителей Johnson&amp;Johnson и CibaVision (однодневные, двухнедельные, ежемесячные, торические и цветные).</div>

<div>&nbsp;</div>

<div>Возвращаясь к нам снова и снова, клиенты благодарят нас за нашу работу. В качестве благодарности за доверие, мы предлагаем нашим клиентам накопительную систему скидок, которая позволит купить очки от 5% до 20 % дешевле. Социальные предложения, сезонные распродажи и регулярные рекламные акции делают наш ассортимент доступным каждому.&nbsp;</div>

<div style="color:red;font-style: italic;font-size:1.6em;margin-top:24p;x">С наилучшими пожеланиями, «Оптика Фаворит»</div>