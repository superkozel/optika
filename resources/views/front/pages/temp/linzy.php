<div id="Content"> Обычно к очковым линзам относятся без особого внимания. Считают, что особой разницы нет и выбирать линзы не нужно. Но это далеко не так. Выбор очковых линз заслуживает не меньшего внимания, чем выбор оправы. Хорошие линзы повысят остроту зрения, защитят глаза от утомления, дополнительно украсят оправу.&nbsp;
    <br>

    <div>
        <br>
    </div>

    <div>
        <table width="95%" cellspacing="0" cellpadding="5" border="0" align="center">
            <tbody>
            <tr><td><img src="/upload/medialibrary/aa5/aa590207037e169b87db09362b8438db.jpg" border="0" width="324" height="202"></td><td><img src="/upload/medialibrary/858/858d4b9812bf07bf9b6f6a1666aa38a8.jpg" border="0" width="321" height="202"></td></tr>
            </tbody>
        </table>

        <br>

        <br>
    </div>

    <div>«Оптика Фаворит» сегодня - это огромное разнообразие очковых линз ведущих производителей мира: Zeiss, Nikon, Seiko, HanmiSwiss.&nbsp;</div>

    <div>
        <br>
    </div>

    <div>Мы предлагаем Вам линзы из разного материала (полимерные и минеральные), разного дизайна (сферические и асферические), прозрачные и затемненные линзы, фотохромные, поляризационные, прогрессивные, бифокальные.&nbsp;</div>

    <div>
        <br>
    </div>

    <div>Именно поэтому мы приглашаем Вас купить очки в салонах «Оптика Фаворит». &nbsp; &nbsp;
        <br>

        <br>
        Помните, купить очки не просто. Перед покупкой обязательно консультируйтесь со <a href="http://optika-favorit.ru/about_us/">специалистом - офтальмологом.</a> </div>

</div>