<div class="EventItem">
    <div class="item-text">

        <div>Любимые очки. Они позволяют лучше видеть, защищают глаза от солнца или просто подчеркивают ваш статус и дополняют образ. Подобрать очки действительно не очень просто, поэтому за любимым аксессуаром нужно ухаживать. Главное ухаживать за очками правильно, чтобы линзы, подвижные части и естественно внешний вид не пострадали, и здесь несколько советов:</div>

        <div>
            <br>
        </div>

        <div>1. Очки нельзя класть на твердую поверхность линзами вниз. Так линзы быстро поцарапаются.</div>

        <div>
            <br>
        </div>

        <div>2. Не следует мыть оправу в холодной воде или использовать моющие средства. Моющие средства содержат щелочь, а она, несомненно, вызовет потемнение оправы. Простого мытья в чистой теплой воде для оправы очков будет более чем достаточно. Загрязнения в местах соприкосновения стекол с оправой или на сгибах можно убрать мягкой специальной или косметической кисточкой. Грубые предметы, например иголка, могут поцарапать линзы.</div>

        <div>
            <br>
        </div>

        <div>3. Пыль, отпечатки пальцев, следы от влаги - все это портит внешний вид очков и значительно снижает четкость зрительного восприятия. Эти проблемы смогут решить специальные салфетки и спрей для очистки очковых линз.</div>

        <div>
            <br>
        </div>

        <div>Самые эффективные салфетки для протирки очков изготовлены из микрофибры. Этот высокотехнологичный материал, волокна которого в сотни раз тоньше человеческого волоса бережно очищает поверхность линзы. Салфетки для очков из микрофибры удаляют любые загрязнения: пыль, грязи, жир, мгновенно впитывают влагу и не оставляют никаких следов. Их можно использовать очень долго – при необходимости мягкая ткань легко стирается моющим средством, не содержащим агрессивных компонентов.</div>

        <div>
            <br>
        </div>

        <div>Еще один замечательный метод очистки очковых линз – влажные салфетки для очков в индивидуальной упаковке. Они пропитаны особым составом, который мгновенно удаляет любые загрязнения, снимает статическое электричество. Поэтому линзы дольше остаются чистыми. Поверхность линзы после обработки такой салфеткой не запотевает, когда вы попадаете с холодной улицы в теплое помещение. </div>

        <div>
            <br>
        </div>

        <div>Спрей для очистки очковых линз эффективно очищает поверхность от загрязнений, которые образуются в процессе пользования очками, ухудшая светопропускание и внешний вид очков. Спрей уменьшает запотевание очковых линз, обладает антистатическим эффектом. </div>

        <div>
            <br>
        </div>

        <div>4. Не пренебрегайте футляром для очков. Идеально хранить очки в жестком футляре, даже тогда, когда они просто лежат на полке.</div>

        <div>
            <br>
        </div>

        <div>5. Не стоит поддаваться соблазну или привычке и использовать свои очки в качестве ободка для волос. Это уменьшит срок службы оправы. Все крепления в этом случае растянутся, форма исказится. </div>

        <div>
            <br>
        </div>

        <div>Каким бы тщательным ни был уход за очками, все же офтальмологи советуют менять очковые линзы один раз в несколько лет.</div>

        <div>
            <br>
        </div>

        <div>Соблюдайте наши простые советы, и пусть окружающий вас мир радует своей красотой и красками, а в случае возникновения трудностей обращайтесь к специалистам Оптика Фаворит. В наших салонах имеется большой выбор&nbsp;<a href="http://www.optika-favorit.ru/catalog/glasses_accessories/">средств по уходу за очками и линзами</a>. &nbsp;Попробуйте – и убедитесь, насколько полезными бывают мелочи!</div>

        <div>
            <br>
        </div>

        <div><img src="/upload/medialibrary/332/ochki-futlar-salfetka.jpg" border="0" width="400" height="231"></div>
    </div>
</div>