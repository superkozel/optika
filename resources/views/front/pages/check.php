<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html">
    <link type="text/css" rel="stylesheet" href="/check/resources/screen.css">
    <script type="text/javascript" src="/check/resources/jquery-1.js"></script>
    <title>Проверка зрения</title>
    <script type="text/javascript">
        var locale = 'en_US';
        var eyeTestVersion = 'desktop'; // [dektop, tablet];
        var config = {
            ajaxApiUrl: '/bitrix/templates/carl_zeiss_test/locale.php',
            resultLayerDelay: 1000,
            imgSources: {
                creditcard: '/check/resources/creditcard.gif',
                t2CircleBg: '/check/resources/circle_bg.png',
                t3Deuteranop: '/check/resources/deuteranop.png',
                t3Protanop: '/check/resources/protanop.png',
                t3Tritranop: '/check/resources/tritranop.png'
            }
        }
    </script>
    <script type="text/javascript" src="/check/resources/lib.js"></script>
    <script type="text/javascript" src="/check/resources/plugin.js"></script>
</head>
<body>

<div id="middle">

    <div class="en_US unitSystemInch" id="zeissEyeTest">
        <h1 class="label" data-labelkey="header">Онлайн Проверка Зрения ZEISS </h1>
        <div style="display: none;" id="status">
            <ul>
                <li id="status_i1"> <span class="label" data-labelkey="status_i1">Подготовка</span> </li>

                <li id="status_i2"> <span class="label" data-labelkey="status_i2">1. Проверка Остроты Зрения</span> </li>

                <li id="status_i3"> <span class="label" data-labelkey="status_i3">2. Проверка Контрастности</span> </li>

                <li id="status_i4"> <span class="label" data-labelkey="status_i4">3. Проверка Цветовосприятия</span> </li>

                <li id="status_i5"> <span class="label" data-labelkey="status_i5">Результат</span> </li>

                <li style="display: none;" id="status_i6" class="voucher"> <span class="label" data-labelkey="status_i6">Купон</span> </li>
            </ul>
        </div>

        <!-- startseite -->

        <div style="display: block;" id="p1" class="cPage">
            <div style="display: none;" id="p1_startButtonVoucher" class="buttonWrap voucher trClick"> <span class="buttonBlue introButton"> <span class="label" data-labelkey="p1_start_button_voucher">Начните проверку и сохраните Купон.</span> </span> </div>

            <div id="p1_startButtonNoVoucher" class="buttonWrap noVoucher trClick"> <span class="buttonBlue"> <span class="label" data-labelkey="p1_start_button_no_voucher">Начать проверку</span> </span> </div>

            <div id="p1_teaserWrap">
                <div id="p1_teaser1">
                    <h2 class="label" data-labelkey="p1_teaser1_header">Лучшее зрение = Лучшее качество жизни</h2>

                    <p class="label" data-labelkey="p1_teaser1_copy">Лучшее зрение -
                        лучшее качество жизни. Посещение ближайшего офтальмолога позволит
                        провести полное обследование Вашего зрения и подобрать индивидуальные
                        линзы. ZEISS онлайн проверка зрения поможет Вам легко и быстро узнать,
                        не настало ли время провести профессиональное обследование зрения.</p>
                    <span class="icon"></span> </div>

                <div id="p1_teaser2">
                    <h2 class="label" data-labelkey="p1_teaser2_header">Три эффективных теста</h2>

                    <p class="label" data-labelkey="p1_teaser2_copy">Проверка зрения онлайн делится на три части:</p>

                    <ul>
                        <li class="label" data-labelkey="p1_teaser2_listitem1">Проверка Остроты Зрения</li>

                        <li class="label" data-labelkey="p1_teaser2_listitem2">Проверка Контрастности Зрения</li>

                        <li class="label" data-labelkey="p1_teaser2_listitem3">Проверка Цветовосприятия</li>
                    </ul>
                    <span class="icon"></span> </div>

                <div style="display: none;" id="p1_teaser3Voucher" class="voucher">
                    <h2 class="label" data-labelkey="p1_teaser3_header_voucher">Получить Купон</h2>

                    <p class="label voucher1" data-labelkey="p1_teaser3_copy_voucher1">[no label]</p>

                    <p class="label voucher2" data-labelkey="p1_teaser3_copy_voucher2">[no label]</p>

                    <p class="label voucher3" data-labelkey="p1_teaser3_copy_voucher3">В конце теста, Вы сможете найти ближайшую ZEISS оптику.</p>
                    <span class="icon"></span> </div>

                <div id="p1_teaser3NoVoucher" class="noVoucher">
                    <h2 class="label" data-labelkey="p1_teaser3_header_no_voucher">Совет</h2>

                    <p class="label" data-labelkey="p1_teaser3_copy_no_voucher">Проверяйте зрение регулярно у ближайшего к Вам ZEISS офтальмолога.</p>
                    <span class="icon"></span> </div>

                <div id="p1_teaser3Germany">
                    <h2>конкурс</h2>

                    <p>100 лет ZEISS очков. 100 лет, чтобы лучше видеть. Это требует
                        праздника! После завершения онлайн-визуальный осмотр может принять
                        участие в нашем конкурсе и выиграть при определенном везении один из 100
                        призов!</p>
                </div>
            </div>

            <div style="display: none;" id="oldBrowserFallback">
                <h1 class="label" data-labelkey="p1_old_browser_fallback_headline">Ваш веб-браузер не поддерживается.</h1>

                <p class="label" data-labelkey="p1_old_browser_fallback_copy1">Для проведения ZEISS Онлайн Проверки Зрения, Вам понадобится современный веб-браузер.</p>

                <p class="label" data-labelkey="p1_old_browser_fallback_copy2">Последние версии поддерживаемых браузеров можно найти тут:</p>

                <ul id="alternativeBrowserList">
                    <li class="first"> <a id="alternativeBrowser1" href="#" target="_blank" class="nolinkrewrite"> <strong class="label" data-labelkey="p1_old_browser_fallback_browser1_title">Chrome</strong> <span class="label" data-labelkey="p1_old_browser_fallback_browser1_company">Google</span> </a> </li>

                    <li> <a id="alternativeBrowser2" href="#" target="_blank" class="nolinkrewrite"> <strong class="label" data-labelkey="p1_old_browser_fallback_browser2_title">Firefox</strong> <span class="label" data-labelkey="p1_old_browser_fallback_browser2_company">Mozilla</span> </a> </li>

                    <li> <a id="alternativeBrowser3" href="#" target="_blank" class="nolinkrewrite"> <strong class="label" data-labelkey="p1_old_browser_fallback_browser3_title">Internet Explorer</strong> <span class="label" data-labelkey="p1_old_browser_fallback_browser3_company">Microsoft</span> </a> </li>

                    <li class="last"> <a id="alternativeBrowser4" href="#" target="_blank" class="nolinkrewrite"> <strong class="label" data-labelkey="p1_old_browser_fallback_browser4_title">Safari</strong> <span class="label" data-labelkey="p1_old_browser_fallback_browser4_company">Apple</span> </a> </li>
                </ul>

                <p class="label" data-labelkey="p1_old_browser_fallback_copy3">Пожалуйста, свяжитесь со своим системным администратором, если у Вас не достаточно прав.</p>
            </div>
        </div>

        <!-- Kalibrierung -->

        <div style="display: none;" id="p3" class="cPage">
            <div class="copyText">
                <p class="label" data-labelkey="p3_copy">Так как настройки
                    мониторов и цветовой гаммы различаются на каждом компьютере, то для
                    правильного отображения теста и получения лучшего результата
                    тестирования необходимы предварительные настройки. Пожалуйста,
                    ознакомьтесь с  тремя последующими шагами и Выполните инструкции. Как
                    только Вы закончите, мы сразу же Вычислим правильные настройки экрана
                    для прохождения тестирования.</p>

                <p class="label" data-labelkey="p3_box3_copy">Если Вы используете
                    какие-либо корректоры зрения (очки или контактные линзы), Вы должны их
                    одеть во время проведения теста. Если Вы используете больше чем один
                    корректор зрения, то, пожалуйста, оденьте тот, в котором Вы обычно
                    работаете за компьютером.</p>
            </div>

            <div id="p3_box1" class="borderBox">
                <div class="borderBoxInner">
                    <h2 class="label" data-labelkey="p3_box1_header">1. Калибровка экрана</h2>

                    <p class="label" data-labelkey="p3_box1_copy">Отрегулируйте
                        расстояние между двумя линиями, используя клавиши +/-. Дистанция между
                        ними должна быть ровно 5,4 см. Чтобы точно измерить это расстояние,
                        можете воспользоваться сантиметром, или наименьшей стороной кредитной
                        или дебетовой карты.</p>

                    <div id="csContainer">
                        <div id="csOuterWrap">
                            <div id="csDimension"><img src="/check/resources/creditcard.gif" alt="" width="100%"><span class="dimensionValue">2.125 inch</span></div>

                            <div id="csExpand" class="plusButton buttonGrey"><span class="buttonGreyInner">+</span></div>

                            <div id="csNarrow" class="minusButton buttonGrey"><span class="buttonGreyInner">-</span></div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="p3_box2" class="borderBox">
                <div class="borderBoxInner">
                    <h2 class="label" data-labelkey="p3_box2_header">2. Калибровка цвета</h2>

                    <p class="label" data-labelkey="p3_box2_copy">Отрегулируйте яркость центрального окошка как на полосатой рамке с помощью ползунка.</p>

                    <div id="ccContainer">
                        <div id="ccOuterWrap">
                            <div id="ccOuter">
                                <div id="ccInnerBg"></div>

                                <div id="ccInner"></div>
                            </div>

                            <div id="ccSlider"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="p3_box4" class="borderBox">
                <div class="borderBoxInner">
                    <h2 class="label" data-labelkey="p3_box4_header">3. Расстояние до экрана</h2>

                    <p class="label" data-labelkey="p3_box4_copy">Пожалуйста, сядьте на расстояние 70 - 100 см от экрана. Это позволит достичь лучших результатов.</p>
                    <span class="deskImage"></span> </div>
            </div>

            <div id="p3_nextStepWrap">
                <div class="checkboxWrap">
                    <div class="checkboxWrapInner"> <input id="p3_startCheckbox" type="checkbox"> <label for="dummyForAttr"> <span class="styledCheckbox"></span> <span class="label" data-labelkey="p3_checkbox_label">Я подтверждаю, что для достижения наилучшего результата тестирования я следовал всем инструкциям по установке.</span> </label> </div>

                    <p id="p3_openDisclaimer" class="label" data-labelkey="p3_open_disclaimer">Я прочитал и согласен с отказом от ответственности.</p>
                </div>

                <div id="p3_startButton" class="nextStepButton"> <span class="buttonBlue"> <span class="label" data-labelkey="p3_start_button">Готово и продолжить</span> </span> </div>
            </div>

            <div id="p3_disclaimer_layer" style="display: none; ">
                <div class="layerContentInner">
                    <div class="layerTop"></div>

                    <div class="layerMiddle">
                        <h2 class="label" data-labelkey="p2_headline">Отказ от ответственности</h2>

                        <p class="label" data-labelkey="p2_copy1">Эта онлайн проверка
                            зрения не является медицинским обследованием и не может заменить
                            обследование специально обученным профессионалом. Она не предназначена
                            для использования в постановке диагнозов заболеваний или других
                            состояний, а также для лечения или профилактики заболевания. Эта
                            проверка только показывает общее состояние Вашего зрения. Мы рекомендуем
                            использовать эту проверку в сочетании с полноценным обследованием у
                            офтальмолога.</p>

                        <h3 class="label" data-labelkey="p2_headline2">Независимое Медицинское Заключение</h3>

                        <p class="label" data-labelkey="p2_copy2">Проверка зрения,
                            предоставляемая Carl Zeiss Vision, Inc. не является заменой
                            профессионального решения оптиков, офтальмологов и офтальмологов при
                            диагностике и лечении пациентов. Оптики, офтальмологи несут полную
                            ответственность за все медицинские и диагностические решения, и
                            выписанные ими рецепты.  Проверка зрения и все представленное тут не
                            должно рассматриваться как вмешательство, ограничение или любое другое
                            воздействие на профессиональную ответственность к каждому отдельно
                            взятому пациенту или на медицинские заключения, сделанные от лица
                            пациента. Оптики, офтальмологи всегда должны проводить свои собственные,
                            ни от кого не зависящие медицинские заключения.</p>

                        <div id="p3_closeDisclaimerLayer" class="closeLayer"></div>
                    </div>

                    <div class="layerBottom"></div>
                </div>
            </div>
        </div>

        <!-- Test 1 -->

        <div style="display: none;" id="p4" class="cPage">
            <div class="infoBoxSet">
                <div class="adviceDistance borderBox">
                    <div class="borderBoxInner"></div>
                </div>

                <div class="adviceGlasses borderBox">
                    <div class="borderBoxInner"></div>
                </div>
            </div>

            <div id="t1Container" class="tContainer">
                <div id="t1Wrap" class="tWrap"> <canvas height="315" width="310" id="t1Canvas" class="tCanvas"></canvas>
                    <div id="contrastElWrap" class="letterWrap" style="width: 100px; height: 100px; margin-top: -50px; margin-left: -50px; ">
                        <div id="contrastEl" class="l">
                            <div class="mask mask1"></div>

                            <div class="mask mask2"></div>
                        </div>
                    </div>

                    <div id="t1Top" class="tTop"></div>

                    <div id="t1Bottom" class="tBottom"></div>

                    <div id="t1Left" class="tLeft"></div>

                    <div id="t1Right" class="tRight"></div>
                </div>
            </div>

            <div class="howToUseNote">
                <p class="label copyTextBig" data-labelkey="p4_copy">Используйте
                    клавиатуру или мышь для Выбора направления открытой стороны стрелки “E”.
                    Ваш результат появится как только мы оценим остроту Вашего зрения.</p>
            </div>

            <div id="p4_instruct_layer" style="display: none; ">
                <div class="layerContentInner">
                    <div class="layerTop"></div>

                    <div class="layerMiddle">
                        <h2 class="label" data-labelkey="p4_instruct_headline">Введение</h2>

                        <p class="label" data-labelkey="p4_instruct_copy1">Начните
                            проверку Вашей текущей остроты зрения. Острота зрения показывает, можете
                            ли видеть все чётко, или же вам нужно скорректировать зрение очками,
                            которые будут оптимально подобраны под Ваше зрение.</p>

                        <p class="label" data-labelkey="p4_instruct_copy2">Сейчас мы покажем “E” в разных размерах и проверим остроту Вашего зрения.</p>

                        <div class="buttonWrap p4_closeLayerButton"> <span class="buttonBlue"> <span class="label" data-labelkey="p4_instruct_button">Начать проверку остроты зрения</span> </span> </div>
                    </div>

                    <div class="layerBottom"></div>
                </div>
            </div>
        </div>

        <!-- Ergebniss 1 -->

        <div style="display: none;" id="p5" class="cPage">
            <div class="visual"></div>

            <div id="r1Container">
                <div class="r1_result r1_result0">
                    <p class="label copyTextBig" data-labelkey="p5_result0">Острота
                        Вашего зрения ниже оптимальной. Тем не менее, Вы, возможно, сможете
                        видеть лучше с очками ZEISS, подобранными специально под Ваше зрение. На
                        рисунке выше показано, как Вы сможете видеть, одев специально
                        подобранные очки. Ваше зрение через такие очки будет четким и ясным.</p>
                </div>

                <div class="r1_result r1_result1">
                    <p class="label copyTextBig" data-labelkey="p5_result1">Острота
                        Вашего зрения не совсем оптимальна. Тем не менее, Вы возможно можете
                        видеть лучше с очками ZEISS, подобранными специально под Ваше зрение. На
                        рисунке Выше показано, как Вы сможете видеть, одев специально
                        подобранные очки. Ваше зрение через такие очки будет четким и ясным.</p>
                </div>

                <div class="r1_result r1_result2">
                    <p class="label copyTextBig" data-labelkey="p5_result2">Острота
                        Вашего зрения оптимальна. Вы легко можете читать даже мелкий шрифт. Тем
                        не менее, мы рекомендуем регулярно проверять зрение у офтальмолога, так
                        как оно может измениться.</p>
                </div>
            </div>

            <p class="imageDisclaimer label" data-labelkey="image_disclaimer">Изображение только для демонстрации</p>

            <div id="p5_nextStepWrap" class="nextStepButton">
                <div id="p5_nextButton" class="buttonBlue"> <span class="label" data-labelkey="continue">Продолжить</span> </div>
            </div>
        </div>

        <!-- Test 2 -->

        <div style="display: none;" id="p6" class="cPage">
            <div class="infoBoxSet">
                <div class="adviceDistance adviceDistance2 borderBox">
                    <div class="borderBoxInner"></div>
                </div>

                <div class="adviceGlasses borderBox">
                    <div class="borderBoxInner"></div>
                </div>
            </div>

            <div id="t2Container" class="tContainer">
                <div id="t2Wrap" class="tWrap"> <canvas height="315" width="310" id="t2Canvas" class="tCanvas"></canvas>
                    <div id="circleElWrap" class="circleWrap" style="width: 100px; height: 100px; margin-left: -50px; margin-top: -50px; ">
                        <div id="circleEl" class="openLeft" style="background-color: rgb(55, 55, 55); "> <img src="/check/resources/circle_bg.png" alt="">
                            <div class="mask"></div>
                        </div>
                    </div>

                    <div id="t2Top" class="tTop"></div>

                    <div id="t2Bottom" class="tBottom"></div>

                    <div id="t2Left" class="tLeft"></div>

                    <div id="t2Right" class="tRight"></div>
                </div>
            </div>

            <div class="howToUseNote">
                <p class="label copyTextBig" data-labelkey="p6_copy">Используйте
                    клавиатуру или мышь для Выбора направления открытой стороны стрелки “C”.
                    Ваш результат появится, как только мы оценим остроту Вашего зрения.</p>
            </div>

            <div id="p6_instruct_layer" style="display: none; ">
                <div class="layerContentInner">
                    <div class="layerTop"></div>

                    <div class="layerMiddle">
                        <h2 class="label" data-labelkey="p6_instruct_headline">Введение</h2>

                        <p class="label" data-labelkey="p6_instruct_copy1">Теперь Вы имеете представление как можно улучшить остроту Вашего зрения. Давайте перейдем ко второму тесту.</p>

                        <p class="label" data-labelkey="p6_instruct_copy2">Хорошая контрастность зрения также важна для комфортного зрения - не только чтения, но также зрения в даль или ночью. </p>

                        <p class="label" data-labelkey="p6_instruct_copy3">Теперь на экране мы покажем “C” разные уровни контрастности и проверим контрастность Вашего зрения.</p>

                        <div class="buttonWrap"> <span class="buttonBlue p6_closeLayerButton"> <span class="label" data-labelkey="p6_instruct_button">Начать проверку контрастности зрения</span> </span> </div>
                    </div>

                    <div class="layerBottom"></div>
                </div>
            </div>
        </div>

        <!-- Ergebniss 2 -->

        <div style="display: none;" id="p7" class="cPage">
            <div class="visual"></div>

            <div id="r2Container">
                <div class="iScription">
                    <p class="label r2_result r2_result0 copyTextBig" data-labelkey="p7_result0_iscription">Контрастность
                        Вашего зрения  не совсем оптимальна; возможно, Вы можете видеть лучше в
                        очках ZEISS с линзами, использующие i.Scription® технологию. На рисунке
                        показана контрастность Вашего зрения  – особенно ночью – можно улучшить</p>

                    <p class="label r2_result r2_result1 copyTextBig" data-labelkey="p7_result1_iscription">Контрастность
                        Вашего зрения в норме, возможно, Вы сможете видеть лучше в очках ZEISS с
                        линзами, использующими i.Scription® технологию. На рисунке показана
                        контрастность Вашего зрения  – особенностью ночью – можно улучшить</p>

                    <p class="label r2_result r2_result2 copyTextBig" data-labelkey="p7_result2_iscription">Контрастность Вашего зрения оптимальна.</p>
                </div>

                <div style="display: none;" class="noIScription">
                    <p class="label r2_result r2_result0 copyTextBig" data-labelkey="p7_result0">Контрастность
                        Вашего зрения ниже оптимальной; возможно Вы сможете видеть лучше в
                        очках ZEISS с линзами, использующими антибликовую технологию. На рисунке
                        показан контраст Вашего зрения  –, особенно, когда Вы были ослеплены
                        ярким светом ночью – можно улучшить.</p>

                    <p class="label r2_result r2_result1 copyTextBig" data-labelkey="p7_result1">Контрастность
                        Вашего зрения в норме, но возможно Вы чувствовали бы себя лучше в очках
                        ZEISS с линзами, использующими антибликовую технологию. На рисунке
                        показана контрастность Вашего зрения  –, особенно, если бы Вы были
                        ослеплены ярким светом ночью – можно улучшить.</p>

                    <p class="label r2_result r2_result2 copyTextBig" data-labelkey="p7_result2">Контрастность Вашего зрения оптимальна.</p>
                </div>
            </div>

            <p class="imageDisclaimer label" data-labelkey="image_disclaimer">Изображение только для демонстрации</p>

            <div id="p7_nextStepWrap" class="nextStepButton">
                <div id="p7_nextButton" class="buttonBlue"> <span class="label" data-labelkey="continue">Продолжить</span> </div>
            </div>
        </div>

        <!-- Test 3 -->

        <div style="display: none;" id="p8" class="cPage">
            <div class="infoBoxSet">
                <div class="adviceDistance adviceDistance2 borderBox">
                    <div class="borderBoxInner"></div>
                </div>

                <div class="adviceGlasses borderBox">
                    <div class="borderBoxInner"></div>
                </div>
            </div>

            <div id="t3Container" class="tContainer">
                <div id="t3Wrap" class="tWrap">
                    <div id="t3ImgWrap">
                        <div id="t3Img"></div>
                    </div>

                    <div id="t3Top" class="tTop"></div>

                    <div id="t3Bottom" class="tBottom"></div>

                    <div id="t3Left" class="tLeft"></div>

                    <div id="t3Right" class="tRight"></div>
                </div>
            </div>

            <div class="howToUseNote">
                <p class="label copyTextBig" data-labelkey="p8_copy">Используйте
                    клавиатуру или мышь для Выбора направления открытой стороны стрелки “C”.
                    Ваш результат появится как только мы оценим остроту Вашего зрения.</p>
            </div>

            <div id="p8_instruct_layer" style="display: none; ">
                <div class="layerContentInner">
                    <div class="layerTop"></div>

                    <div class="layerMiddle">
                        <h2 class="label" data-labelkey="p8_instruct_headline">Введение</h2>

                        <p class="label" data-labelkey="p8_instruct_copy1">Большинство людей легко различают зеленый, красный или голубой цвета. Однако, некоторые испытывают затруднения с этим.</p>

                        <p class="label" data-labelkey="p8_instruct_copy2">Проблемы с
                            цветовосприятием обычно появляются у людей в очень юном возрасте. Однако
                            это не всегда так. Используя проверку цветовосприятия, определите, есть
                            ли у Вас проблемы или нет.</p>

                        <p class="label" data-labelkey="p8_instruct_copy3">Теперь мы покажем  на экране “C” различные цветовые уровни контраста и проверим Ваше цветовосприятие.</p>

                        <div class="buttonWrap"> <span class="buttonBlue p8_closeLayerButton"> <span class="label" data-labelkey="p8_instruct_button">Проверьте теперь Ваше цветовосприятие</span> </span> </div>
                    </div>

                    <div class="layerBottom"></div>
                </div>
            </div>
        </div>

        <!-- Ergebniss 3 -->

        <div style="display: none;" id="p9" class="cPage">
            <div class="visual"></div>

            <div id="r3Container">
                <div class="r3_result r3_result0">
                    <p class="label copyTextBig" data-labelkey="p9_result0_1">У Вас есть проблемы с цветовосприятием. Пожалуйста, посетите офтальмолога для дальнейшей диагностики.</p>

                    <p class="label copyTextBig" data-labelkey="p9_result0_2">К
                        сожалению, проблемы с цветовосприятием не могут быть исправлены очками
                        или контактными линзами. Тем не менее, существует множество советов по
                        улучшению Вашей повседневной жизни, несмотря на проблемы с
                        цветовосприятием.</p>
                </div>

                <div class="r3_result r3_result2">
                    <p class="label copyTextBig" data-labelkey="p9_result2">У вас нет проблем с цветовосприятием и Вы можете воспринимать все в полном цвете.</p>
                </div>
            </div>

            <p class="imageDisclaimerFotolia">@Argonautis - Fotolia.com</p>

            <div id="p9_nextStepWrap" class="nextStepButton">
                <div id="p9_nextButton" class="buttonBlue"> <span class="label" data-labelkey="p9_nextButton">Результат</span> </div>
            </div>
        </div>

        <!-- Ergebniss ?bersicht -->

        <div style="display: none;" id="p10" class="cPage">
            <div class="visual"></div>

            <div id="p10_resultImage">
                <div id="p10_resultTest1" class="resultBox">
                    <h4 class="label" data-labelkey="p10_result_t1_headline">Острота зрения</h4>
                    <span class="label p10_otimum" data-labelkey="optimum">Оптимум</span> <span class="label p10_you" data-labelkey="you">Вы</span> <span class="p10_tooltip">
          <p class="label r1_result r1_result0" data-labelkey="p10_r1_result0"></p>

          <p class="label r1_result r1_result1" data-labelkey="p10_r1_result1"></p>

          <p class="label r1_result r1_result2" data-labelkey="p10_r1_result2"></p>
         </span> </div>

                <div id="p10_resultTest2" class="resultBox">
                    <h4 class="label" data-labelkey="p10_result_t2_headline">Контрастность зрения</h4>
                    <span class="label p10_otimum" data-labelkey="optimum">Оптимум</span> <span class="label p10_you" data-labelkey="you">Вы</span> <span class="p10_tooltip">
          <p class="label r2_result r2_result0" data-labelkey="p10_r2_result0"></p>

          <p class="label r2_result r2_result1" data-labelkey="p10_r2_result1"></p>

          <p class="label r2_result r2_result2" data-labelkey="p10_r2_result2"></p>
         </span> </div>

                <div id="p10_resultTest3" class="resultBox">
                    <h4 class="label" data-labelkey="p10_result_t3_headline">Цветовосприятие</h4>
                    <span class="label p10_otimum" data-labelkey="optimum">Оптимум</span> <span class="label p10_you" data-labelkey="you">Вы</span> <span class="p10_tooltip">
          <p class="label r3_result r3_result0" data-labelkey="p10_r3_result0"></p>

          <p class="label r3_result r3_result2" data-labelkey="p10_r3_result2"></p>
         </span> </div>
            </div>

            <div id="p10_finalResult"> <span id="p10_finalResultCopy" class="label" data-labelkey="p10_rf_headline">Благодарим Вас за участие в онлайн проверке зрения ZEISS.</span> <span id="p10_finalResultCopyStrong" class="label" data-labelkey="p10_rf_headline_strong">Ваши результаты:</span>
                <div id="p10_box1">
                    <h4 class="label rf_result rf_result0" data-labelkey="p10_rf_result0">Показатели Вашего зрения ниже оптимальных.</h4>

                    <p class="label rf_result rf_result0" data-labelkey="p10_rf_result0_copy">Мы рекомендуем Вам записаться на прием к офтальмологу для более тщательной проверки зрения.</p>

                    <h4 class="label rf_result rf_result1" data-labelkey="p10_rf_result1">Можно улучшить показатели Вашего зрения.</h4>

                    <p class="label rf_result rf_result1" data-labelkey="p10_rf_result1_copy">Мы
                        советуем Вам провести более тщательную проверку зрения у офтальмолога и
                        проконсультироваться, как улучшить показатели Вашего зрения.</p>

                    <h4 class="label rf_result rf_result2" data-labelkey="p10_rf_result2">Поздравляем! У Вас хорошие показатели зрения.</h4>

                    <p class="label rf_result rf_result2" data-labelkey="p10_rf_result2_copy">Тем не менее, необходимо ежегодно проходить полную проверку зрения у офтальмолога.</p>
                </div>

                <div id="p10_box2">
                    <p class="label" data-labelkey="p10_rf_please_remember">Пожалуйста,
                        помните, что эта онлайн проверка зрения ZEISS  предназначена только для
                        быстрой проверки показателей Вашего зрения. Она не может использоваться
                        вместо полноценной проверки зрения ZEISS офтальмологом. Для того чтобы
                        получить рецепт на очки и проверить свое зрение, пожалуйста, посетите
                        специалиста.</p>
                </div>

                <div id="p10_box3">
                    <p style="display: none;" class="label voucher voucher1" data-labelkey="p10_get_voucher_copy_voucher1">[no label]</p>

                    <p style="display: none;" class="label voucher voucher2" data-labelkey="p10_get_voucher_copy_voucher2">[no label]</p>

                    <p style="display: none;" class="label voucher voucher3" data-labelkey="p10_get_voucher_copy_voucher3">Получите
                        купон на бесплатное прохождение профессионального глазного обследования
                        или на обновление продукта у ближайшего к Вам  ZEISS оптика или
                        офтальмолога.</p>

                    <div style="display: none;" id="p10_getVoucherWrap" class="buttonWrap voucher"> <span class="buttonBlue"> <span id="p10_getVoucherButton" class="label" data-labelkey="p10_get_voucher_button">Здесь Вы можете получить свой купон</span> </span> </div>

                    <p class="label noVoucher" data-labelkey="p10_find_dealer_copy">Мы
                        хотим показать вам, как инновации ZEISS могут помочь вам улучшить
                        зрение. На следующей странице Вы сможете найти ближайшую к Вам оптику.</p>

                    <div id="p10_findDealerWrap" class="buttonWrap noVoucher"> <span class="buttonBlue"> <span id="p10_findDealerButton" class="label trClick" data-labelkey="p10_find_dealer_button">Проконсультироваться со специалистом</span> </span> </div>
                </div>
            </div>
        </div>

        <div style="opacity: 0; display: none;" id="loadingScreen"></div>

        <div id="orientationErrorWrap">
            <div id="orientationError" style="display: none; ">
                <div class="layerContentInner">
                    <div class="layerTop"></div>

                    <div class="layerMiddle">
                        <h2 class="label" data-labelkey="orientation_error_headline">Пожалуйста, измените ориентацию на альбомную</h2>

                        <p class="label" data-labelkey="orientation_error_copy">Для
                            более качественного отображения Онлайн Проверки Зрения, пожалуйста,
                            расположите монитор горизонтально, повернув его на 90 градусов</p>
                    </div>

                    <div class="layerBottom"></div>
                </div>
            </div>
        </div>
    </div>
</div>

</body></html>