<div dir="ltr" lang="ru-RU"><p style="margin-top:0.19in;margin-bottom:0.19in;">
        <br><br>
    </p>
    <p style="margin-top:0.19in;margin-bottom:0.19in;">
        <font face="Times New Roman, serif"><font size="3" style="font-size:12pt;"><b><font face="Arial, serif"><font
                                size="2" style="font-size:10pt;"><u>Текст
                                «Договор оферты»:</u></font></font></b></font></font></p>
    <p style="margin-top:0.19in;margin-bottom:0.19in;">
        <font face="Times New Roman, serif"><font size="3" style="font-size:12pt;"><b><font face="Arial, serif"><font
                                size="2" style="font-size:10pt;">Договор
                            публичной
                            оферты на получение
                            и использование
                            электронной
                            бонусной карты.</font></font></b></font></font></p>
    <p style="margin-top:0.19in;margin-bottom:0.19in;">
        <font face="Times New Roman, serif"><font size="3" style="font-size:12pt;"><font face="Arial, serif"><font
                            size="2" style="font-size:10pt;">Я
                        информирован(а)
                        и согласен(на)
                        с тем, что целью
                        обработки и
                        использования
                        моих персональных
                        данных является
                        обеспечение
                        работы бонусной
                        программы и
                        предоставление
                        мне информации
                        и персональных
                        приложений
                        исключительно
                        в рамках моего
                        участия в бонусной
                        программе. Я
                        согласен(на)
                        на обработку
                        моих персональных
                        данных, указанных
                        в настоящей
                        заявке, с помощью
                        средств автоматизации
                        и электронно-вычислительной
                        техники и
                        использование,
                        в том числе для
                        проведения
                        рассылок с
                        применением&nbsp;SMS-услуг
                        операторов
                        мобильной
                        связи, электронной
                        почты и почтовой
                        связи. Мое согласие
                        действует до
                        его отзыва,
                        оформленного
                        мною в письменном
                        виде и направленного
                        по почтовому
                        адресу.</font></font></font></font></p>
    <p style="margin-top:0.19in;margin-bottom:0.19in;">
        <font face="Times New Roman, serif"><font size="3" style="font-size:12pt;"><font face="Arial, serif"><font
                            size="2" style="font-size:10pt;">1.&nbsp;Общие
                        положения</font></font></font></font></p>
    <p style="margin-top:0.19in;margin-bottom:0.19in;">
        <font face="Times New Roman, serif"><font size="3" style="font-size:12pt;"><font face="Arial, serif"><font
                            size="2" style="font-size:10pt;">1.1
                        Настоящий
                        Договор является
                        публичным
                        договором-офертой
                        (предложением)
                        в адрес любого
                        физического
                        лица (в дальнейшем
                        Держатель) на
                        получение и
                        дальнейшее
                        использование
                        электронной
                        бонусной карты,
                        по которой
                        производится
                        начисление
                        и списание
                        бонусов при
                        совершении
                        покупок.&nbsp;(далее
                        – карта).&nbsp;Электронной
                        бонусной картой
                        является
                        индивидуальный
                        набор символов,
                        высылаемых
                        Держателю в
                        соответствии
                        с п.1.2 Договора
                        – оферты.<br>1.2. Факт
                        получения карты
                        Держателем
                        является полным
                        и безоговорочным
                        акцептом (принятием)
                        данного Договора,
                        т. е. Держатель,
                        получивший
                        карту по SMS-запросу
                        или через оператора
                        компании, считается
                        ознакомившимся
                        с настоящим
                        публичным
                        Договором и,
                        в&nbsp;соответствии
                        с Гражданским
                        Кодексом Российской
                        Федерации,
                        рассматривается
                        как лицо, вступившее
                        с компанией
                        в договорные
                        отношения на
                        основании
                        настоящего
                        Договора публичной
                        оферты.</font></font></font></font></p>
    <p style="margin-top:0.19in;margin-bottom:0.19in;">
        <font face="Times New Roman, serif"><font size="3" style="font-size:12pt;"><b><font face="Arial, serif"><font
                                size="2" style="font-size:10pt;">2.&nbsp;Права
                            и обязанности
                            Держателя
                            электронной
                            бонусной карты</font></font></b></font></font></p>
    <p style="margin-top:0.19in;margin-bottom:0.19in;">
        <font face="Times New Roman, serif"><font size="3" style="font-size:12pt;"><font face="Arial, serif"><font
                            size="2" style="font-size:10pt;">2.1
                        Держатель имеет
                        право получать
                        бонусы за покупки,
                        совершенные
                        в салоне, в
                        количестве,
                        предусмотренном
                        условиями
                        бонусной программы.</font></font></font></font></p>
    <p style="margin-top:0.19in;margin-bottom:0.19in;">
        <font face="Times New Roman, serif"><font size="3" style="font-size:12pt;"><font face="Arial, serif"><font
                            size="2" style="font-size:10pt;">2.2
                        Держатель имеет
                        право списывать
                        бонусы в счет
                        оплаты покупок
                        в салоне в
                        количестве,
                        предусмотренном
                        условиями
                        бонусной программы.</font></font></font></font></p>
    <p style="margin-top:0.19in;margin-bottom:0.19in;">
        <font face="Times New Roman, serif"><font size="3" style="font-size:12pt;"><font face="Arial, serif"><font
                            size="2" style="font-size:10pt;">2.3
                        Держатель имеет
                        право в любой
                        момент выйти
                        из бонусной
                        программы.</font></font></font></font></p>
    <p style="margin-top:0.19in;margin-bottom:0.19in;">
        <font face="Times New Roman, serif"><font size="3" style="font-size:12pt;"><font face="Arial, serif"><font
                            size="2" style="font-size:10pt;">2.4
                        Держатель
                        соглашается
                        с тем, что мобильная
                        карта в силу
                        своего формата
                        будет отправлена
                        в виде SMS-сообщения
                        на мобильный
                        телефон, номер
                        которого Держатель
                        предоставляет
                        компании.<br>2.5
                        Держатель
                        подтверждает,
                        что предоставленный
                        им номер мобильного
                        телефона принадлежит
                        ему и является
                        действующим.
                        В случае смены
                        SIM-карты Держатель
                        обязуется
                        уведомить
                        оператора
                        компании перед
                        совершением
                        заказа по карте
                        для восстановления
                        и перерегистрации
                        карты.<br>2.6 При
                        утере мобильного
                        телефона или
                        SIM-карты, на который
                        была оформлена
                        карта, Держатель
                        обязуется в
                        кратчайшее
                        время уведомить
                        об этом оператора
                        компании.<br>2.7 Для
                        зачисления
                        бонусов на
                        карту Держатель
                        обязуется
                        своевременно
                        использовать
                        номер мобильного
                        телефона, на
                        который была
                        зарегистрирована
                        карта, в форме
                        заказа на сайте.<br>2.8
                        Для списания
                        бонусов Держатель
                        обязуется до
                        совершения
                        покупки по
                        карте использовать
                        номер мобильного
                        телефона, на
                        который была
                        зарегистрирована
                        карта, в форме
                        заказа на сайте.<br>2.9
                        Держатель
                        согласен получать
                        от компании
                        дополнительную
                        информацию
                        на свой мобильный
                        телефон, такую,
                        как: информация
                        о начислении
                        и списании
                        бонусов, уведомления
                        о приближении
                        истечения срока
                        действия бонусов,
                        уведомления
                        об изменениях
                        условий действия
                        бонусной программы,
                        а также подарки
                        в виде дополнительных
                        бонусов и привилегий,
                        предоставляемых
                        по карте
                        компаниями-партнерами.</font></font></font></font></p>
    <p style="margin-top:0.19in;margin-bottom:0.19in;">
        <font face="Times New Roman, serif"><font size="3" style="font-size:12pt;"><b><font face="Arial, serif"><font
                                size="2" style="font-size:10pt;">3.&nbsp;Обязанности</font></font></b></font></font></p>
    <p style="margin-top:0.19in;margin-bottom:0.19in;">
        <font face="Times New Roman, serif"><font size="3" style="font-size:12pt;"><font face="Arial, serif"><font
                            size="2" style="font-size:10pt;">3.1
                        Компания обязуется
                        обеспечить
                        прием карт и
                        возможность
                        начисления
                        и списания
                        бонусов по
                        карте Держателя
                        при выполнении
                        им условий,
                        описанных в
                        данном Договоре.<br>3.2
                        Своевременно
                        уведомлять
                        с помощью
                        SMS-сообщений
                        о&nbsp;возможных
                        изменениях
                        в работе бонусной
                        программы и
                        приближении
                        истечения срока
                        действия бонусов.<br>3.3
                        Не разглашать
                        и не передавать
                        третьим лицам
                        и организациям
                        персональную
                        информацию,
                        передаваемую
                        держателем
                        карты в ходе
                        исполнения
                        Договора.</font></font></font></font></p>
    <p style="margin-top:0.19in;margin-bottom:0.19in;">
        <font face="Times New Roman, serif"><font size="3" style="font-size:12pt;"><b><font face="Arial, serif"><font
                                size="2" style="font-size:10pt;">4.&nbsp;Порядок
                            списания и
                            начисления
                            бонусов</font></font></b></font></font></p>
    <p style="margin-top:0.19in;margin-bottom:0.19in;">
        <font face="Times New Roman, serif"><font size="3" style="font-size:12pt;"><font face="Arial, serif"><font
                            size="2" style="font-size:10pt;">4.1.
                        Один бонус,
                        начисленный
                        на карту, дает
                        право на приобретение
                        товаров.<br>4.2. Правом
                        на получения
                        карты обладает
                        любое физическое
                        лицо, назвавшее
                        консультанту
                        номер своего
                        мобильного
                        телефона, к
                        которому будет
                        привязана
                        карта.<br>4.3. Бонусы
                        начисляются
                        при любом заказе,
                        совершенной
                        Держателем
                        с использованием
                        карты .</font></font></font></font></p>
    <p style="margin-top:0.19in;margin-bottom:0.19in;">
        <font face="Times New Roman, serif"><font size="3" style="font-size:12pt;"><font face="Arial, serif"><font
                            size="2" style="font-size:10pt;">4.4.
                        Компания оставляет
                        за собой право
                        как на начисление
                        дополнительных
                        бонусов, так
                        и на предоставление
                        дополнительных
                        привилегий
                        по карте.<br>4.5. Компания
                        оставляет за
                        собой право
                        на изменение
                        условий работы
                        бонусной программы
                        при оповещении
                        об этом держателей
                        карт по SMS и обновлении
                        текста настоящего
                        Договора на
                        сайте.</font></font></font></font></p>
    <p style="margin-top:0.19in;margin-bottom:0.19in;">
        <font face="Times New Roman, serif"><font size="3" style="font-size:12pt;"><b><font face="Arial, serif"><font
                                size="2" style="font-size:10pt;">5.&nbsp;Ответственность
                            сторон</font></font></b></font></font></p>
    <p style="margin-top:0.19in;margin-bottom:0.19in;">
        <font face="Times New Roman, serif"><font size="3" style="font-size:12pt;"><font face="Arial, serif"><font
                            size="2" style="font-size:10pt;">5.1
                        В случае если
                        Держатель
                        получает
                        SMS-оповещение
                        о несанкционированном
                        списании бонусов,
                        он обязан
                        незамедлительно
                        уведомить
                        оператора
                        о&nbsp;произошедшем.
                        В противном
                        случае Компания
                        не несет ответственности
                        за списанные
                        с карты Держателя
                        бонусы и не
                        компенсирует
                        их.<br>5.2 Потенциальный
                        Держатель карты
                        несет ответственность
                        за предоставление
                        действительного
                        и собственного
                        номера мобильного
                        телефона. В
                        случае предоставления
                        некорректной
                        или заведомо
                        ложной информации,
                        он несет всю
                        ответственность
                        по претензиям,
                        возникающим
                        со стороны
                        истинного
                        владельца
                        номера мобильного
                        телефона.&nbsp;<br>5.3
                        Каждая из Сторон
                        несет ответственность
                        перед другой
                        Стороной за
                        неисполнение
                        или ненадлежащее
                        исполнение
                        обязательств
                        настоящей
                        оферты в соответствии
                        с действующим
                        законодательством
                        РФ.</font></font></font></font></p>
    <p style="margin-top:0.19in;margin-bottom:0.19in;">
        <font face="Times New Roman, serif"><font size="3" style="font-size:12pt;"><b><font face="Arial, serif"><font
                                size="2" style="font-size:10pt;">6.&nbsp;Форс-мажор</font></font></b></font></font></p>
    <p style="margin-top:0.19in;margin-bottom:0.19in;">
        <font face="Times New Roman, serif"><font size="3" style="font-size:12pt;"><font face="Arial, serif"><font
                            size="2" style="font-size:10pt;">6.1
                        Стороны освобождаются
                        от ответственности
                        за частичное
                        или полное
                        неисполнение
                        обязательств
                        по Договору,
                        вызванное
                        обстоятельствами
                        непреодолимой
                        силы (форс-мажор),
                        возникших после
                        заключения
                        Договора. К
                        форс-мажорным
                        обстоятельствам,
                        в частности,
                        могут быть
                        отнесены природные
                        и промышленные
                        катастрофы,
                        пожары и наводнения,
                        прочие стихийные
                        бедствия, запреты
                        полномочных
                        государственных
                        органов, военные
                        действия и
                        гражданские
                        беспорядки,
                        террористические
                        акты, сбои в
                        работе электронного
                        оборудования
                        и средств связи,
                        произошедшие
                        вне зоны влияния
                        Сторон.</font></font></font></font></p>
    <p style="margin-top:0.19in;margin-bottom:0.19in;">
        <font face="Times New Roman, serif"><font size="3" style="font-size:12pt;"><b><font face="Arial, serif"><font
                                size="2" style="font-size:10pt;">7.&nbsp;Срок
                            действия Договора</font></font></b></font></font></p>
    <p style="margin-top:0.19in;margin-bottom:0.19in;">
        <font face="Times New Roman, serif"><font size="3" style="font-size:12pt;"><font face="Arial, serif"><font
                            size="2" style="font-size:10pt;">7.1
                        Настоящий
                        Договор вступает
                        в силу с момента
                        получения
                        Держателем
                        карты и действует
                        бессрочно до
                        момента, пока
                        Держатель не
                        изъявит желание
                        выйти из бонусной
                        программы.&nbsp;</font></font></font></font></p>
    <p style="margin-bottom:0.11in;"><br><br>
    </p></div>