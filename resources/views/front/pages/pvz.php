<?=Bootstrap3Form::open(['method' => 'GET']);?>
<?=Bootstrap3Form::select2Row('city', 'Город',
    \ActiveRecord\DeliveryPoint::select('city_name')->orderBy('city_name')->groupBy('city_name')->pluck('city_name', 'city_name'), Request::get('city'), ['onchange' => '$(event.currentTarget).parents("form").submit()'])?>
<?=Bootstrap3Form::close();?>
<?
$select = \ActiveRecord\DeliveryPoint::select();
?>
<? if (Request::get('city')):?>
<?
$select->where('city_name', Request::get('city'));
?>
<? endif;?>
<table class="table table-condensed">
    <thead>
        <tr>
            <td>Название</td>
            <td>Адрес</td>
            <td>Режим работы</td>
            <td>Телефон</td>
            <td>Курьер</td>
            <td>Только оплата</td>
        </tr>
    </thead>
<? foreach ($select->get()->groupBy('city_name') as $city => $deliveryPoints):?>
    <tr>
        <th colspan="7"><?=$city?></th>
    </tr>
    <? foreach ($deliveryPoints as $deliveryPoint):?>
        <?
        /**
         * @var \ActiveRecord\DeliveryPoint $deliveryPoint
         */
        ?>
        <tr>
            <td>
                <?=$deliveryPoint->name?>
            </td>
            <td>
                <?=$deliveryPoint->address?> <a target="_blank" href="https://maps.yandex.ru/?text=<?=$deliveryPoint->coords_x?>+<?=$deliveryPoint->coords_y?>">
                    <i class="fa fa-map-pin"></i> <?=$deliveryPoint->coords_x?> <?=$deliveryPoint->coords_y?></a>
                <?=$deliveryPoint->metro?>
            </td>
            <td>
                <?=$deliveryPoint->name?>
            </td>
            <td>
                <?=$deliveryPoint->phone?>
            </td>
            <td>
                <?=$deliveryPoint->courier ? 'Да' : 'Нет'?>
            </td>
            <td>
                <?=$deliveryPoint->only_pay? 'Да' : 'Нет'?>
            </td>
        </tr>
    <? endforeach;?>
<? endforeach;?>
</table>
