    <div class="EventItem">
        <div class="item-text">
            <img src="/upload/iblock/f14/cleaning.jpg" border="0" width="400" height="425">
            <div>
                <div>
                    <br>
                </div>

                <div><font color="#ff0000">Внимание!</font></div>

                <div><font color="#ff0000">Данная услуга бесплатна ТОЛЬКО для покупателей "Оптика Фаворит" и распространяется на товар, купленный в салонах "Оптика Фаворит".</font></div>

                <div>
                    <br>
                </div>

                <div><font color="#ff0000">
                        <br>
                    </font></div>

                <div><font color="#ff0000">
                        <br>
                    </font></div>
            </div>
        </div>

        <div class="item-salons-list">
            <div class="h3">Салоны, в которых предоставляется услуга:</div><br>
            <div class="salons-item">
                <div class="BoxTitle Title">г. Москва</div>
                <table cellspacing="1" cellpadding="10" border="0" width="100%">
                    <tbody>
                    <tr class="even">
                        <td width="40%"><a href="/salons/m_volgogradskiy_pr_t/">м. Волгоградский проспект</a></td>
                        <td width="40%">Волгоградский проспект, дом 17</td>
                        <td width="20%">
                            <div>8 (495) 671-72-37</div>
                        </td>
                    </tr>
                    <tr class="odd">
                        <td width="40%"><a href="/salons/m_dinamo/">м. Динамо</a></td>
                        <td width="40%">Ходынский бульвар, дом 4, ТЦ "АВИАПАРК", 2-й этаж</td>
                        <td width="20%">
                            <div>8 (495) 587-77-98</div>
                        </td>
                    </tr>
                    <tr class="even">
                        <td width="40%"><a href="/salons/m_marksistskaya/">м. Марксистская</a></td>
                        <td width="40%">ул. Марксистская, дом 1, ТЦ "Гастроном Таганский"</td>
                        <td width="20%">
                            <div>8 (495) 988-50-24</div>
                        </td>
                    </tr>
                    <tr class="odd">
                        <td width="40%"><a href="/salons/denezhnyy_per_dom_30/">м. Смоленская</a></td>
                        <td width="40%">Денежный переулок, дом 30</td>
                        <td width="20%">
                            <div>8 (495) 698-60-08</div>
                        </td>
                    </tr>
                    <tr class="even">
                        <td width="40%"><a href="/salons/vernadskogo/">м. Юго-Западная, м. Проспект Вернадского</a></td>
                        <td width="40%">Боровское шоссе, дом 51, ТЦ "Ново-Переделкино", 3-й этаж</td>
                        <td width="20%">
                            <div>8 (499) 737-95-23</div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="salons-item">
                <div class="BoxTitle Title">г. Балашиха</div>
                <table cellspacing="1" cellpadding="10" border="0" width="100%">
                    <tbody>
                    <tr class="even">
                        <td width="40%"><a href="/salons/prospekt_lenina/">пр-т Ленина</a></td>
                        <td width="40%">проспект Ленина, дом 22, аптека "Бринфарм"</td>
                        <td width="20%">
                            <div>8 (916) 043-26-25</div>
                        </td>
                    </tr>
                    <tr class="odd">
                        <td width="40%"><a href="/salons/ul_karla_marksa/">ул. Карла Маркса</a></td>
                        <td width="40%">ул. Карла Маркса, дом 3, аптека "Бринфарм"</td>
                        <td width="20%">
                            <div>8 (916) 043-25-50</div>
                        </td>
                    </tr>
                    <tr class="even">
                        <td width="40%"><a href="/salons/apteka_brinfarm/">ул. Свердлова</a></td>
                        <td width="40%">ул. Свердлова, дом 23, аптека “Бринфарм”</td>
                        <td width="20%">
                            <div>8 (495) 523-80-75 </div>
                        </td>
                    </tr>
                    <tr class="odd">
                        <td width="40%"><a href="/salons/apteka_brinfarm_sovetskaya/">ул. Советская</a></td>
                        <td width="40%">ул. Советская, дом 2/9, аптека "Бринфарм"</td>
                        <td width="20%">
                            <div>8 (495) 521-46-87 </div>
                        </td>
                    </tr>
                    <tr class="even">
                        <td width="40%"><a href="/salons/ul_fadeeva/">ул. Фадеева</a></td>
                        <td width="40%">ул. Фадеева, дом 5, аптека "Бринфарм"</td>
                        <td width="20%">
                            <div>8 (495) 521-93-12</div>
                        </td>
                    </tr>
                    <tr class="odd">
                        <td width="40%"><a href="/salons/tts_svetofor/">ш. Энтузиастов</a></td>
                        <td width="40%">шоссе Энтузиастов, дом 1Б, ТЦ “Светофор”, 1-й этаж</td>
                        <td width="20%">
                            <div>8 (495) 525-41-53</div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="salons-item">
                <div class="BoxTitle Title">г. Дзержинский</div>
                <table cellspacing="1" cellpadding="10" border="0" width="100%">
                    <tbody>
                    <tr class="even">
                        <td width="40%"><a href="/salons/dzerzhinskiy/">ул. Лермонтова</a></td>
                        <td width="40%">ул. Лермонтова, дом 24а, ТЦ "Камея", 2-й этаж</td>
                        <td width="20%">
                            <div>8 (916) 614-06-35</div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="salons-item">
                <div class="BoxTitle Title">г. Железнодорожный</div>
                <table cellspacing="1" cellpadding="10" border="0" width="100%">
                    <tbody>
                    <tr class="even">
                        <td width="40%"><a href="/salons/apteka_brinfarm_zhd/">ул. Колхозная</a></td>
                        <td width="40%">ул. Колхозная, дом 7, аптека “Бринфарм”</td>
                        <td width="20%">
                            <div>8 (498) 664-05-65 </div>
                        </td>
                    </tr>
                    <tr class="odd">
                        <td width="40%"><a href="/salons/ul_mayakovskogo/">ул. Маяковского</a></td>
                        <td width="40%">ул. Маяковского, дом 18</td>
                        <td width="20%">
                            <div>8 (498) 664-05-64 </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="salons-item">
                <div class="BoxTitle Title">г. Жуковский</div>
                <table cellspacing="1" cellpadding="10" border="0" width="100%">
                    <tbody>
                    <tr class="even">
                        <td width="40%"><a href="/salons/zhukovskiy/">ул. Гагарина, д.67</a></td>
                        <td width="40%">ул. Гагарина, дом 67, ТЦ "Океан", 3-й этаж</td>
                        <td width="20%">
                            <div>8 (498) 483-50-42</div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="salons-item">
                <div class="BoxTitle Title">г. Ивантеевка</div>
                <table cellspacing="1" cellpadding="10" border="0" width="100%">
                    <tbody>
                    <tr class="even">
                        <td width="40%"><a href="/salons/trts_gagarin/">Советский пр-т</a></td>
                        <td width="40%">Советский проспект, дом 2А, ТРЦ "Гагарин", 1-й этаж</td>
                        <td width="20%">
                            <div>8 (496) 506-17-04</div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="salons-item">
                <div class="BoxTitle Title">г. Красногорск</div>
                <table cellspacing="1" cellpadding="10" border="0" width="100%">
                    <tbody>
                    <tr class="even">
                        <td width="40%"><a href="/salons/krasnogorsk/">ул. 50-летия Октября</a></td>
                        <td width="40%">ул. 50-летия Октября, дом 12, ТЦ "Парк", 1-й этаж</td>
                        <td width="20%">
                            <div>8 (498) 720-23-36</div>
                        </td>
                    </tr>
                    <tr class="odd">
                        <td width="40%"><a href="/salons/krasnogorsk_2/">ул. Ленина</a></td>
                        <td width="40%">ул. Ленина, дом 35А, ТЦ "Солнечный рай" (ТЦ "Два капитана"), 1-й этаж</td>
                        <td width="20%">
                            <div>8 (498) 505-65-25</div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="salons-item">
                <div class="BoxTitle Title">г. Лыткарино</div>
                <table cellspacing="1" cellpadding="10" border="0" width="100%">
                    <tbody>
                    <tr class="even">
                        <td width="40%"><a href="/salons/lytkarino_2/">Микрорайон 5</a></td>
                        <td width="40%">5-й микрорайон, 2-й квартал, стр.17, аптека "ГорЗдрав"</td>
                        <td width="20%">
                            <div>8 (916) 043-26-08</div>
                        </td>
                    </tr>
                    <tr class="odd">
                        <td width="40%"><a href="/salons/tts_vesna/">ул. Парковая</a></td>
                        <td width="40%">ул. Парковая, стр.2, ТЦ "Весна", 1-й этаж</td>
                        <td width="20%">
                            <div>8 (495) 580-77-17</div>
                        </td>
                    </tr>
                    <tr class="even">
                        <td width="40%"><a href="/salons/lytkarino/">ул. Советская</a></td>
                        <td width="40%">ул. Советская, дом 14, аптека "А5"</td>
                        <td width="20%">
                            <div>8 (916) 918-98-41</div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="salons-item">
                <div class="BoxTitle Title">г. Люберцы</div>
                <table cellspacing="1" cellpadding="10" border="0" width="100%">
                    <tbody>
                    <tr class="even">
                        <td width="40%"><a href="/salons/tc_gorodok/">микрорайон городок-Б</a></td>
                        <td width="40%">микрорайон городок-Б, ул. Почтовое отделение 3, дом 100, ТЦ "Городок"</td>
                        <td width="20%">
                            <div>8 (495) 741-57-48</div>
                        </td>
                    </tr>
                    <tr class="odd">
                        <td width="40%"><a href="/salons/oktyabrskiy_pr_t/">Октябрьский пр-т</a></td>
                        <td width="40%">Октябрьский проспект, дом 112, ТЦ "Выходной", 2-й этаж</td>
                        <td width="20%">
                            <div>8 (495) 775-93-01</div>
                        </td>
                    </tr>
                    <tr class="even">
                        <td width="40%"><a href="/salons/ul_initsiativnaya/">ул. Инициативная</a></td>
                        <td width="40%">ул. Инициативная, дом 7Б, ТЦ "Люберецкие торговые ряды", 1-й этаж</td>
                        <td width="20%">
                            <div>8 (498) 500-08-77</div>
                        </td>
                    </tr>
                    <tr class="odd">
                        <td width="40%"><a href="/salons/lyubertsy/">ул. Смирновская</a></td>
                        <td width="40%">ул. Смирновская, дом 17</td>
                        <td width="20%">
                            <div>8 (495) 554-80-24</div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="salons-item">
                <div class="BoxTitle Title">г. Мытищи</div>
                <table cellspacing="1" cellpadding="10" border="0" width="100%">
                    <tbody>
                    <tr class="even">
                        <td width="40%"><a href="/salons/fermerskiy_rynok_ekobazar_mytishchi/">бул. Ветеранов</a></td>
                        <td width="40%">бульвар Ветеранов, стр.2, Фермерский рынок "ЭКОБАЗАР"</td>
                        <td width="20%">
                            <div>8 (495) 775-93-02</div>
                        </td>
                    </tr>
                    <tr class="odd">
                        <td width="40%"><a href="/salons/sharapovskiy_pr_d/">Шараповский пр-д</a></td>
                        <td width="40%">Шараповский проезд, вл.2, ТРК "Красный Кит-2", 1-й этаж</td>
                        <td width="20%">
                            <div>8 (495) 407-08-06</div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="salons-item">
                <div class="BoxTitle Title">г. Одинцово</div>
                <table cellspacing="1" cellpadding="10" border="0" width="100%">
                    <tbody>
                    <tr class="even">
                        <td width="40%"><a href="/salons/ul_govorova/">ул. Говорова</a></td>
                        <td width="40%">ул. Говорова, дом 163, ТЦ "Атлас", 1-й этаж </td>
                        <td width="20%">
                            <div>8 (498) 720-97-88</div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="salons-item">
                <div class="BoxTitle Title">г. Химки</div>
                <table cellspacing="1" cellpadding="10" border="0" width="100%">
                    <tbody>
                    <tr class="even">
                        <td width="40%"><a href="/salons/yubileynyy_prospekt/">Юбилейный пр-т</a></td>
                        <td width="40%">Юбилейный проспект, дом 50, аптека</td>
                        <td width="20%">
                            <div>8 (495) 775-93-83</div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>