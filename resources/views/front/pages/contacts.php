<div class="Contacts ContentBorder">
    <div class="BoxTitle Title">Контакты</div>

    <div class="ItemDisc">
        <p><strong>ООО "ОчиАле"
                <br>
            </strong></p>

        <p><strong>ОГРН 1137746935644</strong></p>

        <p><strong>ИНН 7725805598&nbsp;
                <br>
            </strong></p>

        <p><strong>Телефон:</strong> (495) 231-46-72</p>

        <p><strong>Адрес офиса:</strong> Москва, ул. Вавилова, д.5, кор.3, офис 202</p>

        <p><strong>E-mail: </strong> <a href="mailto:info@optika-favorit.ru">info@optika-favorit.ru</a></p>

        <p><b>Телефон интернет-магазина:</b> (495) 730-60-58</p>

        <p><b>Мы в соц. сетях:</b></p>
        <div>
            <a target="_blank" href="http://vk.com/club<?=Settings::get('vk')?>" style="background: url('/images/social_icons.png');height:30px;width:30px;background-size:100%;background-position:0px -90px;float:left;margin-right:4px;"></a>
            <a target="_blank" href="https://twitter.com/<?=Settings::get('twitter')?>" style="background: url('/images/social_icons.png');height:30px;width:30px;background-size:100%;background-position:0px -60px;float:left;margin-right:4px;"></a>
            <a target="_blank" href="https://www.facebook.com/<?=Settings::get('facebook')?>/" style="background: url('/images/social_icons.png');height:30px;width:30px;background-size:100%;background-position:0px -30px;float:left;margin-right:4px;"></a>
            <a href="https://www.instagram.com/<?=Settings::get('instagram')?>/"><img src="/images/instagram.png" style="width:30px;"/></a>
        </div>
    </div>

    <div style="margin-top:36px;">
        <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Aea63bee69066e8e0b92e43b59279999fd08144aee376e0ae11721c2bc5512b6d&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true"></script>
    </div>
    <div class="row" style="padding:24px 24px;background-color:#fafafa;display: inline-block;margin-top:36px;">
        <? if (session('success')):?>
            <div class="alert alert-success">
                Ваше обращение успешно отправлено
            </div>
        <? else:?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 style="margin-top:12px;">Связь с нами</h3>
                </div>
                <div class="panel-body">
                    <p>Задайте нам вопрос или оставьте свое пожелание по работе нашего магазина.</p>

                    <?=view('front.forms.contactus')?>
                </div>
            </div>
        <? endif;?>
    </div>
</div>