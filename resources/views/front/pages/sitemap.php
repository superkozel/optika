<h3><?=Html::link('/', 'Главная')?></h3>
<? $pageRenderer = function($pages, callable $field = null) use (&$pageRenderer){
    echo '<ul>';
    foreach ($pages as $page) {
        echo '<li>';
        echo Html::link($page->getUrl(), $field ? $field($page) : $page->h1);
        if ($page->children) {
            $pageRenderer($page->children);
        }
        echo '</li>';
    }
    echo '</ul>';
};?>
<? $pageRenderer(Page::whereNull('parent_id')->get());?>

<h3><a href="<?=actionts('UserController@index')?>">Личный кабинет</a></h3>
<ul>
    <li>
        <a href="<?=actionts('UserController@bonus')?>">Бонусная карта</a>
    </li>
    <li>
        <a href="<?=actionts('BasketController@index')?>">Корзина</a>
    </li>
    <li>
        <a href="<?=actionts('FavoriteController@index')?>">Избранное</a>
    </li>

    <? if ($lastOrder = \App\User::lastOrder()):?>
        <li>
            <a href="<?=actionts('OrderController@view', ['id' => $lastOrder->id])?>">Последний заказ</a>
        </li>
    <? endif;?>

    <? if (Auth::user()):?>
        <li>
            <a href="<?=actionts('UserController@orders')?>">История покупок</a>
        </li>
        <li>
            <a href="<?=actionts('UserController@getProfile')?>">Изменить личные данные</a>
        </li>
        <li>
            <a href="<?=actionts('UserController@getPassword')?>">Изменить пароль</a>
        </li>
        <li>
            <a href="<?=actionts('Auth\LoginController@logout')?>">Выход</a>
        </li>
    <? else:?>
        <li>
            <a href="<?=actionts('Auth\RegisterController@register')?>">Регистрация</a>
        </li>
        <li>
            <a href="<?=actionts('Auth\LoginController@login')?>">Вход</a>
        </li>
    <? endif;?>
</ul>

<h3><?=Html::link('/catalog', 'Каталог товаров')?></h3>

<? $pageRenderer(CatalogCategory::whereParentId(1)->get(), function($category) { return $category->name;});?>

<h3><a href="<?=actionts('SalonController@index')?>">Салоны</a></h3>
<ul>
    <? foreach (City::has('salons')->get() as $salonCity):?>
        <li><?=Html::link(actionts('SalonController@index', ['city' => $salonCity->slug]), $salonCity->name)?>
            <ul>
                <? foreach($salonCity->salons as $salon):?>
                    <li><?=Html::link($salon->getUrl(), $salon->address)?>
                        <? foreach ($salon->metros()->withPivot('range')->get() as $metro):?>
                            <span class="metro-station line-m<?=intval($metro->line->number)?>">
                                <?=$metro->name?>
                                <? if ($metro->pivot->range):?>
                                    <span style="font-style: italic;color:darkgrey"><?=$metro->pivot->range?> <?=plural($metro->pivot->range, 'метр', 'метра', 'метров')?></span>
                                <? endif;?>
                            </span>
                        <? endforeach;?>
                    </li>
                <? endforeach;?>
            </ul>
        </li>
    <? endforeach;?>
</ul>

<h3><a href="<?=actionts('UserController@index')?>">Услуги</a></h3>
<ul>
    <? foreach(\ActiveRecord\SalonService::all() as $service):?>
        <li><?=Html::link($service->getUrl(), $service->name)?></li>
    <? endforeach;?>
</ul>

<h3><a href="<?=actionts('ActionController@index')?>">Акции действующие</a></h3>
<ul>
    <? foreach(Action::where('active_to', '>', date('Y-m-d H:i:s'))->get() as $action):?>
        <li><?=Html::link($action->getUrl(), $action->name)?></li>
    <? endforeach;?>
</ul>

<h3><a href="<?=actionts('NewsController@index')?>">Новости</a></h3>
<ul>
    <? foreach(News::orderBy('id', 'DESC')->limit(10)->get() as $news):?>
        <li><?=Html::link($news->getUrl(), $news->created_at->format('Y-m-d H:i') . ' | ' .$news->name)?></li>
    <? endforeach;?>
    <li><a href="<?=actionts('NewsController@index')?>">Остальные новости...</a></li>
</ul>