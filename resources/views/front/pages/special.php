<div class="AllEventsBox">
    <div class="event-item">
        <div class="event-item-images">
            <a href="/special/konsultatsiya_spetsialista_kak_ukhazhivat_za_ochkami/">
                <img src="/upload/iblock/274/2745ee2d291a8fd174813c68811c569e.jpg" alt="">
            </a>
        </div>
        <div class="event-item-text">
            <div class="event-item-title">
                <a href="/special/konsultatsiya_spetsialista_kak_ukhazhivat_za_ochkami/">Консультация специалиста: как ухаживать за очками</a>
            </div>
            <div class="event-item-txt">Несколько советов, как ухаживать за очками, чтобы они дольше служили и радовали владельца.</div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="event-item">
        <div class="event-item-images">
            <a href="/special/konsultatsiya_spetsialista_ochki_dlya_raboty_za_kompyuterom/">
                <img src="/upload/iblock/f9a/f9a18cf383ee3eef2b02db71c507e158.jpg" alt="">
            </a>
        </div>
        <div class="event-item-text">
            <div class="event-item-title">
                <a href="/special/konsultatsiya_spetsialista_ochki_dlya_raboty_za_kompyuterom/">Консультация специалиста: очки для работы за компьютером</a>
            </div>
            <div class="event-item-txt">Зачем нужны очки при хорошем зрении и насколько монитор компьютера вреден для глаз</div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="event-item">
        <div class="event-item-images">
            <a href="/special/konsultatsiya_spetsialista_kak_pravilno_vybirat_ochki/">
                <img src="/upload/iblock/e08/e08db746a257f545eb5a55b3e1ab9dc8.jpg" alt="">
            </a>
        </div>
        <div class="event-item-text">
            <div class="event-item-title">
                <a href="/special/konsultatsiya_spetsialista_kak_pravilno_vybirat_ochki/">Консультация специалиста: как правильно выбирать очки</a>
            </div>
            <div class="event-item-txt">Можно ли подобрать очки самому или все же довериться профессионалу</div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="event-item">
        <div class="event-item-images">
            <a href="/special/ultrazvukovaya_chistka_ochkov_zamena_vintov_i_nosouporov_besplatno/">
                <img src="/upload/iblock/0c2/0c23aaff856289d9c75faa9670113aa7.jpg" alt="">
            </a>
        </div>
        <div class="event-item-text">
            <div class="event-item-title">
                <a href="/special/ultrazvukovaya_chistka_ochkov_zamena_vintov_i_nosouporov_besplatno/">Ультразвуковая чистка очков, замена винтов и носоупоров - бесплатно</a>
            </div>
            <div class="event-item-txt"></div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="ClearFix"></div>
</div>