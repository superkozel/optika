<div class="EventItem">
    <div class="item-text">
        Как подобрать очки? Вопрос весьма и весьма непростой… Неправильно подобранные очки из низкокачественных материалов приводят к ухудшению зрения, это обычно сопровождается утомляемостью глаз и головными болями.&nbsp;
        <div>
            <br>
        </div>

        <div>Важным при выборе очков является подбор правильных линз и оправы. Для начала необходимо обратиться к офтальмологу, который, благодаря специальному оборудованию, подберет нужные диоптрии, определит расстояние между зрачками, учтет все индивидуальные особенности пациента. Ни в коем случае не пренебрегайте услугами офтальмолога! Помните, что зрение желательно проверять не реже 1 раза в год. Золотое правило - "предупредить легче чем лечить" в нашем случае работает идеально.</div>

        <div>
            <br>
        </div>

        <div>После этой процедуры можно смело идти в оптику для выбора удобной оправы, вида и цвета линз. Все это подбирается в зависимости от вашего образа жизни и ваших потребностей.&nbsp;</div>

        <div>
            <br>
        </div>

        <div>1.	Если Вы нуждаетесь в очках для чтения, то в этом случае можно подобрать узкую оправу, так как взгляд будет концентрироваться в одном направлении и обширный обзор здесь не требуется.&nbsp;</div>

        <div>
            <br>
        </div>

        <div>2.	В том случае, когда длительное время Вы проводите за компьютером, то уместным будет использование линз с антибликовым покрытием и специализированным покрытием, специально разработанным для таких линз.&nbsp;</div>

        <div>
            <br>
        </div>

        <div>3.	Для активного отдыха Вам подойдут оправы, которые плотно прилегают к лицу с линзами «хамелеонами» (фотохромными линзами, которые изменяют степень затемненности в зависимости от яркости освещения).&nbsp;</div>

        <div>
            <br>
        </div>

        <div>В настоящее время, линзы изготавливаются как из классического оптического стекла (минеральные линзы), так и из специальных полимеров (полимерные линзы). Пластиковые (полимерные) линзы на данный момент ничуть не уступают стеклянным (минеральным).&nbsp;</div>

        <div>На очковые линзы могут быть нанесены дополнительные покрытия, которые улучшают качество линзы. Это упрочняющие, антирефлексные (просветляющими), водоотталкивающие, антистатические покрытия.</div>

        <div>Очки с такими покрытиями выполняют как основную функцию коррекции зрения, так и дополнительно позволяют лучше видеть сумерках, темноте. Линзы со специальными покрытиями имеют более долгий срок службы, меньше загрязняются, за ними легче ухаживать.</div>

        <div>
            <br>
        </div>

        <div>Надеемся, что мы помогли Вам разобраться в вопросе: «Как подобрать очки?». А если сомнения все же остались – ждем Вас на персональную бесплатную консультацию в любом из&nbsp;<a href="http://www.optika-favorit.ru/salons/">наших салонов</a>, расположенных в Москве и Московской области. </div>
    </div>
</div>
<div align="right" style="margin-top:5px;"><a href="/shares/" class="Button">вернуться</a></div>
