<div style="background-color:#fafafa;padding-top:36px;">
    <div class="container-fluid">
        <?=Bootstrap3Form::model($order, ['errors' => $errors]);?>
        <?=Bootstrap3Form::hidden('bonuses_used', Input::get('bonuses_used'))?>
        <div class="row">
            <div class="col-md-12">
                <? if ($fitting):?>
                    <h3 class="nomargin">Вы оформляете заказ на примерку очков или оправ.</h3>
                    <ul class="checkmarks">
                        <li>Заказ осуществляется до выбранного салона</li>
                        <li>На месте вы можете приобрести понравившиеся очки и заказать изготовление очков из выбранной оправы</li>
                        <li>В нашем салоне вы сможете воспользоваться дополнительно любой из предоставляемых услуг и проконсультироваться со специалистом</li>
                    </ul>
                <? endif;?>
            </div>
            <div class="col-md-5">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3>Ваш заказ</h3>
                    </div>
                    <?=view('front.order.parts.items', compact(['order', 'items']))->render()?>
                    <div class="panel-footer">
                        <a href="<?=actionts('BasketController@index')?>" class="btn btn-primary btn-sm pull-right"><i class="fa fa-pencil"></i> Изменить заказ</a>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="panel <? if ($errors->has('delivery_type_id')
                    || $errors->has('salon_id')
                    || $errors->has('delivery_date')
                    || $errors->has('delivery_time')
                    || $errors->has('address')
                ):?>panel-danger<?else:?>panel-default<?endif;?>">
                    <div class="panel-heading">
                        <h3><i class="fa fa-truck"></i> Способ доставки</h3>
                        <? if ($errors->has('delivery_type_id')):?>
                            <?=$errors->get('delivery_type_id')[0]?>
                        <?endif;?>
                    </div>
                    <div class="list-group">
                        <? foreach (DeliveryType::availableForOrder($order) as $d):?>
                            <div class="list-group-item delivery-type delivery-<?=$d->getId()?>">
                                <div class="radio">
                                    <label for="delivery_<?=$d->getId()?>">
                                        <input type="radio" id="delivery_<?=$d->getId()?>" name="delivery_type_id" value="<?=$d->getId()?>" <?=$order->delivery_type_id == $d->getId() ? 'checked="checked"' : ''?>/>
                                        <?=$d->getName()?> - <span style="font-style: italic;"><?=$d->getPriceText($order)?></span>
                                    </label>
                                </div>
                                <? if ($d->is(DeliveryType::SAMOVYVOZ)):?>
                                        <div class="delivery-details delivery-<?=$d->getId()?>" style="display: none;">

                                        <? $dates = $d->getAvailableDatesForOrder($order);
                                            $select = array_combine(
                                                array_map(function($date){return $date->format('d.m.Y');}, $dates),
                                                array_map(function($date){return $date->formatLocalized('%d.%m %A');}, $dates)
                                            )
                                        ?>

                                        <?=Bootstrap3Form::selectRow('delivery_date', 'Дата получения', $select)?>

                                        <div class="form-group">
                                            <?=Bootstrap3Form::label('salon_id', 'Выберите подходящий салон')?>

                                            <div class="filtering-search" style="position: relative">
                                                <i class="fa fa-close" style="color:red;position: absolute;right:10px;top:10px;"></i>
                                                <?=Bootstrap3Form::text('salon_search', null, ['placeholder' => 'поиск салона по адресу'])?>
                                            </div>
                                            <style>
                                                mark{
                                                    padding:0px;color:red;font-weight:bold
                                                }
                                                .filtering-search i{display: none;}
                                                .filtering-search.contains i{display: block;}

                                            </style>
                                            <script>
                                                var highlight = function(el, term){
                                                    var src_str = $(el).html();
                                                    term = term.replace(/(\s+)/,"(<[^>]+>)*$1(<[^>]+>)*");
                                                    var pattern = new RegExp("("+term+")", "gi");

                                                    if (src_str.search(pattern) > -1) {
//
//                                                    src_str = src_str.replace(pattern, "<mark>$1</mark>");
//                                                    src_str = src_str.replace(/(<mark>[^<>]*)((<[^>]+>)+)([^<>]*<\/mark>)/,"$1</mark>$2<mark>$4");
//
//                                                    $(el).html(src_str);

                                                        return true;
                                                    }
                                                    else {
                                                        return false
                                                    }
                                                }

                                                var clearFilteringSearch = function(el) {
                                                    $(el).parent().find('input').val('').change().parent().removeClass('contains')
                                                }

                                                $('.filtering-search input[name=salon_search]').bind('keyup change', function(){
                                                    if ($(this).val()) {
                                                        $(this).parent().addClass('contains');
                                                    }
                                                    else {
                                                        $(this).parent().removeClass('contains');
                                                    }

                                                    // pull in the new value
                                                    var searchTerm = $(this).val();

                                                    // remove any old highlighted terms
                                                    $('[name="salon_id"]').parents('label').find('mark').contents().unwrap();
                                                    $('[name="salon_id"]').parents('div.radio').show();

                                                    if (searchTerm.length < 1)
                                                        return;

                                                    $('[name="salon_id"]').each(function(k, v){
                                                        var $el = $(v).parent();
                                                        if (highlight($el, searchTerm)) {
                                                        }
                                                        else {
                                                            $el.parents('div.radio').hide()
                                                        }
                                                    });
                                                }).bind('keydown', function(event){
                                                    if (event.keyCode == 27) {
                                                        console.log(event.keyCode);
                                                        clearFilteringSearch(event.currentTarget);
                                                    }
                                                });
                                                $('.filtering-search i').click(function(event){
                                                    clearFilteringSearch(event.currentTarget);
                                                })
                                            </script>

                                            <?= view('front.basket.parts.salon_select', compact('order'))->render()?>
                                        </div>
                                    </div>
                                <? endif;?>
                                <? if ($d->is(DeliveryType::DOSTAVKA, DeliveryType::DOSTAVKA2)):?>
                                    <div class="delivery-details delivery-<?=$d->getId()?>" style="display: none;">
                                        <? if ($making):?>
                                            <div class="alert alert-warning"><b>Дата доставки согласуется с менеджером</b>. Срок изготовления 7-14 дней</div>
                                        <? else:?>
                                            <?php
                                            $dates = $d->getAvailableDatesForOrder($order);

                                            $select = array_combine(
                                                array_map(function($date){return $date->format('d.m.Y');}, $dates),
                                                array_map(function($date){return $date->formatLocalized('%d.%m %A');}, $dates)
                                            )
                                            ?>
                                            <?=Bootstrap3Form::selectRow('delivery_date', 'Дата доставки', $select, null, ['onchange' => 'updateTimeIntervals(event.target)'])?>

                                            <script>
                                                $().ready(function(){
                                                    $('[name=delivery_date]').trigger('change');
                                                });

                                                function updateTimeIntervals(el) {
                                                    var input = $(el).parents('.delivery-details').find('[name=delivery_time]');
                                                    if (input.length == 0)
                                                        return;

                                                    var selected = $(el).find('option:selected');

                                                    if (selected.html().indexOf('Суббота') != -1){
                                                        value = input.closest('.form-group').hide()
                                                        $(el).parents('.delivery-details').find('.delivery-time-info').show();
                                                    }
                                                    else {
                                                        value = input.closest('.form-group').show()
                                                        $(el).parents('.delivery-details').find('.delivery-time-info').hide();
                                                    }
                                                }
                                            </script>

                                            <? if ($intervals = $intervals = $d->timeIntervals()) :?>
                                                <?=Bootstrap3Form::selectRow('delivery_time', 'Желательное время доставки', array_combine($intervals, $intervals))?>
                                            <? endif;?>
                                            <div class="alert alert-info delivery-time-info"><i class="fa fa-time"></i> Доставка в течении дня с 9:00 до 18:00</div>
                                        <? endif;?>
c
                                    </div>
                                <? endif;?>
                            </div>
                        <? endforeach?>
                    </div>
                </div>

                <div <? if ($order->sum == 0):?>style="display: none;"<? endif;?> class="panel <? if ($errors->has('payment_type_id')):?>panel-danger<?else:?>panel-default<?endif;?>">
                    <div class="panel-heading">
                        <h3><i class="fa fa-money"></i> Способ оплаты</h3>
                        <? if ($errors->has('payment_type_id')):?>
                            <?=$errors->get('payment_type_id')[0]?>
                        <?endif;?>
                    </div>
                    <div class="list-group">
                        <? foreach (\Enum\PaymentType::availableForOrder($order) as $d):?>
                            <div class="list-group-item payment-type-block payment-<?=$d->getId()?>">
                                <div class="radio">
                                    <label for="payment_<?=$d->getId()?>">
                                        <input type="radio" id="payment_<?=$d->getId()?>" name="payment_type_id" value="<?=$d->getId()?>" <?=$order->payment_type_id == $d->getId() ? 'checked="checked"' : ''?>/>
                                        <?
                                        $images = [
                                            \Enum\PaymentType::KURIERU => 'http://www.santehnica.ru/files/images/resized/payment/5/0/50-1.138x48.png',
                                            \Enum\PaymentType::KARTOY_KURIERU => 'http://www.santehnica.ru/files/images/resized/payment/7/0/70-visa.138x48.png',
                                            \Enum\PaymentType::KARTOY => 'http://www.santehnica.ru/files/images/resized/payment/7/0/70-visa.138x48.png',
                                            \Enum\PaymentType::ONLINE => 'http://www.santehnica.ru/files/images/resized/payment/8/0/80-onlayn-oplata.138x48.png',
                                        ]
                                        ?>
                                        <img src="<?=$images[$d->getId()]?>"/>
                                        <?=$d->getName()?>
                                    </label>
                                </div>
                            </div>
                        <? endforeach?>
                    </div>
                </div>

                <style>
                    .list-group-item:hover{cursor:pointer}
                    .list-group-item.active{background: #fafafa;color:black;border-bottom:1px solid #d3e0e9;border-top:1px solid #d3e0e9;}
                    .list-group-item.active:hover{background: #fafafa;color:black;border-bottom:1px solid #d3e0e9;border-top:1px solid #d3e0e9;}
                </style>
                <script>
                    function disableInputs(el)
                    {
                        $(el).find("textarea, input, select").prop("disabled", 'disabled');
                    }
                    function enableInputs(el)
                    {
                        $(el).find("textarea, input, select").removeProp("disabled");
                    }
                    function updateForm()
                    {
                        disableInputs($('.delivery-details').hide());

                        var deliveryId = $('[name=delivery_type_id]:checked').val();

                        $('.delivery-details.delivery-' + deliveryId).show()
                        enableInputs($('.delivery-details.delivery-' + deliveryId).show());

                        var availablePayments;
                        if (deliveryId == <?=DeliveryType::SAMOVYVOZ?>) {
                            availablePayments = [<?=\Enum\PaymentType::KURIERU?>, <?=\Enum\PaymentType::ONLINE?>, <?=\Enum\PaymentType::KARTOY?>]
                        }
                        else if (deliveryId == <?=DeliveryType::POCHTA?>) {
                            availablePayments = [<?=\Enum\PaymentType::ONLINE?>, <?=\Enum\PaymentType::KARTOY?>]
                        }
                        else {
                            availablePayments = [<?=\Enum\PaymentType::KURIERU?>, <?=\Enum\PaymentType::ONLINE?>, <?=\Enum\PaymentType::KARTOY?>, <?=\Enum\PaymentType::KARTOY_KURIERU?>]
                        }

                        $('.payment-type-block').hide();
                        for (i in availablePayments) {
                            $('.payment-type-block.payment-' + availablePayments[i]).show();
                        }
                        $('.payment-type-block:not(:visible)').find('input[type=radio]').removeProp('checked');

                        //подсвечиваем верхний блок
                        $('.list-group-item').removeClass('active');
                        $('[name=delivery_type_id]:checked, [name=payment_type_id]:checked').parents('.list-group-item').addClass('active');
                    }

                    $('[name=payment_type_id], [name=delivery_type_id]').change(function(){
//                        slideTo($(this).parents('.list-group-item'), -10);
                        updateForm();
                    });

                    $().ready(function(){
                        updateForm();
                        $('.list-group-item').click(function(){
                            if ($(this).hasClass('active'))
                                return;

                            $(this).children('.radio').find('input[type=radio]:eq(0)').prop('checked', 'checked').change();
                        })
                    })
                </script>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3><i class="fa fa-user"></i> Получатель</h3>
                    </div>
                    <div class="panel-body">
                        <?=Bootstrap3Form::textRow('name', 'Имя')?>
                        <?=Bootstrap3Form::textRow('phone', 'Телефон')?>
                        <script type="text/javascript">
                            $('[name=phone]').mask("8(999)999-99-99");
                        </script>
                        <?=Bootstrap3Form::textRow('email', 'E-mail')?>
                        <?=Bootstrap3Form::textareaRow('comment', 'Комментарий к заказу', null, ['placeholder' => 'Примечания для курьера и менеджера'])?>

                        <div class="form-group <? if ($errors->has('personal_accept')):?>has-error<?endif;?>">
                            <?=Bootstrap3Form::checkbox('personal_accept', 'Согласен на <a class="link" target="_blank" href="' . Page::findBySlug('politika-konfidencialnosti')->getUrl() . '">передачу и обработку персональных данных</a>', 1, old('personal_accept', 1))?>
                            <?=Bootstrap3Form::error($errors->get('personal_accept'))?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="pull-right">
            <a href="<?=actionts('BasketController@index')?>" class="btn btn-default btn-lg"><i class="fa fa-arrow-left"></i> Назад в корзину</a>
            <button type="submit" class="btn btn-primary btn-lg"><i class="fa fa-check"></i> Отправить заказ</button>
        </div>
        <? Bootstrap3Form::close();?>
    </div>
</div>

<style>
    .panel-heading h3{margin-top:11px;}
</style>