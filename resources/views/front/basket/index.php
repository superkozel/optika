    <?
/** @var Basket $basket */

/** @var \Illuminate\Support\ViewErrorBag $errors */
$maxBonuses = $user ? min($user->getBonuses(), $basket->getMaxUsedBonuses()) : 0;
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-9">
            <div class="panel panel-danger<? if ($basket->getTotalCount()):?>panel-warning<?endif;?>" style="border:1px solid #ddd;border-top:none;">
                <? if ($basket->getTotalCount()):?>
                <table class="table table-condensed basket-table" width=100%>
                    <cols>
                        <col width="110"/>
                        <col width=""/>
                        <col width=""/>
                        <col width="60"/>
                        <col width="120"/>
                        <col width="40"/>
                    </cols>
                    <tr>
                        <th style="padding:24px 5px;"></th>
                        <th style="padding:24px 5px;;">Позиция</th>
                        <th style="padding:24px 5px;text-align: center;">Стоимость</th>
                        <th style="padding:24px 5px;">Кол-во</th>
                        <th style="padding:24px 5px;text-align: right;">Подытог</th>
                        <th style="padding:24px 5px;"></th>
                    </tr>
                    <? foreach ($basket->getContents() as $data):?>
                        <? if($data->getCount() == 0)continue;?>
                        <? $item = $data->getProduct();?>
                        <tr data-id="<?=$data->getId()?>" data-price="<?=$data->getSellingPrice()?>" data-base-price="<?=$item->getBasePrice()?>" class="tile">
                            <td style="text-align:center;vertical-align:top">
                                <? if ($image = $item->getImage()):?>
                                    <a style="position: relative;" href="<?=$image->getUrl('full')?>" class="fresco">
                                        <?=ProductImage::img($image, 'mini', $item->name, ['style' => 'max-width:100%;cursor:zoom-in;'])?>
                                    </a>
                                <? else:?>
                                    <?=ProductImage::img($image, 'mini', $item->name, ['style' => 'max-width:100%;'])?>
                                <? endif;?>
                            </td>
                            <td style="vertical-align:middle">
                                <a href="<?=$item->getUrl()?>" class="title"><?=$item->name?></a>
                                <br/>
                                <span class="subtitle">
                                    <?=mb_ucfirst($item->getType()->getName())?><br/>
                                    <? if ($item->getType()->is(ProductType::LINZI, ProductType::KONTAKTNIE_LINZY)):?>
                                        <? $attributes = $item->getType()->getAttributeSet()->attrs()->whereIn('code', [Attribute::ID_SFERA, Attribute::ID_TSILINDR])->get();?>
                                        <? foreach($attributes as $attr):?>
                                            <?=$attr->name?> : <?=$attr->valueString($item->getProperty($attr->id))?>
                                        <? endforeach;?>
                                    <? endif;?>
                                </span>
                                <? if(count($data->getChildren()) > 0):?><b><?=$data->getSellingPrice(1, true)?> руб.</b><?endif;?>

                                <? if (! empty($data->getModifiers()[ProductModifier::FITTING])):?>
                                    <div class="label label-success"><i class="fa fa-eye"></i> Примерка</div>
                                <? endif;?>
                                <? if ($data->getProduct()->getType()->is(ProductType::OPRAVA) && empty($data->getModifiers()[ProductModifier::FITTING])):?>
                                    <? if (! empty($data->getModifiers()[ProductModifier::LENS_LEFT])):?>
                                        <div class="basket-item-lenses">
                                            <br/>
                                            <?
                                            $leftLensProduct = Product::find($data->getModifiers()[ProductModifier::LENS_LEFT]);
                                            $rightLensProduct = Product::find($data->getModifiers()[ProductModifier::LENS_RIGHT]);
                                            ?>
                                            <div>Левая линза: <a href="<?=$leftLensProduct->getUrl()?>"><?=$leftLensProduct->name?></a> <span style="font-weight:bold;">+ <?=$leftLensProduct->getSellingPrice()?> руб.</span></div>
                                            <div>Правая линза:  <a href="<?=$rightLensProduct->getUrl()?>"><?=$rightLensProduct->name?></a> <span style="font-weight:bold;">+ <?=$rightLensProduct->getSellingPrice()?> руб.</span></div>

                                            <div style="float:left;margin-bottom:12px;">
                                                <a onclick="javascript:removeLenses(<?=$data->getId()?>, event.currentTarget)" class="btn btn-sm btn-default"><i class="fa fa-close"></i> Убрать линзы</a>
                                                <a href="<?=CatalogCategory::findBySlug('lenses')->getUrl();?>" class="btn btn-sm btn-default btn-primary">Выбрать другие линзы</a>
                                            </div>

                                            <div class="clearfix"></div>

                                            <div>
                                                <?
                                                $modifier = ProductModifier::find(ProductModifier::MAKING);
                                                ?>
                                                <?=Bootstrap3Form::checkbox(ProductModifier::MAKING, 'Требуется изготовление (' . $modifier->getPrice($data, 1) . ' руб.)', 1, $data->getModifier(ProductModifier::MAKING),
                                                    [
                                                        'rowOptions' => ['style' => 'margin-bottom:0px;'],
                                                        'onchange' => "changeModifier('" . ProductModifier::MAKING . "', $(event.currentTarget).prop('checked') ? 1 : null, event.currentTarget)"
                                                    ]
                                                )?>
                                            </div>

                                            <div>
                                                <?
                                                $modifier = ProductModifier::find(ProductModifier::TONING);
                                                $toning = $data->getModifier(ProductModifier::TONING);
                                                ?>
                                                <?=Bootstrap3Form::checkbox('tonings', 'Тонирование линз (от ' . $modifier->getPrice($data, 1) . ' руб.)', 1, $toning, [
                                                    'rowOptions' => ['style' => 'margin-bottom:0px;'],
                                                    'onchange' => 'showToningOptions(event.currentTarget)'])?>
                                                <script>
                                                    function showToningOptions(el) {
                                                        if (! $(el).prop('checked')) {
                                                            $('.toning-options').hide();
                                                            changeModifier('<?=ProductModifier::TONING?>', null, el )
                                                        }
                                                        else {
                                                            $('.toning-options').show();

                                                            if ($('[name=toning]:checked').length == 0) {
                                                                $('[name=toning]:first').prop('checked', 'checked');
                                                                var value = 1;
                                                            }
                                                            else {
                                                                var value = $('[name=toning]:checked').val();
                                                            }
                                                            changeModifier('<?=ProductModifier::TONING?>', value, el);
                                                        }
                                                    }
                                                </script>

                                                <div class="toning-options" style="margin-left:20px;<? if(! $toning):?>display: none;<? endif;?>">
                                                    <? $options = ['1' => 'Обычное тонирование (' . $modifier->getPrice($data, 1) . ' руб.)', 2 => 'Тонирование "Маска" (' . $modifier->getPrice($data, 2) . ' руб.)'];?>
                                                    <? foreach ($options as $id => $label):?>
                                                        <div class="radio">
                                                            <label for="toning_<?=$id?>">
                                                                <?=Bootstrap3Form::radio(ProductModifier::TONING, $id, $toning == $id, ['id' => 'toning_' . $id, 'onchange' => 'changeModifier("' . ProductModifier::TONING . '", $(event.currentTarget).val(), event.currentTarget)']);?> <?=$label?>
                                                            </label>
                                                        </div>
                                                    <? endforeach;?>
                                                </div>
                                            </div>

                                        </div>
                                    <? else:?>
                                        <br/>
                                        <a href="<?=CatalogCategory::findBySlug('lenses')->getUrl();?>" class="btn btn-sm btn-default btn-primary">Выбрать линзы</a>
                                    <? endif;?>
                                <? endif;?>
                                <? if ($data->getModifiers()):?>
                                    <? foreach($data->getModifiers() as $modId => $modValue):?>
                                        <? if ($modId == ProductModifier::LENS_LEFT):?>
                                        <? else:?>
                                            <? $mod = ProductModifier::find($modId)?>
                                        <? endif;?>
                                    <? endforeach;?>
                                <? endif;?>
                            </td>
                            <td style="text-align:center;vertical-align:middle">
                                <div class="base" style="<? if ($item->getBasePrice() <= $data->getSellingPrice()):?>display:none;<?endif;?>text-decoration:line-through;color:gray"><?=moneyFormat($item->getBasePrice($data->getCount()))?> руб.</div>
                                <? if (empty($data->getModifiers()[ProductModifier::FITTING])):?>
                                    <div class="price"><?=moneyFormat($data->getSellingPrice())?> руб.</div>
                                <? else:?>
                                    <span style="color:green">Бесплатная примерка</span>
                                <? endif;?>
                            </td>

                            <td class="tile-count" style="vertical-align:middle;">
                                <? if (empty($data->getModifiers()[ProductModifier::FITTING])):?>
                                    <?=Form::input('number', 'basket[' . $item->id . ']', $data->getCount(), array('data-id' => $item->id, 'style' => 'max-width:58px;text-align:center;', 'min' => 1))?>
                                <? endif;?>
                            </td>

                            <td style="vertical-align:middle;text-align: right;position: relative;" class="tile-price">
                                <? if (empty($data->getModifiers()[ProductModifier::FITTING])):?>
                                    <div class="selling"><?=moneyFormat($data->getSellingPrice() * $data->getCount())?> руб.</div>

                                    <? if (BONUS_PROGRAM_ACTIVE && $bonus = $item->getBonuses($user)):?>
                                        <span class="bonus" style="color:red">+ <?=$bonus * $data->getCount()?> <?=plural($bonus * $data->getCount(), 'бонус', 'бонуса', 'бонусов')?></span>
                                    <? endif;?>
                                <? endif;?>
                            </td>

                            <td style="text-align: left;vertical-align: middle;">
                                <a class="basket-item-close" href="javascript:removeBasketItem('<?=$data->getId()?>',  function(result){
                                if (result.success) {
                                    $('[data-id=' + result.id  + ']').slideUp().remove();
                                    if (result.total == 0){$('.submit-order-button').addClass('disabled')}
                                }})">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    <? endforeach?>
                    <? if (BONUS_PROGRAM_ACTIVE):?>
                        <tr class="bonuses-row" style="display: none;">
                            <td colspan="4" style="text-align:right;padding:24px 0px;line-height:1;">
                                Оплата бонусами:
                            </td>
                            <td colspan="2" style="text-align:right;padding:24px 16px;padding-left:12px;line-height:1em;color:red;" class="total-bonuses">— 0 руб.</td>
                        </tr>
                    <? endif;?>
                    <tr>
                        <td colspan="2" style="text-align:left;padding:24px 0px;line-height:1;padding-left:20px;">
                            <div class="input-group promocode">
                                <input value="<?=$basket->getPromocode()?>" name="promocode" class="form-control" placeholder="Промокод на скидку">
                                <span class="input-group-btn">
                                    <a class="btn btn-primary" type="button"
                                       data-loading-text="<i class='fa fa-circle-o-notch fa-spin' ></i>&nbsp;&nbsp;Активация"
                                       data-success-text="<i class='fa fa-check'></i> Активирован"
                                       data-error-text="<i class='fa fa-close'></i> Неверный код"
                                    >Активировать</a>
                                </span>
                            </div>
                            <div class="promocode-hint" style="display:<? if ($basket->getPromocode()):?>block<?else:?>none<?endif;?>;">Скидка 10% на весь заказ</div>
                            <? if ($basket->getPromocode()):?>
                                <script>
                                    $('.promocode').addClass('has-success')
                                    $('.promocode a').button('success').addClass('btn-success');
                                </script>
                            <? endif;?>
                        </td>
                        <td colspan="2" style="text-align:right;padding:24px 0px;line-height:1;">
                            Итого<br/>
                            без доставки
                        </td>

                        <th colspan="2" style="text-align:left;padding:24px 0px;padding-left:12px;font-size:2em;line-height:1em;" class="total-price">
                            <?=moneyFormat($basket->getTotalPrice())?> руб.
                        </th>
                    </tr>
                    <? if (($avans = $basket->calculateRequiredPaymentSum()) > 0):?>
                        <tr style="color:red;">
                            <td colspan="4" style="text-align: right;border-top:0px;">
                                Требуется предоплата
                                на изготовление
                            </td>

                            <th colspan="2" style="text-align:left;padding-left:12px;font-size:1.5em;line-height:1em;border-top:0px;" class="total-prepayment">
                                <?=moneyFormat($avans)?> руб.
                            </th>
                        </tr>
                    <? endif;?>
                    <tr>
                        <th colspan="7" style="text-align:right;padding:24px;">
                            <div class="pull-left">
                                <a class="btn btn-default btn-lg" href="<?=actionts('BasketController@drop')?>" style="margin-right:6px;"><i class="fa fa-close"></i> Очистить корзину</a>
                            </div>
                            <div class="pull-right">
                                <a
                                    onclick="var bonusesUsed = $('[name=bonuses]').val(); if (bonusesUsed){$(event.currentTarget).attr('href', $(event.currentTarget).attr('href') + '?bonuses_used=' + bonusesUsed);}"
                                    href="<?=actionts('BasketController@checkout')?>"
                                    class="submit-order-button btn btn-primary btn-lg <?if ($basket->getContentsCount() == 0):?>disabled<?endif;?>">
                                <i class="fa fa-check"></i> Оформить заказ</a>
                            </div>
                        </th>
                    </tr>
                </table>
                <? else:?>
                    <div class="panel-body">
                        <div style="color:red;">Корзина пуста</div>

                        Начните выбирать товары в <a href="<?=\App\Http\Controllers\CatalogController::createUrl()?>">нашем каталоге</a>
                    </div>
                <? endif;?>
            </div>
        </div>
        <div class="col-md-3">
            <? if (! $basket->isFitting()):?>
                <? if (BONUS_PROGRAM_ACTIVE):?>
                    <? if (! Auth::user()):?>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h3 style="margin-top:0px;">Экономьте с нами!</h3>
                                <hr/>
                                <p>
                                    Зарегистрируйтесь на сайте, чтобы
                                    получать бонусы за каждый заказ
                                    и <b>экономить на покупках</b>.
                                </p>
                            </div>
                            <div class="panel-footer">
                                <a class="btn btn-success" style="display: block;"  href="/register/"><i class="fa fa-sign-in"></i> Зарегистрироваться</a>
                            </div>
                        </div>
                    <? elseif (! Auth::user()->bonus_program_active):?>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h3 style="margin-top:0px;">Экономьте с нами!</h3>
                                <hr/>
                                <p>
                                    Примите участие в нашей <b>бонусной программе</b>
                                    чтобы получить бонусы за ваш заказ.</p>
                                <p>
                                    С помощью бонусов можно
                                    будет оплачивать последующие заказы на нашем сайте.</p>
                            </div>
                            <div class="panel-footer">
                                <a class="btn btn-success" style="display: block;" href="<?=actionts('UserController@bonus')?>"><i class="fa fa-check"></i> Принять участие</a>
                            </div>
                        </div>
                    <? else:?>
                        <div class="panel panel-primary">
                            <div class="panel-body">
                                <h3 style="margin-top:0px;">У вас <b style="color:blue"><?=plural($user->getTotalBonuses(), 'бонус', 'бонуса', 'бонусов', true)?></b></h3>

                                Вы можете накопить или списать бонусы за эту покупку<br/>
                                Номер телефона: <?=$user->phone?><br/>
                                За покупку будет начислено бонусов: 250<br/>
                                Доступно бонусов для списания <?=$user->getBonuses()?><br/>
                                <br/>
                                Оплатить бонусами
                                <?=Bootstrap3Form::input('number', 'bonuses', $bonuses, ['placeholder' => 'Укажите количество бонусов', 'min' => 1, 'max' => $maxBonuses])?>
                            </div>
                        </div>
                    <? endif;?>
                <? endif;?>
            <? else:?>
                <h3 class="nomargin">Условия примерки:</h3>
                <ul style="margin-left: -24px;">
                    <li>Заказ осуществляется до выбранного салона.</li>
                    <li>На месте вы можете приобрести понравившиеся очки и заказать изготовление очков из выбранной оправы.</li>
                    <li>В нашем салоне вы сможете воспользоваться дополнительно любой из предоставляемых услуг и проконсультироваться со специалистом</li>
                    <li>Заказать можно не более 4-х оправ.</li>
                </ul>
            <? endif;?>
        </div>
    </div>
</div>

<script>
    function activatePromocode(code, callback)
    {
        var data = {
            promocode: code
        };

        Basket.update(data, callback);
    }

    function removeLenses(frameId, el)
    {
        $.ajax({
            url: '<?=actionts('BasketController@removeLenses')?>',
            method: 'POST',
            data: {
                id: frameId
            }
        }).success(function(result){
            updateBasketTable(result.basket);
            $(el).parents('.basket-item-lenses').html('<br/><a href="<?=CatalogCategory::findBySlug('lenses')->getUrl();?>" class="btn btn-sm btn-default btn-primary">Выбрать линзы</a>');
        })
    }

//    function requireMakingModifierClick(el)
//    {
//        var frameId = $(el).parents('tr').data('id');
//
//        $.ajax({
//            url: '<?//=actionts('BasketController@updateModifier')?>//',
//            method: 'POST',
//            data: {
//                id: frameId,
//                modifier: '<?//=ProductModifier::MAKING?>//',
//                value: $(el).prop("checked") ? 1 : ''
//            }
//        }).success(function(result){
//            updateBasketTable(result.basket);
//        })
//    }
//
//    function toningModifierChange()
//    {
//
//    }

    function changeModifier(name, value, el)
    {
        var basketItemId = $(el).parents('tr').data('id');

        $.ajax({
            url: '<?=actionts('BasketController@updateModifier')?>',
            method: 'POST',
            data: {
                id: basketItemId,
                modifier: name,
                value: value
            }
        }).success(function(result){
            updateBasketTable(result.basket);
        })
    }

    $('.promocode a').click(function(){
        var code = $('.promocode').find('input').val();

        if (! code) {
            $('.promocode').find('a').removeClass('btn-success').addClass('btn-danger').button('error');
            $('.promocode').removeClass('has-success').addClass('has-error');
            $('.promocode-hint').hide();
            return;
        }

        var $this = $(this);
        $this.button('loading');

        activatePromocode(code, function(result){
            $this.button('reset');
            if (result.success) {

                updateBasketTable(result.basket);

                $('.promocode').find('a').addClass('btn-success').removeClass('btn-danger').button('success');
                $('.promocode').addClass('has-success').removeClass('has-error');
                $('.promocode-hint').show();

            }
            else {
                $('.promocode').find('a').removeClass('btn-success').addClass('btn-danger').button('error');
                $('.promocode').removeClass('has-success').addClass('has-error');
                $('.promocode-hint').hide();
            }
        })

    });

    var curBasket = <?=json_encode($basket->toArray())?>;
    function updateBasketTable(basket) {
        basket = basket || curBasket;
        curBasket = basket;

        var bonusesUsed = $('[name=bonuses]').val() || 0;
        for (i in basket.contents) {
            var $tr = $('tr[data-id=' + basket.contents[i].id + ']');

            var price = basket.contents[i].price;
            var basePrice = $tr.data('base-price');
            var count = $tr.find('.tile-count input').val();
            var bonus = basket.contents[i].bonus;

            if (basePrice > price) {
                $tr.find('.base').show();
            }
            else {
                $tr.find('.base').hide();
            }
            $tr.find('.base').html(moneyFormat(basePrice) + ' руб.');
            $tr.find('.selling').html(moneyFormat((basket.contents[i].price || price) * count) + ' руб.');
            $tr.find('.price').html(moneyFormat((basket.contents[i].price || price)) + ' руб.');
            $tr.find('.bonus').html('+ ' + bonus * count + ' ' + plural((bonus * count), 'бонус', 'бонуса', 'бонусов'));
        }

        $('.bonuses-row').toggle(bonusesUsed > 0);
        $('.total-bonuses').html('— ' + moneyFormat(bonusesUsed) + ' руб.');
        $('.basket-table .total-prepayment').html(moneyFormat(basket.prepayment) + ' руб.');
        $('.basket-table .total-price').html(moneyFormat(basket.sum - bonusesUsed) + ' руб.');
    }

    $().ready(function(){;
       $('.tile-count input').on('change keypress', function(event){
           var $el = $(event.currentTarget).parents('tr');

           var id = $el.data('id');
           var basePrice = $el.data('base-price');
           var count = $(event.currentTarget).val();

           $debouncedUpdate(id, count, function(result){
               if (result.success) {
                   updateBasketTable(result.basket);
               }
           });
       });

       var maxBonuses = <?=$maxBonuses?>;
        $('[name=bonuses]').on('change keyup', function(event){
            if ($(this).val() > maxBonuses) {
                $(this).val(maxBonuses);
            }
            if ($(this).val() < 0) {
                $(this).val('');
            }
            updateBasketTable();
        });
    });
</script>
<style>
    .promocode-hint{margin-top:6px;font-size:0.9em;}
    a.basket-item-close:hover{color:red}
    a.basket-item-close{
        font-size: 1em;
        line-height: 19px;
        width: 22px;
        padding-top: 2px;
        float: left;
        text-align: center;
        background-color: lightgray;
        display: block;
        color: black;
        border-radius: 30px;
        font-weight: bold;
        height: 22px;}
</style>


<? if (count($recent = \App\User::getRecent())):?>
    <h2>Вы недавно смотрели</h2>
    <? foreach ($recent as $product):?><div class="col-md-3 col-xs-4 nopadding col-inline">
        <?=view('front.product.card', ['product' => $product, 'rowSize' => 6, 'simple' => true])?>
    </div><? endforeach;?>
<? endif;?>