<? foreach (Salon::select(['name', 'id', 'address'])->with('metros')->get() as $salon):?>
    <div class="radio">
        <label for="salon_<?=$salon->id?>">
            <input type="radio" id="salon_<?=$salon->id?>" name="salon_id"
                   value="<?=$salon->id?>" <?=$salon->id == $order->salon_id ? 'checked="checked"' : ''?>/>

            <?=$salon->address?>
            <? foreach ($salon->metros()->withPivot('range')->get() as $metro):?>
                <div class="metro-station line-m<?=intval($metro->line->number)?>">
                    <?=$metro->name?>
                </div>
            <? endforeach?>
        </label>
    </div>
<? endforeach;?>