<table class="search-suggest">
    <colgroup>
        <col width="90px"/>
    </colgroup>
    <? foreach ($brands as $i => $brand):?>
        <tr data-id="<?=$brand->id?>">
            <? if ($i == 0):?>
                <td style="background-color: #fafafa;text-align: right;vertical-align: top;" rowspan="<?=count($brands)?>">
                    Бренды
                </td>
            <? endif;?>
            <td class="result-row" data-href="<?=$brand->getUrl()?>">
                <div class="pull-left">
                    <?=BrandImage::img($brand->getImage(), 'mini', null, ['style' => 'margin-right:6px;width:40px;float:left;'])?>
                    <div style="display: inline-block;">
                        <a>
                            <?=$brand->name?>
                         </a>
                    </div>
                </div>
                <div class="pull-right">
                    <div style="display: inline-block;color:gray;font-size:12px;font-weight:bold">
                        <?=plural($brand->products()->count(), 'товар', 'товара', 'товаров', true)?>
                    </div>
                </div>
            </td>
        </tr>
    <? endforeach;?>
    <? foreach ($categories as $i => $category):?>
        <tr data-id="<?=$category->id?>">
            <? if ($i == 0):?>
                <td style="background-color: #fafafa;text-align: right;vertical-align: top;" rowspan="<?=count($categories)?>">
                    Категории
                </td>
            <? endif;?>
            <td class="result-row" data-href="<?=$category->getUrl()?>">
                <div class="pull-left">
<!--                    --><?//=BrandImage::img($category->getImage(), 'mini', null, ['style' => 'margin-right:6px;width:40px;float:left;'])?>
                    <div style="display: inline-block;">
                        <a>
                            <?=$category->name?>
                        </a>
                    </div>
                </div>
                <div class="pull-right">
                    <div style="display: inline-block;color:gray;font-size:12px;font-weight:bold">
                        <?=plural($category->getProductCount(), 'товар', 'товара', 'товаров', true)?>
                    </div>
                </div>
            </td>
        </tr>
    <? endforeach;?>
    <? foreach ($products as $i => $product):?>
        <tr data-id="<?=$product->id?>" data-href="<?=$product->getUrl()?>">
            <? if ($i == 0):?>
                <td style="background-color: #fafafa;text-align: right;vertical-align: top;" rowspan="<?=count($products)?>">
                    Товары
                </td>
            <? endif;?>
            <td class="result-row">
                <div class="pull-left">
                    <?=ProductImage::img($product->getImage(), 'mini', null, ['style' => 'margin-right:6px;width:40px;float:left;position:absolute;left:5px;'])?>
                    <div style="display: inline-block;padding-left:46px">
                        <a>
                            <?=$product->name?> <br/>
                            <span class="search-result-subtitle"><?=$product->type->getName()?></span>
                        </a>
                    </div>
                </div>
                <div class="pull-right">
                    <div style="position: absolute;right:3px;bottom:3px;color:gray;font-size:12px;font-weight:bold">
                        <?=moneyFormat($product->getSellingPrice())?> руб.
                    </div>
                </div>
            </td>
        </tr>
    <? endforeach;?>
</table>
<style>
    .search-suggest{width:100%;border-radius: 6px;}
    .search-suggest tr {border:1px solid lightgray;}
    .search-suggest td{padding:6px;border:1px solid lightgray}
    .search-suggest .result-row{position: relative;cursor: pointer}
    .search-result-subtitle {font-size:0.9em;color:black;}
    .search-suggest tr:hover{background-color: #fdfdfd;}
    .search-suggest tr.active{background-color: whitesmoke;}
</style>
