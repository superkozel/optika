<!DOCTYPE html>
<?
/** @var Basket $basket */
/** @var User $user */
?>
<html lang="en">
<head>
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.png">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="<?=csrf_token()?>" />
    <? if (! empty($noindex)):?>
    <meta name="robots" content="noindex, nofollow" />
    <? endif;?>
    <title><?=$title?></title>

    <?=Html::script('/js/vendor/jquery-1.11.3.min.js')?>

    <?=Html::script('/bootstrap/js/bootstrap.min.js')?>
    <?=Html::style('/bootstrap/css/bootstrap.min.css')?>

    <?=Html::style('/css/app.css')?>
    <?=Html::style('/css/main.css')?>
    <?=Html::script('/js/underscore.js')?>
    <?=Html::script('/js/vendor/maskedinput.js')?>
    <?=Html::script('/js/script.js')?>

    <?=Html::style('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css')?>

    <?=Html::style('/fresco/fresco.css')?>
    <?=Html::script('/fresco/fresco.js')?>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $().ready(function(){
            $("[data-toggle=popover]").popover();
        })
    </script>
</head>
<body>
<header>
    <div class="hidden-xs" style="position: absolute;right:0px;top:0px;display: inline-block">
        <div id="google_translate_element"></div>
        <script type="text/javascript">
            function googleTranslateElementInit() {
                new google.translate.TranslateElement({pageLangugage: 'ru', includedLanguages: 'en,ja,zh-CN', layout: google.translate.TranslateElement.FloatPosition.TOP_RIGHT}, 'google_translate_element');
            }
        </script>
        <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
    </div>

    <div id="flash" style="position: fixed;top:4px;right:4px;font-size:1.5em;z-index: 9;"></div>
    <script>
        function flash(text, level) {
            var cls = level;
            var icon;
            if (level == 'success') {
                icon = 'check';
            }
            else if (level == 'error') {
                icon = 'remove';
                cls = 'danger';
            }
            else if (level == 'warning') {
                cls = 'warning';
                icon = 'warning-sign';
            }
            else if (level == 'info') {
                icon = 'info-sign';
            }
            var flash = $('<div class="alert alert-dismissable alert-' + cls + '"><i class="fa fa-' + icon + '"></i> ' + text + '</div>');
            $('#flash').append(flash);
            flash.delay(5000).fadeOut('slow', function(){flash.detach().remove()});
        }
    </script>
    <? if (Flash::has()):?>
        <? $flash = Flash::get();?>
        <script>
            flash('<?=$flash['text']?>', '<?=$flash['level']?>');
        </script>
    <? endif;?>

    <? if (Settings::get('bonus_program_active')):?>
        <? if (! Auth::user() || ! Auth::user()->bonus_program_active):?>
            <div style="height:32px;background-color: lightgreen;line-height:32px;display:block;" class="topad">
                <div class="container">
                    Учавствуйте в бонусной программе и получите 1000 бонусов на ваш первый заказ! #Акция
                    &nbsp;&nbsp;<a onclick="showModalSimple('register-bonus-program')" class="btn btn-success btn-xs">Получить 1000 бонусов</a>

                    <a onclick="setcookie('hide_bonus_ad'); $('.topad').hide();" class="pull-right">
                        <i class="fa fa-close"></i>
                    </a>
                </div>
            </div>
            <script>
                if (! getcookie('hide_bonus_ad')) {
                    setTimeout(function(){console.log($('.topad').slideDown().length);}, 0)
                }
            </script>
            <div style="display: none;" id="register-bonus-program-template">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Закрыть"><span aria-hidden="true">×</span></button>
                    Регистрация в бонусной программе
                </div>
                <div class="modal-body">
                    <?=Bootstrap3Form::open(['class' => 'form-horizontal', 'method' => "POST", 'url' => actionts('Auth\RegisterController@register')])?>
                    <?=csrf_field()?>

                    <? $options = ['inputDivOptions' => ['class' => 'col-md-6'], 'labelOptions' => ['class' => 'col-md-4']];?>

                    <?=Bootstrap3Form::hidden('simple', 1)?>
                    <?=Bootstrap3Form::textRow('phone', 'Телефон', null, $options)?>
                    <script type="text/javascript">
                        $('[name=phone]').mask("8(999)999-99-99");
                    </script>
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <?=Bootstrap3Form::checkbox('agreed', 'Прочитал и согласен с <a target="_blank" class="link" href="' . Page::findBySlug('bonusnaya-programma')->getUrl() . '">условиями участия в бонусной программе</a>', 1, $options)?>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Зарегистрироваться
                            </button>
                        </div>
                    </div>
                    <?=Bootstrap3Form::close()?>
                </div>
            </div>
        <? endif;?>
    <? endif;?>
    <style>
        .top-menu {
            line-height: 30px;
            height: 30px;
            color: black;
            box-shadow: 11px -1px 8px #989898;
        }
        .top-menu a{padding-right:24px;color:black;}

        .header-basket{
            color: black;
            background-color: #f9f9f9;
            padding: 2px 12px;
            display: block;
            text-align: center;
            border-bottom: 2px solid red;
        }
        .header-basket span{font-size:15px;FONT-WEIGHT:BOLD;

            color:#ff5656;
            font-weight: bold;
        }
        .top-phone {
            font-size: 1.5em;
            text-align: center;
            font-weight: bold;
            line-height: 1em;
            margin-top: 8px;
        }
        .top-phone a {
            color: #f7114f;
        }
        .top-phone a:hover {
            color: coral;
        }
    </style>
    <div class="top-menu hidden-xs">
        <div class="container">
            <a href="<?=actionts('SalonController@index')?>">Салоны</a>
            <a href="<?=actionts('ActionController@index')?>">Акции</a>
            <a href="<?=actionts('ServiceController@index')?>">Услуги</a>

            <span class="dropdown">
                <a class="dropdown-toggle" id="informaciyaMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    Информация
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" aria-labelledby="informaciyaMenu">
                    <li><a href="<?=Page::url(7)?>">Доставка</a></li>
                    <li><a href="<?=Page::url(17)?>">Способы оплаты</a></li>
                    <li><a href="<?=Page::url(8)?>">Гарантия</a></li>
                    <li><a href="<?=Page::url(18)?>">Обмен и возврат</a></li>
                    <? if (BONUS_PROGRAM_ACTIVE):?>
                        <li><a href="<?=Page::url(9)?>">Бонусная программа</a></li>
                    <? endif;?>
                </ul>
            </span>
            <span class="dropdown">
                <a class="dropdown-toggle" id="onasMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    О компании
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" aria-labelledby="onasMenu">
                    <li><a href="<?=Page::url(1)?>">О компании</a></li>
                    <li><a href="<?=Page::url(2)?>">СМИ о Нас</a></li>
                    <li><a href="<?=Page::url(3)?>">Мы в соцсетях</a></li>
                    <li><a href="<?=Page::url(4)?>">Вакансии</a></li>
                    <li><a href="<?=Page::url(5)?>">Контакты</a></li>
                </ul>
            </span>
            <a href="<?=Page::url(11)?>">Проверка зрения онлайн</a>
            <a href="<?=Page::url(5)?>">Контакты</a>

            <div class="pull-right">
                <? if (Route::has('login')):?>
                    <div class="top-right links">
                        <? if (Auth::check()):?>
                            <span style="color:#d22c77"><span style="font-style: italic;">Приветствуем, </span>
                                <span class="dropdown" style="padding:6px;">
                                    <a class="dropdown-toggle" id="profileMenu" style="font-weight: bold;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        <i class="fa fa-user"></i>
                                        <?=$user->name?>
                                        <span class="caret"></span>
                                    </a>

                                      <ul class="dropdown-menu" aria-labelledby="profileMenu">
                                          <? if ($user && $user->isAdmin()):?>
                                              <li><a href="<?=url('/admin/')?>">Панель управления</a></li>
                                          <? endif;?>
                                        <li><a href="<?=url('/user/')?>">Личный кабинет</a></li>
                                        <li><a href="<?=url('/user/orders/')?>">Заказы</a></li>
                                        <li><a href="<?=actionts('UserController@getProfile')?>">Настройки профиля</a></li>
                                          <? if (BONUS_PROGRAM_ACTIVE):?>
                                        <li><a href="<?=url('/user/bonus')?>">Бонусы <span class="label label-success"><?=$user->getBonuses()?></span></a></li>
                                          <? endif;?>
                                        <li role="separator" class="divider"></li>
                                        <li>
                                            <a href="javascript:logout()">
                                                <i class="fa fa-sign-out"></i> Выход
                                            </a>
                                        </li>
                                      </ul>
                                </span>
                            </span>

                            <? if (BONUS_PROGRAM_ACTIVE):?>
                            <a href="<?=url('/user/bonus')?>">Бонусная карта <span class="label label-success"><?=$user->getBonuses()?> Б</span></a>
                            <? endif;?>
                        <? else:?>
                            <a href="<?=url('/login/')?>">Вход</a>
                            <a href="<?=url('/register/')?>">Регистрация</a>
                            <? if (BONUS_PROGRAM_ACTIVE):?>
                                <a style="color:#f7114f;font-weight:bold;" href="javascript:showModalSimple('register-bonus-program')">
                                    Оформить бонусную карту
                                </a>
                            <? endif;?>
                        <? endif;?>
                    </div>
                <? endif;?>
            </div>
        </div>
    </div>
    <div style="background-color: #006ab0;height:153px;">
        <div class="container">
            <?=view('front.layouts.parts.mobile-menu')?>
            <div class="row" style="margin-top:10px;margin-bottom:10px;">
                <div class="col-sm-4 col-md-3 b-logo">
                    <a href="/">
                        <img src="/images/logo.png" id="logo"/><br/>
                    </a>
    <!--                <div style="color:#d43127;font-weight:bold;line-height:1;">--><?//=plural(Salon::count(), 'салон', 'салона', 'салонов', true)?><!-- оптики в Москве и области</div>-->
                    <div class="hidden-xs">
                        <a target="_blank" href="http://vk.com/club<?=Settings::get('vk')?>" style="background: url('/images/social_icons.png');height:30px;width:30px;background-size:100%;background-position:0px -90px;float:left;margin-right:4px;"></a>
                        <a target="_blank" href="https://twitter.com/<?=Settings::get('twitter')?>" style="background: url('/images/social_icons.png');height:30px;width:30px;background-size:100%;background-position:0px -60px;float:left;margin-right:4px;"></a>
                        <a target="_blank" href="https://www.facebook.com/<?=Settings::get('facebook')?>/" style="background: url('/images/social_icons.png');height:30px;width:30px;background-size:100%;background-position:0px -30px;float:left;margin-right:4px;"></a>
                        <a target="_blank" href="https://www.instagram.com/<?=Settings::get('instagram')?>/" style="height:30px;width:30px;float:left;">
                            <img src="/images/instagram.png" style="width:30px;"/>
                        </a>
                    </div>
                </div>
                <div class="col-sm-4 col-md-5 hidden-xs" style="text-align: center;">
                    <div class="col-xs-6 nopadding">
                        <div class="top-phone">
                            <a href="tel:<?=Settings::get('phone')?>"><?=Settings::get('phone')?></a></div>
                        <div>Интернет-магазин, пн-вс 9-18</div>
                    </div>
                    <div class="col-xs-6 nopadding">
                        <div class="top-phone">
                            <a href="tel:<?=Settings::get('phone2')?>"><?=Settings::get('phone2')?></a>
                        </div>
                        <div>Офис, пн-пт 9-18</div>
                    </div>
                    <div class="clearfix">
                    </div>
                </div>
                <div class="col-sm-4 hidden-xs" style="text-align: right;">
                    <? $lastOrder = $basket->getContentsCount() == 0 ? \App\User::lastOrder() : null;?>
                    <a href="<?=$lastOrder ? actionts('OrderController@view', ['id' => $lastOrder->id]) : actionts('BasketController@index')?>" class="header-basket" style="width:100%;position:relative;">
                        <div style="position:absolute;font-size:25px;left:8px">
                            <? if ($lastOrder):?>
                                <i class="fa fa-truck"></i>
                            <? else:?>
                                <i class="fa fa-shopping-basket"></i>
                            <? endif;?>
                        </div>
                        <div style="float:left;text-align: left;padding-left:6px;margin-left:25px;">
                            <? if (! $lastOrder):?>
                                <span>Ваша корзина</span>
                                <div style="line-height:1em;font-size:14px;">
                                    <span class="total-count"><?=plural($basket->getTotalCount(), 'товар', 'товара', 'товаров', true)?></span>
                                    на <span class="total-price"><?=moneyFormat($basket->getTotalPrice())?></span> рублей
                                </div>
                            <? else:?>
                                <span>Ваш заказ #<?=$lastOrder->getNumber()?></span>
                                <div style="line-height:1em;font-size:14px;">
                                    статус: <?=$lastOrder->status->getName()?>, на сумму <?=moneyFormat($lastOrder->sum)?> руб.
                                </div>
                            <? endif;?>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                    <a href="<?=actionts('FavoriteController@index')?>" class="header-favorites"  style="display:block;width:50%;float:left;text-align: center;background-color: #ff4747;color:white;padding:4px 0px;">
                        Избранное <span class="badge"><?=\ActiveRecord\Favorite::current()->count()?></span>
                    </a>
                    <a href="<?=actionts('BasketController@index')?>" style="width:50%;float:left;text-align: center;background-color: #15bf90;color:white;padding:4px 0px;">
                        Примерка <span class="badge"><?=count($basket->getFittings())?></span>
                    </a>
                    </div>
                <script>
                    function logout()
                    {
                        $.ajax({
                            url: '/logout/',
                            method: 'POST',
                            data: {
                                "_token": "<?=csrf_token()?>"
                            }
                        }).success(function(){
                            window.location.reload();
                        })
                    }

                    Basket.on('add, remove, update', function(result){
                        $('.header-basket').find('.total-price').text(moneyFormat(result.basket.sum));
                        $('.header-basket').find('.total-count').html(plural(result.basket.count, 'товар', 'товара', 'товаров', true));
                    });
                    Favorites.on('add, remove, update', function(result){
                        $('.header-favorites span').text(result.count);
                    });
                </script>
                </div>
            </div>
        </div>
    </div>

    <div style="background-color: #c32222;">
        <div class="container">
            <div class="row" style="position:relative">
                <div class="col-md-3">
                    <div class="catalog-menu-left <? if ($_SERVER['REQUEST_URI'] == '/'):?><?endif;?>">
                        <script>
                            var menuCloseOutside;
                            var closeMenu = function()
                            {
                                $('.catalog-menu-left .left-categories-list').slideUp(100).hide();
                                $(document).off('mouseup', menuCloseOutside);
                                $('.catalog-menu-left').removeClass('opened');
                            }

                            function toggleTopMenu(e)
                            {
//                                e.preventDefault();
                                $('.catalog-menu-left').toggleClass('opened');

                                menuCloseOutside = function (e)
                                {
                                    var container = $('.catalog-menu-left');

                                    if (!container.is(e.target) // if the target of the click isn't the container...
                                            && container.has(e.target).length === 0) // ... nor a descendant of the container
                                    {
                                        closeMenu();
                                    }
                                };


                                if ($('.catalog-menu-left').hasClass('opened')){
                                    $('.catalog-menu-left .left-categories-list').hide().slideDown(140);
                                    $(document).mouseup(menuCloseOutside);
                                }
                                else {
                                    closeMenu();
                                }

//                            $(window).click(function() {
//                            });

                                return false;
                            }
                        </script>
                        <a class="catalog-button" href="<?=\App\Http\Controllers\CatalogController::createUrl()?>" onclick="_.delay(function(){toggleTopMenu(event)}, 100);return false;">
                            <span style="color:white;" href="<?=\App\Http\Controllers\CatalogController::createUrl()?>"><i class="fa fa-bars"></i> Каталог</span>
                            <div class="top-menu-toggle" style="position: absolute;right:4px;cursor:pointer;padding:12px;top:-12px;color:white;">
                                <i class="fa fa-chevron-down"></i>
                            </div>
                        </a>
                        <script>
                            $('.catalog-button').dblclick(function(){
                                window.location = $(this).attr('href');
                                closeMenu();
                            });
                        </script>
                        <style>
                            .catalog-button{

                                font-size:1.1em;
                                position:relative;display:block;text-align: center;
                                background-color: #ff4747;
                                font-weight:bold;color:white;line-height:38px;text-transform: uppercase;
                            }
                            .catalog-button:hover{
                                background-color: #fb6b6b;
                            }
                            .top-menu-toggle:hover{
                                color:white;
                            }
                            .left-categories-list {
                                list-style: none;
                                box-shadow: 0px 0px 6px lightgray;
                            }

                            .left-categories-list a {
                                height: 36px;
                                line-height: 36px;
                                border: 1px solid lightgray;
                                border-top: 0px;
                                padding-left: 10px;
                                display: block;
                                text-transform: uppercase;
                                font-size: 0.9em;
                                letter-spacing: 1px;
                                font-weight: bold;
                            }
                            .left-categories-list a:hover {
                                background-color: #fafafa;
                            }

                            .catalog-menu-left {position:relative;z-index:10;cursor:pointer;margin:4px 0px;}
                            .catalog-menu-left .left-categories-list{display:none;position: absolute;width:100%;background-color: white;}
                            .catalog-menu-left.opened .left-categories-list{display: block;}
                        </style>
                        <div class="left-categories-list">
                            <? $menu = array(
                                'Медицинские оправы' => CatalogCategory::findBySlug('medical_frames')->getUrl(),
                                'Контактные линзы' => CatalogCategory::findBySlug('contact_lenses')->getUrl(),
                                'Солнцезащитные очки' => CatalogCategory::findBySlug('sunglasses')->getUrl(),
                                'Готовые очки' => CatalogCategory::findBySlug('finished_glasses')->getUrl(),
                                'Очковые линзы' => CatalogCategory::findBySlug('lenses')->getUrl(),
                                'Аксессуары для очков' => CatalogCategory::findBySlug('glasses_accessories')->getUrl(),
                                'Подарочные сертификаты' => CatalogCategory::findBySlug('gift_certificates')->getUrl(),
                            );?>
                            <? foreach ($menu as $name => $href):?>
                            <a href="<?=$href?>"><?=$name?></a>
                            <? endforeach;?>
                        </div>
                    </div>
                </div>
<!--                --><?//
//                    if (! Cache::has('top_menu')) {
//                        Cache::put('top_menu', view('front.layouts.parts.menu')->render(), 10);
//                    }
//                ?>
<!--                --><?//=Cache::get('top_menu')?>
                <div <? if ($_SERVER['REQUEST_URI'] != '/'):?>class="hidden-xs"<?endif;?>>
                    <?=view('front.layouts.parts.menu')->render()?>
                </div>
                <div class="col-md-3 hidden-xs">
                    <div class="search-block">
                        <form id="search-form">
                            <i class="fa fa-search" style="position: absolute;margin-top:12px;margin-left:12px;"></i>
                            <a id="searchCancelButton" style="display: none;"><i class="fa fa-close" style="position: absolute;right:0px;margin-top:12px;margin-right:12px;"></i></a>
                            <input id="searchField" autocomplete="off"
                                   title="Поиск в каталоге по артикулу, названию, типу или категории товара"
                                   placeholder="Поиск в каталоге" type="text"
                                   name="search"
                            >
                        </form>
                        <div class="search-results"></div>
                    </div>
                    <style>
                        .search-block{position: relative;margin:4px 0px;}
                        .search-block .search-results{position: absolute;left:-90px;right:0px;
                            width:auto;
                            /*border:1px solid lightgray;*/
                            z-index:18;box-sizing: content-box;
                            border-radius: 6px 0px 6px 6px;
                            background-color: white;
                            margin-top:-3px;
                        }
                        .search-block input{width:100%;line-height:32px;padding:0px 36px;border:3px solid lightgrey;}
                        .search-block input:focus {border-color: darkgrey;}
                    </style>
                    <script>
                        var searchResult = $('.search-results');
                        searchResult.click('tr', function(){

                        });
                        function search_request_ajax(keyword, callback)
                        {
                            $.ajax({
                                type: 'GET',
                                dataType: 'html',
                                data: {
                                    q: keyword
                                },
                                url: '<?=actionts('SearchController@suggest')?>',
                                success: function(result){
                                    searchResult.html(result);

                                    searchResult.find('tr:first').addClass('active');

                                    if (callback)
                                        callback(result);
                                }
                            })
                        }

                        $('#search-form').on('submit', function(){
//                        var value = $('#searchField').val();
//                        if (value) document.location.href = "/?keyword=" + value + "&format=search";
                            return false;
                        });

                        $('#searchField').toggleClass("filled", $('#searchField').val().length > 0).on('keyup', function(e){
                            var keyword = "";

                            var curli = $('.search-results tr.active');
                            if (e.keyCode == 40 || e.keyCode == 38)
                            {
                                if (e.keyCode == 40)
                                {
                                    var nextli = curli.next('[data-id]');
                                    if (nextli.length == 0)
                                    {
                                        var next_block = curli.closest('.search-block').next('.search-block');
                                        if (next_block.length == 0)
                                            nextli = searchResult.find('[data-id]:first');
                                        else
                                            nextli = next_block.find('[data-id]:first');
                                    }

                                }
                                if (e.keyCode == 38)
                                {
                                    var nextli = curli.prev('[data-id]');
                                    if (nextli.length == 0)
                                    {
                                        var prev_block = curli.closest('.search-block').prev('.search-block');
                                        if (prev_block.length == 0)
                                            nextli = searchResult.find('[data-id]:last');
                                        else
                                            nextli = prev_block.find('[data-id]:last');
                                    }
                                }

                                curli.removeClass('active');
                                nextli.addClass('active');
                            }
                            else if (e.keyCode == 13 && curli.length > 0) {


                                document.location.href = curli.data('href');
                                return false;
                            }
                            else if (e.keyCode == 27) {
                                $(this).val('');
                                searchResult.html('');
                            }
                            else
                            {
                                var keyword = $(this).val();
                                if (keyword.length >  1) {
                                    $('#searchField').addClass('loading');
                                    search_request_ajax(keyword, function(){
                                        $('#searchField').removeClass('loading');
                                    });
                                }
                            }

                            if ($(this).val().length > 0) {

                                $('#searchCancelButton').show();
                            }
                            else {
                                $('#searchCancelButton').click();
                            }
                        });

                        $('#searchCancelButton').click(function(){
                            $(this).hide();
                            $('#searchField').val('').removeClass("filled").focus();
                            searchResult.html('');
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
</header>