<?php
/** @var Basket $basket */
?>
<div onclick="slideTo('body')" id="scroller" class="b-top" style="display: none;"><i class="fa fa-chevron-up"></i></div>
<style>
    .b-top{width: 50px;
        height: 50px;
        background-color: #008dd0;
        text-align: center;
        padding: 10px 0;
        position: fixed;
        bottom: 10px;
        right: 10px;;
        cursor: pointer;
        display: none;
        color: #f5f5f5;
        font-size: 20px;
        z-index: 99;
    }
    @media screen and (max-width: 768px) {
        .b-top{
            bottom: 65px;
        }
    }
    #footer a {
        color:#6bb6e0;
        text-decoration:underline;
    }
    #footer a:hover {
        text-decoration:none;
    }
</style>
<script>
    $(window).scroll(function () {
        if ($(this).scrollTop() > 0) {
            $('#scroller').fadeIn();
        } else {
            $('#scroller').fadeOut();
        }
    });

    Basket.on('add, remove, update', function(result){
        $('.mobile-footer').find('.basket-count').html(result.basket.count);
    });
    Favorites.on('add, remove, update', function(result){
        $('.mobile-footer').find('.favorite-count').html(result.count);
    });
</script>
<style>
    .mobile-footer a {
        display:block;
        font-size:12px;
        text-align: center;
        color:white;
        position:relative;;
    }
    .mobile-footer i{
        font-size:24px;
    }

    .mobile-footer-counter-inner{
        font-size:1em;
        border:2px solid rgba(255,255,255,0.9);
        background-color: red;
        color:white;
        width:18px;
        height:18px;
        line-height:15px;
        text-align: center;
        font-weight:bold;
        border-radius:12px;
        position:absolute;
        margin-left:16px;
    }

    .mobile-footer-counter{
        position:absolute;
        right:50%;
        top:-12px;
    }

    .mobile-footer-counter span{
        width:18px;
        position:absolute
        text-align: center;
    }
</style>
<div class="mobile-footer visible-xs" style="position:fixed;bottom:0px;left:0px;background-color: #c32222;width:100%;padding:4px 0px;z-index:1;">
    <div class="row" stype="margin:0px;">
        <a href="<?=actionts('BasketController@index')?>" class="col-xs-3 nopadding">
            <i class="fa fa-shopping-basket"></i>
            <br/>
            Корзина
            <? if ($basket->getContentsCount() > 0):?>
                <div class="mobile-footer-counter">
                    <div class="mobile-footer-counter-inner basket-count">
                        <span><?=$basket->getContentsCount()?></span>
                    </div>
                </div>
            <? endif;?>
        </a>
        <a href="<?=actionts('FavoriteController@index')?>" class="col-xs-3 nopadding">
            <i class="fa fa-star"></i>
            <br/>
            Избранное
            <? if (($count = \ActiveRecord\Favorite::current()->count()) > 0):?>
                <div class="mobile-footer-counter">
                    <div class="mobile-footer-counter-inner favorite-count">
                        <span><?=$count?></span>
                    </div>
                </div>
            <? endif;?>
        </a>
        <a href="<?=actionts('SalonController@index')?>" class="col-xs-3 nopadding">
            <i class="fa fa-home"></i>
            <br/>
            Салоны
        </a>
        <a href="<?=actionts('ActionController@index')?>" class="col-xs-3 nopadding">
            <i class="fa fa-percent"></i>
            <br/>
            Акции
        </a>
    </div>
</div>
<footer id="footer" style="background-color: #444;padding:24px 0px;color:white;">
    <div class="container">
        <div class="row" style="margin-bottom:24px;position:relative;">
            <div style="position: absolute;bottom:0px;right:24px;">
                <a style="color:white;" class="h3" href="/sitemap/"><i class="fa fa-sitemap"></i> Карта сайта</a>
            </div>
            <div class="col-sm-3">
                <b>© Оптика-Фаворит, 2001 - <?=date('Y')?></b>
                <br/><br/>
                Сеть специализированных магазинов оптики в Москве, Балашихе, Дзержинском, Долгопрудном, Железнодорожном, Жуковском, Ивантеевке, Красногорске, Лыткарино, Люберцах, Мытищах, Одинцово, Химках
                <br/><br/>
                <span>
                ООО «ОчиАле»
                г.Москва, ул.Вавилова, д.5, корп.3
                ОГРН 1137746935644,
                ИНН 7725805598
                </span>

                <br/>
                <br/>
                <span>
                +7 (495) 231-46-72
                Пн-Пт 09:00–18:00
                </span>
            </div>
            <div class="col-sm-3">
                <div class="h4 notopmargin">О компании</div>

                <a href="<?=actionts('NewsController@index')?>">Новости</a><br/>
                <a href="<?=Page::findBySlug('contacts')->getUrl()?>">Контакты</a><br/>
                <a href="<?=actionts('SalonController@index')?>">Салоны</a><br/>
                <a href="<?=Page::findBySlug('smi')->getUrl()?>">СМИ о нас</a><br/>
            </div>
            <div class="col-sm-3">
                <div class="h4 notopmargin">Салоны</div>

                <a href="<?=actionts('SalonController@index')?>">Наши салоны</a><br/>
                <a href="<?=actionts('ServiceController@index')?>">Услуги</a><br/>
                <a href="<?=actionts('ActionController@index')?>">Акции</a><br/>
            </div>
            <div class="col-sm-3">
                <div class="h4 notopmargin">Каталог товаров</div>

                <a href="<?=CatalogCategory::findBySlug('medical_frames')->getUrl()?>">Оправы</a><br/>
                <a href="<?=CatalogCategory::findBySlug('contact_lenses')->getUrl()?>">Контактные линзы</a><br/>
                <a href="<?=CatalogCategory::findBySlug('sunglasses')->getUrl()?>">Солнцезащитные очки</a><br/>
                <a href="<?=CatalogCategory::findBySlug('finished_glasses   ')->getUrl()?>">Готовые очки</a><br/>
                <a href="<?=CatalogCategory::findBySlug('lenses')->getUrl()?>">Линзы очковые</a><br/>
                <a href="<?=CatalogCategory::findBySlug('gift_certificates')->getUrl()?>">Подарочные сертификаты</a><br/>
            </div>
        </div>
        <img style="max-width:100%;" src="/images/footer-warning.png"/>
    </div>
</footer>

<? if (CALLBACK_ENABLED):?>
    <!-- Begin LeadBack code {literal} -->
    <script>
        var _emv = _emv || [];
        _emv['campaign'] = 'd0f82e1046ccbd26a95b3bf6';

        (function() {
            var em = document.createElement('script'); em.type = 'text/javascript'; em.async = true;
            em.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'leadback.ru/js/leadback.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(em, s);
        })();
    </script>
    <!-- End LeadBack code {/literal} -->
<? endif;?>
</body>
</html>