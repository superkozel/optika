<script type="text/html" id="add-to-basket-template">
    <div class="modal fade" role="dialog" tabindex="-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Закрыть"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Добавление в корзину</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <img src="<%=product.image.medium%>" style="max-width:100%"/>
                        </div>
                        <div class="col-sm-6">
                            <span style="color:#2aabd2;font-weight:bold;"><%=product.name%></span>
                            <div class="alert alert-warning"><i class="fa fa-info"></i> Выберите линзы</div>
                        </div>
                    </div>
                    <p>Товар добавлен в корзину…</p>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-sm-6">
                            <button type="button" class="col-xs-12 btn btn-default" data-dismiss="modal">Продолжить покупки</button>
                        </div>
                        <div class="col-sm-6">
                            <a href="<?=actionts('BasketController@index')?>" class="col-xs-12 btn btn-primary"><i class="fa fa-check"></i> Перейти в корзину</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>
<script type="text/html" id="added-to-basket-template">
    <% var fitting = fitting || false %>
    <div class="modal fade" role="dialog" tabindex="-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Закрыть"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title"><% if (fitting){%>Оправа добавлена в примерку<%} else {%>Товар добавлен в корзину<% } %></h4>
                </div>
                <div class="modal-body">
                    <% if (added.length == 1) { %>
                    <% var product = added[0].item.product%>
                    <div class="row">
                        <div class="col-sm-4">
                            <img src="<%=product.image.medium%>" style="max-width:100%"/>
                        </div>
                        <div class="col-sm-8">
                            <span style="color:#2aabd2;font-weight:bold;"><%=product.name%></span>
                            <div class="alert alert-success"><i class="fa fa-check"></i> <% if (fitting){%>Добавлен в примерку<%} else {%>Добавлен в корзину<% } %></div>
                            <!--                            <p>-->
                            <!--                                <a href="console.log(product.basket_item_id)"><i class="fa fa-close"></i> Убрать из корзины</a></p>-->
                            <!--                             <p>-->
                            <% if (! fitting){%>
                            В вашей корзине <b><%=basket.count%> <%=plural(basket.count, 'товар', 'товара', 'товаров')%></b><br/>
                            на сумму <b><%=moneyFormat(basket.sum)%> Р.</b>
                            <% } else {%>
                            <h3>Примерка</h3>

                            <div class="row">
                                <% for (i=1;i<=4;i++){%>
                                    <% var item = fittings[i - 1];%>
                                    <div class="col-xs-3" style="position:relative;">
                                         <div style="position: relative;background: url('/images/noimage.png');background-size: contain;background-repeat: no-repeat;background-position: center center;">
                                         <% if (item){%>
<!--                                            <span style="position: absolute;left:3px;top:3px;"><%=i%>. </span>-->
                                            <img src="<%=item.product.image.small%>" style="width:100%;"/>
                                            <%=item.product.model_name%>
                                         <% } else {%>
    <!--                                         <img src="" style="width:100%;"/>%>-->
                                         <% } %>
                                         </div>
                                        <% if (item){%>
                                            <i style="cursor:pointer;position:absolute;right:3px;top:3px;color:red;" onclick="var el = event.currentTarget;Basket.remove(<%=item.id%> ,function(result){$(el).closest('div').html('');})" class="fa fa-close"></i>
                                        <% } %>
                                     </div>
                                <% } %>
                            </div>
                            <h4>Вы можете выбрать <b>еще <%=plural(4 - (fittings.length), 'оправу', 'оправы', 'оправ', true)%></b> для примерки.</h4>
                            <% } %>
                            </p>
                        </div>
                    </div>
                    <% if (! fitting){%>
                    <% if (product.type_id == <?=ProductType::OPRAVA?>){%>
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2">
                            <a href="<?=CatalogCategory::findBySlug('lenses')->getUrl()?>" onclick="openSelectLensesModal('<%=product.basket_item_id%>')" type="button" class="col-xs-12 btn btn-success btn-lg"><i class="fa fa-plus"></i> Подобрать линзы для оправы</a>
                        </div>
                    </div>
                    <% } %>
                    <% } %>
                    <% } else {%>
                    <div class="row">
                        <div class="col-sm-12">
                            <table class="table table-condensed table-bordered">
                                <% _.each(added, function(add){%>
                                <tr>
                                    <td><img src="<%=add.item.product.image.mini%>"/></td>
                                    <td><%=add.item.product.name%><br/>
                                        <%=add.item.product.type%>
                                    </td>
                                    <td>Добавлено: <%=add.count%></td>
                                </tr>
                                <% }) %>
                            </table>
                        </div>
                        <div class="col-sm-12">
                            <div class="alert alert-success"><i class="fa fa-check"></i> Добавлены в корзину</div>
                            В вашей корзине <b><%=basket.count%> <%=plural(basket.count, 'товар', 'товара', 'товаров')%></b><br/>
                            на сумму <b><%=moneyFormat(basket.sum)%> Р.</b>
                            </p>
                        </div>
                    </div>
                    <% } %>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-sm-6">
                            <button type="button" class="col-xs-12 btn btn-default" data-dismiss="modal"><% if (fitting){%>Продолжить выбор<% } else { %>Продолжить покупки<% } %></button>
                        </div>
                        <div class="col-sm-6">
                            <% if (fitting){%>
                            <a href="<?=actionts('BasketController@index')?>" class="col-xs-12 btn btn-primary"><i class="fa fa-check"></i> Оформить заказ</a>
                            <% } else { %>
                            <a href="<?=actionts('BasketController@index')?>" class="col-xs-12 btn btn-primary"><i class="fa fa-check"></i> Перейти в корзину</a>
                            <% } %>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>

<script type="text/html" id="quickorder-template">
    <div class="modal fade" role="dialog" tabindex="-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Закрыть"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Быстрый заказ</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <img src="<%=product.image.medium%>" style="max-width:100%"/>
                        </div>
                        <div class="col-sm-6">
                            <span style="color:#2aabd2;font-weight:bold;"><%=product.name%></span>

                            <p>
                                Заполните форму и наш консультант сам свяжется с вами,
                                чтобы помочь оформить до конца заказ и подобрать линзы
                            </p>

                            <?=Bootstrap3Form::open()?>
                            <?=Bootstrap3Form::textareaRow('phone', 'Телефон')?>
                            <?=Bootstrap3Form::submit('Отправить')?>
                            <?=Bootstrap3Form::close()?>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-xs-12">
                            <button type="button" class="col-xs-12 btn btn-default" data-dismiss="modal">Оформить заказ</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>

<script type="text/html" id="modal-template">
    <div class="modal fade" role="dialog" tabindex="-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <% if (content){%>
                <%=content%>
                <% } %>
            </div>
        </div>
    </div>
</script>
<script type="text/html" id="bonus-modal-template">
    <!--    <div class="modal-header">-->
    <!--        <h4 style="margin:0px;"><i class="fa fa-thumbs-up"></i> Получайте бонусы и экономьте на покупке оптики</h4>-->
    <!--    </div>-->
    <div class="modal-body">
        <img src="http://usu.kz/img/page_salon_bonuses.jpg" style="width:100%;"/>
        <br/><br/>
        <p>
            Каждый раз, когда вы совершаете покупку в <b>любом из наших салонов или интернет магазине</b>,
            вы можете <b>получать бонусы</b>, позволяющие оплатить часть последующих заказов.
        </p>
        <p style="text-decoration: underline">
            Чтобы начать накапливать бонусы, необходимо получить виртуальную бонусную карту
        </p>
    </div>
    <div class="modal-footer">
        <div class="row">
            <div class="col-xs-6">
                <a href="<?=Page::findBySlug('bonusnaya-programma')->getUrl()?>" class="btn btn-default col-xs-12" href="">Читать подробнее</a>
            </div>
            <div class=" col-xs-6">
                <a class="btn btn-primary col-xs-12" href="<?=actionts('UserController@bonus')?>"><i class="fa fa-check"></i> Принять участие в программе</a>
            </div>
        </div>
    </div>
</script>

<script type="text/html" id="izgotovlenie-template">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Закрыть"><span aria-hidden="true">×</span></button>
        <div class="h4">Бесплатное изготовление очков</div>
    </div>
    <div class="modal-body">
        <img src="http://optikatrio.ru/images/product/l/2795ef1f.jpg" style="width:100%;"/>
        <br/><br/>
        <p>
            На основе <b>любой оправы</b> из нашего ассортимента вы можете заказать <b>бесплатное изготовление очков</b>.
        </p>
        <p><b>Срок изготовления</b> 7-14 дней, в зависимости от наличия линз и оправ и сложности работы.</p>

        <div class="h1">Как заказать изготовление очков из оправы:</div>
        <ol>
            <li>Выберите оправу, нажмите "Купить"</li>
            <li>Выберите подходящие линзы для данной оправы</li>
            <li>Мы посчитаем срок изготовления и предложим ближайшую возможную дату доставки</li>
        </ol>
    </div>
    <div class="modal-footer">
        <div class="row">
            <div class="col-xs-6 col-xs-offset-6" data-dismiss="modal">
                <a class="btn btn-default col-xs-12"><i class="fa fa-check"></i> Продолжить покупки</a>
            </div>
        </div>
    </div>
</script>

<script type="text/html" id="fitting-modal-template">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Закрыть"><span aria-hidden="true">×</span></button>
        <div class="h1">Примерка оправ на заказ</div>
    </div>
    <div class="modal-body">
        <img style="width:100%;" src="http://zzrenie.ru/wp-content/uploads/2016/11/338987.jpg"/>
        <br/><br/>
        <p>Покупка очков - ответственное занятие, ведь от у
            Неправильный выбор оправы может доставить множество неудобств и привести к разочарованию в покупке.</p>

        <p style="text-decoration: underline">Наш магазин помогает покупать только то, что вам действительно подходит!</p>
        <p>
            Вы можете заказать на примерку <b>до четырых оправ</b>, которые будут доставлены в выбранный вами салон.
        </p>
    </div>
    <div class="modal-footer">
        <div class="row">
            <div class="col-xs-6 " data-dismiss="modal">
                <a class="btn btn-primary col-xs-12"><i class="fa fa-info"></i> Читать подробнее</a>
            </div>
            <div class="col-xs-6 " data-dismiss="modal">
                <a class="btn btn-default col-xs-12"><i class="fa fa-check"></i> Продолжить покупки</a>
            </div>
        </div>
    </div>
</script>
<script type="text/html" id="delivery-modal-template">
</script>

<script type="text/html" id="payment-modal-template">
</script>

<script type="text/html" id="vibrat-opravy-dlya-linz-modal-template">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Закрыть"><span aria-hidden="true">×</span></button>
        <h3>Выберите оправу для линз</h3>
    </div>
    <? if ($basket->getContentsCount() > 0):?>
    <div class="lens-selector">
        <table class="table table-condensed" style="width:100%">
            <? foreach ($basket->getPurchases() as $basketItem):?>
                <? if ($curProduct = $basket->getItem($basketItem->getProductId()) AND $curProduct->type->is(ProductType::OPRAVA)):?>
                    <? $id = 'oprava_' . $basketItem->getId();?>
                    <tr onclick="configurateLenses(<%=lensId%>, $(this).data('basket-item-id'))" style="margin-bottom:0px;font-weight:normal;cursor:pointer;" for="<?=$id?>" data-basket-item-id="<?=$basketItem->getId()?>">
                        <!--                                    --><?//=Bootstrap3Form::radio('oprava', 'оправа', null, ['id' => $id, 'style' => 'float:left;height:42px;margin:0px 12px;'])?>
                        <td>
                            <?=ProductImage::img($curProduct->images->first(), 'small', null, ['style' => 'margin:0px 12px;float:left;'])?>
                        </td>
                        <td>
                            <h4><b style="color:red;"><?=$basketItem->getCount()?>x</b> <?=$curProduct->name?></h4>
                            <? if (empty($basketItem->getModifiers()[ProductModifier::LENS_LEFT])):?>
                                Без линз
                            <? else:?>
                                Линзы: <?=$basketItem->getModifiers()[ProductModifier::LENS_LEFT]?>; Слева: asd, Справа: Фывф
                            <? endif;?>
                        </td>
                        <td style="text-align: center;vertical-align: middle">
                            <? if (! empty($basketItem->getModifiers()[ProductModifier::LENS_LEFT])):?>
                                <a class="btn btn-primary btn-md">Поменять линзы</a>
                            <? else:?>
                                <a class="btn btn-primary btn-md">Вставить линзы</a>
                            <? endif;?>
                        </td>
                    </tr>
                    <style>
                        .lens-selector tr{cursor:pointer;}
                        .lens-selector tr:hover{background-color: #fafafa}
                    </style>
                <? endif;?>
            <? endforeach;?>
        </table>
        <? else:?>
            <div class="modal-body">
                <p class="alert alert-warning">В корзине еще нет ни одной оправы.</p>
            </div>
            <div class="modal-footer">
                <a href="<?=CatalogCategory::findBySlug('medical_frames')->getUrl()?>" class="btn btn-primary">Перейти в каталог оправ</a>
            </div>
        <? endif;?>
    </div>
</script>