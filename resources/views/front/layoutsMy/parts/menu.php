<div class="col-md-6 catalog-horizontal-links">
    <?
    $medicalFramesCategory = CatalogCategory::findBySlug('medical_frames');
    $contactLensesCategory = CatalogCategory::findBySlug('contact_lenses');
    $sunglasssesCategory = CatalogCategory::findBySlug('sunglasses');
    $menu = array(
        'Солнцезащитные очки' => [CatalogCategory::findBySlug('sunglasses')->getUrl(),
            [
                [
                    ['По форме', ['Кошачий глаз', 'Круглые', 'Квадратные']],
                    ['Модные', ['2017 мужские', '2017 женские', 'Необычные', 'Хит сезона']],
                ],
                [
                    ['Пол', [
                        'Мужские' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['prinadlezhnost' => 'muzhskie']),
                        'Женские' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['prinadlezhnost' => 'zhenskie']),
                    ]],
                    ['Возраст', [
                        'Детские' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['prinadlezhnost' => 'detskie']),
                        'Подростку' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['prinadlezhnost' => 'podrostkovye']),
                        'Для взрослого' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['prinadlezhnost' => 'zhenskie,muzhskie,uniseks']),
                    ]],
                    ['По цене', [
                        'До 5000 руб.' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['price' => '-5000']),
                        'От 5000 до 10000 руб.' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['price' => '5000-10000']),
                        'От 10000 до 20000 руб.' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['price' => '10000-20000']),
                        'От 20000 руб.' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['price' => '20000-']),
                    ]],

                ],
                [
                    ['Популярные бренды', [
                        'RayBan' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['brand' => 'ray_ban']),
                        'Polaroid' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['brand' => 'polaroid']),
                        'Lina Latini' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['brand' => 'lina_latini']),
                        'Lucia Valdi' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['brand' => 'lucia_valdi']),
                        'FreshLook' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['brand' => 'freshlook']),
                        'Maxima' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['brand' => 'maxima']),
                        'Zeiss' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['brand' => 'zeiss']),
                    ]],
                    ['Элитные бренды', [
                        'DITA' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['brand' => 'ray_ban']),
                        'Tom Brown' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['brand' => 'tom_brown']),
                        'CHROME HEARTS' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['brand' => 'chrome_hearts']),
                        'GUCCI' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['brand' => 'cucci']),
                        'FENDI' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['brand' => 'fendi']),
                        'PRADA' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['brand' => 'prada']),
                    ]],
                ],
                [
                    ['Аксессуары', ['Футляр', 'Салфетка', 'Средства ухода']]
                ],
            ]
        ],
        'Оправы' => [
            CatalogCategory::findBySlug('medical_frames')->getUrl(),
            [
                [
                    ['По форме', [
                        'Овальные',
                        'Кошачий глаз',
                        'Круглые',
                        'Квадратные',
                        'Авиатор',
                        'Бабочки',
                        'Wayfarer',
                        'Клубмастер']
                    ],
                    ['Стиль', ['Модные', 'Современные', 'Яркие', 'Классические']],
                    [['Изготовление очков на заказ', '/uslugi/']],
//                                    [['Ремонт очков', '/uslugi/']],
                ],
                [
                    ['Пол', [
                        'Мужские' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['prinadlezhnost' => 'muzhskie']),
                        'Женские' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['prinadlezhnost' => 'zhenskie']),
                    ]],
                    ['Возраст', [
                        'Детские' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['prinadlezhnost' => 'detskie']),
                        'Подростку' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['prinadlezhnost' => 'podrostkovye']),
                        'Для взрослого' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['prinadlezhnost' => 'zhenskie,muzhskie,uniseks']),
                    ]],
                    ['По цене', [
                        'До 5000 руб.' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['price' => '-5000']),
                        'От 5000 до 10000 руб.' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['price' => '5000-10000']),
                        'От 10000 до 20000 руб.' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['price' => '10000-20000']),
                        'От 20000 руб.' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['price' => '20000-']),
                    ]],

                ],
                [
                    ['Конструкция', [
                        'Винтовые' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['kontrukciya' => 'vintovye']),
                        'Лесочные' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['kontrukciya' => 'lesochnye']),
                        'Монолинза' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['kontrukciya' => 'monolinza']),
                        'Половинка' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['kontrukciya' => 'polovinka']),
                        'Полнооправные' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['kontrukciya' => 'polnoopravnye']),
                    ]],
                    ['Материал', [
                        'Металлические' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['material' => 'metall']),
                        'Золотые' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['material' => 'zoloto']),
//                                        'Минерал' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['material' => 'mineral']),
//                                        'Мономер' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['material' => 'monomer']),
                        'Пластик' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['material' => 'plastik']),
//                                        'Полимер' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['material' => 'polimer']),
                    ]]
                ],
                [
                    ['Элитные бренды', [
                        'DITA' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['brand' => 'dita']),
                        'Tom Brown' => \App\Http\Controllers\CatalogController::createUrlSimple($sunglasssesCategory, ['brand' => 'tom_brown']),
                        'CHROME HEARTS' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['brand' => 'chrome_hearts']),
                        'GUCCI' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['brand' => 'cucci']),
                        'FENDI' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['brand' => 'fendi']),
                        'PRADA' => \App\Http\Controllers\CatalogController::createUrlSimple($medicalFramesCategory, ['brand' => 'prada']),
                    ]],
                    ['Модные'],
                    ['Хиты 2017'],
                ],
            ]
        ],
//        'Готовые очки' => [CatalogCategory::findBySlug('finished_glasses')->getUrl(),
//            [
//                [
//                    ['По форме', ['Кошачий глаз', 'Круглые', 'Квадратные']]
//                ],
//                [
//                    ['Пол', ['Мужские', 'Женские']],
//                    ['Возраст', ['Детские', 'Подростку', 'Для взрослого']]
//                ],
//                [
//                    ['Конструкция', ['Винтовые', 'Лесочные', 'Монолинза', 'Половинка', 'Полнооправные']]
//                ],
//                [
//                    ['Популярные бренды', ['Acuvue', 'AIR OPTIX', 'Clear', 'FreshLook', 'Maxima', 'Zeiss']],
//                ],
//            ]
//        ],
        'Линзы' => [CatalogCategory::findBySlug('lenses')->getUrl(),
            [
                [
                    ['Поиск по рецепту', [
                        ['Сфера', 'opticheskaya-sila-sfera', CatalogFilter::findBySlug('opticheskaya-sila-sfera')->getOptions(true)],
                        ['Цилиндр', 'cilindr', CatalogFilter::findBySlug('cilindr')->getOptions(true)],
                        ['Аддидация', 'addidaciya', CatalogFilter::findBySlug('addidaciya')->getOptions(true)]
                    ], 'form', CatalogCategory::findBySlug('lenses')->getUrl()]
                ],
                [
                    ['Тип линз', [
                        'Традиционные',
                        'Бифокальные',
                        'Прогрессивные',
                        'Офисные',
                        'Специальные'
                    ]],

                    ['Дизайн поверхности', [
                        'Сферический' => \App\Http\Controllers\CatalogController::createUrlSimple(CatalogCategory::findBySlug('lenses'), ['geometriya-linzy' => 'sfericheskaya']),
                        'Асферический' => \App\Http\Controllers\CatalogController::createUrlSimple(CatalogCategory::findBySlug('lenses'), ['geometriya-linzy' => 'asfericheskaya']),
                        'Би- Асферический' => \App\Http\Controllers\CatalogController::createUrlSimple(CatalogCategory::findBySlug('lenses'), ['geometriya-linzy' => 'dvojnaya-asferika']),
//                                        'Лентикулярный' => \App\Http\Controllers\CatalogController::createUrlSimple(CatalogCategory::findBySlug('lenses'), ['geometriya-linzy' => 'asfericheskaya']),
                    ]],
                ],

                [
                    ['Особенности линз', [
                        'Прозрачные',
                        'Фотохромные',
                        'Поляризационные',
                        'Солнцезащитные',
                        'Тонированыые',
                        'Специальные'
                    ]],
                ],

                [
                    ['Материал', [
                        'Полимер' => \App\Http\Controllers\CatalogController::createUrlSimple(CatalogCategory::findBySlug('lenses'), ['material' => 'polimer']),
                        'Минерал' => \App\Http\Controllers\CatalogController::createUrlSimple(CatalogCategory::findBySlug('lenses'), ['material' => 'mineral']),
                        'Пластик' => \App\Http\Controllers\CatalogController::createUrlSimple(CatalogCategory::findBySlug('lenses'), ['material' => 'plastik']),
                        'Мономер' => \App\Http\Controllers\CatalogController::createUrlSimple(CatalogCategory::findBySlug('lenses'), ['material' => 'monomer']),
                    ]],
                    ['Покрытие', [
                        'Базовое' => \App\Http\Controllers\CatalogController::createUrlSimple(CatalogCategory::findBySlug('lenses'), ['pokrytie-linzy' => 'bazovoe']),
                        'Бизнес' => \App\Http\Controllers\CatalogController::createUrlSimple(CatalogCategory::findBySlug('lenses'), ['pokrytie-linzy' => 'biznes']),
                        'Премиальное' => \App\Http\Controllers\CatalogController::createUrlSimple(CatalogCategory::findBySlug('lenses'), ['pokrytie-linzy' => 'premialnoe']),
                    ]],
                ]
            ]
        ],
        'Контактные линзы' => [
            CatalogCategory::findBySlug('contact_lenses')->getUrl(),
            [
                [
                    ['Режим замены', [
                        'Однодневные' => \App\Http\Controllers\CatalogController::createUrl(CatalogCategory::findBySlug('one_day_lenses')),
                        'Двухнедельные',
                        'Одномесячные',
                        'Длительного ношения'
                    ]],
                    ['Тип линз', [
                        'Торические' => \App\Http\Controllers\CatalogController::createUrl(CatalogCategory::findBySlug('toric_lenses')),
                        'Цветные и оттеночные' => \App\Http\Controllers\CatalogController::createUrl(CatalogCategory::findBySlug('color')),
                        'Цветные для тёмных глаз',
                        'Crazy(с рисунком)' => \App\Http\Controllers\CatalogController::createUrl(CatalogCategory::findBySlug('toric_lenses')),
                        'Спортивные'
                    ]],
                ],
                [
                    ['Популярные бренды', [
                        'Acuvue' => \App\Http\Controllers\CatalogController::createUrlSimple($contactLensesCategory, ['brand' => 'acuvue']),
                        'AIR OPTIX' => \App\Http\Controllers\CatalogController::createUrlSimple($contactLensesCategory, ['brand' => 'air_optix']),
                        'Clear' => \App\Http\Controllers\CatalogController::createUrlSimple($contactLensesCategory, ['brand' => 'clear']),
                        'FreshLook' => \App\Http\Controllers\CatalogController::createUrlSimple($contactLensesCategory, ['brand' => 'freshlook']),
                        'Maxima' => \App\Http\Controllers\CatalogController::createUrlSimple($contactLensesCategory, ['brand' => 'maxima']),
                        'Zeiss' => \App\Http\Controllers\CatalogController::createUrlSimple($contactLensesCategory, ['brand' => 'zeiss']),
                    ]]
                ],
                [
                    ['Сопутствующие товары', [
                        'Растворы и капли' => CatalogCategory::findBySlug('solutions_and_eye_drops')->getUrl(),
                        'Футляры' => CatalogCategory::findBySlug('accessories')->getUrl(),
                        'Щипцы' => CatalogCategory::findBySlug('accessories')->getUrl(),
                        'Очки-тренажёры',
                    ]]
                ],
                [
                    ['Поиск по рецепту', [
                        ['Оптическая сила', 'opticheskaya-sila-sfera', CatalogFilter::findBySlug('opticheskaya-sila-sfera')->getOptions(true)],
                        ['Радиус кривизны', 'cilindr', CatalogFilter::findBySlug('cilindr')->getOptions(true)],
                        ['Аддидация', 'addidaciya', CatalogFilter::findBySlug('addidaciya')->getOptions(true)]
                    ], 'form', $contactLensesCategory->getUrl()]
                ],
            ]
        ],
        'Подарочные карты' => [CatalogCategory::findBySlug('gift_certificates')->getUrl()],
    );?>
    <? foreach ($menu as $name => $href):?><div class="top-menu-horizontal-link <? if (! empty($href[1])):?>has-children<?endif?>">
        <a href="<?=$href[0]?>"><?=$name?></a>
        <? if (! empty($href[1])):?>
            <div class="sub-menu">
                <div style="background-color: white;border:1px solid darkgrey;box-shadow: 0px 0px 3px lightgrey;padding:22px;border-top:none;">
                    <a href="" style="position: absolute;right:3px;top:3px;">
                        <i class="fa fa-close"></i>
                    </a>
                    <? foreach ($href[1] as $subMenuBlock):?>
                        <div class="col-sm-3">
                            <? foreach ($subMenuBlock as $k => $subMenuBlockBlock):?>
                                <? if (! is_array($subMenuBlockBlock[0])):?>
                                    <span class="sub-menu-header" style="display:block;<? if ($k > 0):?>margin-top:8px;<?endif;?>"><?=$subMenuBlockBlock[0]?></span>
                                <? else:?>
                                    <a href="<?=$subMenuBlockBlock[0][1]?>" class="sub-menu-header" style="display:block;color:#982eb9;<? if ($k > 0):?>margin-top:8px;<?endif;?>`"><?=$subMenuBlockBlock[0][0]?></a>
                                <? endif;?>

                                <? if (! empty($subMenuBlockBlock[2])):?>
                                    <?=Bootstrap3Form::open(['url' => $subMenuBlockBlock[3], 'method' => 'GET']);?>
                                    <? foreach ($subMenuBlockBlock[1] as $subMenuBlockEl):?>
                                        <?=Bootstrap3Form::selectRow($subMenuBlockEl[1], $subMenuBlockEl[0], ['' => '-'] + $subMenuBlockEl[2]   )?>
                                    <? endforeach;?>
                                    <button class="btn btn-success" type="submit" value="1">Показать варианты</button>
                                    <?=Bootstrap3Form::close();?>
                                <? else:?>
                                    <? if (! empty($subMenuBlockBlock[1])):?>
                                        <? foreach ($subMenuBlockBlock[1] as $subMenuBlockEl => $subMenuBlockElVal):?>
                                            <? if (is_numeric($subMenuBlockEl)):?>
                                                <a><?=$subMenuBlockElVal?></a>
                                            <? else:?>
                                                <?=Html::link($subMenuBlockElVal, $subMenuBlockEl)?>
                                            <? endif;?>
                                        <? endforeach;?>
                                    <? endif;?>
                                <? endif;?>
                            <? endforeach;?>
                        </div>
                    <? endforeach;?>
                    <div class="clearfix"></div>
                </div>
            </div>
        <? endif;?>
        </div><? endforeach;?>
</div>
<style>
    .sub-menu{position:absolute;left:0px;width:100%;z-index:99;padding:0px 16px;}
    .sub-menu a{display: block;line-height:26px;}
    .sub-menu a:hover{background-color: whitesmoke;}
    .sub-menu-header{font-weight:bold;font-size:1.1em;color:#dc4444;text-transform: uppercase}

    .top-menu-horizontal-link .sub-menu{
        visibility: hidden;
        transition: visibility 0s;
        transition-delay: 0.4s;
    }
    .top-menu-horizontal-link > a {color:white;font-size:1em;text-transform: uppercase;font-size:0.89em;
        text-align:center;
        line-height:46px;
        display: block;
    }
    .top-menu-horizontal-link:hover{background-color: #ff4747}
    .top-menu-horizontal-link:hover > a{color:lightyellow;}
    .top-menu-horizontal-link:hover .sub-menu{
        visibility: visible;
        transition-delay: 0.2s;
    }
    .top-menu-horizontal-link .sub-menu a:not([href]){
        color:darkgray;
    }
    .top-menu-horizontal-link {
        display: block;
    }
    .top-menu-horizontal-link > a{
        padding:0px;
        position:relative;
    }
    .top-menu-horizontal-link.has-children > a:hover:after{
        content: "";
        display: block;
        position: absolute;
        bottom: -9px;
        left: 50%;
        width: 0;
        height: 0;
        margin-left: -10px;
        border-style: solid;
        border-width: 10px 10px 0;
        border-color: #ff4747 transparent transparent transparent;
        z-index: 1011;
    }

    .catalog-horizontal-links{position:initial}

    @media (min-width: 768px) {
        .top-menu-horizontal-link {
            display: inline-block;
            margin:0px;
        }
        .top-menu-horizontal-link > a {
            padding:0px 7px;
        }

        .catalog-horizontal-links .top-menu-horizontal-link:first-child{margin-left:-15px;}
        .catalog-horizontal-links .top-menu-horizontal-link:last-child{margin-right:-15px;}

        .top-menu-horizontal-link  a {
            text-align:left;
        }
    }
</style>