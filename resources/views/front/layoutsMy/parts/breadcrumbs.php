<div class="container">
	<? if (! empty($breadcrumbs) && $breadcrumbs->count() > 0):?>
		<ol class="breadcrumb">
			<li><a href="/">Главная</a></li>
			<? foreach ($breadcrumbs as $crumb):?>
				<li><a <? if (! empty($crumb['url'])):?>href="<?=$crumb['url']?>"<?endif;?>><?=$crumb['label']?></a></li>
			<? endforeach;?>
		</ol>
	<? endif;?>
</div>