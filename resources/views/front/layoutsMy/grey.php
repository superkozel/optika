<?=view('front.layouts.parts.header')->render()?>
    <div class="content grey">
        <?=view('front.layouts.parts.breadcrumbs')?>
        <div class="container">
            <h1><?=$h1?></h1>
        </div>
        <?=$content?>
    </div>
<?=view('front.layouts.parts.jstemplates')->render()?>
<?=view('front.layouts.parts.footer')->render()?>