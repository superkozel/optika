<?=view('front.layouts.parts.header')->render()?>
    <div class="content">
        <?=view('front.layouts.parts.breadcrumbs')?>
        <div class="container">
            <h1><?=$h1?></h1>
            <div class="row">
                <div class="col-md-8">
                    <?=$content?>
                </div>
                <div class="col-md-4">
                    <? if (count($page->children) > 0):?>
                        <? $parent = $page;?>
                    <? elseif ($page->parent):?>
                        <? $parent = $page->parent;?>
                    <? endif;?>

                    <style>
                        .subcategory-link:hover {background-color:whitesmoke;}
                        .subcategory-link.active{background-color:#2aabd2;color:white;}
                        .category-link.active{background-color:#2aabd2;color:white;}
                    </style>
                    <? if (! empty($parent)):?>
                        <div style="border-bottom:0px;">
                            <a href="<?=$parent->getUrl()?>" class="h3 notopmargin category-link <? if ($parent->isCurrent()):?>active<?endif;?>" style="border-bottom:1px solid lightgrey;display:block;padding:0px 12px;line-height: 2em;margin-bottom:0px;"><?=$parent->getH1()?></a>
                            <? foreach($parent->children()->where('disabled', 0)->get() as $child):?>
                                <a class="subcategory-link <? if ($child->isCurrent()):?>active<?endif;?>" style="border-bottom:1px solid lightgrey;line-height: 3;display: block;padding:0px 12px;" href="<?=$child->getUrl()?>"><?=$child->getH1()?></a>
                            <? endforeach;?>
                        </div>
                    <? endif;?>
                </div>
            </div>
        </div>
    </div>
<?=view('front.layouts.parts.jstemplates')->render()?>
<?=view('front.layouts.parts.footer')->render()?>