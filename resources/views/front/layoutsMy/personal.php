<?=view('front.layouts.parts.header')->render()?>
<div class="content">
    <?=view('front.layouts.parts.breadcrumbs')?>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4 style="margin-top:0px;">Покупатель</h4>
                        <? if (Auth::user()):?>

                            <? if (BONUS_PROGRAM_ACTIVE):?>
                            <a href="<?=actionts('UserController@bonus')?>"><i class="fa fa-credit-card"></i> Бонусная карта <span class="label label-success"><?=$user->getBonuses()?></span></a><br/>
                            <? endif;?>
                            <a href="<?=actionts('BasketController@index')?>"><i class="fa fa-shopping-basket"></i> Корзина <span class="label label-danger"><?=$basket->getTotalCount()?></span></a><br/>
                            <a href="<?=actionts('FavoriteController@index')?>"><i class="fa fa-heart-o"></i> Избранное <span class="label label-info"><?=\ActiveRecord\Favorite::current()->count()?></a><br/>

                            <hr/>
                            <h4>Заказы</h4>
                            <? $lastOrder = \App\User::lastOrder();?>
                            <? if ($lastOrder):?>
                                <a href="<?=actionts('OrderController@view', ['id' => $lastOrder->id])?>"><i class="fa fa-fire"></i> Последний заказ</a> <span class="label label-info"><?=$lastOrder->status->getName()?></span><br/>
                            <? endif;?>
                            <a href="<?=actionts('UserController@orders')?>"><i class="fa fa-history"></i> История покупок</a><br/>
                            <hr/>

                            <a href="<?=actionts('UserController@getProfile')?>"><i class="fa fa-vcard"></i> Изменить личные данные</a><br/>
                            <a href="<?=actionts('UserController@getPassword')?>"><i class="fa fa-key"></i> Изменить пароль</a><br/>
                            <br/>
                            <a>
                                <form method="POST" onclick="$(event.currentTarget).submit()" action="<?=actionts('Auth\LoginController@logout')?>">
                                    <i class="fa fa-sign-out"></i> Выход
                                    <?=Bootstrap3Form::token()?>
                                </form>
                            </a>
                        <? else:?>

                            <a href="<?=actionts('BasketController@index')?>"><i class="fa fa-shopping-basket"></i> Корзина <span class="label label-danger"><?=$basket->getTotalCount()?></span></a><br/>
                            <a href="<?=actionts('FavoriteController@index')?>"><i class="fa fa-heart-o"></i> Избранное <span class="label label-info"><?=\ActiveRecord\Favorite::current()->count()?></a><br/>

                            <? $lastOrder = \App\User::lastOrder();?>
                            <? if ($lastOrder):?>
                                <hr/>
                                <h4>Заказы</h4>
                                <a href="<?=actionts('OrderController@view', ['id' => $lastOrder->id])?>"><i class="fa fa-fire"></i> Последний заказ</a> <span class="label label-info"><?=$lastOrder->status->getName()?></span><br/>
                            <? endif;?>
                            <hr/>

                            <a href="<?=actionts('Auth\RegisterController@register')?>"><i class="fa fa-sign-in"></i> Вход</a><br/>
                            <a href="<?=actionts('Auth\RegisterController@register')?>"><i class="fa fa-product-hunt"></i> Регистрация</a><br/>
                        <? endif;?>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <h1><?=$h1?></h1>
                <?=$content?>
            </div>
        </div>
    </div>
</div>
<?=view('front.layouts.parts.jstemplates')->render()?>
<?=view('front.layouts.parts.footer')->render()?>