<?=view('front.layouts.parts.header')->render()?>
    <div class="content">
        <?=view('front.layouts.parts.breadcrumbs')?>
        <div class="container">
            <div class="col-xs-12">
                <div class="row">
                    <h1><?=$h1?></h1>
                    <?=$content?>
                </div>
            </div>
        </div>
    </div>
<?=view('front.layouts.parts.jstemplates')->render()?>
<?=view('front.layouts.parts.footer')->render()?>