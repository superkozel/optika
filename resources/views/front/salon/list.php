<?php
/** @var SalonService $selectedService */
/** @var City $selectedCity */
/** @var Salon[] $salons */
?>
<script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script>
    shopsCoords = [
        <? foreach($salons as $salon):?>
            <?$coords = explode(',', $salon->coordinates);?>
            <? $metroData = ''?>
            <? foreach ($salon->metros()->withPivot('range')->get() as $metro):?>
                <? $metroData.='<div style="margin-left:20px" class="metro-station line-m' . intval($metro->line->number) . '">' . $metro->name . ($metro->pivot->range ? ('  <span style="font-style: italic;color:darkgrey">' . $metro->pivot->range . ' ' . plural($metro->pivot->range, 'метр', 'метра', 'метров') .'</span>') : '') . '</div>';?>
            <? endforeach;?>
            ['<?=$coords[0]?>','<?=$coords[1]?>','«Оптика Фаворит» на <?=$salon->name?>','<?=$salon->address?><br/><?=$metroData?><br/><span style="color:red"><?=$salon->phone?></span><br/><br/><b>Режим работы</b><br/><?=str_replace("\n", '', $salon->mode)?><br/><?=SalonImage::img($salon->images->get(0), 'medium', null)?>'],
        <? endforeach;?>
    ]
</script>

<script>
    var myMap;

    $().ready(function(){
        $('.radio input[name=service]').change(function(){
            newUrl = window.location.pathname;
            if ($(this).val())
                newUrl += '?service=' + $(this).val();

            window.location = newUrl;
        });
    })

    function showShopMap(e)
    {
        $('.list-view button').removeClass('active');
        $(e.currentTarget).addClass('active');

        $('.shop-list').hide();
        $('.shop-map').show();

        if (! myMap) {
            initiateMap();
        }
    }

    function showShopList(e)
    {
        $('.list-view button').removeClass('active');
        $(e.currentTarget).addClass('active');

        $('.shop-map').hide();
        $('.shop-list').show();
    }

    function initiateMap(){
        ymaps.ready(function () {
            console.log(12313);
            myMap = new ymaps.Map('map', {
                center: [55.7488816,37.6088263],
                zoom: 10,
                controls: ['typeSelector']
            });

            myMap.controls.add('zoomControl', {position: {top: 10, left: 10}, size: 'small'});
            myMap.controls.get('typeSelector').state.set('size','small');
            myMap.behaviors.disable('scrollZoom');

            shopsCollection = new ymaps.GeoObjectCollection(null, {
                iconLayout: 'default#image',
                iconImageHref: '/marker_s.png',
                iconImageSize: [26, 26],
                iconImageOffset: [-26, -26]
            });

            for (var i = 0; i < shopsCoords.length; i++) {
                placemark = new ymaps.Placemark([shopsCoords[i][0], shopsCoords[i][1]], {
                    balloonContentHeader: shopsCoords[i][2],
                    balloonContentBody: shopsCoords[i][3]
                });

                shopsCollection.add(placemark);
            }

            // Показываем маркеры на карте
            myMap.geoObjects.add(shopsCollection);

            // Карта автоматом показывает все маркеры
            myMap.setBounds(ymaps.geoQuery(shopsCollection).getBounds(), {zoomMargin: [50, 50, 50, 50]});
            myMap.setZoom(myMap.getZoom(),{checkZoomRange: true});

            /*
             Когда надо обновить маркеры на карте на основе фильтра:

             shopsCollection.removeAll();
             // новые маркеры добавляем в коллецию (for () {})
             myMap.geoObjects.add(shopsCollection);

             */
        });
    }
</script>
<div class="container-fluid">
    <style>
        .one{padding:12px 0px;}
        .one:nth-child(even){background-color: whitesmoke;}
    </style>
    <script>

    </script>
    <h1><?=$h1?></h1>
    <div class="row">
        <div class="col-md-12">
            <div class="radio" style="margin-right:24px;display: inline-block;">
                <? $id = 'service_empty';?>
                <label name="service" for="<?=$id?>"><?=Bootstrap3Form::radio('service', '', ! $selectedService, ['id' => $id])?> Любые</label>
            </div>
            <? foreach (\ActiveRecord\SalonService::all() as $service):?>
                <? $id = 'service_' . $service->getId();?>
                <div class="radio" style="margin-right:24px;display: inline-block;">
                    <label name="service" for="<?=$id?>"><?=Bootstrap3Form::radio('service', $service->getId(), ($selectedService && $selectedService->id == $service->id), ['id' => $id])?> <?=$service->getName()?></label>
                </div>
            <? endforeach;?><br/>
            <div class="dropdown" style="display: inline-block;">
                <a style="border-bottom:1px dashed" class="btn dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    <? if (empty($selectedCity)):?>Москва и область<?else:?><?=$selectedCity->name?><? endif;?>
                    <span class="caret"></span>
                </a>

                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                    <li><a href="<?=actionts('SalonController@index', ['service' => ($selectedService ? $selectedService->getId() : null)])?>">Москва и область</a></li>
                    <? foreach (City::has('salons')->get() as $city):?>
                        <li><a href="<?=actionts('SalonController@index', ['city' => $city->slug, 'service' => ($selectedService ? $selectedService->getId() : null)])?>"><?=$city->name?></a></li>
                    <? endforeach;?>
                </ul>
            </div>

            <div class="pull-right">
                <div class="btn-group list-view" role="group" aria-label="...">
                    <button onclick="showShopList(event)" type="button" class="btn btn-default active">Списком</button>
                    <button onclick="showShopMap(event)" type="button" class="btn btn-default">На карте</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="shop-list">
            <div class="col-md-12">
                <? $groups = $salons->groupBy('city_id')->sortBy(function($group, $cityId){return $cityId;});?>
                <? foreach ($groups as $cityId => $group):?>
                    <div class="h2"><?=City::find($cityId)->name?></div>
                    <div style="margin-bottom:48px;">
                        <? foreach ($group as $salon):?>
                            <div class="row one" style="margin-bottom:12px;">
                                <div>
                                <div class="col-md-5">
                                    <div style="float:left;position:absolute;">
                                        <a href="<?=$salon->images->get(0)->getUrl('full')?>"><? if (count($salon->images) > 0):?>
                                            <?=SalonImage::img($salon->images->get(0), 'small', null, ['style' => 'max-width:100%;'])?>
                                        <? endif;?></a>
                                    </div>
                                    <div style="float:left;padding-left:120px;">
                                        <a href="<?=actionts('SalonController@show', ['id' => $salon->slug])?>">
                                            <b>«Оптика Фаворит» на <?=$salon->name?></b>
                                            <br/>
                                            <span style="color:black"><?=$salon->address?></span>
                                            <br/>
                                        </a>
                                        <a href="tel:" style="color:red;"><?=$salon->phone?></a>
                                        <br/>
                                        <b>Услуги: </b>
                                        <?=join(', ', array_map(function($s){return $s->getName();}, $salon->services))?>
                                        <br/><br/>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <? foreach ($salon->metros()->withPivot('range')->get() as $metro):?>
                                        <div class="metro-station line-m<?=intval($metro->line->number)?>">
                                            <?=$metro->name?><br/>
                                            <? if ($metro->pivot->range):?>
                                                <span style="font-style: italic;color:darkgrey"><?=$metro->pivot->range?> <?=plural($metro->pivot->range, 'метр', 'метра', 'метров')?></span>
                                            <? endif;?>
                                        </div>
                                    <? endforeach;?>
                                </div>
                                <div class="col-md-3">
                                    <?=$salon->mode?>
                                </div>
                                </div>
                            </div>
                        <? endforeach;?>
                    </div>
                <? endforeach;?>
            </div>
        </div>
    </div>
    <div class="shop-map" style="margin-top:24px;display: none;">
        <div class="map shops-map" id="map" style="width:100%;height:745px;margin-bottom:40px;"></div>
    </div>

</div>