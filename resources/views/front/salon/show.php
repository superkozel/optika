<div class="container-fluid" style="padding-top:24px">
    <div class="row">
        <div class="col-md-3">
            <div>
                <? foreach ($salon->images as $images):?>
                    <a style="display: block;" href="<?=$images->getUrl('full')?>" class="fresco" data-fresco-group="salon">
                        <?=SalonImage::img($images, 'large', null, ['style' => 'max-width:100%;margin-bottom:24px;'])?>
                    </a>
                <? endforeach;?>
            </div>
        </div>
        <div class="col-md-9">
            <h1>«Оптика Фаворит» на <?=$salon->name?><? if ($user && $user->isAdmin()):?> <a href="<?=$salon->getEditUrl()?>"><i class="fa fa-pencil"></i></a><?endif?></h1>
            <p><?=$salon->address?><br/>
                <? foreach ($salon->metros()->withPivot('range')->get() as $metro):?>
                    <div class="metro-station line-m<?=intval($metro->line->number)?>">
                        <?=$metro->name?> <span style="font-style: italic;color:darkgrey"><?=$metro->pivot->range?> метров</span>
                    </div>
                <? endforeach;?>
            </p>

            <div class="row">
                <div class="col-md-4">
                    <h2>Режим работы</h2>
                    <p>
                        <?=$salon->mode?>
                    </p>
                </div>
                <div class="col-md-4">
                    <h2>Услуги</h2>
                    <ul class="checkmarks">
                        <? foreach ($salon->services as $service):?>
                            <li><?=$service->getName()?></li>
                        <? endforeach;?>
                    </ul>
                </div>
                <div class="col-md-4">
                    <h2 style="color:red"><?=$salon->phone?></h2>
                </div>
            </div>

            <hr/>

            <?=$salon->description?>


            <script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
            <script>
                shopsCoords = [
                    <?$coords = explode(',', $salon->coordinates);?>
                    <? $metroData = ''?>
                    <? foreach ($salon->metros()->withPivot('range')->get() as $metro):?>
                        <? $metroData.='<div style="margin-left:20px" class="metro-station line-m' . intval($metro->line->number) . '">' . $metro->name . ($metro->pivot->range ? ('  <span style="font-style: italic;color:darkgrey">' . plural($metro->pivot->range, 'метр', 'метра', 'метров', true) .'</span>') : '') . '</div>';?>
                    <? endforeach;?>
                    ['<?=$coords[0]?>','<?=$coords[1]?>','«Оптика Фаворит» на <?=$salon->name?>','<?=$salon->address?><br/><?=$metroData?><br/><span style="color:red"><?=$salon->phone?></span><br/><br/><b>Режим работы</b><br/><?=str_replace("\n", '', $salon->mode)?><br/><?=SalonImage::img($salon->images->get(0), 'medium', null)?>'],
                ]
            </script>

            <script>
                ymaps.ready(function () {
                    var myMap = new ymaps.Map('map', {
                        center: [55.7488816,37.6088263],
                        zoom: 10,
                        controls: ['typeSelector']
                    });

                    myMap.controls.add('zoomControl', {position: {top: 10, left: 10}, size: 'small'});
                    myMap.controls.get('typeSelector').state.set('size','small');
                    myMap.behaviors.disable('scrollZoom');

                    shopsCollection = new ymaps.GeoObjectCollection(null, {
                        iconLayout: 'default#image',
                        iconImageHref: '/marker_s.png',
                        iconImageSize: [26, 26],
                        iconImageOffset: [-26, -26]
                    });

                    for (var i = 0; i < shopsCoords.length; i++) {
                        placemark = new ymaps.Placemark([shopsCoords[i][0], shopsCoords[i][1]], {
                            balloonContentHeader: shopsCoords[i][2],
                            balloonContentBody: shopsCoords[i][3]
                        });

                        shopsCollection.add(placemark);
                    }

                    // Показываем маркеры на карте
                    myMap.geoObjects.add(shopsCollection);

                    // Карта автоматом показывает все маркеры
                    myMap.setBounds(ymaps.geoQuery(shopsCollection).getBounds(), {zoomMargin: [50, 50, 50, 50]});
                    myMap.setZoom(myMap.getZoom(),{checkZoomRange: true});

                    /*
                     Когда надо обновить маркеры на карте на основе фильтра:

                     shopsCollection.removeAll();
                     // новые маркеры добавляем в коллецию (for () {})
                     myMap.geoObjects.add(shopsCollection);

                     */

                });
            </script>
            <div class="map shops-map" id="map" ></div>
            <style>
                #map {width:100%;height:445px;margin-bottom:40px;}
            </style>
        </div>
    </div>
</div>