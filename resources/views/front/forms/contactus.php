<?=Bootstrap3Form::open(['method' => 'post', 'action' => 'SiteController@feedback'])?>
<?=Bootstrap3Form::errorSummary($errors)?>
<?=Bootstrap3Form::token()?>
<?=Bootstrap3Form::textRow('name', 'Как к вам обращаться', Input::old('name', ($user ? $user->name : '')))?>
<?=Bootstrap3Form::textRow('email', 'email', Input::old('email', ($user ? $user->email : '')))?>
<?=Bootstrap3Form::textareaRow('content', 'Сообщение', Input::old('content'))?>
<?=Bootstrap3Form::checkbox('accept_agreement_personal', 'Ознакомился и согласен с условиями передачи и обработки персональных данных', 0)?>

<div style="text-align:right">
    <button class="btn btn-primary"><i class="fa fa-paper-plane"></i> Отправить</button>
</div>
<?=Bootstrap3Form::close()?>
