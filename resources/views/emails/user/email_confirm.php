@section('content')

Для подтверждения адреса<br/>
<a href="<?=actionts('UserController@confirmEmail', ['token' => $token])?>">нажмите сюда</a>

<!-- Greeting -->
<h1 style="{{ $style['header-1'] }}">
    {{$h1}}
</h1>

<!-- Intro -->
@foreach ($introLines as $line)
<p style="{{ $style['paragraph'] }}">
    {{ $line }}
</p>
@endforeach

<!-- Action Button -->
@if (isset())
<table style="$actionText{{ $style['body_action'] }}" align="center" width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center">
            <?php
            switch ($level) {
                case 'success':
                    $actionColor = 'button--green';
                    break;
                case 'error':
                    $actionColor = 'button--red';
                    break;
                default:
                    $actionColor = 'button--blue';
            }
            ?>

            <a href="{{ $actionUrl }}"
               style="{{ $fontFamily }} {{ $style['button'] }} {{ $style[$actionColor] }}"
               class="button"
               target="_blank">
                {{ $actionText }}
            </a>
        </td>
    </tr>
</table>
@endif

<!-- Outro -->
@foreach ($outroLines as $line)
<p style="{{ $style['paragraph'] }}">
    {{ $line }}
</p>
@endforeach

<!-- Salutation -->
<p style="{{ $style['paragraph'] }}">
    Regards,<br>{{ config('app.name') }}
</p>

<!-- Sub Copy -->
@if (isset($actionText))
<table style="{{ $style['body_sub'] }}">
    <tr>
        <td style="{{ $fontFamily }}">
            <p style="{{ $style['paragraph-sub'] }}">
                Если по какой-то причине, вы не можете "{{ $actionText }}" button,
                скопируйте ссылку ниже и адресную строчку вашего браузера:
            </p>

            <p style="{{ $style['paragraph-sub'] }}">
                <a style="{{ $style['anchor'] }}" href="{{ $actionUrl }}" target="_blank">
                    {{ $actionUrl }}
                </a>
            </p>
        </td>
    </tr>
</table>
@endif

@stop
