@extends('emails.layouts.main')

<? include(resource_path('views/emails/layouts/styles.php'))?>

@section('content')

<!-- Greeting -->
<h1 style="{{ $style['header-1'] }}">
    @if (! empty($greeting))
    {{ $greeting }}
    @else
    @if ($level == 'error')
    Whoops!
    @else
    Hello!
    @endif
    @endif
</h1>

<!-- Intro -->
@foreach ($introLines as $line)
<p style="{{ $style['paragraph'] }}">
    {!! $line !!}
</p>
@endforeach

<!-- Action Button -->
@if (isset($actionText))
<table style="{{ $style['body_action'] }}" align="center" width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center">
            <?php
            switch ($level) {
                case 'success':
                    $actionColor = 'button--green';
                    break;
                case 'error':
                    $actionColor = 'button--red';
                    break;
                default:
                    $actionColor = 'button--blue';
            }
            ?>

            <a href="{{ $actionUrl }}"
               style="{{ $fontFamily }} {{ $style['button'] }} {{ $style[$actionColor] }}"
               class="button"
               target="_blank">
                {{ $actionText }}
            </a>
        </td>
    </tr>
</table>
@endif

<!-- Outro -->
@foreach ($outroLines as $line)
<p style="{{ $style['paragraph'] }}">
    {!! $line !!}
</p>
@endforeach

<!-- Salutation -->
<p style="{{ $style['paragraph'] }}">
    С наилучшими пожеланиями,
    <br>{{ config('app.name') }}
</p>

<!-- Sub Copy -->
@if (isset($actionText))
<table style="{{ $style['body_sub'] }}">
    <tr>
        <td style="{{ $fontFamily }}">
            <p style="{{ $style['paragraph-sub'] }}">
                Если вы не можете нажать на кнопку "{{ $actionText }}" или ничего не происходит,
                скопируйте данную ссылку в адресную строку вашего браузера:
            </p>

            <p style="{{ $style['paragraph-sub'] }}">
                <a style="{{ $style['anchor'] }}" href="{{ $actionUrl }}" target="_blank">
                    {{ $actionUrl }}
                </a>
            </p>
        </td>
    </tr>
</table>
@endif

@stop