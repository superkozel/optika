<div class="content">
    <div style="width:500px;margin:50px auto;text-align: center;">
        <img style="max-width: 100%;" src="http://leahyopticians.com/communities/6/000/001/028/346//images/1912910.png"/>
        <h1 style="text-align: center;">404 <br/>Страница не найдена</h1>

        <p>Страница могла быть удалена, либо вы попали на сайт по неверной ссылке</p>

        <p>Попробуйте поискать на <a href="/" class="link">главной странице</a> или в <a class="link" href="<?=\App\Http\Controllers\CatalogController::createUrl()?>">каталоге</a></p>

        <p>
            <?=Html::link('tel:' . Settings::get('phone'), Settings::get('phone'))?>
            <?=Html::link('mailto:' . Settings::get('email'), Settings::get('email'))?>
        </p>
    </div>
</div>