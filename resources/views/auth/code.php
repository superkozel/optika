<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?=view('front.user.parts.sms_form', [
                        'user' => $user,
                        'login' => $login,
                        'action' => actionts('Auth\LoginController@login'),
                        'sendAction' => actionts('UserController@sendSmsCode')
                    ])->render()?>
                </div>
            </div>
        </div>
    </div>
</div>