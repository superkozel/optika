<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Регистрация</div>
                <div class="panel-body">
                    <?=Bootstrap3Form::open(['errors' => $errors, 'class' => 'form-horizontal', 'method' => "POST", 'url' => actionts('Auth\RegisterController@register')])?>
                            {{ csrf_field() }}

                        <? $options = ['inputDivOptions' => ['class' => 'col-md-6'], 'labelOptions' => ['class' => 'col-md-4']];?>
                        <?=Bootstrap3Form::textRow('name', 'Имя*', null, $options)?>
                        <?=Bootstrap3Form::textRow('lastname', 'Фамилия*', null, $options)?>

                        <?=Bootstrap3Form::inputRow('date', 'birthdate', 'Дата рождения', null, $options)?>
                        <div class="row">
                            <div class="col-md-offset-4 col-md-6" style="margin-top:-14px;margin-bottom:14px;color:#0068ac">
                                <span>Мы дарим подарки в День Рождения</span>
                            </div>
                        </div>

                        <?=Bootstrap3Form::textRow('phone', 'Телефон*', null, $options)?>
                        <script type="text/javascript">
                            $('[name=phone]').mask("8(999)999-99-99");
                        </script>
                        <?=Bootstrap3Form::inputRow('email', 'email', 'Email', null, $options)?>
                        <?=Bootstrap3Form::textRow('password', 'Пароль*', null, $options)?>
                        <? if (BONUS_PROGRAM_ACTIVE):?>
                            <div class="form-group <? if ($errors->has('agreed')):?>has-error<?endif;?>" style="margin-bottom:0px;">
                                <div class="col-md-8 col-md-offset-4">
                                    <?=Bootstrap3Form::checkbox('agreed', 'Прочитал и согласен с <a class="link" target="_blank" href="' . Page::findBySlug('bonusnaya-programma')->getUrl() . '">условиями участия в бонусной программе</a>', 1, $options)?>
                                    <?=Bootstrap3Form::error($errors->get('agreed'))?>
                                </div>
                            </div>
                        <? endif;?>

                        <div class="form-group <? if ($errors->has('personal_accept')):?>has-error<?endif;?>">
                            <div class="col-md-8 col-md-offset-4">
                                <?=Bootstrap3Form::checkbox('personal_accept', 'Согласен на <a class="link" target="_blank" href="' . Page::findBySlug('politika-konfidencialnosti')->getUrl() . '">передачу и обработку персональных данных</a>', 1, $options)?>
                                <?=Bootstrap3Form::error($errors->get('personal_accept'))?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-check"></i> Зарегистрироваться
                                </button>
                            </div>
                        </div>
                    <?=Bootstrap3Form::close()?>
                </div>
            </div>
        </div>
    </div>
</div>
