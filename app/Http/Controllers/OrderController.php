<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 27.12.2016
 * Time: 11:15
 */

namespace App\Http\Controllers;


use App\User;
use Illuminate\Support\Facades\Session;

class OrderController extends BaseFrontendController
{
    public function thanks()
    {
        $lastOrder = \Order::find(Session::get('order_id'));

        if (\Auth::user()) {
            $lastOrder = \Auth::user()->orders()->orderBy('created_at', 'DESC')->first();
        }

        return $this->getLayout('1col')
            ->setH1AndTitle('Заказ успешно оформлен')
            ->setBreadcrumbs($this->breadcrumbs()->add('Заказ оформлен'))
            ->setContent(\View::make('front.order.thanks', array(
                'order' => $lastOrder
            )))->render()
            ;
    }

    public function view($id)
    {
        $order = \Order::findOrFail($id);

        $ok = false;
        if (\Auth::user() && \Auth::user()->id == $order->user_id) {
            $ok = true;
        }
        else if (\Session::has('order_id') && \Session::get('order_id') == $order->id){
            $ok = true;
        }
        else if (\Input::has('token') && ($order->token == \Input::get('token'))) {
            \Session::set('order_id', $order->id);
            $ok = true;
        }

        if (! $ok) {
            \Session::put('url.intended', \Request::path());
            return \Redirect::to(actionts('Auth\LoginController@login'));
        }

        return $this->getLayout(\Auth::user() ? 'personal' : '1col')
            ->setH1AndTitle('Заказ №' . $order->id)
            ->setBreadcrumbs($this->breadcrumbs()
                ->add('Заказы', actionts('UserController@orders'))
                ->add('Заказ №' . $order->id)
            )
            ->setContent(\View::make('front.order.view', array(
                'order' => $order
            )))->render()
            ;
    }

    public function load($orderId)
    {
        $order = \Order::find($orderId);

        if ((! \Auth::user() || ! $this->getUser()->is($order->user)) && \Session::get('order_id') != $order->id) {
            \Flash::error('Нет доступа к изменению заказа');
            return \Redirect::back();
        }

        if (! $order->status->is(\OrderStatus::NEWS)) {
            \Flash::error('Невозможно изменить заказ: не новый');
            return \Redirect::back();
        }

        if ($order->paid_sum > 0) {
            \Flash::error('Невозможно изменить заказ: заказ оплачен');
            return \Redirect::back();
        }

        User::getBasket()
            ->loadOrder($order)
            ->save()
        ;

        \Flash::success('Заказ загружен обратно в корзину');
        $after = \Input::get('after', 'index');
        return \Redirect::to(actionts('BasketController@' . $after));
    }

    public function repeat($orderId)
    {
        $order = \Order::find($orderId);

        if (! \Auth::user() || ! $this->getUser()->is($order->user)) {
            \Flash::error('Нет доступа к заказу');
            return \Redirect::back();
        }

        User::getBasket()
            ->reset()
            ->loadOrderContents($order)
            ->save()
        ;
    }

    public function pay($orderId)
    {
        $order = \Order::find($orderId);
    }

    public function cancel($orderId)
    {
        /** @var \Order $order */
        $order = \Order::find($orderId);

        $ok = false;
        if ($order->status->is(\OrderStatus::NEWS)) {
            if (\Auth::user() && $order->user_id == \Auth::user()->id) {
                $ok = true;
            }
            else if (User::getBasket()->getOrderId() == $orderId) {
                $ok = true;
            }
            else {
                $err = 'Нет права отмены заказа';
            }
        }
        else {
            $err = 'Невозможно отменить заказ в работе';
        }

        if ($ok) {
            $order->status_id = \OrderStatus::CANCELED;
            $order->reason = \Input::get('reason');
            $order->save();

            \Flash::success('Заказ отмёнен');
        }
        else {
            \Flash::error($err);
        }

        return \Redirect::back();
    }

    public function renew($orderId)
    {
        /** @var \Order $order */
        $order = \Order::find($orderId);
        $ok = false;
        if ($order->status->is(\OrderStatus::CANCELED)) {
            if (\Auth::user() && $order->user_id == \Auth::user()->id) {
                $ok = true;
            }
            else if (User::getBasket()->getOrderId() == $orderId) {
                $ok = true;
            }
            else {
                $err = 'Нет права возобновления заказа';
            }
        }
        else {
            $err = 'Невозможно возобновить неотменённый заказ';
        }

        if ($ok) {
            $order->status_id = \OrderStatus::NEWS;
            $order->save();

            \Flash::success('Заказ возобновлён');
        }
        else {
            \Flash::error($err);
        }

        return \Redirect::back();
    }
}