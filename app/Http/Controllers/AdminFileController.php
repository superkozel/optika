<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 26.04.2017
 * Time: 1:17
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminFileController extends BaseAdminController
{
    public static function routes()
    {
        \Route::group(array('prefix' => 'filemanager'), function(){
            $controller = 'AdminFileController';

            \Route::get('files/', $controller . '@files');
            \Route::get('images/', $controller . '@images');
        });
    }

    public function render($title, $type)
    {
        return $this->getLayout()
            ->setH1AndTitle($title)
            ->setBreadcrumbs($this->breadcrumbs()->add('Бренды'))
            ->setContent(view('admin.filemanager.index', array(
                'type' => $type
            )))
            ->render()
            ;
    }

    public function files()
    {
        return $this->render('Загрузка файлов', 'files');
    }

    public function images()
    {
        return $this->render('Загрузка картинок', 'images');
    }
}