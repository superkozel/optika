<?php

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 24.03.2017
 * Time: 6:49
 */
class Auth extends \App\Http\Controllers\Controller
{

    public function index()
    {
        function generateToken($length = 50) {
            $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
            $count = mb_strlen($chars);
            for ($i = 0, $result = ''; $i < $length; $i++) {
                $index = rand(0, $count - 1);
                $result .= mb_substr($chars, $index, 1);
            }
            return $result;
        }
        global $USER;
//AddMessage2Log('hit');
        if($_POST['login'] != '' && $_POST['pass'] != '')
        {
//	AddMessage2Log('post is correct');
            if($USER->Login($_POST['login'], $_POST['pass']) === true)
            {
                //если у пользователя есть токен, то отдадим ему его
                $arFields = $GLOBALS['USER_FIELD_MANAGER']->GetUserFields("USER", $USER->GetID());
                if($arFields['UF_TOKEN']['VALUE'] != '')
                {
                    echo $arFields['UF_TOKEN']['VALUE'];
                    die();
                }
                //будем генерить токены до тех пор, пока не будет сгенерирован уникальный токен
                do {
                    $token = generateToken();
                    $rsUsers = CUser::GetList(($by="personal_country"), ($order="desc"), array('UF_TOKEN' => $token));
                }
                while ($user = $rsUsers->GetNext());
                $token = generateToken();
                $user = new CUser;
                $fields = Array(
                    "UF_TOKEN" => $token
                );
                $user->Update($USER->GetID(), $fields);
                echo $token;
                /*echo json_encode(array(
                    'FIRST_NAME' => iconv("WINDOWS-1251", "UTF-8", $USER->GetFirstName()),
                    'LAST_NAME'  => iconv("WINDOWS-1251", "UTF-8", $USER->GetLastName()),
                    'TOKEN'      => $token
                    )
                );*/
            }
            else
                echo 'fail';
        }
        if($_POST['token'] != '')
        {
            $rsUsers = CUser::GetList(($by="personal_country"), ($order="desc"), array('UF_TOKEN' => $_POST['token']));
            if($user = $rsUsers->GetNext())
            {
                $USER->Authorize($user['ID']);
                echo 'success';
            }
            else
                echo 'fail';
        }
    }
}