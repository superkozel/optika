<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 28.12.2016
 * Time: 3:51
 */

namespace App\Http\Controllers;


use App\Http\Requests\AdminActionRequest;

class AdminActionController extends BaseAdminCrudController
{
    protected static $labels = 'акция|акции';

    public function store(AdminActionRequest $request)
    {
        $action = parent::createModelFromRequest($request);
        $this->saveImage($action, $request, 'image');

        return \Redirect::to($action->getEditUrl());
    }

    public function update($id, AdminActionRequest $request)
    {
        $action = parent::updateModelFromRequest($id, $request);
        $this->saveImage($action, $request, 'image');

        return \Redirect::to($action->getEditUrl());
    }
}