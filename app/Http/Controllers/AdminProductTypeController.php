<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 10.01.2017
 * Time: 15:57
 */

namespace App\Http\Controllers;

class AdminProductTypeController extends BaseAdminCrudController
{
    public function index()
    {
        return $this->getLayout()
            ->setH1AndTitle('Типы товаров')
            ->setContent(\View::make('admin.productType.list', array(
                'products' => \Product::all()
            )))->render()
            ;
    }
}