<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 28.12.2016
 * Time: 3:14
 */

namespace App\Http\Controllers;


use App\Http\Requests\AdminPageRequest;

class AdminPageController extends BaseAdminCrudController
{
    protected static $labels = 'Страница|Страницы';
    protected static $names = 'page|pages';

    public function store(AdminPageRequest $request)
    {
        $page = parent::createModelFromRequest($request);

        return \Redirect::to($page->getEditUrl());
    }

    public function update($id, AdminPageRequest $request)
    {
        $page = parent::updateModelFromRequest($id, $request);

        return \Redirect::to($page->getEditUrl());
    }
}