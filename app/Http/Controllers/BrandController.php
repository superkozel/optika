<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 21.04.2017
 * Time: 4:32
 */

namespace App\Http\Controllers;

class BrandController extends BaseFrontendController
{
    public function index()
    {
        return $this->getLayout('1col')
            ->setH1AndTitle('Бренды оптики')
            ->setBreadcrumbs($this->breadcrumbs()->add('Бренды'))
            ->setCanonical(actionts('BrandController@index'))
            ->setContent(view('front.brand.list', array(
                'brands' => \Brand::where('products_count', '>', 0)->get()
            )))
            ->render()
            ;
    }
}