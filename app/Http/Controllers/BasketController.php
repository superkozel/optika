<?php
/**
 * User: superk
 * Date: 24.09.14
 * Time: 19:22
 */
namespace App\Http\Controllers;

use App\Notifications\Manager\NewOrder;
use App\Notifications\OrderCreated;
use App\Notifications\OrderUpdated;
use App\User;
use Enum\PaymentType;
use Illuminate\Support\Facades\Input;

class BasketController extends BaseFrontendController
{
    /**
     * @var \Basket
     */
    protected static $basket;

    /**
     * @return \Basket
     */
    public static function getBasket()
    {
        return User::getBasket();
    }

    public function breadcrumbs()
    {
        return parent::breadcrumbs()
            ->add('Личный кабинет', actionts('UserController@index'))
            ->add('Корзина', actionts('BasketController@index'))
            ;
    }

    public function index()
    {
        return $this->getLayout()
            ->setH1AndTitle('Корзина')
            ->setBreadcrumbs($this->breadcrumbs())
            ->setContent(\View::make('front.basket.index', [
                'bonuses' => Input::get('bonuses')
            ]))->render()
            ;
    }

    public function getCheckout()
    {
        if ($this::getBasket()->getContentsCount() == 0)
            return \Redirect::to(actionts('BasketController@index'));

        /** @var \Order $order */
        list($order, $items) = \Order::makeFromBasket($this->getBasket(), \Input::old());

        if ($user = \Auth::user()) {
            $order->email = $user->email;
            $order->phone = $user->phone;
            $order->name = $user->name;
        }

        $fitting = $order->type->is(\OrderType::FITTING);
        $making = count($this::getBasket()->getMakings()) > 0;

        return $this->getLayout()
            ->setH1AndTitle($order->id ? 'Изменить данные заказа' : ($fitting ? 'Заказать примерку' : 'Оформить заказ'))
            ->setBreadcrumbs($this->breadcrumbs())
            ->setContent(\View::make('front.basket.checkout', array(
                'making' => $making,
                'fitting' => $fitting,
                'order' => $order,
                'items' => $items
            )))->render()
            ;
    }


    /**
     * @return $this|\Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function checkout()
    {
        if ($this::getBasket()->getContentsCount() == 0)
            return \Redirect::to(actionts('BasketController@index'));

        $requestData = Input::except('_token', 'salon_search');
        $orderData = array_except($requestData, ['personal_accept']);

        /** @var \Order $order */
        list($order, $items) = \Order::makeFromBasket(static::getBasket(), $orderData);

        $validator = $order->validator($requestData);
        $validator->mergeRules([
            'personal_accept' => 'required',
        ]);

        if ($validator->passes()) {
            $order->fill($orderData);

            $ok = \DB::transaction(function() use ($order, $items){
                $findUserForOrder = User::where('phone' , $order->phone)->first() ?: User::where('email' , $order->email)->first();

                if (\Auth::user()) {
                    $user = \Auth::user();
                }
                else if ($findUserForOrder){
                    $user = $findUserForOrder;
                }
//                else if (Input::has('create_user')) {
//                    $user = new User();
//                    $user->name = $order->name;
//                    $user->phone = $order->phone;
//                    $user->email = $order->email;
//                    $user->sendWelcomeEmail();
//
//                    $user->save();
//
//                    $order->user_id = $findUserForOrder->id;
//                }
                else {
                    $user = new User();
                    $user->email = $order->email;
                }


                $order->user_id = $user->id;

                $isNewRecord = ! $order->id;

                $ok = $order->save()
                    && is_numeric($order->items()->delete())
                    && $order->items()->saveMany($items);

                if (! $ok)
                    \DB::rollBack();

                if ($isNewRecord) {
                    $notification = new OrderCreated($order);
                    $managerNotification = new NewOrder($order);

                    $manager = User::where('email', \Config::get('mail.notify'))->first();
                    if ($manager)
                        \Notification::send($manager, $managerNotification);
                }
                else {
                    $notification = new OrderUpdated($order);
                }
                \Notification::send($user, $notification);

                return $ok;
            });

            if ($ok) {
                \Session::set('order_id', $order->id);
                static::getBasket()
                    ->setOrderId($order->id)
                    ->save();

                $order->sendEmails();

                static::getBasket()->
                    reset()->
                    save();

                if (\Request::ajax()) {
                    return \Response::json(array('success' => true, 'order_id' => $order->id));
                } else {
                    return \Redirect::to('order/thanks');
                }
            }
            else {
                return \Redirect::back()->withInput(\Input::all());
            }

        } else {
            return \Redirect::back()->withErrors($validator)->withInput(\Input::all());
        }
    }

    function add()
    {
        $basket = static::getBasket();
        if ($basket->getOrderId() && ! $basket->getOrder()->status->is(\OrderStatus::NEWS))
            $basket->reset();

        $added = [];
        foreach (\Input::get('items') as $itemData) {
            $product = \Product::findOrFail($itemData['id']);

            $basketItem = $basket->
                add($product, $itemData['count'], ! empty($itemData['modifiers']) ? $itemData['modifiers'] : null);

            $added[] = [
                'count' => $itemData['count'],
                'item' => $basketItem
            ];
        }

        $basket->save();

        foreach ($added as &$addedItem) {
            $addedItem['item'] = $addedItem['item']->toArray();
        }

        if (\Request::ajax()){
            return \Response::json(array(
                'success' => true,
                'basket' => $basket->toArray(),
                'added' => $added
            ));
        }
        else {
            return \Redirect::back();
        }
    }

    function addLenses()
    {
        $frameId = \Input::get('frame_id');
        $count = \Input::get('count', 1);
        $identical = Input::get('identical');
        $basket = static::getBasket();

        if ($identical) {
            $leftLensId = $rightLensId = Input::get('lens_id_left');
        }
        else {
            $rightLensId = Input::get('lens_id_right');
            $leftLensId = Input::get('lens_id_left');
        }

        if ($frameId) {
            $added = $this->_removeOrReplaceLenses($basket, $frameId, $count, $leftLensId, $rightLensId);
        }
        else {
            $added = [];
            $added[] = [
                'count' => $count,
                'item' => $basket->add(\Product::find($leftLensId), $count, null),
            ];
            $added[] = [
                'count' => $count,
                'item' => $basket->add(\Product::find($rightLensId), $count, null),
            ];
        }

        $basket->save();
        foreach ($added as &$addedItem) {
            $addedItem['item'] = $addedItem['item']->toArray();
        }

        if (\Request::ajax()){
            return \Response::json(array(
                'success' => true,
                'basket' => $basket->toArray(),
                'added' => $added
            ));
        }
        else {
            return \Redirect::back();
        }
    }

    protected function removeLenses()
    {
        $frameId = \Input::get('id');
        $basket = static::getBasket();

        $this->_removeOrReplaceLenses($basket, $frameId);

        $basket->save();

        if (\Request::ajax()){
            return \Response::json(array(
                'success' => true,
                'basket' => $basket->toArray(),
            ));
        }
        else {
            return \Redirect::back();
        }
    }

    protected function _removeOrReplaceLenses(\Basket $basket, $frameId, $count = null, $leftLensId = null, $rightLensId = null)
    {
        $basketItem = $basket->getBasketItemById($frameId);

        $frame = $basketItem->getProduct();

        if ($count)
            $count = min($basketItem->getCount(), $count);
        else
            $count = $basketItem->getCount();

        if ($leftLensId && $rightLensId) {
            $newModifiers = ['lens_id_left' => $leftLensId, 'lens_id_right' => $rightLensId];
        }
        else {
            $newModifiers = null;
        }

        $withoutLenses = $basket->getBasketItem($frame, $newModifiers);
        if ($basketItem->getCount() == $count) {
            if ($withoutLenses) {
                $withoutLenses->setCount($withoutLenses->getCount() + $count);
                $basket->remove($basketItem->getId());

                $basketItem = $withoutLenses;
            }
            else {
                $basketItem->setModifiers($newModifiers);
            }
        }
        else {
            $basketItem->setCount($basketItem->getCount() - $count);
            if ($withoutLenses) {
                $withoutLenses->setCount($withoutLenses->getCount() + $count);
                $basketItem = $withoutLenses;
            }
            else {
                $basketItem = $basket->add($frame, $count, $newModifiers);
            }
        }

        $added = [];
        return $added;
    }

    function update()
    {
        $basket = static::getBasket();
        if (! empty($basket->order) && ! $basket->order->is(\OrderStatus::NEWS))//скидываем корзину, если предыдущий заказ перешел из нового в другой статус
            $basket->reset();

        $err = '';
        if (\Input::has('items')) {
            foreach (\Input::get('items') as $itemData) {
                $basket->
                update($itemData['id'], $itemData['count']);
            }
        }

        if (\Input::get('delivery'))
            $basket->setDelivery(Input::get('delivery'));

        if (\Input::has('promocode')) {
            if (\Input::get('promocode') == '1234') {
                $basket->setPromocode(\Input::get('promocode'));
            }
            else {
                $err = 'Неверный промокод';
            }
        }

        $basket->save();

        if (\Request::ajax()){
            return \Response::json(array(
                'success' => ! $err,
                'error' => $err,
                'basket' => $basket->toArray())
            );
        }
        else {
            if (\Input::get('after'))
                return \Redirect::to(Input::get('after'));
            else
                return \Redirect::back()->withErrors($err);

        }
    }

    public function updateModifier()
    {
        $id = Input::get('id');
        $modifier = Input::get('modifier');
        $value = Input::get('value');

        $basket = static::getBasket();
        $item = $basket->getBasketItemById($id);

        if ($item) {
            $item->setModifier($modifier, $value);
            $basket->save();
        }

        return \Response::json(array(
            'success' => true,
            'basket' => $basket->toArray())
        );
    }

    function remove()
    {
        $id = \Input::get('id');
        $basket = static::getBasket();
        $basketItem = $basket->getBasketItemById($id);

        if ($basketItem) {
//            foreach($basketItem->getChildren() as $child) {
//                $basket->remove($child);
//            }
            $basket->remove($basketItem);
            $basket->save();
        }

        if (\Request::ajax()){
            return \Response::json(array(
                'success' => true,
                'basket' => $basket->toArray(),
                'id' => $id
            ));
        }
        else {
            return \Redirect::back();
        }
    }

    function drop()
    {
        static::getBasket()
            ->clear()
            ->save()
        ;

        if (\Request::ajax()){
            return \Response::json(array(
                'success' => true,
                'basket' => static::getBasket()->toArray())
            );
        }
        else {
            return \Redirect::back();
        }
    }



}