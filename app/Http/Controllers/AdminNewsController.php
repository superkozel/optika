<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 28.02.2017
 * Time: 21:09
 */

namespace App\Http\Controllers;

use App\Http\Requests\AdminNewsRequest;
use App\Http\Requests\Request;

class AdminNewsController extends BaseAdminCrudController
{
    protected static $labels = 'новость|новости';
    protected static $names = 'news|news';

    public function store(AdminNewsRequest $request)
    {
        $news = parent::createModelFromRequest($request);
        $this->saveImage($news, $request, 'image');

        return \Redirect::to($news->getEditUrl());
    }

    public function update($id, AdminNewsRequest $request)
    {
        $news = parent::updateModelFromRequest($id, $request);
        $this->saveImage($news, $request, 'image');

        return \Redirect::to($news->getEditUrl());
    }
}