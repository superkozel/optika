<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 31.12.2016
 * Time: 10:26
 */

namespace App\Http\Controllers;


class AdminController extends BaseAdminController
{
    public function index()
    {
        return $this->getLayout()
            ->setH1AndTitle('Панель управления')
            ->setContent(\View::make('admin.index', array(
            )))->render()
            ;
    }
}