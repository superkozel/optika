<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 12.04.2017
 * Time: 15:10
 */

namespace App\Http\Controllers;

use App\Http\Middleware\EncryptCookies;
use Illuminate\Http\Request;

class ExchangeController extends Controller
{
    public function getLogger(){
        if (empty($this->logger)) {
            $this->logger = new \FileLogger('exchange');
        }
        return $this->logger;
    }
    public function index(Request $request)
    {
        $type = $request->get('type');
        $mode = $request->get('mode');

        $headers = (json_encode($request->headers->all()));
        $sessionId = $request->cookies->get(config('session.cookie'));
        $cookies = $request->headers->get('cookie');
        switch ($type)
        {
            case 'catalog':
                    if ($mode == 'checkauth') {
                    $this->getLogger()->info('app.exchange', ['request' => \Request::all(), 'data' => $request->getContent()]);
                    $response = $this->checkAuth();
                }
                else if ($mode == 'init') {
                    $this->getLogger()->info('app.exchange', ['request' => \Request::all(), 'data' => $request->getContent(), 'session' => $sessionId, 'cookies' => $cookies]);
                    $response =  $this->initExchange();
                }
                else if ($mode == 'file') {
                    $filename = \Input::get('filename');
                    $this->getLogger()->info('app.exchange', ['request' => \Request::all(), 'data' => '', 'session' => $sessionId, 'cookies' => $cookies]);
                    $response = $this->writeFile($filename, $request->getContent(), true);
                }
                else if ($mode == 'import') {
                    $filename = \Input::get('filename');
                    $this->getLogger()->info('app.exchange', ['request' => \Request::all(), 'data' => '', 'session' => $sessionId, 'cookies' => $cookies]);
                    $response = $this->writeFile($filename, $request->getContent());
                }
                break;
        }

        if (! empty($response)) {
            $this->getLogger()->info('app.exchange.response', ['response' => join("\n", $response)]);
            return join("\n", $response);
        }
        else {
            return 'not implemented method';
        }
    }

    public function checkAuth()
    {
        $data = [
            'success',
            config('session.cookie'),
            \Crypt::encrypt(\Session::getId())
        ];

        return $data;
    }

    public function initExchange()
    {
        \Session::set('date', date('d-m-Y'));

        $data = [
            'zip=no',
            'file_limit=5000000'
        ];

        return $data;
    }

    public function writeFile($filename, $contents, $new = false)
    {
        $extension = pathinfo($filename, PATHINFO_EXTENSION);
        $isImage = in_array($extension, ['jpg', 'jpeg', 'gif', 'png', 'bmp']);

        if ($isImage) {
            $path = base_path('exchange/' . str_replace('\\', '/', sanitize_filename($filename)));
        }
        else {
            $path = base_path('exchange/' . $this->getSession() . '/' . str_replace('\\', '/', sanitize_filename($filename)));
        }

        if (! file_exists(dirname($path)))
            mkdir(dirname($path), 0777, true);

        if ($new && $isImage) {
            if (file_exists($path))
                return ['success'];
        }

        $ok = file_put_contents($path, $contents, FILE_APPEND) !== false;

        if (pathinfo($path, PATHINFO_EXTENSION) == 'zip') {
            $this->closeSession();
        }

        if ($ok) {
            return ['success'];
        }
        else
            return ['failure'];
    }

    public function closeSession()
    {
        \Cache::forget('exchange_session');
    }

    public function getSession()
    {
        if (! \Cache::has('exchange_session')) {
            \Cache::put('exchange_session', date('d-m-Y H-i'), 100000);
        }

        return \Cache::get('exchange_session');
    }
}