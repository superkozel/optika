<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 03.01.2017
 * Time: 0:00
 */

namespace App\Http\Controllers;


use ActiveRecord\SalonService;

class SalonController extends BaseFrontendController
{
    public function breadcrumbs()
    {
        return parent::breadcrumbs()->add('Салоны', actionts('SalonController@index'));
    }

    public function index($cityId = null)
    {
        $city = null;
        $service = null;

        $salons = \Salon::with(['images', 'city', 'metros']);
        $crumbs = $this->breadcrumbs();
        if ($cityId) {
            $city = \City::findBySlug($cityId);

            if (! $city) {
                return $this->show($cityId);
            }

            $salons->where('city_id', $city->id);
            $crumbs->add($city->name);

        }

        if (\Input::has('service') && $service = SalonService::find(\Input::get('service'))) {
            $salons->where('service_ids', 'LIKE', '%' . $service->getId() . '%');
        }

        $canonical = actionts('SalonController@index', ['city' => $cityId]);

        return $this->getLayout()
            ->setTitle('Магазины «Оптика Фаворит» ' . (! empty($city) ? ($city->name) : '') .  ' - адреса, телефоны')
            ->setCanonical($canonical)
//            ->setMetaDescription($metaDesc)
            ->setH1('Магазины «Оптика Фаворит»' . ($city ? (' ' . $city->name) : ''))
            ->setBreadcrumbs($crumbs)
            ->setContent(\View::make('front.salon.list', array(
                'salons' => $salons->get(),
                'selectedCity' => $city,
                'selectedService' => $service
            )))->render()
            ;
    }

    public function show($id)
    {
        $salon = \Salon::findBySlug($id);

        if (! $salon) {
            return $this->index($id);
        }

        $canonical = actionts('SalonController@show', ['id' => $id, 'city' => $salon->city->getSlug()]);

        return $this->getLayout()
            ->setTitle('Салон Оптика Фаворит ' . $salon->address . ' ' . $salon->metro, ' - адрес, телефон, режим работы')
            ->setCanonical($canonical)
//            ->setMetaDescription($metaDesc)
            ->setH1($salon->name)
            ->setBreadcrumbs($this->breadcrumbs()->add($salon->name, $canonical))
            ->setContent(\View::make('front.salon.show', array(
                'salon' => $salon
            )))->render()
            ;
    }

    public function favorite($id)
    {
        \Auth::user()->favoriteSalons()->attach($id);

        return \Response::json(['success' => true]);
    }

    public function unfavorite($id)
    {
        \Auth::user()->favoriteSalons()->detach($id);

        return \Response::json(['success' => true]);
    }

    public function createUrl()
    {

    }
}