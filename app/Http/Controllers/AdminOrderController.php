<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 06.03.2017
 * Time: 8:23
 */

namespace App\Http\Controllers;


use App\Http\Requests\AdminOrderRequest;
use App\Notifications\OrderCreated;

class AdminOrderController extends BaseAdminCrudController
{
    protected static $labels = 'Заказ|Заказы';

    public function update($id, AdminOrderRequest $request)
    {
        $order = parent::updateModelFromRequest($id, $request);

        if (! $order->manager_id) {
            $order->manager_id = \Auth::user()->id;
            $order->save();
        }

        return \Redirect::to($order->getEditUrl());
    }

    public function setStatus($id, \Request $request)
    {
        $order = \Order::findOrFail($id);

        $order->status_id = $request->get('status');

        if ($request->has('reason'))
            $order->reason = $request->get('reason');

        if (! $order->manager_id)
            $order->manager_id = \Auth::user()->id;

        $order->save();

        return \Response::json($order->getAttributes());
    }

    public function sendDeliveryDateEmail()
    {

    }

//    public function showDeliveryForm($id)
//    {
//        $order = \Order::findOrFail($id);
//
//        return view('admin.order.delivery_form', ['order' => $order]);
//    }
//
//    public function storeDelivery($id, \Request $request)
//    {
//        $order = \Order::findOrFail($id);
//
//        $order->delivery_date = $request->get('delivery_date');
//        $order->delivery_time = $request->get('delivery_time');
//        $order->salon_id = $request->get('salon_id');
//        $order->address = $request->get('address');
//
//        if ($order->save()){
//            $notification = new OrderCreated($order);
//            $notification->toMail();
//        };
//    }

    public static function routes(){

        $class = static::getClass();
        \Route::post('/order/{id}/status/', 'AdminOrderController@setStatus');

        parent::routes();
    }
}