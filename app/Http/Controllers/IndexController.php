<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 09.01.2017
 * Time: 15:54
 */

namespace App\Http\Controllers;


class IndexController extends BaseFrontendController
{
    public function index()
    {
        return $this->getLayout()
            ->setTitle('Оптика Фаворит сеть салонов оптики в Москве и подмосковье')
            ->setMetaDescription('')
            ->setH1('Оптика Фаворит')
            ->setContent(\View::make('welcome'))->render()
            ;
    }
}