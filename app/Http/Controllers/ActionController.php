<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 16.01.2017
 * Time: 1:15
 */

namespace App\Http\Controllers;


class ActionController extends BaseFrontendController
{
    public function index()
    {
        $page = $this->getBitrixPage();

        $actions =
            \Action::orderBy('created_at', 'DESC')
                ->whereDate('active_to', '>=', date('Y-m-d'))
                ->paginate(20, null, 'page', $page)
                ->setPath(actionts('ActionController@index'))
        ;

        $page = $this->limitPage($actions, $page);

        $getData = [];

        if ($page) {
            $getData['page'] = $page;
        }

        $canonical = actionts('ActionController@index', $getData, false);
        $this->canonicalRedirect($canonical);

        $title = 'Акции на оптику - cамые горячие акции на очки, оправы, солнцезащитные очки и контактные линзы';
        $metaDesc = 'Самые горячие акции на оптику: очки, оправы, солнцезащитные очки и контактные линзы';

        return $this->getLayout()
            ->setH1('Акции на оптику')
            ->setTitle($title)
            ->setCanonical($canonical)
            ->setMetaDescription($metaDesc)
            ->setBreadcrumbs($this->breadcrumbs())
            ->setContent(\View::make('front.action.list', ['actions' => $actions]))->render()
            ;
    }

    public function archive()
    {
        $page = $this->getBitrixPage();

        $actions =
            \Action::orderBy('created_at', 'DESC')
                ->whereDate('active_to', '<', date('Y-m-d'))
                ->paginate(20, null, 'page', $page)
                ->setPath(actionts('ActionController@archive'))
        ;

        $page = $this->limitPage($actions, $page);

        $getData = [];

        if ($page) {
            $getData['page'] = $page;
        }

        $canonical = actionts('ActionController@archive', $getData, false);
        $this->canonicalRedirect($canonical);

        return $this->getLayout()
            ->setH1('Архив акций')
            ->setTitle('Архив акций')
            ->setCanonical($canonical)
            ->setMetaDescription('Страница завершенных акций на оптику сети Оптика Фаворит')
            ->setBreadcrumbs($this->breadcrumbs()->add('Архив', actionts('ActionController@archive')))
            ->setContent(\View::make('front.action.list', ['actions' => $actions, 'archive' => true])->render())
        ;
    }

    public function show($slug)
    {
        $action = \Action::findBySlug($slug);
        return $this->getLayout()
            ->setH1($action->name)
            ->setTitle($action->name)
            ->setBreadcrumbs($this->breadcrumbs()->add($action->name, actionts('ActionController@show', ['slug' => $action->getSlug()])))
                ->setContent(\View::make('front.action.show', ['action' => $action]))
            ;
    }

    public function breadcrumbs()
    {
        return parent::breadcrumbs()->add('Акции', actionts('ActionController@index'));
    }
}