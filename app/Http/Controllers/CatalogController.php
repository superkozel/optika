<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 27.12.2016
 * Time: 11:14
 */

namespace App\Http\Controllers;


use Illuminate\Pagination\AbstractPaginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Morphy\Morphy;
use Morphy\MorphyResult;

class CatalogController extends BaseFrontendController
{
    const PAGE_SIZE = 15;

    const SORT_PRICE = 1;
    const SORT_NEW = 2;
    const SORT_POPULAR = 3;

    const DEFAULT_SORT = CatalogController::SORT_NEW;

    public function index()
    {
//        $tests = [
//            '-3.56',
//            '-3.56',
//            '+3.56',
//            ' +3.56 ',
//            ' -3.56 ',
//            ' 3.56 ',
//            'Как не страть, -фыв+фывфв  жертвой похищения  +'
//        ];
//        foreach ($tests as $test) {
//            dump(\AttributeOption::generateSlug(Rus2Lat($test)));
//        }
//        dd(123);
        $activeFilters = [];

        $contactLensesUrls = '
/catalog/contact_lenses/contact_lenses/100834/:1-Day ACUVUE Oasys (30 шт)
/catalog/contact_lenses/contact_lenses/97950/:Dailies TOTAL 1 (30 шт)
/catalog/contact_lenses/contact_lenses/92439/:Раствор для контактных линз ОПТИ-ФРИ Экспресс (120ml)
/catalog/contact_lenses/contact_lenses/92438/:Капли Корнеокомфорт (10ml)
/catalog/contact_lenses/contact_lenses/92437/:Капли Опти-Фри (15ml)
/catalog/contact_lenses/contact_lenses/92436/:Капли ReNu MultiPlus (8ml)
/catalog/contact_lenses/contact_lenses/92435/:1-Day ACUVUE Moist (180шт)
/catalog/contact_lenses/contact_lenses/70496/:1-Day ACUVUE Define Natural Sparkle (30 шт)
/catalog/contact_lenses/contact_lenses/70495/:1-Day ACUVUE Define Natural Shimmer (30 шт)
/catalog/contact_lenses/contact_lenses/70494/:1-Day ACUVUE TruEye (180 шт)
/catalog/contact_lenses/contact_lenses/68664/:1-Day ACUVUE Moist for Astigmatism (30 шт)
/catalog/contact_lenses/contact_lenses/68663/:1-Day ACUVUE TruEye (90 шт)
/catalog/contact_lenses/contact_lenses/47421/:Раствор для контактных линз Unica Sensitive Avizor (в ассортименте)
/catalog/contact_lenses/contact_lenses/47408/:Ultra Flex Tint (1 фл.)
/catalog/contact_lenses/contact_lenses/47407/:Soflens Natural Colors (2 шт.)
/catalog/contact_lenses/contact_lenses/47406/:Biomedics Colors Premium (2 шт)
/catalog/contact_lenses/contact_lenses/47402/:Fresh Look ColorBlends (2 шт)
/catalog/contact_lenses/contact_lenses/47401/:Fresh Look Colors (2 шт)
/catalog/contact_lenses/contact_lenses/47400/:Fresh Look Dimension (6 шт)
/catalog/contact_lenses/contact_lenses/47399/:Fresh Look Dimension (2 шт)
/catalog/contact_lenses/contact_lenses/43271/:Biomedics (SoftView) Toric (6 шт)
/catalog/contact_lenses/contact_lenses/43269/:AIR OPTIX for ASTIGMATISM  (3 шт)
/catalog/contact_lenses/contact_lenses/43267/:AIR OPTIX AQUA Multifocal  (3 шт)
/catalog/contact_lenses/contact_lenses/43260/:Proclear  (6 шт)
/catalog/contact_lenses/contact_lenses/43259/:CLEAR 58 (6 шт)
/catalog/contact_lenses/contact_lenses/43257/:Biomedics XC (6 шт)
/catalog/contact_lenses/contact_lenses/43256/:Biomedics 55 Evolution (6 шт)
/catalog/contact_lenses/contact_lenses/43248/:Biofinity (6 шт)
/catalog/contact_lenses/contact_lenses/43247/:AIR OPTIX Night&Day (3шт)
/catalog/contact_lenses/contact_lenses/43245/:AVAIRA (6шт)
/catalog/contact_lenses/contact_lenses/43243/:ACUVUE 2 (6шт)
/catalog/contact_lenses/contact_lenses/43242/:1-Day ACUVUE Moist (90шт)
/catalog/contact_lenses/contact_lenses/43241/:Dailies AQUACOMFORT Plus (30 шт)
/catalog/contact_lenses/contact_lenses/43240/:1-Day ACUVUE TruEye (30 шт)
/catalog/contact_lenses/contact_lenses/43239/:SofLens daily disposable (30 шт)
/catalog/contact_lenses/contact_lenses/43220/:Раствор для контактных линз ReNu MultuPlus (в ассортименте)
/catalog/contact_lenses/contact_lenses/43219/:Капли Avizor Comfort Droops (15ml)
/catalog/contact_lenses/contact_lenses/43217/:Капли Avizor Moister Droops (15ml)
/catalog/contact_lenses/contact_lenses/43216/:Acuvue Oasys for Astigmatism (6 шт) 
/catalog/contact_lenses/contact_lenses/43212/:Optima FW (4 шт)
/catalog/contact_lenses/contact_lenses/43211/:AIR OPTIX AQUA (3 шт)
/catalog/contact_lenses/contact_lenses/43210/:ACUVUE OASYS with HYDRACLEAR Plus (6 шт)
/catalog/contact_lenses/contact_lenses/43208/:1-Day ACUVUE Moist (30 шт)
';

        $crumbs = \Breadcrumbs::create();

        $path = trim(parse_url(\Input::server('REQUEST_URI'), PHP_URL_PATH), '/');

        $curPath = '/';
        $path = explode('/', $path);

        $categoryName = '';

        //редирект по старым урлам
        $last = array_last($path);
        if (is_numeric($last)) {
            $product = \Product::whereOldId($last)->first();

            if (! $product)
                abort(404);

            return \Redirect::to($product->getUrl(), 301);
        }

        foreach($path as $k => $categorySlug) {
            if ($curCat = \CatalogCategory::findBySlug($categorySlug)) {
                $curPath .= $curCat->slug . '/';
                $crumbs->add($curCat->name, $curPath);
                $categoryName = $curCat->h1 ? $curCat->h1 : $curCat->name;
            }
            else if ($k == (count($path) - 1) && ($brand = \Brand::findBySlug($categorySlug))){
                $activeFilters[\CatalogFilter::findBySlug('brand')->id] = [$brand->id];
                $curPath .= $brand->slug . '/';
                $crumbs->add($brand->name, $curPath);
                break;
            }
            else {
                break;
            }

            $category = $curCat;
        }

        $filterSet = $category->filterSet;
        /** @var Collection $filters */
        $filters = $filterSet->filters;

        $page = 1;
        $sort = static::DEFAULT_SORT;
        $pageSize = static::PAGE_SIZE;

        foreach (\Input::get() as $k => $v) {
            if (! $v) {
                continue;
            }

            if ($k == 'page') {
                $page = $v;
            }
            else if ($k == 'sort') {
                $sort = in_array($v, [static::SORT_PRICE, static::SORT_NEW, static::SORT_POPULAR]) ? $v : $sort;
            }
            else if ($filters) {
                $filter = $filters->where('slug', $k)->first();

                if (! $filter) {
                    continue;
                }

                if ($filter->type->is(\CatalogFilterType::CLIST, \CatalogFilterType::DROPDOWN)) {
                    $values = explode(',', $v);
                    if ($filter->value_type->is(\CatalogFilterValueType::ATTRIBUTE)) {
                        $ids = \AttributeOption::whereIn('slug', $values)->pluck('id')->all();
                    }
                    else if ($filter->value_type->is(\CatalogFilterValueType::RELATION)) {
                        $relModel = (with(new \Product())->getRelationModel($filter->value_id));
                        $ids = $relModel->whereIn('slug', $values)->pluck('id')->all();
                    }
                    else {
                        $ids = $values;
                    }
                    $activeFilters[$filter->id] = $ids;
                }
                else if ($filter->type_id == \CatalogFilterType::RANGE) {
                    $activeFilters[$filter->id] = $v;
                }
            }
        }

        $products = $category->getProducts($activeFilters);

        if ($sort == static::SORT_PRICE) {
            $products->orderBy('price');
        }
        else if ($sort == static::SORT_NEW) {
            $products->orderBy('id');
        }
        else if ($sort == static::SORT_POPULAR) {
            $products->orderBy('hit');
        }

        $products = $products
            ->with('images')
            ->paginate($pageSize)
            ->setPath(CatalogController::createUrl($category, $activeFilters, null, $sort));

        $maxPage = $products->lastPage();

        if ($page > $maxPage) {
            $page = $maxPage;
        }

        $canonical = CatalogController::createUrl($category, $activeFilters, $page, $sort);

        /** @var MorphyResult $categoryNameMorph */
        $categoryNameMorph = Morphy::morph($categoryName);

        $noindex = 1;
        if (count($activeFilters) == 1 OR (count($activeFilters) == 2 && $category->id == 1)) {
            foreach ($activeFilters as $k => $values) {
                if ($filters && count($values) == 1) {
                    $filter = $filters->where('id', $k)->first();

                    if (! $filter) {
                        $noindex = 1;
                        continue;
                    }

                    if ($filter->type->is(\CatalogFilterType::CLIST, \CatalogFilterType::DROPDOWN)) {
                        if ($filter->value_type->is(\CatalogFilterValueType::ATTRIBUTE)) {
                            $option = \AttributeOption::whereId($values[0])->first();
                            $categoryNameMorph = $option->applyPattern($categoryNameMorph, $category->name_rod);
                        }
                        else if ($filter->value_type->is(\CatalogFilterValueType::RELATION)) {
                            $relModel = (with(new \Product())->getRelationModel($filter->value_id));
                            $categoryNameMorph = $relModel->whereIn('id', $values)->first()
                                ->applyPattern($categoryNameMorph, $category->name_rod);
                        }
                        else {
                            foreach ($categoryNameMorph->getData() as &$str) {
                                $str = $str . ' ' . $filter->name . ' ' . $values[0];
                            }
                        }
                    }
                }
                else {
                    $noindex = 1;
                }
            }
        }
        else if (count($activeFilters) > 0) {
            $noindex = 1;
        }

        if ($category->id == 1) {
            $title = 'Продажа оптики в москве: мединицские оправы, солнцезащитные очки, очки для зрения и контактные линзы';

            $metaDesc = '118 видов оправ, 133 линз, 1355 солнцезащитных очков по лучшим ценам в Москве';

            $seoText = 'Каталог оптики крупнейшей сети оптики в Москве и московской области. В нашем каталог вы сможеет подобрать по вкусу любую оптику,
            как солнцезащитные очки, контактные линзы или медицинские оправы, а также заказать медицинские очки на заказ, которые подойдут именно вам.';
        }
        else {
            $title = $categoryNameMorph->imenitelniy() . ' - купить недорого ' . $categoryNameMorph->vinitelniy() . ' в Москве';

            $metaDesc = $products->total() . ' видов ' . $categoryNameMorph->vinitelniy() . ' по лучшим ценам в Москве и области. Примерка и изготовление очков на заказ с доставкой.';

            $seoText = 'Каталог ' . $categoryNameMorph->roditelniy() . ' в интернет-магазине сети салонов Оптика Фаворит в Москве и московской области. В нашем магазине вы можете подобрать
            и купить недорого ' . $categoryNameMorph->vinitelniy() .' - ' . \Config::get('contacts.phone');
        }

        return response($this->getLayout()
            ->setH1($categoryNameMorph->imenitelniy())
            ->setTitle($title)
            ->setCanonical($canonical)
            ->setNoindex($noindex)
            ->setMetaDescription($metaDesc)
            ->setBreadcrumbs($crumbs)
            ->setContent(\View::make('front.catalog.index', array(
                'category' => $category,
                'products' => $products,

                'activeFilters' => $activeFilters,

                'seoText' => $seoText,
                'canonical' => $canonical,

                'page' => $page,
                'sort' => $sort,

                'parts' => $this->getParts(['products', 'pagination', 'filters'], $category, $products, $filters, $activeFilters)
            )))->render(), $products->total() > 0 ? 200 : 404)
        ;
    }

    public function ajax()
    {
        $page = \Input::get('page');
        $sort = \Input::get('sort');
        $parts = \Input::get('parts');

        $category = \CatalogCategory::find(\Input::get('category_id'));

        $filterSet = $category->filterSet;
        $filters = $filterSet->filters;

        $filterValues = \Input::get('values', []);

        $products = $category->getProducts($filterValues)->with('images');

        if ($sort == static::SORT_PRICE) {
            $products->orderBy('price');
        }
        else if ($sort == static::SORT_NEW) {
            $products->orderBy('id');
        }
        else if ($sort == static::SORT_POPULAR) {
            $products->orderBy('hit');
        }

        $products = $products
            ->paginate(static::PAGE_SIZE)
            ->setPath(CatalogController::createUrl($category, $filterValues, null, $sort));

        return json_encode($this->getParts($parts, $category, $products, $filters, $filterValues));
    }

    public function getParts($parts, $category, $products, $filters, $filterValues)
    {
        $result = [];

        if (in_array('options', $parts)) {
            $result['options'] = json_decode(view('front.catalog.filters', ['filters' => $filters, 'category' => $category, 'filterValues' => $filterValues, 'ajax' => true])->render(), true);
        }
        if (in_array('filters', $parts)) {
            $result['filters'] = view('front.catalog.filters', ['filters' => $filters, 'category' => $category, 'filterValues' => $filterValues, 'ajax' => false])->render();
        }
        if (in_array('products', $parts)) {
            $result['products'] = view('front.catalog.products', ['products' => $products])->render();
            $result['totalPages'] = $products->lastPage();
            $result['total'] = $products->total();
        }
        if (in_array('pagination', $parts)) {
            $result['pagination'] = view('front.catalog.pagination', ['products' => $products])->render();
        }

        return $result;
    }

    public static function createUrlSimple(\CatalogCategory $category = null, $filterSlugs = []) {
        if (! $category)
            $category = \CatalogCategory::find(1);

        $url = $category->getUrl();

        if (! empty($filterSlugs['brand'])) {
            $url .= $filterSlugs['brand'] . '/';
            unset($filterSlugs['brand']);
        }

        if (! empty($filterSlugs)) {
            $url .= '?' . http_build_query($filterSlugs);
        }

        return $url;
    }

    public static function createUrl(\CatalogCategory $category = null, $filterValues = array(), $page = 1, $sort = null)
    {
        if (! $category)
            $category = \CatalogCategory::find(1);

        $url = $category->getUrl();

        $filters = \CatalogFilter::whereIn('id', array_keys($filterValues))->get()->keyBy('id');
        $query = [];
        foreach ($filterValues as $k => $v) {
            if (! is_array($v)){
                $v = [$v];
            }

            $filter = $filters->get($k);

            if (! $filter)
                continue;

            if ($filter->value_type->is(\CatalogFilterValueType::ATTRIBUTE)) {
                $attr = \Attribute::findByXmlId($filter->value_id);

                $query[$filter->slug] = join(',', $attr->options->keyBy('id')->only($v)->pluck('slug')->all());
            }
            else if ($filter->value_type->is(\CatalogFilterValueType::RELATION)) {
                $relModel = (with(new \Product())->getRelationModel($filter->value_id));
                $ids = $relModel->whereIn('id', $v)->pluck('slug')->all();
                if ($filter->slug == 'brand' && count($v) == 1) {
                    $url .= join('', $ids) . '/';
                }
                else {
                    $query[$filter->slug] = join(',', $ids);
                }
            }
            else if ($filter->value_type->is(\CatalogFilterValueType::COLUMN)) {
                $query[$filter->slug] = join(',', $v);
            }
        }

        if ($page > 1){
            $query['page'] = $page;
        }

        if ($sort && $sort != static::DEFAULT_SORT) {
            $query['sort'] = $sort;
        }

        if (count($query)) {
            $url .= '?' . http_build_query($query);
        }

        return $url;
    }
}