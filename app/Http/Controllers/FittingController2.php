<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 16.03.2017
 * Time: 21:45
 */

namespace App\Http\Controllers;


use ActiveRecord\Fitting;

class FittingController extends BaseFrontendController
{
    public function index()
    {
        return $this->getLayout('grey')
            ->setH1AndTitle('Примерка')
            ->setBreadcrumbs($this->breadcrumbs())
            ->setContent(view('front.fitting.list', array(
                'fittings' => Fitting::current()->with('product')->get()
            )))
            ->render()
            ;
    }

    public function add()
    {
        $id = \Request::get('id');
        $error = null;

        if (Fitting::current()->count() >= 4) {
            $error = 'Нельзя заказать больше 4-х оправ на примерку';
        }
        else {
            $object = Fitting::current()->where('product', $id);
            if (! $object) {
                $object = Fitting::forceCreate([
                    'product_id' => $id,
                    'user_id' => \Auth::user() ? \Auth::user()->id : null,
                    'session_id' => \Session::getId()
                ]);
            }
        }

        if ($error) {
            return \Response::json(['success' => false, 'error' => $error]);
        }
        else {
            return \Response::json(['success' => true, 'object' => $object->toArray()]);
        }
    }

    public function remove()
    {
        $id = \Request::get('id');
        $object = Fitting::current()->where('product', $id)->delete();

        return \Response::json(['success' => true, 'id' => $id]);
    }

    public function breadcrumbs()
    {
        return parent::breadcrumbs()
            ->add('Личный кабинет', actionts('UserController@index'))
            ->add('Примерка', actionts('FittingController@index'))
            ;
    }
}