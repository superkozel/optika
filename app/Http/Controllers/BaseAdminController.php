<?php
namespace App\Http\Controllers;

/**
 * 
 * @author Зыков Дмитрий <aesleep@ya.ru>
 * @date 17.04.2015
 */


class BaseAdminController extends Controller
{
    public function getUser()
    {
        return \Auth::user();
    }

    /**
     * @return \Layout
     */
    public function getLayout($layout = 'main')
    {
        \View::share('controller', $this);
        return \Layout::make('admin.layouts.' . $layout)
            ->setBreadcrumbs($this->breadcrumbs());
    }

    public function loadModel($class, $id)
    {
        return $class::find($id);
    }

    public static function routes()
    {
        $class = static::getClass();

        $prefix = camelCaseToUnderscore(str_replace('Admin', '', str_replace('Controller', '', $class)), '-');
        return \Route::group(array('prefix' => $prefix), function() use ($class){
            $controller = $class;

            \Route::get('/', $controller . '@index');
            \Route::get('/create/', $controller . '@create');
            \Route::post('/create/', $controller . '@store');
            \Route::get('/edit/{id}/', $controller . '@edit');
            \Route::post('/edit/{id}/', $controller . '@update');
            \Route::get('/delete/{id}/', $controller . '@delete');
        });
    }

    public function getModelName()
    {
        return str_replace(['Admin', 'Controller'], ['', ''], $this->getClass());
    }

    public function breadcrumbs()
    {
        return \Breadcrumbs::create()->add('Панель управления', actionts('AdminController@index'));
    }
}