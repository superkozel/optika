<?php
namespace App\Http\Controllers;
use Illuminate\Pagination\Paginator;

/**
 * 
 * @author Зыков Дмитрий <aesleep@ya.ru>
 * @date 17.04.2015
 */


class BaseFrontendController extends Controller
{
    /**
     * @return \Breadcrumbs
     */
    protected function breadcrumbs()
    {
        return \Breadcrumbs::create();
    }

    /**
     * @return \Layout
     */
    public function getLayout($layout = 'main', $data = [])
    {
        return \Layout::make('front.layouts.' . $layout, $data);
    }

    /**
     * @return \App\User|null
     */
    public function getUser()
    {
        return \Auth::user();
    }

    public function canonicalRedirect($canonical)
    {
        if ($_SERVER['REQUEST_URI'] != $canonical) {
            \Redirect::to($canonical, 301)->throwResponse();
        }
    }

    /**
     * Получить страницу из PAGEN_1(гет-параметр из пагинации битрикса)
     */
    public function getBitrixPage()
    {
        $page = null;
        if (\Input::has('PAGEN_1')) {
            $page = \Input::get('PAGEN_1');
        };
        return $page;
    }

    public function limitPage($paginator)
    {
        $page = $paginator->currentPage();

        if ($page == 1)
            $page = null;
        elseif ($page > $paginator->lastPage())
            $page = $paginator->lastPage();

        return $page;
    }


}