<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 26.03.2017
 * Time: 7:16
 */

namespace App\Http\Controllers;


use App\User;

class AdminUserController extends BaseAdminCrudController
{
    protected static $labels = 'пользователь|пользователи';
    protected static $names = 'user|users';

    public function getModelName()
    {
        return User::class;
    }

    public function store(AdminUserRequest $request)
    {
        $salon = parent::createModelFromRequest($request);
        $this->saveImages($salon, $request);

        return \Redirect::to($salon->getEditUrl());
    }

    public function update($id, AdminUserRequest $request)
    {
        $salon = parent::updateModelFromRequest($id, $request);
        $this->saveLogo($salon, $request);

        return \Redirect::to($salon->getEditUrl());
    }
}