<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 02.03.2017
 * Time: 6:59
 */

namespace App\Http\Controllers;


use App\Http\Requests\Request;
use App\User;

class BaseAdminCrudController extends BaseAdminController
{
    protected static $labels = 'новость|новости';
    protected static $names = null;

    public function getModelName()
    {
        return str_replace(['Admin', 'Controller'], ['', ''], $this->getClass());
    }

    public function index()
    {
        $modelClass = $this->getModelName();
        return $this->getLayout()
            ->setH1AndTitle($this->label(1))
            ->setContent(\View::make('admin.' . $this->name(0) . '.list', array(
                $this->name(1) => $modelClass::search(\Input::all())
            )))->render()
            ;
    }

    public function edit($id)
    {
        $modelClass = $this->getModelName();
        return $this->getLayout()
            ->setH1AndTitle('редактировать ' . $this->label(0))
            ->addBreadcrumb('редактировать')
            ->setContent(\View::make('admin.' . $this->name(0) . '.form', array(
                $this->name(0) => $modelClass::find($id)
            )))->render()
            ;
    }

    public function updateAction($id, $request)
    {
        $this->updateModelFromRequest($id, $request);

        return \Redirect::back();
    }

    public function create()
    {
        $modelClass = $this->getModelName();
        return $this->getLayout()
            ->setH1AndTitle('Создать ' . $this->label(0))
            ->addBreadcrumb('Создать')
            ->setContent(\View::make('admin.' . $this->name(0) .  '.form', array(
                $this->name(0) => new $modelClass()
            )))->render()
            ;
    }

    public function storeAction($request)
    {
        $model = static::createModelFromRequest($request);

        return \Redirect::to($model->getEditUrl());
    }

    public function createModelFromRequest($request)
    {
        $modelClass = $this->getModelName();

        $model= $modelClass::forceCreate($request->onlyValidated());

        return $model;
    }

    public function updateModelFromRequest($id, $request)
    {
        $modelClass = $this->getModelName();
        $model = $modelClass::find($id);

        $model->unguard();
        $model->fill($request->onlyValidated());

        $model->save();

        return $model;
    }

    public function breadcrumbs()
    {
        return parent::breadcrumbs()->add($this->label(1), $this->getUrl());
    }

    protected function label($many = 0)
    {
        return explode('|', static::$labels)[$many];
    }

    protected function name($many = 0)
    {
        if (! empty(static::$names)) {
            return explode('|', static::$names)[$many];
        }
        else {
            return lcfirst($this->getModelName()) . ($many ? 's' : '');
        }
    }

    protected function saveImage(\ActiveRecord $model, $request, $field)
    {
        $ids = explode(',', \Input::get($field));

        if (method_exists($model, $field)) {//это связь
            $deletedImages = $model->{$field}()->whereNotIn('id', $ids)->get();
            foreach ($deletedImages as $del) {
                $del->delete();
            }

            foreach ($ids as $id) {
                if (! $upload = User::findMyUpload($id))
                    continue;

                $imageClass = $model->getRelationModel($field);
                /** @var \BaseImage $logo */
                $logo = $imageClass::makeFromUpload($upload, $model);
                $logo->save();
            }
        }
        else if (\Schema::hasColumn($model->getTable(), $field)) {

            if (\Input::has('image')) {
                $model->{$field} = 'stub';
                $imageModel = $model->{$field};

                if (($upload = User::findMyUpload(\Input::get($field)))) {
                    $imageModel::makeFromUpload($upload, $model)
                        ->save();
                }
            }
            else if ($model->{$field}){
                $model->{$field}->delete();
            }
        }

        return true;
    }

    public function delete($id)
    {
        $modelClass = $this->getModelName();

        if ($model = $modelClass::find($id))
            $model->delete();

        return \Redirect::back();
    }
}