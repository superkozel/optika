<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 28.12.2016
 * Time: 5:25
 */

namespace App\Http\Controllers;


use App\Http\Requests\AdminSalonRequest;

class AdminSalonController extends BaseAdminCrudController
{
    protected static $labels = 'салон|салоны';

    public function store(AdminSalonRequest $request)
    {
        $salon = parent::createModelFromRequest($request);
        $this->saveImages($salon, $request);

        return \Redirect::to($salon->getEditUrl());
    }

    public function update($id, AdminSalonRequest $request)
    {
        $salon = parent::updateModelFromRequest($id, $request);
        $this->saveLogo($salon, $request);

        return \Redirect::to($salon->getEditUrl());
    }
}