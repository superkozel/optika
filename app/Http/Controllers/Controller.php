<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public static function getClass()
    {
        return (new \ReflectionClass(static::class))->getShortName();
    }

    function getUrl($action = 'index', $parameters = [], $absolute = false)
    {
        return actionts(static::getClass() . '@' . $action, $parameters, $absolute);
    }

    public function saveImageProperty(\ActiveRecord $model, $property)
    {
        if (! \Input::has($property)) {
            return;
        }

        $imageClass = $model->getRelationModel();
        $image = $imageClass::makeFromUpload(User::findMyUpload(\Input::get($property)), $model);

//        foreach (! in_array($delete)) {
//            $model->{$property}()->deleteByIds();
//        };

        $model->save();

        return $model;
    }

}