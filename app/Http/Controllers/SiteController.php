<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 20.02.2017
 * Time: 21:15
 */

namespace App\Http\Controllers;

use App\Http\Requests\CreateFeedbackRequest;

class SiteController extends BaseFrontendController
{
    public function feedback(CreateFeedbackRequest $request)
    {
        \Mail::send('emails.feedback', $request->all(), function(\Illuminate\Mail\Message $message) use ($request)
        {
            $message
                ->replyTo($request->get('email'), $request->get('name'))
                ->to(\Config::get('mail.notify'))
                ->subject('Форма обратной связи')
            ;

            $message;
        });

        \Flash::success('Сообщение отправлено');

        return \Redirect::back()->with('success', true);
    }
}