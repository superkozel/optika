<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 01.03.2017
 * Time: 3:18
 */

namespace App\Http\Controllers;


use App\Http\Requests\AdminCatalogFilterRequest;

class AdminCatalogFilterController extends BaseAdminCrudController
{
    protected static $labels = 'Фильтр|Фильтры';
//    protected static $names = 'catalogFilter|catalogFilters';

    public function store(AdminCatalogFilterRequest $request)
    {
        return parent::storeAction($request);
    }

    public function update($id, AdminCatalogFilterRequest $request)
    {
        return parent::updateAction($id, $request);
    }
}