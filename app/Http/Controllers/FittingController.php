<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 16.03.2017
 * Time: 21:45
 */

namespace App\Http\Controllers;


use ActiveRecord\Fitting;
use App\User;

class FittingController extends BaseFrontendController
{
    public function index()
    {
        return \Redirect::to(actionts('BasketController@index'));
        return $this->getLayout('grey')
            ->setH1AndTitle('Примерка')
            ->setBreadcrumbs($this->breadcrumbs())
            ->setContent(view('front.fitting.list', array(
                'fittings' => Fitting::current()->with('product')->get()
            )))
            ->render()
            ;
    }

    public function add()
    {
        $id = \Input::get('id');
        $product = \Product::find($id);
        $basket = User::getBasket();

        $fittings = $basket->getFittings();
        if (count($fittings) >= 4) {
            return \Response::json(['success' => false, 'error' => 'Вы уже выбрали 4 оправы на примерку. Уберите одну из корзины.']);
        }

        $basketItem = $basket->getBasketItem($id, ['fitting' => 1]);

        if (! $basketItem) {
            $basketItem = $basket->add($product, 1, ['fitting' => 1]);
        }


        $basket->save();

        $added = [[
            'count' => 1,
            'item' => $basketItem->toArray()
        ]];

        $fittings = $basket->getFittings();

        return \Response::json(['success' => true, 'added' => $added, 'basket' => $basket->toArray(), 'fittings' => $fittings]);
    }

    public function remove()
    {
        $id = \Input::get('id');
        $product = \Product::find($id);
        $basket = User::getBasket();

        $basketItem = $basket->getBasketItem($id, ['fitting' => 1]);

        if ($basketItem) {
            $basket->remove($basketItem->getId());
        }

        $basket->save();

        return \Response::json(['success' => true, 'item' => $basketItem ? $basketItem->toArray() : null]);
    }

    public function breadcrumbs()
    {
        return parent::breadcrumbs()
            ->add('Личный кабинет', actionts('UserController@index'))
            ->add('Примерка', actionts('FittingController@index'))
            ;
    }
}