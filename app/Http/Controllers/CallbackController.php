<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 22.09.2017
 * Time: 17:13
 */

namespace App\Http\Controllers;


class CallbackController extends BaseFrontendController
{
    public function list()
    {
        $data = [
            [
                'phone' => '8 999 131 13 13',
                'name' => 'Семён'
            ],
            [
                'phone' => '8 999 666 66 66',
                'name' => 'Алексей'
            ],
            [
                'phone' => '8 144 44 41 66',
                'name' => 'Екатерина'
            ],
        ];

        return \Response::json($data);
    }
}