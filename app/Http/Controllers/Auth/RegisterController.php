<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\BaseFrontendController;
use App\Http\Requests\Request;
use App\Notifications\AfterRegistration;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends BaseFrontendController
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $rules = [
            'birthdate' => 'required|date',
            'phone' => 'required|phone|unique_user_phone',

            'email' => 'email|max:255|unique:users',
            'agreed' => 'bool',
            'personal_accept' => 'required',
            'simple' => 'bool'
        ];
        if (empty($data['simple'])) {
            $rules = array_replace($rules, [
                'name' => 'required:simple|max:255',
                'lastname' => 'required:simple|max:255',
                'password' => 'required:simple|min:6',
            ]);
        }

        return \Validator::make(
            $data,
            $rules, [
            'birthdate.date' => 'Неверная дата рождения',
            'birthdate.date' => 'Неверная дата рождения',
            'phone.unique' => 'Пользователь с таким телефоном уже существует',
            'email.unique' => 'Пользователь с таким email уже существует',
            'personal_accept.required' => 'Необходимо согласие на передачу и обработку персональных данных',
        ], [
            'name' => 'Имя',
            'lastname' => 'Фамилия',
            'phone' => 'Телефон',
            'password' => 'Пароль',
            'birthdate' => 'Дата рождения'
        ]);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(\Illuminate\Http\Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        User::unguard();
        $attributes = array_only($data, ['name', 'lastname', 'birthdate', 'phone', 'email']);
        if (! empty($data['password']))
        $attributes['password'] = bcrypt($data['password']);

        if (array_has($data, 'agreed'))
            $attributes['bonus_program_active'] = !! array_get($data, 'agreed');

        $user = User::create($attributes);

        if ($user->email) {
            $notification = new AfterRegistration($user);
            \Notification::send($user, $notification);
        }

        \Flash::success('Пользователь успешно зарегистрирован');

        return $user;
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        if (\Request::server('HTTP_REFERER') && mb_strpos(\Request::server('HTTP_REFERER'), trim($_SERVER["REQUEST_URI"], '/')) === false) {
            \Session::put('url.intended', \Request::server('HTTP_REFERER'));
        }

        return $this->getLayout()
            ->setH1AndTitle('Регистрация')
            ->setBreadcrumbs($this->breadcrumbs()->add('Регистрация'))
            ->setContent(\View::make('auth.register'))->render()
            ;
    }
}
