<?php

namespace App\Http\Controllers\Auth;

use ActiveRecord\Basket;
use ActiveRecord\Favorite;
use App\Http\Controllers\BaseFrontendController;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\RedirectResponse;

class LoginController extends BaseFrontendController
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Get the post register / login redirect path.
     *
     * @return string
     */
    public function redirectTo()
    {
        $user = \Auth::user();
        if ($user && ! $user->password) {
            return actionts('UserController@getPassword');
        }
        else {
            return '/';
        }
    }

    public function username()
    {
        return 'login';
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function sendFailedLoginResponse(\Illuminate\Http\Request $request)
    {
        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors([
                'password' => 'Неверный пароль',
            ]);
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function validateLogin(\Illuminate\Http\Request $request)
    {
        $this->validate($request, [
            $this->username() => 'required',
        ]);
    }


    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(\Illuminate\Http\Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $loginResult = $this->attemptLogin($request);
        if ($loginResult instanceof RedirectResponse) {
            return $loginResult;
        }
        else if ($loginResult) {
            \Flash::success('Успешный вход');
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin($request)
    {
        $login = $request->get('login');
        $user = User::findByLogin($login);

        if (! $user) {
            return \Redirect::back()->withErrors([$this->username() => 'Пользователь не найден']);
        }

        if ($request->exists('code')) {
            if (! $user->checkSmsCode($request->get('code'))) {
                \Flash::error('Неверный код');
                return \Redirect::back()->withInput()->withErrors(['code' => 'Неверный код']);
            }
            else {
                $this->guard()->login($user, $request->has('remember'));
                $user->password = '';
                $user->phone_confirmed = $user->phone;
                $user->clearSmsCode();
                $user->save();

                return $user;
            }
        }
        else {
            if (! $request->has('password')) {
                if (! $user->password && ! $user->old_password) {
                    return \Redirect::to(actionts('Auth\LoginController@showOneOffCodeForm'))->withInput(['login' => $login]);
                }
                else {
                    return \Redirect::to(actionts('Auth\LoginController@showLoginForm'))->withInput(['login' => $login]);
                }
            }

            $result = $this->guard()->attempt(
                array_replace(['id' => $user->id], $request->only('password')), $request->has('remember')
            );

            if (! $result && $user) {
                $result = $user->attemptOldUserPassword($request->get('password'));

                if ($result) {
                    $user->password = \Hash::make($request->get('password'));
                    $user->save();

                    return $user;
                }
            }

            return $result;
        }
    }

    public function setPassword($request)
    {
        $this->validate($request, ['password' => 'required|min:4']);
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        $login = old('login');
        if (! $login) {
            return $this->getLayout('grey')
                ->setH1AndTitle('Вход в личный кабинет')
                ->setContent(\View::make('auth.login'))->render()
                ;
        }
        else {
            return $this->getLayout('grey')
                ->setH1AndTitle('Вход в личный кабинет')
                ->setContent(\View::make('auth.password')->with('login', $login))->render()
                ;
        }
    }

    public function showOneOffCodeForm()
    {
        $login = \Input::get('login', old('login'));
        $user = User::findByLogin($login);

        if (! $user) {
            return \Redirect::to(actionts('Auth\LoginController@showLoginForm'));
        }

        return $this->getLayout('grey')
            ->setH1AndTitle('Вход в личный кабинет')
            ->setContent(\View::make('auth.code', compact('user', 'login')))->render()
            ;
    }

//    public function sendSmsCode()
//    {
//        $login = \Input::get('login');
//        $user = User::findByLogin($login);
//        if (!$user) {
//            \App::abort(503, 'Пользователь не найден');
//        }
//
//        $err = null;
//        $result = $user->sendSmsCode($err);
//
//        if (! $result) {
//            \Flash::error($err);
//        }
//        else {
//            \Flash::success('СМС отправлено');
//        }
//
//        return \Redirect::back()->withInput(['login' => $login]);
//    }

    public function showSetPasswordForm()
    {
        return $this->getLayout('grey')
            ->setH1AndTitle('Вход в личный кабинет')
            ->setContent(\View::make('auth.login'))->render()
            ;
    }
}