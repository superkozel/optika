<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 03.03.2017
 * Time: 21:41
 */

namespace App\Http\Controllers;


use ActiveRecord\Favorite;
use App\User;

class FavoriteController extends BaseFrontendController
{
    public function index()
    {
        return $this->getLayout('personal')
            ->setH1AndTitle('Избранное')
            ->setBreadcrumbs($this->breadcrumbs())
            ->setContent(view('front.favorite.list', array(
                'favorites' => Favorite::current()->get()
            )))
            ->render()
            ;
    }

    public function add()
    {
        $id = \Request::get('id');

        Favorite::unguard();
        $favorite = Favorite::firstOrCreate([
            'product_id' => $id,
            'user_id' => \Auth::user() ? \Auth::user()->id : null,
            'session_id' => \Session::getId()
        ]);

        $data = $favorite->getAttributes();

        $success = true;
        return \Response::json(array('success' => $success, 'object' => $favorite->toArray(), 'count' => Favorite::current()->count()));
    }

    public function remove()
    {
        $id = \Request::get('id');

        if (\Auth::user()) {
            $select = \Auth::user()->favorites();
        }
        else {
            $select = Favorite::where('session_id', \Session::getId());
        }

        $success = $select->whereProductId($id)->delete();

        return \Response::json(array('success' => $success, 'count' => Favorite::current()->count()));
    }

    public function breadcrumbs()
    {
        return parent::breadcrumbs()
            ->add('Личный кабинет', actionts('UserController@index'))
            ->add('Избранное', actionts('FavoriteController@index'))
            ;
    }
}