<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 09.01.2017
 * Time: 13:37
 */

namespace App\Http\Controllers;


use App\User;

class ProductController extends BaseFrontendController
{
    public function show($id)
    {
        $product = \Product::find(explode('_', $id)[1]);
        $canonical = $product->getUrl();

        $title = $product->name . ' - купить в Москве с доставкой ' . $product->type->name . ' ' . $product->article;
        $metaDesc = 'Продажа медицинской оправы по цене в Москве звоните 123131323. Доставка медицинских оправ с примеркой ';

        $crumbs = \Breadcrumbs::create();
//        $category = \CatalogCategory::findForItem();
        /** @var \CatalogCategory $category */
        $category = \CatalogCategory::whereHas('predefinedValues', function($query) use ($product){
            $query->where('filter_id', \CatalogFilter::whereValueId('type_id')->first()->id)->where('value', $product->type->id);
        })->first();

        $curCategory = $category;
        $path = [];
        while ($curCategory) {
            $path[] = $curCategory;
            $curCategory = $curCategory->parent;
        }

        foreach (array_reverse($path) as $curCategory) {
            $crumbs->add($curCategory->name, $curCategory->getUrl());
        }

        $crumbs->add($product->name, $product->getUrl());

        \Recent::add($product);

        return $this->getLayout()
            ->setH1($product->name)
            ->setTitle($title)
            ->setCanonical($canonical)
            ->setMetaDescription($metaDesc)
            ->setBreadcrumbs($crumbs)
            ->setContent(\View::make('front.product.view', ['product' => $product]))->render()
            ;
    }

    public function configurate($id)
    {
        $product = \Product::find($id);
        return view('front.product.parts.configurateLenses', ['product' => $product, 'frameId' => \Input::get('frame_id')]);
    }

    public function availability($id)
    {
        return view('front.product.availability', ['product' => \Product::findOrFail($id), 'user' => \Auth::user()]);
    }
}