<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 11.02.2017
 * Time: 14:25
 */

namespace App\Http\Controllers;


use App\Http\Requests\AdminCatalogCategoryRequest;
use App\User;

class AdminCatalogCategoryController extends BaseAdminCrudController
{
    protected static $labels = 'Категория|Категории';
    protected static $names = 'catalogCategory|catalogCategories';

    public function store(AdminCatalogCategoryRequest $request)
    {
        $category = parent::createModelFromRequest($request);

        if (\Input::has('image')) {
            if (($upload = User::findMyUpload(\Input::get('image')))) {
                \CategoryImage::makeFromUpload($upload, $category)
                    ->save();
            }
        }
        else if ($category->image){
            $category->image->delete();
        }

        return \Redirect::to($category->getEditUrl());
    }

    public function update($id, AdminCatalogCategoryRequest $request) {
        $category = parent::updateModelFromRequest($id, $request);

        if (\Input::has('image')) {
            if (($upload = User::findMyUpload(\Input::get('image')))) {
                \CategoryImage::makeFromUpload($upload, $category)
                    ->save();
            }
        }
        else if ($category->image){
            $category->image->delete();
        }

        return \Redirect::to($category->getEditUrl());
    }
}