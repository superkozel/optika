<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 31.12.2016
 * Time: 18:54
 */

namespace App\Http\Controllers;


use App\Http\Requests\AdminBrandRequest;
use App\User;

class AdminBrandController extends BaseAdminCrudController
{
    protected static $labels = 'Бренд|Бренды';
    protected static $names = 'brand|brands';

    public function store(AdminBrandRequest $request)
    {
        $brand = parent::createModelFromRequest($request);
        $this->saveImage($brand, $request, 'image');

        return \Redirect::to($brand->getEditUrl());
    }

    public function update($id, AdminBrandRequest $request)
    {
        $brand = parent::updateModelFromRequest($id, $request);
        $this->saveImage($brand, $request, 'image');

        return \Redirect::to($brand->getEditUrl());
    }
}