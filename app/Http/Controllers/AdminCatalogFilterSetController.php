<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 12.01.2017
 * Time: 22:07
 */

namespace App\Http\Controllers;


use App\Http\Requests\AdminCatalogFilterSetRequest;

class AdminCatalogFilterSetController extends BaseAdminCrudController
{

    protected static $labels = 'Набор фильтров|Наботы фильтров  ';
    protected static $names = 'catalogFilterSet|catalogFilterSets';

    public function store(AdminCatalogFilterSetRequest $request)
    {
        $set = parent::createModelFromRequest($request);

        return \Redirect::to($set->getEditUrl());
    }

    public function update($id, AdminCatalogFilterSetRequest $request)
    {
        $set = parent::updateModelFromRequest($id, $request);

        $filters = [];
        foreach (\Input::get('filters') as $id => $filterData) {
            if (! empty($filterData['enabled'])) {
                $filters[$id] = ['sort' => (int)$filterData['sort']];
            }
        }
        $set->filters()->sync($filters);

        return \Redirect::to($set->getEditUrl());
    }

//    public function index()
//    {
//        return $this->getLayout()
//            ->setH1AndTitle('Наборы фильтров')
//            ->setContent(\View::make('admin.catalogFilterSet.list', array(
//                'sets' => \CatalogFilterSet::all()
//            )))->render()
//            ;
//    }
//
//
//    public function create()
//    {
//        $set = new \CatalogFilterSet();
//
//        $set->fill(\Input::all());
//
//        return $this->getLayout()
//            ->setH1AndTitle('Создать набор фильтров')
//            ->setContent(\View::make('admin.catalogFilterSet.create', array(
//                'set' => $set
//            )))->render()
//            ;
//    }
//
//    public function store()
//    {
//        $set = new \AttributeSet();
//
//        $validator = \Validator::make(
//            \Input::only('name', 'product_type_id'),
//            [
//                'name' => 'required|min:3',
//                'product_type_id' => 'required|in:' . join(',', array_keys(\ProductType::getNames())),
//            ]
//        );
//
//
//        if ($validator->passes()) {
//            $set->name = \Input::get('name');
//            $set->product_type_id = \Input::get('product_type_id');
//            $set->saveOrFail();
//
//            $ids = \Input::get('attribute_ids');
//            if (empty($ids)) {
//                $ids = [];
//            }
//            $set->attributes()->sync(array_keys($ids));
//            $set->saveOrFail();
//
//            return \Redirect::action('AdminAttributeSetController@edit', ['id' => $set->id]);
//        }
//        else {
//            return \Redirect::back()->withErrors($validator)->withInput();
//        }
//    }
//
//    public function edit($id)
//    {
//        $set = \AttributeSet::find($id);
//
//        $set->fill(\Input::all());
//
//        return $this->getLayout()
//            ->setH1AndTitle('Редактировать набор свойств')
//            ->setContent(\View::make('admin.attributeSet.create', array(
//                'set' => $set
//            )))->render()
//            ;
//    }
//
//    public function update($id)
//    {
//        $set = \AttributeSet::find($id);
//
//        $validator = \Validator::make(
//            \Input::only('name', 'product_type_id'),
//            [
//                'name' => 'required|min:3',
//                'product_type_id' => 'required|in:' . join(',', array_keys(\ProductType::getNames())),
//            ]
//        );
//
//
//        if ($validator->passes()) {
//            $set->name = \Input::get('name');
//            $set->product_type_id = \Input::get('product_type_id');
//
//            $ids = \Input::get('attribute_ids');
//            if (empty($ids)) {
//                $ids = [];
//            }
//            $set->attributes()->sync(array_keys($ids));
//            $set->saveOrFail();
//
//            return \Redirect::action('AdminAttributeSetController@edit', ['id' => $set->id]);
//        }
//        else {
//            return \Redirect::back()->withErrors($validator)->withInput();
//        }
//    }

}