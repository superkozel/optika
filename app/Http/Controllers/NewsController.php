<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 15.01.2017
 * Time: 16:10
 */

namespace App\Http\Controllers;


use Psy\Util\Json;

class NewsController extends BaseFrontendController
{
    public function breadcrumbs()
    {
        return \Breadcrumbs::create()->add('Новости', actionts('NewsController@index'));
    }

    public function index()
    {
        $page = null;
        if (\Input::has('PAGEN_1')) {
            $page = \Input::get('PAGEN_1');
        }

        $news = \News::orderBy('created_at', 'DESC')->paginate(20, null, 'page', $page);

        $news->setPath(actionts('NewsController@index'));

        $title = 'Новости';
        $metaDesc = 'Новости';
        $crumbs = $this->breadcrumbs();

        $canonical = actionts('NewsController@index', ['page' => $news->currentPage()], false);
        $this->canonicalRedirect($canonical);

        return $this->getLayout()
            ->setH1($title)
            ->setTitle($title)
            ->setMetaDescription($metaDesc)
            ->setBreadcrumbs($crumbs)
            ->setContent(\View::make('front.news.list', ['news' => $news]))->render()
            ;
    }

    public function show($slug)
    {
        $news = \News::findBySlug($slug);

        $title = $news->name;
        $crumbs = $this->breadcrumbs();
        $canonical = actionts('NewsController@show', ['slug' => $news->getSlug()]);

        return $this->getLayout()
            ->setTitle($title)
            ->setCanonical($canonical)
            ->setH1($title)
            ->setMetaDescription(strip_tags($news->preview_text))
            ->setBreadcrumbs($crumbs->add($title, $canonical))
            ->setContent(\View::make('front.news.show', ['news' => $news]))->render()
            ;
    }

    public function ajax()
    {
        $news = \News::orderBy('created_at', 'DESC')->with('image')->paginate(\Input::get('page_size', 12));
        $template = \Input::get('template', 'main-page-card');

        $data = $news->map(function($news) use ($template){
            return view('front.news.' . $template, compact('news'))->render();
        });
        return \Response::json([
            'content' => $data->all()
        ]);
    }
}