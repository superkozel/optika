<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 12.02.2017
 * Time: 13:10
 */

namespace App\Http\Controllers;


use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\UpdateProfileRequest;
use App\Http\Requests\UserActivateBonusProgramRequest;
use App\User;
use Carbon\Carbon;

class UserController extends BaseFrontendController
{
    public function __construct()
    {
        $this->middleware('needpassword', ['except' => ['getPassword', 'updatePassword', 'sendSmsCode', 'confirmEmail']]);
    }

    public function index()
    {
        return $this->getLayout('personal')
            ->setTitle('Личный кабинет')
            ->setH1('Личный кабинет')
            ->setBreadcrumbs($this->breadcrumbs())
            ->setContent(\View::make('front.user.index', array(
                'user' => $this->getUser()
            )))->render()
            ;
    }

    public function getProfile()
    {
        return $this->getLayout('personal')
            ->setH1AndTitle('Персональные данные')
            ->setBreadcrumbs($this->breadcrumbs()->add('Персональные данные'))
            ->setContent(\View::make('front.user.edit', array(
                'user' => $this->getUser()
            )))->render()
            ;
    }

    public function updateProfile(UpdateProfileRequest $request)
    {
        $oldValues = $this->getUser()->getAttributes();

        $user = $this->getUser();

        $user->fill($request->onlyValidated());

        if ($user->save()) {
            if ($user->email && $oldValues['email'] != $user->email) {
                $user->sendEmailConfirmation();
            }

            \Flash::success('Профиль успешно изменён');
            return \Redirect::back();
        }
        else {
            dd('ошибка');
        }
    }

    public function sendEmailConfirmation()
    {
        $user = $this->getUser();

        if (! $user->email) {
            \Flash::error('Не указана электронная почта');
        }
        else if ($user->email == $user->email_confirmed) {
            \Flash::error('Почтовый адрес подтвержден');
        }
        else {
            $user->sendEmailConfirmation();
            \Flash::success('Письмо с кодом отправлено на почту ' . $user->email);
        }

        return \Redirect::back();
    }

    public function getPassword()
    {
        return $this->getLayout('personal')
            ->setH1AndTitle(\Auth::user()->password ? 'Изменить пароль' : 'Установка пароля')
            ->setBreadcrumbs($this->breadcrumbs()->add('Изменить пароль'))
            ->setContent(\View::make('front.user.password', array(
                'user' => $this->getUser()
            )))->render()
            ;
    }

    public function updatePassword(ChangePasswordRequest $request)
    {
        $user = $this->getUser();

        $user->password = \Hash::make($request->get('new_password'));

        $user->save();

        \Flash::success('Пароль успешно изменен');

        return \Redirect::back();
    }

    public function bonus()
    {
        return $this->getLayout('personal')
            ->setH1AndTitle('Бонусная карта')
            ->setBreadcrumbs($this->breadcrumbs()->add('Бонусная карта'))
            ->setContent(\View::make('front.user.bonus', array(
            )))->render()
            ;
    }

    public function bonusActivate(UserActivateBonusProgramRequest $request)
    {
        \Auth::user()->update(['bonus_program_active' => 1]);
        \Flash::success('Бонусная программа успешно активирована');

        return \Redirect::back();
    }

    public function orders()
    {
        return $this->getLayout('personal')
            ->setH1AndTitle('История покупок')
            ->setBreadcrumbs($this->breadcrumbs()->add('История покупок'))
            ->setContent(\View::make('front.user.orders', array(
                'orders' => $this->getUser()->orders()->orderBy('created_at', 'DESC')->get()
            )))->render()
            ;
    }

    public function confirmEmail()
    {
        $err = null;

        if (\Input::has('token')) {
            $email = \Input::get('email');
            $user = User::where('email', $email)->first();
            if (! $user) {
                app:abort(404);
            }
            if ($user->checkEmailConfirmation(\Input::get('token'))) {
                $user
                    ->confirmEmail()
                    ->save();

                \Flash::success('Email успешно подтвержден');

                if (! \Auth::user()) {
                    \Auth::login($user);
                }
                return \Redirect::to(actionts('UserController@getProfile'));
            } else {
                \Flash::error('Неверный код подтверждения');
            }
        }

        return $this->getLayout('personal')
            ->setH1AndTitle('Подтверждение электронной почты')
            ->setBreadcrumbs($this->breadcrumbs()->add('Подтверждение электронной почты'))
            ->setContent(\View::make('front.user.confirm_email', array(
                'error' => $err
            )))->render()
            ;
    }

    public function confirmPhone()
    {
        $err = null;
        $pause = User::SMS_PAUSE;

        if (\Input::method() == 'POST' && \Input::exists('code')) {
            return $this->checkPhoneConfirmationCode(\Input::get('code'));
        }

        return $this->getLayout('personal')
            ->setH1AndTitle('Подтверждение телефона')
            ->setBreadcrumbs($this->breadcrumbs()->add('Подтверждение телефона'))
            ->setContent(\View::make('front.user.confirm_phone', array(
                'error' => $err,
                'pause' => $pause
            )))->render()
            ;
    }

    public function checkPhoneConfirmationCode($code)
    {
        $user = \Auth::user();
        if ($this->getUser()->checkSmsCode($code)) {
            $user->phone_confirmed = $user->phone;
            $user->clearSmsCode();
            $user->save();

            \Flash::success('Телефон подтверджён успешно');
            return \Redirect::back();
        }
        else {
            \Flash::error('Неверный код');
            return \Redirect::back()->withInput()->withErrors(['code' => 'Неверный код']);
        }
    }

    public function sendSmsCode()
    {
        if (\Input::has('login')) {
            $login = \Input::get('login');
            $user = User::findByLogin($login);
        }
        else {
            $user = $this->getUser();
        }

        if (!$user) {
            \App::abort(503, 'Пользователь не найден');
        }

        $err = null;
        $result = $user->sendSmsCode($err);

        if (! $result) {
            \Flash::error($err);
        }
        else {
            \Flash::success('СМС-код отправлен');
        }

        return \Redirect::back()->withInput();
    }

    public function sendBonuses()
    {
        \Flash::success('Бонусы успешно отправлены');

        return \Redirect::back()->withInput();
    }


    protected function breadcrumbs()
    {
        return parent::breadcrumbs()->add('Личный кабинет', actionts('UserController@index'));
    }
}