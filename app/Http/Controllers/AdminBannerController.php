<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 28.12.2016
 * Time: 3:51
 */

namespace App\Http\Controllers;


use App\Http\Requests\AdminBannerRequest;

class AdminBannerController extends BaseAdminCrudController
{
    protected static $labels = 'акция|акции';

    public function store(AdminBannerRequest $request)
    {
        $banner = parent::createModelFromRequest($request);

        $this->saveImage($banner, $request, 'image');

        return \Redirect::to($banner->getEditUrl());
    }

    public function update($id, AdminBannerRequest $request)
    {
        $banner = parent::updateModelFromRequest($id, $request);
        $this->saveImage($banner, $request, 'image');

        return \Redirect::to($banner->getEditUrl());
    }
}