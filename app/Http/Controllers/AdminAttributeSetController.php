<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 07.01.2017
 * Time: 10:34
 */

namespace App\Http\Controllers;

use App\Http\Controllers\BaseAdminController;

class AdminAttributeSetController extends BaseAdminCrudController
{
    protected static $labels = 'Набор атрибутов|Наборы атрибутов';
}