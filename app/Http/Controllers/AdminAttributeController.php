<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 06.01.2017
 * Time: 13:12
 */

namespace App\Http\Controllers;


class AdminAttributeController extends BaseAdminCrudController
{
    protected static $labels = 'Атрибут|Атрибуты';

    public function create()
    {
        $attr = new \Attribute();

        $attr->fill(\Input::all());

        return $this->getLayout()
            ->setH1AndTitle('Создать атрибут')
            ->setContent(\View::make('admin.attribute.create', array(
                'attribute' => $attr
            )))->render()
            ;
    }


    public function store()
    {
        $attr = new \Attribute();

        $validator = \Validator::make(
            \Input::only('name', 'type_id', 'class'),
            [
                'name' => 'required|min:3',
                'type_id' => 'required|in:' . join(',', array_keys(\AttributeType::getNames())),
            ]
        );

        if ($validator->passes()) {
            $attr->name = \Input::get('name');
            $attr->type_id = \Input::get('type_id');

            if ($attr->type_id == \AttributeType::MODEL) {
                $attr->class = \Input::get('class');
            }

            if ($attr->type_id == \AttributeType::SELECT) {
                $options = \Input::get('options');

                foreach ($options as $id=> $v) {
                    var_dump($options);
                }
            }

            $attr->save();

            return \Redirect::action('AdminAttributeController@edit', ['id' => $attr->id]);
        }
        else {
            return \Redirect::back()->withErrors($validator)->withInput();
        }
    }


    public function edit($id)
    {
        $attr = \Attribute::find($id);

        return $this->getLayout()
            ->setH1AndTitle('Редактировать свойство')
            ->setContent(\View::make('admin.attribute.create', array(
                'attribute' => $attr
            )))->render()
            ;
    }

    public function update($id)
    {
        $attr = \Attribute::find($id);

        $validator = \Validator::make(
            \Input::only('name', 'type_id', 'class'),
            [
                'name' => 'required|min:3',
                'type_id' => 'required|in:' . join(',', array_keys(\AttributeType::getNames())),
            ]
        );

        if ($validator->passes()) {
            $attr->name = \Input::get('name');
            $attr->type_id = \Input::get('type_id');

            if ($attr->type_id == \AttributeType::MODEL) {
                $attr->class = \Input::get('class');
            }

            if ($attr->type_id == \AttributeType::SELECT) {
                $options = \Input::get('options');

                foreach ($options as $id=> $v) {
                    var_dump($options);
                }
            }

            $attr->save();

            return \Redirect::action('AdminAttributeController@edit', ['id' => $attr->id]);
        }
        else {
            return \Redirect::back()->withErrors($validator)->withInput();
        }
    }
}