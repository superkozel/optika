<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 18.04.2017
 * Time: 0:33
 */

namespace App\Http\Controllers;


use ActiveRecord\PaymentTransaction;
use Carbon\Carbon;
use Enum\PaymentSystem;
use Enum\PaymentTransactionStatus;
use Illuminate\Http\Request;
use YandexMoney\Utils;

class PaymentYandexKassaController extends Controller
{
    const STATUS_OK = 0;
    const STATUS_AUTH_FAILED = 1;
    const STATUS_INVALID_REQUEST = 100;
    const STATUS_INTERNAL_ERROR = 200;

    public static function routes()
    {
        \Route::any('/yandexkassa/', 'PaymentYandexKassaController@processRequest');
        \Route::get('/yandexkassa/success/', 'PaymentYandexKassaController@success');
        \Route::get('/yandexkassa/fail/', 'PaymentYandexKassaController@fail');
        \Route::any('/yandexkassa/{action}/', 'PaymentYandexKassaController@processAction');

        return true;
    }

    protected $logger;
    protected function getLogger()
    {
        if (empty($this->logger)) {
            $this->logger = new \FileLogger('yandexkassa');
        }

        return $this->logger;
    }

    public function processRequest(Request $request)
    {
        return $this->processAction($request->get('action'), $request);
    }

    public function success(Request $request)
    {
        \Flash::success('Заказ успешно оплачен');
        return \Redirect::to(actionts('OrderController@view', ['id' => $request->get('orderNumber')]));
    }


    public function fail(Request $request)
    {
        \Flash::error('Ошибка оплаты');
        return \Redirect::to(actionts('OrderController@view', ['id' => $request->get('orderNumber')]));
    }

    public function processAction($act, Request $request)
    {
        $err = null;
        $data = $request->all();
//        $data = $this->mockData();
        $action = array_get($data, 'action');
        $this->getLogger()->info('processing request', $data);

        try {
            $order = \Order::find(array_get($data, 'orderNumber'));

            if (! $order) {
                throw new \Exception('Invalid request, no order number', static::STATUS_INVALID_REQUEST);
            }

            $validator = $this->validator($data, $order);

            if ($validator->passes()) {
                $status = static::STATUS_OK;

                switch ($action)
                {
                    case 'checkOrder':
                        $this->checkOrder($data);
                        break;
                    case 'cancelOrder':
                        $this->cancelOrder($data);
                        break;
                    case 'paymentAviso':
                        $this->aviso($data);
                        break;
                    default:
                        throw new \Exception('Invalid action', static::STATUS_INVALID_REQUEST);
                        break;
                }
            }
            else {
                if ($validator->errors()->has('md5')) {
                    $status = static::STATUS_AUTH_FAILED;
                }
                else {
                    $status = static::STATUS_INVALID_REQUEST;
                }

                $err = $validator->errors()->first();
            }
        }
        catch (\Exception $e) {
            $err = $e->getMessage();

            if ($e->getCode())
                $status = $e->getCode();
            else
                $status = static::STATUS_INTERNAL_ERROR;

            throw new \Exception($err, $status);
        }


        return $this->buildResponse(array_get($data, 'action'), array_get($data, 'invoiceId'), $status, $err);
    }

    public function mockData()
    {
        return json_decode('{"orderNumber":"3","orderSumAmount":"10.00","cdd_exp_date":"0719","shopArticleId":"411005","paymentPayerCode":"4100322062290","paymentDatetime":"2017-05-04T02:45:05.144+03:00","cdd_rrn":"351674607953","external_id":"deposit","paymentType":"AC","requestDatetime":"2017-05-04T02:45:05.138+03:00","depositNumber":"b9SBolIsqnrINJY84hSdQbnZgkoZ.001f.201705","nst_eplPayment":"true","cdd_response_code":"00","cps_user_country_code":"RU","orderCreatedDatetime":"2017-05-04T02:45:03.410+03:00","sk":"u8b5ea1bf1912feeb6d838e152b6ac56a","action":"paymentAviso","shopId":"98691","scid":"548639","shopSumBankPaycash":"1003","shopSumCurrencyPaycash":"10643","rebillingOn":"false","orderSumBankPaycash":"1003","cps_region_id":"20674","orderSumCurrencyPaycash":"10643","merchant_order_id":"3_040517024444_00000_98691","unilabel":"209c7f2b-0009-5000-8000-000020a3a24d","cdd_pan_mask":"427638|7223","customerNumber":"1","yandexPaymentId":"2570084952909","shopDefaultUrl":"http://optika/order/3/view/","invoiceId":"2000001152592","shopSumAmount":"9.65","md5":"0379F8A5F9B8825174A559470EAE3495"}', true);
    }

    protected function md5($data)
    {
        //'action;orderSumAmount;orderSumCurrencyPaycash;orderSumBankPaycash;shopId;invoiceId;customerNumber;shopPassword';
        $md5 = strtoupper(md5(join(';', [
            $data['action'],
            $data['orderSumAmount'],
            $data['orderSumCurrencyPaycash'],
            $data['orderSumBankPaycash'],
            $data['shopId'],
            $data['invoiceId'],
            $data['customerNumber'],
            config('yandexkassa.password')
        ])));

        $this->getLogger()->info('generated md5=[' . $md5 . ']');

        return $md5;
    }

    /**
     * Building XML response.
     * @param  string $functionName  "checkOrder" or "paymentAviso" string
     * @param  string $invoiceId     transaction number
     * @param  string $result_code   result code
     * @param  string $message       error message. May be null.
     * @return string                prepared XML response
     */
    private function buildResponse($functionName, $invoiceId, $result_code, $message = null) {
        $performedDatetime = Utils::formatDate(new \DateTime());
        $response = '<?xml version="1.0" encoding="UTF-8"?><' . $functionName . 'Response performedDatetime="' . $performedDatetime .
            '" code="' . $result_code . '" ' . ($message != null ? 'message="' . $message . '"' : "") . ' invoiceId="' . $invoiceId . '" shopId="' . config('yandexkassa.shopId') . '"/>';
        $this->getLogger()->info('response', ['content' => $response]);
        return response($response, 200, ['Content-Type' => 'text/xml']);
    }

    public static function getOrderCustomerNumber(\Order $order)
    {
        return $order->user_id ? $order->user_id : ('+7' . $order->phone);
    }

    public function validator($data, \Order $order)
    {
        $md5 = $this->md5($data);

        return \Validator::make($data, [
            'md5' => 'required|in:'. $md5,
            'shopId' => 'required|in:' . config('yandexkassa.shopId'),
            'invoiceId' => 'required',
            'orderNumber' => 'required|exists:orders,id',
            'customerNumber' => 'required|in:' . static::getOrderCustomerNumber($order),
//            'orderSumAmount' => 'required|numeric|min:' . $order->payment_required_sum . '|max:' . $order->payment_required_sum,


//                'orderCreatedDatetime' => '',
//                'orderSumCurrencyPaycash' => '',
//                'orderSumBankPaycash' => '',
//                'shopSumAmount' => '',
//                'shopSumCurrencyPaycash' => '',
//                'shopSumBankPaycash' => '',
//                'paymentPayerCode' => '',
//                'paymentType' => '',
        ]);
    }

    public function checkOrder($data)
    {
        PaymentTransaction::unguard();
        PaymentTransaction::updateOrCreate(
            ['transaction_id' => $data['invoiceId']],
            [
                'order_id' => $data['orderNumber'],
                'transaction_id' => $data['invoiceId'],
                'sum' => $data['orderSumAmount'],
                'payment_system_id' => PaymentSystem::YANDEXKASSA,
                'status_id' => PaymentTransactionStatus::NEWS,
            ]
        );
    }

    public function cancelOrder($data)
    {
        $transaction = PaymentTransaction::whereInvoiceId($data['invoiceId'])->first();
        $transaction->update(['status_id' => PaymentTransactionStatus::CANCELED]);
    }

    public function aviso($data)
    {
        $transaction = PaymentTransaction::whereTransactionId($data['invoiceId'])->first();
        if ($transaction->status_id == PaymentTransactionStatus::NEWS) {
            \DB::transaction(function() use ($transaction){
                PaymentTransaction::unguard();
                $transaction->update([
                    'status_id' => PaymentTransactionStatus::COMPLETE,
                    'paid_at' => Carbon::now()
                ]);

                /** @var \Order $order */
                $order = $transaction->order;
                $order->paid_sum += $transaction->sum;
//                $order->paid = (int)($transaction->sum == $order->payment_required_sum);
                $order->save();
            });
        }
        else {
            throw new \Exception('transaction canceled or already completed');
        }
    }

}