<?php
/**
 *
 * @author Зыков Дмитрий <aesleep@ya.ru>
 * @date 12.05.2015
 */

namespace App\Http\Controllers;


use App\User;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManagerStatic;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class AdminUploadController extends BaseAdminController
{
    public function store()
    {
        $files = \Input::file('upload');

        $result = array();

        if (! empty($files)) {
            foreach ($files as $file) {
                $upload = User::findMyUploadByName($file->getClientOriginalName());

                if (! $upload) {
                    $upload = \Upload::createFromUploadedFile($file);

                    if ($user = \Auth::user())
                        $upload->user_id = $user->id;
                    else
                        \Session::push('uploads', $upload->id);

                    $upload->save();
                }

                /** @var UploadedFile $file */
                $result[] = array(
                    'id' => 'u' . $upload->id,
                    'thumb' => $upload->thumb()
                );
            }
        }
        if (\Input::has('url') && strlen(\Input::get('url')) > 7) {
            $url = \Input::get('url');
//            foreach (\Input::get('url') as $url) {
                try {
                    $upload = User::findMyUploadByName($url);

                    if (! $upload) {
                        $upload = \Upload::createFromUrl($url);

                        if ($user = \Auth::user())
                            $upload->user_id = $user->id;
                        else
                            \Session::push('uploads', $upload->id);

                        $upload->save();
                    }

                    /** @var UploadedFile $file */
                    $result[] = array(
                        'id' => 'u' . $upload->id,
                        'thumb' => $upload->thumb()
                    );
                }
                catch (\Exception $e) {
                    return \Response::json(array(
                        'success' => false,
                        'error' => $e->getMessage()
                    ));
                }
//            }
        }

        return \Response::json(array(
            'success' => true,
            'result' => $result
        ));
    }

    public function index()
    {
        return array_map(function(\Upload $upload){
            return array(
                'id' => 'u' . $upload->id,
                'thumb' => $upload->thumb()
            );
        }, User::myUploads());
    }

//    /**
//     * Stores new upload
//     *
//     */
//    public function store()
//    {
//        $file = Input::file('file');
//
//        try {
//			$upload = Upload::makeFromUploadedFile($file);
//        } catch(Exception $exception){
//            // Something went wrong. Log it.
//            Log::error($exception);
//            $error = array(
//                'name' => $file->getClientOriginalName(),
//                'size' => $file->getSize(),
//                'error' => $exception->getMessage(),
//            );
//            // Return error
//            return Response::json($error, 400);
//        }
//
//        // If it now has an id, it should have been successful.
//        if ( $upload->id ) {
//            $newurl = URL::asset($upload->publicpath().$upload->filename);
//
//            // this creates the response structure for jquery file upload
//            $success = new stdClass();
//            $success->name = $upload->filename;
//            $success->size = $upload->size;
//            $success->url = $newurl;
//            $success->thumbnailUrl = $newurl;
//            $success->deleteUrl = action('UploadController@delete', $upload->id);
//            $success->deleteType = 'DELETE';
//            $success->fileID = $upload->id;
//
//            return Response::json(array( 'files'=> array($success)), 200);
//        } else {
//            return Response::json('Error', 400);
//        }
//    }
//
//    public function delete($id)
//    {
//        $upload = Upload::find($id);
//        $upload->delete();
//
//        $success = new stdClass();
//        $success->{$upload->filename} = true;
//
//        return Response::json(array('files'=> array($success)), 200);
//    }
//
//
//    // return a path without starting with /public
//    public function publicpath() {
//        return str_replace('/public/', '/', URL::asset($this->path) . '/');
//    }
}