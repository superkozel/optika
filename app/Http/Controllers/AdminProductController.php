<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 31.12.2016
 * Time: 18:54
 */

namespace App\Http\Controllers;


use App\User;

class AdminProductController extends BaseAdminCrudController
{
    protected $label = 'Товар|Товары';

    public function create()
    {
        $product = new \Product();

        return $this->getLayout()
            ->setH1AndTitle('Создать товар')
            ->setContent(\View::make('admin.product.create', array(
                'product' => $product
            )))->render()
            ;
    }

    public function store()
    {
        $product = new \Product();

        return $this->createOrUpdate($product);
    }


    public function edit($id)
    {
        $product = \Product::find($id);

        return $this->getLayout()
            ->setH1AndTitle('Редактировать товар')
            ->setContent(\View::make('admin.product.create', array(
                'product' => $product
            )))->render()
            ;
    }

    public function update($id)
    {
        $product = \Product::find($id);

        return $this->createOrUpdate($product);
    }

    public function createOrUpdate($product)
    {
        $validator = \Validator::make(
            array_filter(\Input::only('name', 'type_id', 'model_name', 'article', 'description', 'short_description', 'brand_id', 'availability', 'price', 'discount', 'new', 'popular', 'hit'), function($v){return $v !== '' && ! is_null($v);}),
            [
                'name' => 'required|min:3',
                'type_id' => 'required|in:' . join(',', array_keys(\ProductType::getNames())),
                'brand_id' => 'sometimes|exists:brands,id',
                'price' => 'numeric',
                'discount' => 'integer|min:0|max:100'
            ]
        );

        if ($validator->passes()) {
            $product->unguard();
            $product->fill($validator->getData());
            $product->saveOrFail();

            $product->images = \Upload::findMany(\Input::get('photos'));

            return \Redirect::action('AdminProductController@edit', ['id' => $product->id]);
        }
        else {
            return \Redirect::back()->withErrors($validator)->withInput();
        }
    }

    public function attributes($typeId, $id = null)
    {
        $type = \ProductType::find($typeId);
        if ($id) {
            $product = \Product::find($id);
        }
        else {
            $product = new \Product();
        }

        $attributes = $type->attributes;
        return \View::make('admin.product.attributes', ['attributes' => $attributes, 'product' => $product]);
    }
}