<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 31.12.2016
 * Time: 19:47
 */

namespace App\Http\Controllers;


class SearchController extends BaseFrontendController
{
    public function index()
    {
        return $this->getLayout('1col')
            ->setH1AndTitle('Поиск по сайту')
            ->setBreadcrumbs($this->breadcrumbs()->add('Поиск'))
            ->setContent(view('front.search.index', array(
            )))
            ->render()
            ;
    }

    public function suggest()
    {
        $q = \Input::get('q');

        $products = \Product::where('name', 'LIKE', '%' . $q . '%')->limit(5)->get();

        $categories = $this->searchCategories($q);

        $brands = $this->searchBrands($q);

        return view('front.search.suggest', ['products' => $products, 'categories' => $categories, 'brands' => $brands]);
    }

    protected function searchCategories($q)
    {
        $indexableCatalogCategories = null;

        $realCategories = \CatalogCategory::where('name', 'LIKE', '%' . $q . '%')->limit(5)->get();

        return $realCategories;
    }

    protected function searchBrands($q)
    {
        return \Brand::where('name', 'LIKE', '%' . $q . '%')->limit(3)->get();
    }
}