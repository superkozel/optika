<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 02.03.2017
 * Time: 9:16
 */

namespace App\Http\Requests;


class AdminSalonRequest extends AdminRequest
{
    public function rules()
    {
        return [
            'name' => 'required|min:3',
            'address' => 'required|min:3',
            'city_id' => 'required|exists:cities,id',
            'specialization_id' => '',
            'coordinates' => '',
            'mode' => 'required',
            'description' => '',
            'service_ids' => '',
        ];
    }
}