<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 20.03.2017
 * Time: 16:54
 */

namespace App\Http\Requests;


class UserActivateBonusProgramRequest extends Request
{
    public function authorize()
    {
        return \Auth::user();
    }

    public function rules()
    {
        return [
            'accepted' => 'required|in:true,1',
        ];
    }

    public function messages()
    {
        return [
            'accepted.required' => 'Необходимо прочитать и принять условия договора-оферты',
        ];
    }
}