<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 02.03.2017
 * Time: 9:16
 */

namespace App\Http\Requests;


class AdminActionRequest extends AdminRequest
{

    public function rules()
    {
        return [
            'name' => 'required|string|min:3',
            'preview_text' => 'string',
            'content' => 'required|string',
            'active_from' => 'required|date',
            'active_to' => 'required|date|after:active_from',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Название',
            'preview_text' => 'Описание короткое',
            'content' => 'Описание',
            'active_from' => 'Дата начала',
            'active_to' => 'Последний день акции',
        ];
    }
}