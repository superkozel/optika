<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 22.02.2017
 * Time: 22:06
 */

namespace App\Http\Requests;

class UpdateProfileRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $userId = \Auth::user()->id;
        return [
            'password' => 'required|auth_password',
            'name' => 'required|min:2',
            'lastname' => 'required|min:2',
            'birthdate' => 'date',
            'email' => 'required|email|unique_user_email:' . $userId,
            'phone' => 'phone|unique_user_phone:' . $userId,
        ];
    }

    public function attributes()
    {
        return [
            'password' => 'Пароль',
            'name' => 'Имя',
            'lastname' => 'Фамилия',
            'birthdate' => 'Дата рождения',
            'email' => 'email',
            'phone' => 'Телефон',
        ];
    }
}