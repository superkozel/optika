<?php
/**
 * Created by PhpStorm.
 * User: LittlePoo
 * Date: 30.11.2015
 * Time: 17:56
 */

namespace App\Http\Requests;

use Illuminate\Validation\Factory;

class ChangePasswordRequest extends Request
{

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return \Auth::user();
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{

		return [
			'old_password' => 'required|auth_password',
			'new_password' => 'required|min:4',
		];
	}
}