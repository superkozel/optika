<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 02.03.2017
 * Time: 23:27
 */

namespace App\Http\Requests;


class AdminBrandRequest extends AdminRequest
{
    public function rules()
    {
        return [
            'name' => 'required|min:3',
            'alt_name' => 'min:3',
            'short_description' => '',
            'description' => '',
            'sort' => '',
        ];
    }
}