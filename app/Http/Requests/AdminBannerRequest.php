<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 29.07.2017
 * Time: 11:42
 */

namespace App\Http\Requests;


class AdminBannerRequest extends AdminRequest
{
    public function rules()
    {
        return [
            'name' => 'required|string|min:3',
            'url' => 'required|url',
            'position_id' => 'required|string',
            'active_to' => 'date',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Название',
            'position_id' => 'Позиция',
            'url' => 'Адрес',
            'active_to' => 'Последний день акции',
        ];
    }

}