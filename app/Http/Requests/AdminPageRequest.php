<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 20.05.2017
 * Time: 1:18
 */

namespace App\Http\Requests;


class AdminPageRequest extends AdminRequest
{
    public function rules()
    {
        return [
            'h1' => 'required',
            'title' => 'required',
            'metadesc' => '',
            'keywords' => '',
            'parent_id' => '',
            'layout' => '',
            'content' => '',
        ];
    }
}