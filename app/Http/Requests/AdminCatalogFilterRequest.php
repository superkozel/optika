<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 12.04.2017
 * Time: 15:20
 */

namespace App\Http\Requests;


class AdminCatalogFilterRequest extends AdminRequest
{
    public function rules()
    {
        return [
            'name' => 'required|min:3',
            'value_type' => 'required|array_in:' . join(',', array_keys(\CatalogFilterValueType::getNames())),
            'type_id' => 'required|array_in:' . join(',', array_keys(\CatalogFilterType::getNames())),
            'value_id' => 'required',
            'sort_type' => 'required|array_in:' . join(',', array_keys(\CatalogFilterSortType::getNames())),
            'sort_desc' => 'bool',
            'description' => 'string'
        ];
    }
}