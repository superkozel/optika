<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 01.03.2017
 * Time: 9:36
 */

namespace App\Http\Requests;


class AdminNewsRequest extends AdminRequest
{
    public function rules()
    {
        return [
            'name' => 'required|min:3',
            'preview_text' => 'min:15',
            'content' => 'required',
        ];
    }
}