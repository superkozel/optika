<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 12.05.2017
 * Time: 11:36
 */

namespace App\Http\Requests;

class AdminOrderRequest extends AdminRequest
{
    public function rules()
    {
        /** @var \Order $order */
        $order = \Order::findOrFail($this->route('id'));

        return array_only(
            $order->validator($this->all())->getRules(),
            [
                'name', 'phone', 'email', 'status_id', 'delivery_date', 'delivery_time', 'salon_id', 'address', 'comment', 'manager_comment'
            ]);
    }

//    public function names() {
//        /** @var \Order $order */
//        $order = \Order::findOrFail($this->get('id'));
//    }

//    public function rules()
//    {
//        return [
//            'name' => 'required|min:3',
//            'preview_text' => 'min:15',
//            'content' => 'required',
//        ];
//    }
}