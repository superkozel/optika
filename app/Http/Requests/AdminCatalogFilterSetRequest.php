<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 12.04.2017
 * Time: 11:57
 */

namespace App\Http\Requests;


class AdminCatalogFilterSetRequest extends AdminRequest
{
    public function rules()
    {
        return [
            'name' => 'required|min:3',
        ];
    }
}