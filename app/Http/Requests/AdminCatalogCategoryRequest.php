<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 05.03.2017
 * Time: 8:32
 */

namespace App\Http\Requests;


class AdminCatalogCategoryRequest extends AdminRequest
{
    public function rules()
    {
        $id = $this->route('id');
        return [
            'name' => 'required|min:3',
            'name_rod' => 'required',
            'parent_id' => 'available_parent:' . \CatalogCategory::class . ',' . $id,
            'description' => '',
            'filter_set_id' => 'exists:catalog_filter_sets,id',

            'h1' => '',
            'title' => '',
            'metadesc' => '',
            'seotext' => '',
        ];
    }

}