<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateFeedbackRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name' => 'required|min:3',
			'email' => 'required|email',
			'content' => 'required|min:15',
            'accept_agreement_personal' => 'required|',
		];
	}

	public function attributes()
	{
		return [
			'name' => 'Ваше имя',
			'email' => 'email',
			'content' => 'Текст сообщения',
            'accept_agreement_personal' => '',
		];
	}

    public function messages()
    {
        return [
            'accept_agreement_personal.required' => 'Необходимо подтвердить согласие с условиями',
        ];
    }

}
