<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class Request extends FormRequest
{
	function onlyValidated($onlyKeys = null)
	{
	    $arr = [];
	    $keys = array_keys($this->rules());
	    foreach ($keys as $key) {
	        if ($this->exists($key))
	            $arr[$key] = $this->get($key);
        }

        return $arr;//попробую так, потому что в Page parent_id не работает
//        dd(array_replace_empty($arr));
//		return array_replace_empty($arr);
	}
}
