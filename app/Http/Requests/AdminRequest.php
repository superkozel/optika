<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 01.03.2017
 * Time: 9:37
 */

namespace App\Http\Requests;


use App\User;

class AdminRequest extends Request
{
    public function authorize()
    {
        return \Auth::user() && \Auth::user()->isAdmin();
    }

    /**
     * Validate the class instance.
     *
     * @return void
     */
    public function validate()
    {
        $this->prepareForValidation();

        $instance = $this->getValidatorInstance();

        if (! $this->passesAuthorization()) {
            $this->failedAuthorization();
        } elseif (! $instance->passes()) {
            \Flash::error('Ошибка заполнения');
            $this->failedValidation($instance);
        }
        else {
            \Flash::success('Успешно сохранено');
        }
    }
}