<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class MustSetPassword
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $redirectTo = actionts('UserController@getPassword');
        if (! Auth::user()->password) {
            \Flash::warning('Необходимо установить пароль');

            return \Redirect::to($redirectTo);
        }

        return $next($request);
    }
}
