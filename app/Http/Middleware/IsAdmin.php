<?php
/**
 * Created by PhpStorm.
 * User: LittlePoo
 * Date: 03.12.2015
 * Time: 19:05
 */

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class IsAdmin
{
	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
        if (! $this->auth->guest())
		{
			if (! $this->auth->user()->isAdmin()) {
				if ($request->ajax())
				{
					return response('Unauthorized.', 401);
				}
				else
				{
					$referer = \Request::header('referer');

					if(empty($referer)) {
						return redirect()->to('/');
					} else {
						return redirect()->back();
					}
				}
			}
		}
		else {
			if ($request->ajax())
			{
				return response('Unauthorized.', 401);
			}
			else
			{
				return redirect()->guest('/login/');
			}
		}

		return $next($request);
	}
}