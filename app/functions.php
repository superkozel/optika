<?
function slug($msg) {
    return mb_strtolower(preg_replace("/-$/i", "", preg_replace("/^-/i", "", preg_replace("/[-]{1,}/i", "-", preg_replace("/[^_0-9a-z]/i", "-", Rus2Lat($msg))))), 'utf-8');
}

function Rus2Lat($msg) {
    $iso = array(
        "А" => "A", "Б" => "B", "В" => "V", "Г" => "G", "Д" => "D",
        "Е" => "E", "Ё" => "YO", "Ж" => "ZH",
        "З" => "Z", "И" => "I", "Й" => "J", "К" => "K", "Л" => "L",
        "М" => "M", "Н" => "N", "О" => "O", "П" => "P", "Р" => "R",
        "С" => "S", "Т" => "T", "У" => "U", "Ф" => "F", "Х" => "H",
        "Ц" => "C", "Ч" => "CH", "Ш" => "SH", "Щ" => "SH", "Ъ" => "'",
        "Ы" => "Y", "Ь" => "", "Э" => "E", "Ю" => "YU", "Я" => "YA",
        "а" => "a", "б" => "b", "в" => "v", "г" => "g", "д" => "d",
        "е" => "e", "ё" => "yo", "ж" => "zh",
        "з" => "z", "и" => "i", "й" => "j", "к" => "k", "л" => "l",
        "м" => "m", "н" => "n", "о" => "o", "п" => "p", "р" => "r",
        "с" => "s", "т" => "t", "у" => "u", "ф" => "f", "х" => "h",
        "ц" => "c", "ч" => "ch", "ш" => "sh", "щ" => "sh", "ъ" => "",
        "ы" => "y", "ь" => "", "э" => "e", "ю" => "yu", "я" => "ya"
    );

    return strtr($msg, $iso);
}

function utf8_for_xml($string)
{
    return preg_replace ('/[^\x{0009}\x{000a}\x{000d}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}]+/u', ' ', $string);
}

/**
 * Action with trailing slash, laravel workaround
 */
function actionts($name, $parameters = [], $absolute = true)
{
    $url = action($name, $parameters, $absolute);
    $parts = parse_url($url);

    return (! empty($parts['host']) ? ($parts['scheme'] . '://' . $parts['host']) : '') . $parts['path'] . '/' . (! empty($parts['query']) ? ('?' .$parts['query']) : '');
}

function plural($count, $one, $two, $many, $append = false)
{
    $countInput = $count;
    $count = abs($count) % 100;
    $lcount = $count % 10;

    if ($count >= 11 && $count <= 19) $result=$many;
    else if ($lcount >= 2 && $lcount <= 4) $result=$two;
    else if ($lcount == 1) $result=$one;
    else $result = $many;
    return $append ? $countInput . ' ' . $result : $result;
}

function codepoint2utf8($codepoint)
{
    $num = hexdec(str_replace("U+","", $codepoint));

    if($num<=0x7F)       return chr($num);
    if($num<=0x7FF)      return chr(($num>>6)+192).chr(($num&63)+128);
    if($num<=0xFFFF)     return chr(($num>>12)+224).chr((($num>>6)&63)+128).chr(($num&63)+128);
    if($num<=0x1FFFFF)   return chr(($num>>18)+240).chr((($num>>12)&63)+128).chr((($num>>6)&63)+128).chr(($num&63)+128);
    return '';
}

function moneyFormat($amount)
{
    return number_format($amount, 0, '.', ' ');

}

function extract_numbers($str)
{
    return preg_replace("/[^0-9]/","",$str);;
}

function alternate_chunk($array, $parts) {
    $t = 0;
    $result = array();
    $max = ceil(count($array) / $parts);
    foreach(array_chunk($array, $max) as $v) {
        if ($t < $parts) {
            $result[] = $v;
        } else {
            foreach($v as $d) {
                $result[] = array($d);
            }
        }
        $t += count($v);
    }
    return $result;
}
if(!function_exists('mb_ucfirst')) {
    function mb_ucfirst($str, $enc = 'utf-8') {
        return mb_strtoupper(mb_substr($str, 0, 1, $enc), $enc).mb_substr($str, 1, mb_strlen($str, $enc), $enc);
    }
}

function formatDate($time)
{
//    return strftime('%d %B %Y', $time);
    return (new IntlDateFormatter('ru_RU', IntlDateFormatter::LONG, IntlDateFormatter::NONE, 'Europe/Moscow'))->format($time);
}

function array_replace_empty($array)
{
    $emptyTest = function($val){
        return $val === '0' OR ! empty($val);
    };

    return array_map(function($v) use ($emptyTest){
        return $emptyTest($v) ? $v : null;
    }, $array);
}

function sanitize_filename($name){
    return str_replace(array(':', '?', '='), '', $name);
}

function file_url($url){
    $parts = parse_url($url);

    if (empty($parts['path']))
        throw new Exception('incorrect url');

    $path_parts = array_map('rawurldecode', explode('/', $parts['path']));

    return
        $parts['scheme'] . '://' .
        $parts['host'] .
        implode('/', array_map('rawurlencode', $path_parts))
        ;
}


function extractPhone($str)
{
    //поиск телефона по регулярке
    $pattern = '/((\+?\d+[\s\-\.]?)?((\(\d+\)|\d+)[\s\-\.]?)?(\d[\s\-\.]?){5,6}\d)/x';
    preg_match_all($pattern, $str, $matches);

    if (! empty($matches[0][0]))
        return extract_numbers($matches[0][0]);
}

function extractEmail($str)
{
    //поиск email по регулярке
    $pattern = '/[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/ix';
    preg_match_all($pattern, $str, $matches);
    if (!empty($matches[0][0])){
        return $matches[0][0];
    }
}

function camelCaseToUnderscore($str, $underscore = '_') {
    return ltrim(strtolower(preg_replace('/[A-Z]([A-Z](?![a-z]))*/', $underscore . '$0', $str)), $underscore);
}

function filterPhoneFormat($phone) {
    return '8' . mb_substr(extract_numbers($phone), 1);
}

function parseFloat($v)
{
    return (float) str_replace(',', '.', $v);
}