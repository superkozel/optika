<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 28.04.2017
 * Time: 20:41
 */

namespace App\Listeners;

use Illuminate\Auth\Events\Registered;

class BindOrdersToRegisteredUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  auth.login  $event
     * @return void
     */
    public function handle(Registered $event)
    {
        /** @var User $user */
        $user = $event->user;

        if ($user->phone) {
            \Order::where('phone', $user->phone)->whereNull('user_id')->update(['user_id' => $user->id]);
        }

        if ($user->email) {
            \Order::where('email', $user->email)->whereNull('user_id')->update(['user_id' => $user->id]);
        }
    }
}
