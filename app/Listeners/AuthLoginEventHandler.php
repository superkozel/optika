<?php

namespace App\Listeners;

use ActiveRecord\Favorite;
use App\Providers\EventServiceProvider;
use App\User;
use \Illuminate\Auth\Events\Login;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AuthLoginEventHandler
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  auth.login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        /** @var User $user */
        $user = $event->user;

        $currentBasket = \Basket::create(\BasketDbStorage::create(User::$oldSessionId));
        if ($currentBasket->getContentsCount() > 0) {
            $currentBasket
                ->setUser($user)
                ->save()
            ;
        }

        $ids = $user->favorites()->pluck('product_id');
        Favorite::where('session_id', User::$oldSessionId)->whereNotIn('product_id', $ids)->update(['session_id' => \Session::getId(), 'user_id' => $user->id]);
        Favorite::whereNull('user_id')->where('session_id', User::$oldSessionId)->delete();

        return true;
    }
}
