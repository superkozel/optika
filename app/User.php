<?php

namespace App;

use ActiveRecord\Favorite;
use App\Notifications\ConfirmEmail;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Mail\Mailer;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * App\User
 *
 * @property int $id
 * @property string $login
 * @property string $name
 * @property string $lastname
 * @property string $email
 * @property string $phone
 * @property string $password
 * @property string $old_password
 * @property string $secret
 * @property string $last_login_at
 * @property bool $admin
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property bool $email_confirmed
 * @property bool $phone_confirmed
 * @property-read \Illuminate\Database\Eloquent\Collection|\Order[] $orders
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @method static \Illuminate\Database\Query\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereLogin($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereLastname($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User wherePhone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereOldPassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereSecret($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereLastLoginAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereAdmin($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereEmailConfirmed($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User wherePhoneConfirmed($value)
 * @mixin \Eloquent
 * @property \Carbon\Carbon $phone_confirmation_sent_at
 * @method static \Illuminate\Database\Query\Builder|\App\User wherePhoneConfirmationSentAt($value)
 * @property string $bonus_card_number
 * @property bool $bonus_program_active
 * @property-read \Illuminate\Database\Eloquent\Collection|\ActiveRecord\Favorite[] $favorites
 * @method static \Illuminate\Database\Query\Builder|\App\User whereBonusCardNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereBonusProgramActive($value)
 * @property string $birthdate
 * @property string $phone_confirmation_code
 * @property bool $role_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\Product[] $favoriteProducts
 * @property-read \Illuminate\Database\Eloquent\Collection|\Salon[] $favoriteSalons
 * @property-read mixed $role
 * @method static \Illuminate\Database\Query\Builder|\App\User whereBirthdate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User wherePhoneConfirmationCode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereRoleId($value)
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'password', 'remember_token', 'phone_confirmation_code', 'secret', 'old_password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'secret', 'old_password',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'phone_confirmation_sent_at',
    ];

    public static $oldSessionId = null;

    public static function boot()
    {
        if (\Session::isStarted()) {
            static::$oldSessionId = \Session::getId();
        }

        return parent::boot();
    }

    public function isAdmin()
    {
        return $this->role->is(\UserRole::ADMIN, \UserRole::SUPERADMIN);
    }

    public function orders()
    {
        return $this->hasMany(\Order::class);
    }

    /** @return HasMany */
    public function favorites()
    {
        return $this->hasMany(Favorite::class);
    }

    /** @return HasMany */
    public function favoriteProducts()
    {
        return $this->belongsToMany(\Product::class, 'favorites');
    }

    public function favoriteSalons()
    {
        return $this->belongsToMany(\Salon::class, 'favorite_salons');
    }

    public static function findByLogin($login)
    {
        if (is_numeric($login) && strlen((int)$login == 5)) {
            $field = 'bonus_card_number';
        } elseif ($phone = extractPhone($login)) {
            $field = 'phone';
            if ($login[0] == '7') {
                $login[0] = '8';
            }
            $login = $phone;
        } else if ($email = extractEmail($login)) {
            $field = 'email';
            $login = $email;
        } else {
            $field = 'login';
        }

        return static::where($field, $login)->first();
    }

    /**
     * @return $this|array|\Illuminate\Database\Eloquent\Collection
     */
    public static function myUploads()
    {
        $sessionUploads = array();

        if (\Session::has('uploads'))
            $sessionUploads = \Upload::findMany(\Session::get('uploads'));

        if ($user = \Auth::user()) {

            foreach ($sessionUploads as $sessionUpload) {
                $sessionUpload->user_id = $user->id;

                $sessionUpload->save();
            }

            \Session::forget('uploads');

            $uploads = \Upload::where('user_id', '=', $user->id)->get();
        } else {
            $uploads = $sessionUploads;
        }

        return $uploads;
    }

    /**
     * @param $name
     * @return mixed
     */
    public static function findMyUploadByName($name)
    {
        foreach (static::myUploads() as $upload) {
            if (strcmp($upload->original_name, $name) === 0)
                return $upload;
        }
    }

    /**
     * @param $id
     * @return \Upload
     */
    public static function findMyUpload($id)
    {
        foreach (static::myUploads() as $upload) {
            $id = extract_numbers($id);

            if ($upload->id == $id)
                return $upload;
        }
    }

    public static function search($filters = [])
    {
        $select = static::select()->orderBy('id', 'DESC');
        $columns = \Schema::getColumnListing(static::getModel()->getTable());
        foreach ($filters as $k => $v) {
            if ($v == '-')
                continue;

            if (in_array($k, $columns)) {
                $select->where($k, $v);
            }
        }

        return $select;
    }

    protected static $basket;

    /**
     * @return \Basket
     */
    public static function getBasket()
    {
        if (!static::$basket)
            static::$basket = \Basket::create(\BasketDbStorage::create(\Session::getId(), \Auth::user()));

        return static::$basket;
    }

    public static function lastOrder()
    {
        if ($user = \Auth::user()) {
            return $user->orders()->orderBy('id', 'desc')->first();
        }
        else if ($orderId = \Session::get('order_id')) {
            return \Order::find($orderId);
        }
    }

    public function sendEmailConfirmation()
    {
        $message = new ConfirmEmail($this);

        \Notification::send($this, $message);

//        return \Mail::send('emails.user.email_confirm', array('token' => $token), function (\Illuminate\Mail\Message $message) {
//            $message
//                ->to($this->email)
//                ->subject('Подтверждение электронной почты');
//        });
    }

    public function generateEmailToken()
    {
        return \Hash::make($this->email . $this->id);
    }

    public function checkEmailConfirmation($token)
    {
        return \Hash::check($this->email . $this->id, $token);
    }


    /**
     * БИТРИКС - Проверяем, является ли $password текущим паролем пользователя.
     *
     * @param int $userId
     * @param string $password
     *
     * @return bool
     */
    function attemptOldUserPassword($password)
    {
        $oldwpd = $this->old_password;

        $salt = substr($oldwpd, 0, (strlen($oldwpd) - 32));

        $realPassword = substr($oldwpd, -32);
        $password = md5($salt . $password);

        return ($password == $realPassword);
    }

    public function isEmailConfirmed()
    {
        return $this->email && ($this->email == $this->email_confirmed);
    }

    public function isPhoneConfirmed()
    {
        return $this->phone && ($this->phone == $this->phone_confirmed);
    }

    public function hasBonusCard()
    {
        return !!$this->bonus_card_number;
    }

    public function getBonuses()
    {
        return 941;
    }

    public static function isFavorite(\Product $product)
    {
        return Favorite::current()->where('id', $product->id)->exists();
    }

    public static function getRecent($limit = null)
    {
        return \Product::whereIn('id', \Recent::all())->get($limit);
    }

    public function getTotalBonuses()
    {
        return 841;
    }

    public function getSendableBonusesAmount()
    {
        return 555;
    }

    const SMS_PAUSE = 120;

    public function sendSmsCode(&$err)
    {
        $pause = static::SMS_PAUSE;

        if ($this->phone_confirmation_sent_at && $this->phone_confirmation_sent_at->addSeconds($pause)->getTimestamp() > time()) {
            $err = 'Подождите, прежде чем отправлять СМС';
            return false;
        }

        $code = \Sms::generateCode();
        $this->phone_confirmation_code = $code;
        $this->phone_confirmation_sent_at = (new Carbon())->setTimestamp(time());
        $this->save();

        if (!$ok = \Sms::send($this->phone, 'Код подтверждения: ' . $code)) {
            $err = 'Ошибка отправки СМС';
            return false;
        }

        return true;
    }

    public function checkSmsCode($code)
    {
//        if ($this->phone_confirmation_sent_at && $this->phone_confirmation_sent_at->addSeconds(static::SMS_PAUSE)->getTimestamp() > time()) {
//            return false;
//        }

        return $this->phone_confirmation_code && ($this->phone_confirmation_code == $code);
    }

    public function clearSmsCode()
    {
        $this->phone_confirmation_code = null;
        $this->phone_confirmation_sent_at = null;
    }

    public function confirmEmail()
    {
        $this->email_confirmed = $this->email;
        $this->clearEmailToken();

        return $this;
    }

    public function clearEmailToken()
    {
        return $this;
    }

    public function setPhoneAttribute($phone)
    {
        if ($phone) {
            $this->attributes['phone'] = filterPhoneFormat($phone);
        }
    }

    public function setPhoneConfirmedAttribute($phone)
    {
        if ($phone) {
            $this->attributes['phone_confirmed'] = filterPhoneFormat($phone);
        }
    }

    public function getRoleAttribute()
    {
        return \UserRole::find($this->role_id);
    }

    public function getEditUrl($parameters = [], $absolute = false)
    {
        return actionts('AdminUserController@update', array_replace($parameters, ['id' => $this->id]), $absolute);
    }
}
