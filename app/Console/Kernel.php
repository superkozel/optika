<?php

namespace App\Console;

use App\Console\Commands\AutomaticProductGrouping;
use App\Console\Commands\ClearOutdateFavorites;
use App\Console\Commands\DisableProductsWithoutImages;
use App\Console\Commands\ExchangeImport;
use App\Console\Commands\GenerateSitemap;
use App\Console\Commands\PVZImport;
use App\Console\Commands\UpdateProductCounts;
use App\Console\Commands\WarmupImages;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        UpdateProductCounts::class,
        GenerateSitemap::class,
        ClearOutdateFavorites::class,
        AutomaticProductGrouping::class,
        ExchangeImport::class,
        WarmupImages::class,
        PVZImport::class,
        DisableProductsWithoutImages::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('updateProductCounts')
            ->hourly();
        $schedule->command('generateSitemap')
            ->daily()->at('1:00');
        $schedule->command('ClearOutdateFavorites')
            ->daily()->at('1:30');
        $schedule->command('exchange:import')
            ->daily()->at('11:00');
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
