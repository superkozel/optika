<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 14.05.2017
 * Time: 16:09
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DisableProductsWithoutImages extends Command
{
    protected $signature = 'catalog:disableWithoutImages';

    protected $description = '';

    public function handle()
    {
        $result = \Product::whereNotIn('type_id', [\ProductType::LINZI, \ProductType::KONTAKTNIE_LINZY])->whereHas('images', function(){}, '=', '0')->update(['disabled' => 1]);
        \Product::whereNotIn('type_id', [\ProductType::LINZI, \ProductType::KONTAKTNIE_LINZY])->whereHas('images', function(){}, '>', '0')->update(['disabled' => 0]);
        dump($result);
    }
}