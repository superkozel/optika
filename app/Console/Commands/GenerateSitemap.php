<?php

namespace App\Console\Commands;
use App\Http\Controllers\CatalogController;

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 19.04.2017
 * Time: 3:24
 */
class GenerateSitemap extends \Illuminate\Console\Command
{
    protected $signature = 'generate:sitemap';

    protected $description = 'Generate sitemap.xml file';

    public function handle()
    {
        $map = new \XmlSitemapGenerator();

        $path = storage_path('sitemap.xml');
        $map->setHost(parse_url(config('app.url'), PHP_URL_HOST));
        $map->setPath($path);

        $map->start();

        $map->add(actionts('NewsController@index'), false);
        foreach (\News::all() as $news) {
            $map->add($news->getUrl());
        }

        foreach (\Page::all() as $page) {
            $map->add($page->getUrl());
        }

        $map->add(actionts('SalonController@index'), false);
        foreach (\City::has('salons')->get() as $salonCity) {
            $map->add(actionts('SalonController@index', ['city' => $salonCity->slug]), false);
        }
        foreach (\Salon::all() as $salon) {
            $map->add($salon->getUrl());
        }

        $map->add(actionts('ActionController@index'), false);
        foreach (\Action::all() as $action) {
            $map->add($action->getUrl());
        }

        foreach (\Product::where('available', '>', 0)->get() as $product) {
            $map->add($product->getUrl());
        }

        foreach (\CatalogCategory::all() as $category) {
            $map->add($category->getUrl());

            foreach ($category->filterSet->filters as $filter) {
                if ($filter->isIndexable()) {
                    foreach ($filter->getOptions() as $optionId => $option) {
                        $values = [$filter->id => [$optionId]];
                        if ($category->getProductSelector($values)->count() > 0) {
                            $map->add(CatalogController::createUrl($category, $values));
                        }
                    }
                }
            }
        }

        $map->end();

        header('Content-type: text/xml');
        rename($path, public_path('sitemap.xml'));
    }
}