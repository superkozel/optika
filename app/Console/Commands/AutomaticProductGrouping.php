<?php
namespace App\Console\Commands;

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 19.04.2017
 * Time: 2:51
 */
class AutomaticProductGrouping extends \Illuminate\Console\Command
{

    protected $signature = 'catalog:groupProducts';

    protected $description = '';


    //автоматическая группировка товаров по названию(временный хак)
    public function handle()
    {
        $groups = \DB::select(\DB::raw('
            SELECT SUBSTR(`name` FROM (INSTR(`name`, " ") + 1)) as subname, MAX(group_id) as group_id, MIN(id) as id 
            FROM products
            WHERE type_id IN (' . \ProductType::LINZI . ', ' . \ProductType::KONTAKTNIE_LINZY .')
            GROUP BY subname'));

        foreach ($groups as $row) {
            \Product::where('name', 'LIKE', '%' . $row->subname)->update(['group_id' => $row->id]);
        }
//
//        $select = \Product::whereIn('type_id', [\ProductType::OPRAVA, \ProductType::SOLNECHNIE_OCHKI])->whereNull('group_id');
//        $total = $select->count();
//        $i = 0;
//        $select->select('id')->chunk(100, function($productsIds) use (&$i, $total) {
//            echo sprintf('%s of %s' . "\n", $i * 100, $total);
//            $i++;
//
//            foreach($productsIds as $productId) {
//                $product = \Product::find($productId->id);
//                $substr = array_slice(explode(' ', $product->name), 1);
//                $substr = join(' ', $substr);
//
//                $findExistGroup = \Product::where('name', 'LIKE', '%' . $substr)->whereNotNull('group_id')->first();
//
//                $groupId = null;
//                if ($findExistGroup) {
//                    $groupId = $findExistGroup->group_id;
//                }
//                else {
//                    $groupId = $product->id;
//                }
//
//                \Product::where('name', 'LIKE', '%' . $substr)->update(['group_id' => $groupId]);
//            }
//        });
    }
}