<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 12.05.2017
 * Time: 10:52
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SendDeliverySMS extends Command
{
    protected $signature = 'sms:delivery';

    protected $description = '';

    public function handle()
    {
        foreach (\Order::whereStatusId(\OrderStatus::ACCEPTED)->get() as $order) {
            \Sms::send($order->phone, "Доставка вашего заказа завтра, с 13:00 в салоне по адресу");
        }
    }
}