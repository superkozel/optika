<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 28.04.2017
 * Time: 7:53
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;

class WarmupImages extends Command
{
    protected $signature = 'warmup:images';

    protected $description = '';

    public function handle()
    {
        $total = \Product::count();
        $i = 0;
        \Product::with('images')->chunk(100, function($products) use (&$i, $total) {
            $i++;
            echo ($i * 100) . ' of ' . $total . "\n";
            /** @var \Product[] $products */
            foreach ($products as $product){
                foreach($product->images as $img) {
                    $img->warmUp();
                }
            }
        });
    }
}