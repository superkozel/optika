<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 21.04.2017
 * Time: 21:53
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;

class UpdateProductCounts extends Command
{
    protected $signature = 'updateProductCounts';

    protected $description = 'Update product counts for brands and categories';

    public function handle()
    {
        $root = \CatalogCategory::find(1);
        $brandFilter = \CatalogFilter::findBySlug('brand');
        foreach (\Brand::all() as $brand) {
            $brand->products_count = $root->getProductSelector([$brandFilter->id => [$brand->id]])->count();
            $brand->save();
        }

        foreach (\CatalogCategory::all() as $category) {
            $category->products_count = $category->getProductSelector()->count();
            $category->save();
        }
    }
}