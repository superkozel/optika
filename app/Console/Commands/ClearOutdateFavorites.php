<?php
namespace App\Console\Commands;

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 03.03.2017
 * Time: 21:48
 */
class ClearOutdateFavorites extends \Illuminate\Console\Command
{
    protected $signature = 'favorite:clean';

    protected $description = 'Delete outdates favorites';

    //очистка избранного с датой более года
    public function handle()
    {
        \ActiveRecord\Favorite::where('created_at', '<', strtotime('year ago'));
    }
}