<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 26.04.2017
 * Time: 16:00
 */

namespace App\Console\Commands;


use App\Helpers\Common\FileLogger;
use Flow\Exchange\ExchangeParser;
use Illuminate\Console\Command;

class ExchangeImport extends Command
{
    protected $signature = 'exchange:import';

    protected $description = 'Scan exchange directory for updates';

    public function handle()
    {
        $startTime = time();

        $lastUpdate = null;

        $dir = new \DirectoryIterator(base_path('exchange'));

        $lastParse = \Cache::get('exchange_parse_date', 0);
        $lastParse = 0;
        $max = 0;
        foreach ($dir as $file) {
            if ($file->isDir() && ! $file->isDot()) {
                $timestamp = date_create_from_format('d-m-Y H-i', $file->getBasename());

                if (! $timestamp)
                    continue;

                $timestamp = $timestamp->getTimestamp();

//                if ($timestamp < $lastParse) {
//                    $this::deleteDir($file->getPathname());
//                }

                if (! $max OR $max[0] < $timestamp) {
                    $max = [$timestamp, $file->getPathname()];
                }
            }
        }

        if ($max && ($max[0] > $lastParse)) {
            echo ('scanning [' . $max[1] . "]\n");
            $this->parseExchangeDir($max[1]);

            \Cache::put('exchange_parse_date', $max[0], 180000);
        }

        \Artisan::call('catalog:groupProducts', []);
        \Artisan::call('updateProductCounts', []);
    }

    public static function deleteDir($dirPath) {
        if (! is_dir($dirPath)) {
            throw new InvalidArgumentException("$dirPath must be a directory");
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                self::deleteDir($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dirPath);
    }

    protected function parseExchangeDir($path)
    {
        $iterator = new \DirectoryIterator($path);
        $parser = new ExchangeParser();

        $latest = [];
        foreach ($iterator as $subFile) {
            if ($subFile->isFile() && $subFile->getExtension() == 'xml') {
                $names = explode('__', $subFile->getBasename())[0];
                $catalogId = (int)extract_numbers($names);
                preg_match_all("/[a-zA-Z]+/", $names, $matches);
                $importType = $matches[0][0];

                if (empty($latest[$importType])) {
                    $latest[$importType] = [];
                }
                $latest[$importType][] = $subFile->getPathname();
            }
        }

        //порядок парсинга
        $parseOrder = ['import', 'prices', 'rests'];

        foreach ($parseOrder as $importType) {
            foreach ($latest[$importType] as $fileName) {
                echo ('parsing [' . $fileName . "]\n");
                $parser->parseFile($fileName);
            }
        }
    }
}