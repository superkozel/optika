<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 06.05.2017
 * Time: 10:32
 */

namespace App\Console\Commands;


use ActiveRecord\DeliveryPoint;
use Illuminate\Console\Command;

class PVZImport extends Command
{
    protected $signature = 'import:pvz';

    protected $description = 'Обновление списка Пунктов Выдачи Заказов';

    public function handle()
    {
//        <city_code>5</city_code>
//<name>Абакан</name>
//<city_name>Абакан</city_name>
//<electronic_aut>Да</electronic_aut>
//<address>
//    655017, г. Абакан, ул. Ленинского Комсомола, д. 35, офис №76Н
//    </address>
//<tarif_zone>6</tarif_zone>
//<settlement>АБАКАН</settlement>
//<desc_pvz>
//    3
//Район Черногорского парка, перейти регулируемый пешеходный переход и в правой
//жилой пятиэтажке находится наш офис. Второй офис слева.
//</desc_pvz>
//<visa>Да</visa>
//<metro/>
//<work_time>пн-пт:08.00-20.00 сб:10.00-16.00</work_time>
//<telephone>8-800-700-54-30</telephone>
//<courier>Да</courier>
//<country>Россия</country>
//<delivery_period>8</delivery_period>
//<type/>
//<small_address>ул. Ленинского Комсомола, д. 35, офис №76Н</small_address>
//<area>ХАКАСИЯ РЕСПУБЛИКА</area>
//<only_pay>Нет</only_pay>
//<geo_X>53.722047</geo_X>
//<geo_Y>91.429928</geo_Y>
//<code>191</code>
//<delivery_date>2015-06-25</delivery_date>
//</point>
        foreach (\DostavkaGuruApi::getPVZList() as $k => $pvzData) {
            if (! $pvz = DeliveryPoint::whereExternalId($pvzData['code'])->first()) {
                $pvz = new DeliveryPoint();
            }

            $pvz->external_id = $pvzData['code'];
            $pvz->source_id = 'dostavkaguru';

            $pvz->name = $pvzData['name'];
            $pvz->city_name = $pvzData['city_name'];
            $pvz->description = $pvzData['desc_pvz'];
            $pvz->address = $pvzData['address'];
            $pvz->visa = $this->bool($pvzData['visa']);
            $pvz->metro = is_array($pvzData['metro']) ? join(',', $pvzData['metro']) : $pvzData['metro'];
            $pvz->work_time = $pvzData['work_time'];
            $pvz->phone = $pvzData['telephone'];
            $pvz->courier = $this->bool($pvzData['courier']);
            $pvz->country = $pvzData['country'];
            $pvz->delivery_period = $pvzData['delivery_period'];
            $pvz->type = $pvzData['type'];
            $pvz->small_address = is_array($pvzData['small_address']) ? '' : $pvzData['small_address'];
            $pvz->area = $pvzData['area'];
            $pvz->only_pay = $this->bool($pvzData['only_pay']);
            $pvz->coords_x = $pvzData['geo_X'];
            $pvz->coords_y = $pvzData['geo_Y'];

            dump($pvz->getAttributes());
            $pvz->save();
        }
    }

    protected function bool($val)
    {
        return $val === 'Да';
    }
}