<?php

namespace App\Notifications\Manager;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;


class NewOrder extends Notification
{
    use Queueable;

    /** @var \Order */
    protected $order;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(\Order $order)
    {
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage())
            ->subject('Новый заказ №' . $this->order->getNumber() . (! $this->order->type->is(\OrderType::NORMAL) ? '(' . $this->order->type->getName() . ')' : ''))
            ->greeting('Заказ на сумму ' . moneyFormat($this->order->sum) . ' руб.')
            ->view('emails.layouts.simpleMessage')
            ->line(view('front.order.parts.details', ['order' => $this->order]))
            ->line(view('front.order.parts.items', ['order' => $this->order, 'items' => $this->order->items])->render())
            ;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
