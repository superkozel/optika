<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 15.05.2017
 * Time: 1:27
 */

namespace App\Notifications;


class OrderDeliveryDateUpdated extends Notification
{
    use Queueable;

    protected $order;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(\Order $order)
    {
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage())
            ->subject('Доставка вашего заказа №' . $this->order->getNumber() . ' назначена на ' . $this->order->delivery_date)
            ->greeting('Менеджер назначил доставку заказа на ' . $this->order->delivery_date . ', в интервале ' . $this->order->delivery_time)
            ->view('emails.layouts.simpleMessage')
            ->line('Дата доставки согласовывается по телефону.')
            ->line('Если произошла ошибка, немедленно свяжитесь с нами по телефону ' . \Settings::get('phone'))
            ;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}