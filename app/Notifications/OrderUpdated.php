<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 05.04.2017
 * Time: 7:33
 */

namespace App\Notifications;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;


class OrderUpdated extends Notification
{
    use Queueable;

        protected $order;

        /**
         * Create a new notification instance.
         *
         * @return void
         */
        public function __construct(\Order $order)
    {
        $this->order = $order;
    }

        /**
         * Get the notification's delivery channels.
         *
         * @param  mixed  $notifiable
         * @return array
         */
        public function via($notifiable)
    {
        return ['mail'];
    }

        /**
         * Get the mail representation of the notification.
         *
         * @param  mixed  $notifiable
         * @return \Illuminate\Notifications\Messages\MailMessage
         */
        public function toMail($notifiable)
    {
        return (new MailMessage())
            ->subject('Заказ №' . $this->order->getNumber() . ' успешно изменён')
            ->greeting('Данные по вашему заказу изменены.')
            ->view('emails.layouts.simpleMessage')
            ->line('Скоро с вами свяжется наш менеджер для подтверждения данных заказа. Вы можете получить всю интересующую вас информацию по товарам и состоянии заказа по телефону.')
            ->line(view('front.order.parts.items', ['order' => $this->order, 'items' => $this->order->items])->render())
            ->action('Ваш заказ на сайте', actionts('OrderController@view', ['id' => $this->order->id, 'token' => $this->order->token]))
            ->line('Если вы получили это письмо по ошибке, просто проигнорируйте его.')
            ;
    }

        /**
         * Get the array representation of the notification.
         *
         * @param  mixed  $notifiable
         * @return array
         */
        public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
