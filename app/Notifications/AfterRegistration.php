<?php

namespace App\Notifications;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AfterRegistration extends Notification
{
    use Queueable;

    protected $user;
    public $level = 'success';

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }
    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage())
            ->replyTo('noreply@optika-favorit.ru')
            ->subject($this->user->bonus_program_active ? 'Регистрация в бонусной программе «Оптика Фаворит»' : 'Регистрация на сайте «Оптика Фаворит»')
            ->greeting($this->user->bonus_program_active ? 'Приветствуем в нашей бонусной программе' : 'Рады вас видеть на нашем сайте')
            ->line($this->user->bonus_program_active ?  'Теперь вы можете пользоваться привилегиями участника бонусной программы.' : 'Теперь вам доступны функции личного кабинета')
            ->line($this->user->bonus_program_active ? '<b style="color:red">Вам начислено ' . \Bonus::AWARD_REGISTER. ' бонусов за регистрацию.</b>' : '')
            ->line('Пожалуйста, подтвердите email, указанный в анкете.')
            ->action('Подтверждение email', actionts('UserController@confirmEmail', ['token' => $this->user->generateEmailToken()]))
            ->line($this->user->bonus_program_active ? 'Прочитать условия бонусной программы вы можете ' . \Html::link(\Page::findBySlug('bonusnaya-programma')->getUrl(), 'здесь') : '')
            ->line('Если вы получили это письмо по ошибке, просто проигнорируйте его.')
            ->view('emails.layouts.simpleMessage')
            ;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
