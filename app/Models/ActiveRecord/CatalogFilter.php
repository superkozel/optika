<?php

/**
 * Created by PhpStorm.
 * 
 * User: Superkozel
 * Date: 12.01.2017
 * Time: 21:49
 *
 * @property-read \Attribute $attribute
 * @mixin \Eloquent
 * @property int $id
 * @property int $type_id
 * @property string $slug
 * @property string $name
 * @property bool $value_type
 * @property string $value_id
 * @property string $options
 * @property bool $sort_type
 * @method static \Illuminate\Database\Query\Builder|\CatalogFilter whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\CatalogFilter whereTypeId($value)
 * @method static \Illuminate\Database\Query\Builder|\CatalogFilter whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\CatalogFilter whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\CatalogFilter whereValueType($value)
 * @method static \Illuminate\Database\Query\Builder|\CatalogFilter whereValueId($value)
 * @method static \Illuminate\Database\Query\Builder|\CatalogFilter whereOptions($value)
 * @method static \Illuminate\Database\Query\Builder|\CatalogFilter whereSortType($value)
 * @property-read mixed $sort
 * @property string $description
 * @property bool $sort_desc
 * @property-read mixed $type
 * @method static \Illuminate\Database\Query\Builder|\CatalogFilter whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\CatalogFilter whereSortDesc($value)
 */
class CatalogFilter extends ActiveRecord
{
    use ActiveRecordSlugTrait;

    public $timestamps = false;

    public function getOptions($slugId = false)
    {
        if (! $this->type->is(CatalogFilterType::CLIST, CatalogFilterType::DROPDOWN)) {
            dd('how this happened?');
        }

        $idAttribute = $slugId ? 'slug' : 'id';
        $result = [];
        if ($this->value_type->is(CatalogFilterValueType::ATTRIBUTE)) {
            $attr = Attribute::findByXmlId($this->value_id);

            if ($attr->type_id == AttributeType::SELECT) {
                foreach ($attr->options as $option) {
                    $result[$option->{$idAttribute}] = $option->name;
                }
            }
            else {
                $result = ProductAttribute::select('value')->where('attribute_id', $attr->id)->distinct()->pluck('value')->keyBy('value')->all();
            }
        }
        else if ($this->value_type->is(CatalogFilterValueType::RELATION)) {
            $product = new Product();
            $relation = $product->{$this->value_id}();

            $class = get_class($relation->getRelated());
            foreach ($class::all() as $option) {
                $result[$option->{$idAttribute}] = $option->name;
            }
        }
        else if ($this->value_type->is(CatalogFilterValueType::COLUMN)) {

        }

        if (! is_array($result)) {
            dd($this);
        }

        if ($this->sort_type) {
            $filter = $this;
            uasort($result, function($a, $b) use ($filter){
                switch ($filter->sort_type->getId()) {
                    case CatalogFilterSortType::NUM:
                        return parseFloat($a) > parseFloat($b);
                        break;
                    case CatalogFilterSortType::ALPHANUM:
                        return strcmp($a, $b);
                        break;
                }
            });
        }

        return $result;
    }

    public function setCurrentFilters($filters)
    {
        $this->currentFilters = $filters;
    }

    public function getOptionsWithCount(CatalogCategory $category, $filterValues)
    {
        unset($filterValues[$this->id]);

        $groupBy = null;

        $select = $category->getProductSelector($filterValues);

        if ($this->value_type->is(CatalogFilterValueType::ATTRIBUTE)) {
            $attr = Attribute::findByXmlId($this->value_id);

            $groupBy = 'pa.value';

            $select
                ->select([DB::raw('pa.value as value'), DB::raw('pa.product_id as id')])
                ->leftJoin(with(new ProductAttribute())->getTable() . ' as pa', DB::raw('products.id'), '=', DB::raw('pa.product_id'))
                ->where('pa.attribute_id', $attr->id)
                ->groupBy($groupBy)
            ;
        }
        else if ($this->value_type->is(CatalogFilterValueType::RELATION)) {
            $foreignKey = (with(new Product()))->getRelationForeignKey($this->value_id);

            $groupBy = $foreignKey;
            $select
                ->select([DB::raw($foreignKey . ' as value'), DB::raw('products.id as id')])
                ->groupBy($groupBy)
            ;
        }
        else if ($this->value_type->is(CatalogFilterValueType::COLUMN)) {
            $groupBy = $this->value_id;
            $select
                ->select([DB::raw($this->value_id . ' as value'), DB::raw('products.id as id')])
                ->groupBy($groupBy)
            ;

        }

        $select2 = DB::table(DB::raw("({$select->toSql()}) as sub"))
            ->select([DB::raw('sub.value as value'), DB::raw('COUNT(sub.id) as productCount')])
            ->mergeBindings($select->getQuery())
            ->orderBy('value')
            ->groupBy('value')
        ;

//        DB::enableQueryLog();
        $result = $select2->pluck('productCount', 'value')->all();
        unset($result['']);
//        dd(DB::getQueryLog());

        if ($this->value_type->is(CatalogFilterValueType::ATTRIBUTE) && $attr->type_id != AttributeType::SELECT) {
            foreach ($result as $k => $v) {
                $result[$k] = ['name' => $k, 'count' => $v];
            }
        }
        else {
            $options =  $this->getOptions();

            foreach ($result as $k => $v) {
                $result[$k] = ['name' => $options[$k], 'count' => $v];
            }
        }

        if ($this->sort_type) {
            $filter = $this;
            uasort($result, function($a, $b) use ($filter){
                switch ($filter->sort_type->getId()) {
                    case CatalogFilterSortType::NUM:
                        return parseFloat($a['name']) > parseFloat($b['name']);
                        break;
                    case CatalogFilterSortType::ALPHANUM:
                        return strcmp($a['name'], $b['name']);
                        break;
                    case CatalogFilterSortType::COUNT:
                        return $a['count'] < $b['count'];
                        break;
                    case CatalogFilterSortType::MANUAL://TODO
                        return $a['sort'] > $b['sort'];
                        break;
                }
            });
        }

        return $result;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getTypeAttribute()
    {
        return CatalogFilterType::find($this->type_id);
    }

    public function isIndexable()
    {
        if ($this->getType()->is(CatalogFilterType::CLIST)) {
            return true;
        }
        return false;
    }

    public function getValueTypeAttribute($value)
    {
        return CatalogFilterValueType::find($value);
    }

    public function getSortTypeAttribute($value)
    {
        return CatalogFilterSortType::find($value);
    }
}