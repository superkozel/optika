<?php

/**
 * Created by PhpStorm.
 * 
 * User: Superkozel
 * Date: 06.01.2017
 * Time: 13:13
 *
 * @property int $id
 * @property string $name
 * @property int $type_id
 * @property string $xml_id
 * @property string $code
 * @property bool $required
 * @property bool $multiple
 * @property string $defaultValue
 * @property string $description
 * @property string $class
 * @property int $measure_id
 * @property int $sort
 * @property bool $displayed
 * @property string $column
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\AttributeSet[] $attributeSet
 * @property-read \Illuminate\Database\Eloquent\Collection|\AttributeOption[] $options
 * @method static \Illuminate\Database\Query\Builder|\Attribute whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute whereTypeId($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute whereXmlId($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute whereRequired($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute whereMultiple($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute whereDefaultValue($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute whereClass($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute whereMeasureId($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute whereSort($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute whereDisplayed($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute whereColumn($value)
 * @property-read mixed $type
 */
class Attribute extends ActiveRecord
{
    const ID_SFERA = 'OPTICHESKAYA_SILA_SFERA';

    const ID_TSILINDR = 'CILINDR';

    use ActiveRecordSlugTrait;

    protected static function slugAttribute()
    {
        return 'code';
    }

    public static function generateSlug($name)
    {
        return str_replace('-', '_', mb_strtoupper(slug($name), 'utf-8'));
    }

    public function attributeSet()
    {
        return $this->belongsToMany(AttributeSet::class);
    }

    public function options()
    {
        return $this->hasMany(AttributeOption::class);
    }

    public function valueString($values)
    {
        if (! is_array($values)) {
            $values = [$values];
        }

        $strings = [];
        if ($this->type_id == AttributeType::BOOL) {
            $strings = array_map($values, function($v){return $v == 1 ? 'да' : 'нет';});
        }
        else if ($this->type_id == AttributeType::SELECT) {
            $strings = $this->options()->whereIn('id', $values)->get()->pluck('name')->all();
        }
        else if ($this->type_id == AttributeType::MODEL) {
            $class = $this->class;

            if ($class instanceof BaseEnum) {
                $strings = $class::getByIds($values);
            }
            else {
                $strings = $class::whereIn('id', $values)->get()->pluck('name')->all();
            }
        }
        else {
            $strings = $values;
        }

        return join(', ', $strings);
    }


    /** @return Attribute */
    public static function findByCode($code)
    {
        return static::whereCode($code)->first();
    }

    public function getOptionList()
    {
        $arr = $this->options()->pluck('name', 'id')->all();

        return $arr;
    }


    public static function findByXmlId($xmlid)
    {
        return static::whereXmlId($xmlid)->first();
    }

    public function getTypeAttribute($value) {
        return AttributeType::find($this->type_id);
    }
}