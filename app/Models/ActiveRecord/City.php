<?php

/**
 * Created by PhpStorm.
 * 
 * User: Superkozel
 * Date: 13.01.2017
 * Time: 16:23
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Salon[] $salons
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property int $delivery_price
 * @method static \Illuminate\Database\Query\Builder|\City whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\City whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\City whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\City whereDeliveryPrice($value)
 */
class City extends \Illuminate\Database\Eloquent\Model
{
    use ActiveRecordSlugTrait;

    public $timestamps = false;

    public function salons()
    {
        return $this->hasMany(Salon::class);
    }

    public function getUrl()
    {
        return actionts('SalonController@index', ['city' => $this->slug], false);
    }
}