<?php

/**
 * Created by PhpStorm.
 * 
 * User: Superkozel
 * Date: 12.01.2017
 * Time: 17:33
 *
 * @property-read \Product $product
 * @mixin \Eloquent
 * @property int $id
 * @property int $product_id
 * @property string $name
 * @property string $description
 * @property int $sort
 * @property string $path
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\ProductImage whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductImage whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductImage whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductImage whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductImage whereSort($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductImage wherePath($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductImage whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductImage whereUpdatedAt($value)
 * @property string $from
 * @method static \Illuminate\Database\Query\Builder|\ProductImage whereFrom($value)
 */
class ProductImage extends EloquentImage
{
    function product() {
        return $this->belongsTo('Product');
    }

    public $guarded = array();

    protected static $sizes = array(
        'full' => array(800),
        'large' => array(450, 355, true),
        'medium' => array(250, 250, true),
        'small' => array(100, 100, true),
        'mini' => array(50, 50, true),
    );

    public function getOwnerType()
    {
        return 'product';
    }

    public function getPropertyName()
    {
        return 'images';
    }

    public function getSavePath()
    {
        $objectType = strtolower($this->getOwnerType());

        $milliseconds = round(microtime(true) * 1000);
        return 'storage/images/' . $objectType . '/' . $this->getOwner()->id . '_' . $this->getPropertyName() . '_' . $milliseconds;
    }

    public function stub()
    {
        if ($this->getOwner() && $this->getOwner()->type->is(ProductType::LINZI, ProductType::KONTAKTNIE_LINZY)) {
            return '/images/nolensesimage.jpg';
        }
        else {
            return parent::stub();
        }
    }

}