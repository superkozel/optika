<?php

class Product extends ActiveRecord
{
    protected $guarded = [];

    public function properties()
    {
        return $this->hasMany(ProductAttribute::class);
    }

    public function images()
    {
        return $this->hasMany(ProductImage::class);
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function favorites()
    {
        return $this->hasMany(\ActiveRecord\Favorite::class);
    }

    public function getRelationships()
    {
        return [
            'brand' => 'Бренд'
        ];
    }

    public function getType()
    {
        return ProductType::find($this->type_id);
    }

    public function getTypeAttribute($value)
    {
        return ProductType::find($this->type_id);
    }

    public function setTypeAttribute(ProductType $value)
    {
        return $this->type_id = $value->getId();
    }

    public function getBasePrice($count = 1)
    {
        return $this->old_price * $count;
    }

    public function getSellingPrice($count = 1)
    {
        return $this->price
        * (($promocode = \App\User::getBasket()->getPromocode()) ? 0.9 : 1)
        * $count;
    }

    public function getPotentialPrice($count = 1)
    {
        return $this->getSellingPrice($count);
        $maxDiscount = $this->getSellingPrice($count) * 0.3;
        return $this->getSellingPrice($count) - min(1000, $maxDiscount);
    }

    public function getDiscount()
    {
        return 0;
    }

    public function getImage()
    {
        return $this->images->first();
    }

    protected $_properties = [];

    public function setProperty($attrId, $values)
    {
        if (! is_array($values))
            $values = [$values];

        $this->_properties[$attrId] = $values;
    }

    public function addProperty($attrId, $value)
    {
        if (empty($this->_properties))
            $this->_properties = [];

        $this->_properties[$attrId][] = $value;
    }

    public function getProperty($attrId, $first = false)
    {
        $properties = $this->getProperties();

        if ($attrId instanceof Attribute) {
            $attrId = $attrId->id;
        }

        if (empty($properties[$attrId]))
            return [];
        else
            return $properties[$attrId];
    }

    public function getPropertyStringByCode($code)
    {
        $attr = Attribute::findByCode($code);

        return $attr->valueString($this->getProperty($attr->id));
    }

    public function getPropertyString($attrId)
    {
        $attr = Attribute::find($attrId);

        return $attr->valueString($this->getProperty($attrId));
    }

    public function unsetProperty($attrId)
    {
        unset($this->_properties[$attrId]);
    }

    public function saveProperties()
    {
        $this->properties()->delete();

        $data = [];
        foreach ($this->_properties as $attrId => $values) {
            foreach($values as $value) {
                $data[] =  [
                    'product_id' => $this->id,
                    'attribute_id' => $attrId,
                    'value' => $value
                ];
            }
        }

        $this->properties()->insert($data);
    }

    protected function loadProperties()
    {
        foreach ($this->properties as $prop) {
            $this->_properties[$prop->attribute_id][] = $prop->value;
        }

        return $this->_properties;
    }

    public function getProperties()
    {
        if (empty($this->_properties)) {
            $this->loadProperties();
        }

        return $this->_properties;
    }

    public function getUrl($absolute = false)
    {
        return actionts('ProductController@show', ['id' => slug($this->name) . '_' . $this->id], $absolute);
    }

    public function toArray()
    {
        $data = $this->getAttributes();

        if (! $image = $this->getImage()){
            $image = new ProductImage();
        }

        $data['image'] = $image->toArray();
        $data['type'] = $this->type->name;

        return $data;
    }

    public function getBonuses(\App\User $user = null, $count = 1)
    {
        $add = 0;
        if ($this->type->is(ProductType::KONTAKTNIE_LINZY)) {
            $add += 50 * $count;
        }

        return floor($this->getSellingPrice() / 20) + $add;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection | ProductModifier[]
     */
    public function getModifiers()
    {
        return $this->type->modifiers();
    }

    public function variants()
    {
//        if ($this->group_id)
//            return Product::where('group_id', $this->group_id)->whereNot('id', $this->id);

        $substr = array_slice(explode(' ', $this->name), 1);
        $substr = join(' ', $substr);

        return Product::where('name', 'LIKE', '%' . $substr);
    }

    const AVAILABLE_THRESHOLD = 0;
    public function isAvailable()
    {
        return $this->available > static::AVAILABLE_THRESHOLD;
    }

}