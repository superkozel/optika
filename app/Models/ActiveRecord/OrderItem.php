<?php

/**
 * Created by PhpStorm.
 * 
 * User: Superkozel
 * Date: 18.02.2017
 * Time: 3:25
 *
 * @property int $id
 * @property int $order_id
 * @property int $product_id
 * @property string $name
 * @property bool $count
 * @property float $price
 * @property float $sum
 * @property string $modifiers
 * @property string $created_at
 * @property string $updated_at
 * @property-read \Order $order
 * @property-read \Product $product
 * @method static \Illuminate\Database\Query\Builder|\OrderItem whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderItem whereOrderId($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderItem whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderItem whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderItem whereCount($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderItem wherePrice($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderItem whereSum($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderItem whereModifiers($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderItem whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $bonus
 * @method static \Illuminate\Database\Query\Builder|\OrderItem whereBonus($value)
 */
class OrderItem extends ActiveRecord
{
    public $timestamps = false;

    /** @return \Illuminate\Database\Eloquent\Relations\BelongsTo */
    public function order() {
        return $this->belongsTo(Order::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function setModifiersAttribute($value)
    {
        $this->attributes['modifiers'] = json_encode($value ? $value : []);
    }

    public function getModifiersAttribute($value)
    {
        return $value ? json_decode($value, true) : [];
    }

    /**
     * @return array
     */
    public function getModifiers()
    {
        return $this->modifiers;
    }

    /**
     * @return string
     */
    public function getModifier($modifier)
    {
        return is_array($this->modifiers) ? array_get($this->modifiers, $modifier) : null;
    }

    /**
     * @return string
     */
    public function hasModifier($modifier)
    {
        return is_array($this->modifiers) ? array_has($this->modifiers, $modifier) : false;
    }

    /**
     * @param array $modifiers
     */
    public function setModifiers(array $modifiers = null)
    {
        $this->modifiers = $modifiers;

        return $this;
    }

    /**
     * @return string
     */
    public function setModifier($modifier, $value)
    {
        $modifiers = $this->modifiers;
        if (is_array($modifiers)) {
            if ($value == '' OR is_null($value)) {
                array_forget($modifiers, [$modifier]);
            }
            else {
                array_set($modifiers, $modifier, $value);
            }
        }
        else {
            $modifiers = [$modifier => $value];
        }

        $this->modifiers = $modifiers;
    }

    /**
     * кол-во дней до доставки
     */
    public function getDeliveryDays()
    {
        if ($this->getModifier(ProductModifier::MAKING)) {
            return 14;
        }

        else return 0;
    }
}