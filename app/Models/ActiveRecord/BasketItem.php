<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 15.02.2017
 * Time: 16:17
 */

namespace ActiveRecord;


/**
 * ActiveRecord\BasketItem
 *
 * @property int $id
 * @property int $basket_id
 * @property int $product_id
 * @property bool $count
 * @property float $price
 * @property string $modifiers
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \ActiveRecord\Basket $basket
 * @method static \Illuminate\Database\Query\Builder|\ActiveRecord\BasketItem whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\ActiveRecord\BasketItem whereBasketId($value)
 * @method static \Illuminate\Database\Query\Builder|\ActiveRecord\BasketItem whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\ActiveRecord\BasketItem whereCount($value)
 * @method static \Illuminate\Database\Query\Builder|\ActiveRecord\BasketItem wherePrice($value)
 * @method static \Illuminate\Database\Query\Builder|\ActiveRecord\BasketItem whereModifiers($value)
 * @method static \Illuminate\Database\Query\Builder|\ActiveRecord\BasketItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\ActiveRecord\BasketItem whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $parent_id
 * @method static \Illuminate\Database\Query\Builder|\ActiveRecord\BasketItem whereParentId($value)
 */
class BasketItem extends \ActiveRecord
{
    public function basket()
    {
        return $this->belongsTo(Basket::class);
    }
}