<?php

/**
 * Created by PhpStorm.
 * 
 * User: Superkozel
 * Date: 12.01.2017
 * Time: 17:46
 *
 * @property-read \News $news
 * @mixin \Eloquent
 * @property int $id
 * @property int $news_id
 * @property string $name
 * @property string $description
 * @property int $sort
 * @property string $path
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\NewsImage whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\NewsImage whereNewsId($value)
 * @method static \Illuminate\Database\Query\Builder|\NewsImage whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\NewsImage whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\NewsImage whereSort($value)
 * @method static \Illuminate\Database\Query\Builder|\NewsImage wherePath($value)
 * @method static \Illuminate\Database\Query\Builder|\NewsImage whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\NewsImage whereUpdatedAt($value)
 * @property string $from
 * @method static \Illuminate\Database\Query\Builder|\NewsImage whereFrom($value)
 */
class NewsImage extends EloquentImage
{
    public function news()
    {
        return $this->belongsTo(News::class);
    }

    public $guarded = array();

    protected static $sizes = array(
        'full' => array(800),
		'large' => array(450, 355),
        'medium' => array(250, 250),
        'small' => array(100, 100),
        'mini' => array(69, 69),
    );

    public function getOwnerType()
    {
        return 'news';
    }

    public function getPropertyName()
    {
        return 'image';
    }

    public function getSavePath()
    {
        $objectType = strtolower($this->getOwnerType());

        $milliseconds = round(microtime(true) * 1000);
        return 'storage/images/' . $objectType . '/' . $this->getOwner()->id . '_' . $this->getPropertyName() . '_' . $milliseconds;
    }
}