<?php

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 07.02.2017
 * Time: 17:41
 */
class CatalogFilterValueType extends BaseEnum
{
    const ATTRIBUTE = 1;
    const COLUMN = 2;
    const RELATION = 3;

    public static $names = [
        CatalogFilterValueType::ATTRIBUTE => 'атрибут',
        CatalogFilterValueType::COLUMN => 'поле таблицы',
        CatalogFilterValueType::RELATION => 'связь таблицы',
    ];
}