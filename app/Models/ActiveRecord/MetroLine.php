<?php

/**
 * Created by PhpStorm.
 * 
 * User: Superkozel
 * Date: 14.01.2017
 * Time: 0:00
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $number
 * @property int $city_id
 * @property string $name
 * @property string $color
 * @property string $slug
 * @method static \Illuminate\Database\Query\Builder|\MetroLine whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\MetroLine whereNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\MetroLine whereCityId($value)
 * @method static \Illuminate\Database\Query\Builder|\MetroLine whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\MetroLine whereColor($value)
 * @method static \Illuminate\Database\Query\Builder|\MetroLine whereSlug($value)
 */
class MetroLine extends \Illuminate\Database\Eloquent\Model
{
    public $timestamps = false;
}