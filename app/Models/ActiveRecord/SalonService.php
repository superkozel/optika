<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 13.04.2017
 * Time: 12:02
 */

namespace ActiveRecord;

/**
 * ActiveRecord\SalonService
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string $description
 * @method static \Illuminate\Database\Query\Builder|\ActiveRecord\SalonService whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\ActiveRecord\SalonService whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\ActiveRecord\SalonService whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\ActiveRecord\SalonService whereSlug($value)
 * @mixin \Eloquent
 */
class SalonService extends \ActiveRecord
{
    use \ActiveRecordSlugTrait;

    public $timestamps = false;

    public function salons()
    {
        return \Salon::where('service_ids', 'LIKE', '%' . $this->id . '%');
    }

    public function getName()
    {
        return $this->name;
    }

    public function getId()
    {
        return $this->id;
    }

    public static function calculateIzgotovlenie(\BasketItem $basketItem) {
        $product = $basketItem->getProduct();
        if ($product->type->is(\ProductType::OPRAVA)) {
            $sellingPrice = $basketItem->getSellingPriceWithLenses();

            if ($sellingPrice >= 100000) {
                $servicePrice = 5000;
            }
            else if ($sellingPrice >= 50000) {
                $servicePrice = 4000;
            }
            else {
                $konstrukciya = $product->getProperty(\Attribute::findByCode('KONSTRUKTSIYA'));

                $konstrukciya = array_get($konstrukciya, '0');

                switch ($konstrukciya) {
                    case 'a0f3f2b2-dea7-11e0-a19e-f46d0405f555':
                    case 'a0f3f295-dea7-11e0-a19e-f46d0405f555':
                    case '4896bce6-43e5-11e4-bb02-0025902c4fba':
                        $priceRanges = [
                            '-2999' => 650,
                            '3000-5999' => 950,
                            '6000-9999' => 1500,
                            '10000-14999' => 2000,
                            '15000-29999' => 2500,
                            '30000-' => 3500
                        ];
                        break;

                    case 'e793e41d-deae-11e0-a19e-f46d0405f555':
                    case '144531f3-deaf-11e0-a19e-f46d0405f555':
                    default:
                        $priceRanges = [
                            '-1499' => 200,
                            '1500-2999' => 400,
                            '3000-4499' => 600,
                            '4500-7999' => 1000,
                            '8000-14999' => 1200,
                            '15000-29999' => 2000,
                            '30000-' => 3000,
                        ];
                        break;
                }
            }

            $sellingPrice = floor($sellingPrice);
            foreach ($priceRanges as $range => $curprice) {
                list($from, $to) = explode('-', $range);

                if ((! $from OR $from <= $sellingPrice) && (! $to OR $to >= $sellingPrice)) {
                    $servicePrice = $curprice;
                }
            }

            return $servicePrice;
        }
    }

    public function getUrl()
    {
        return actionts('ServiceController@view', ['id' => $this->slug]);
    }
}