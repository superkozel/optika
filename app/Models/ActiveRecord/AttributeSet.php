<?php

/**
 * Created by PhpStorm.
 * 
 * User: Superkozel
 * Date: 06.01.2017
 * Time: 13:13
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Attribute[] $attributes
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property int $product_type_id
 * @method static \Illuminate\Database\Query\Builder|\AttributeSet whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\AttributeSet whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\AttributeSet whereProductTypeId($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Attribute[] $attrs
 * @property-read mixed $product_type
 */
class AttributeSet extends ActiveRecord
{
    public function attrs()
    {
        return $this->belongsToMany(Attribute::class);
    }
    public function getProductTypeAttribute()
    {
        return ProductType::find($this->product_type_id);
    }

    public $timestamps = false;
}