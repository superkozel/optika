<?php

/**
 * Created by PhpStorm.
 * 
 * User: Superkozel
 * Date: 06.01.2017
 * Time: 13:13
 *
 * @property-read \Product $product
 * @mixin \Eloquent
 * @property int $product_id
 * @property int $attribute_id
 * @property string $value
 * @method static \Illuminate\Database\Query\Builder|\ProductAttribute whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductAttribute whereAttributeId($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductAttribute whereValue($value)
 */
class ProductAttribute extends \Illuminate\Database\Eloquent\Model
{
    public $table = 'products_attributes';

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}