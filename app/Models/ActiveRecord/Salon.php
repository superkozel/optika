<?php

/**
 * Created by PhpStorm.
 * 
 * User: Superkozel
 * Date: 01.01.2017
 * Time: 5:16
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\SalonImage[] $images
 * @property-read \City $city
 * @property-read \Illuminate\Database\Eloquent\Collection|\MetroStation[] $metros
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $code
 * @property string $slug
 * @property string $mode
 * @property string $address
 * @property string $description
 * @property string $service_ids
 * @property string $phone
 * @property int $specialization_id
 * @property int $city_id
 * @property string $coordinates
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Salon whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Salon whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Salon whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Salon whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\Salon whereMode($value)
 * @method static \Illuminate\Database\Query\Builder|\Salon whereAddress($value)
 * @method static \Illuminate\Database\Query\Builder|\Salon whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Salon whereServiceIds($value)
 * @method static \Illuminate\Database\Query\Builder|\Salon wherePhone($value)
 * @method static \Illuminate\Database\Query\Builder|\Salon whereSpecializationId($value)
 * @method static \Illuminate\Database\Query\Builder|\Salon whereCityId($value)
 * @method static \Illuminate\Database\Query\Builder|\Salon whereCoordinates($value)
 * @method static \Illuminate\Database\Query\Builder|\Salon whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Salon whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Salon whereUpdatedAt($value)
 * @property-read mixed $services
 */
use Illuminate\Database\Eloquent\Model;

class Salon extends ActiveRecord
{
    use ActiveRecordSlugTrait;

    public function images()
    {
        return $this->hasMany(SalonImage::class);
    }

    public function getSpecialization()
    {
        return SalonSpecialization::find($this->specialization_id);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function metros()
    {
        return $this->belongsToMany(MetroStation::class);
    }

    public function getUrl()
    {
        return actionts('SalonController@show', ['id' => $this->slug], false);
    }

    public function getServicesAttribute()
    {
        return $this->service_ids ? \ActiveRecord\SalonService::whereIn('id', explode(',', $this->service_ids))->get()->all() : [];
    }
}