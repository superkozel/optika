<?php

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 29.07.2017
 * Time: 21:12
 */
class Banner extends ActiveRecord
{
    public $timestamps = false;

    /**
     * @return CategoryImage
     */
    public function getImageAttribute($value)
    {
        if (!$value)
            return;

        $img = new BannerImage($this);
        return $img
            ->setPath($value)
            ;

    }

    protected $dates = [
        'active_to',
    ];

    public function getPositionAttribute()
    {
        return BannerPosition::find($this->position_id);
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function img($size = null, $attrs = [])
    {
        return BannerImage::img($this->image, $size, $this->name, $attrs);
    }

    public static function getByPosition($position)
    {
        return static::where('position_id', $position)->where('active_to', '<', DB::raw('NOW()'));
    }
}