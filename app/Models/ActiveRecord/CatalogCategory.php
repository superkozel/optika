<?php

/**
 * Created by PhpStorm.
 * 
 * User: Superkozel
 * Date: 09.01.2017
 * Time: 12:44
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property int $parent_id
 * @property string $description
 * @property string $image
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\CatalogCategory whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\CatalogCategory whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\CatalogCategory whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\CatalogCategory whereParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\CatalogCategory whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\CatalogCategory whereImage($value)
 * @method static \Illuminate\Database\Query\Builder|\CatalogCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\CatalogCategory whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property bool $pseudo
 * @property int $filter_set_id
 * @property-read \CatalogFilterSet $filterSet
 * @property-read \CatalogCategory $parent
 * @property-read \Illuminate\Database\Eloquent\Collection|\CatalogCategory[] $children
 * @property-read \Illuminate\Database\Eloquent\Collection|\CategoryPredefinedValue[] $predefinedValues
 * @method static \Illuminate\Database\Query\Builder|\CatalogCategory wherePseudo($value)
 * @method static \Illuminate\Database\Query\Builder|\CatalogCategory whereFilterSetId($value)
 * @property string $h1
 * @property string $title
 * @method static \Illuminate\Database\Query\Builder|\CatalogCategory whereH1($value)
 * @method static \Illuminate\Database\Query\Builder|\CatalogCategory whereTitle($value)
 * @property string $name_rod
 * @property string $seotext
 * @property string $metadesc
 * @method static \Illuminate\Database\Query\Builder|\CatalogCategory whereNameRod($value)
 * @method static \Illuminate\Database\Query\Builder|\CatalogCategory whereSeotext($value)
 * @method static \Illuminate\Database\Query\Builder|\CatalogCategory whereMetadesc($value)
 * @property bool $visible
 * @method static \Illuminate\Database\Query\Builder|\CatalogCategory whereVisible($value)
 * @property int $products_count
 * @method static \Illuminate\Database\Query\Builder|\CatalogCategory whereProductsCount($value)
 */
class CatalogCategory extends ActiveRecord
{
    use ActiveRecordSlugTrait;
    use TreeRecordTrait;

    public $timestamps = false;
    protected $table = 'catalog_categories';

    public function filterSet()
    {
        return $this->belongsTo(CatalogFilterSet::class, 'filter_set_id');
    }

    public function parent()
    {
        return $this->belongsTo(CatalogCategory::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(CatalogCategory::class, 'parent_id');
    }

    public function predefinedValues()
    {
        return $this->hasMany(CategoryPredefinedValue::class, 'category_id');
    }

    public function getUrl()
    {
        $url = $this->slug . '/';

        $curCat = $this;
        while ($curCat->parent_id) {
            if ($curCat->parent_id == 1) {
                $url = 'catalog/' . $url;
                break;
            }
            else {
                $curCat = $curCat->parent;
                $url = $curCat->slug . '/' . $url;
            }
        }

        return '/' . $url;
    }

    public function getProducts($filters = [])
    {
//        return Product::whereIn('id', $this->getProductSelector($filters)->pluck('id'));//медленный вариант
//        return $this->getProductSelector($filters);//вариант без группировки
        $sub = $this->getProductSelector($filters);
        return Product::from(\DB::raw("({$sub->select('*')->toSql()}) as sub"))
            ->mergeBindings($sub->getQuery()) // you need to get underlying Query Builder
            ;
    }

    public function getProductSelector($filters = [])
    {
        $predefinedValues = [];
        foreach ($this->predefinedValues as $predef) {
            if (empty($predefinedValues[$predef->filter_id]))
                $predefinedValues[$predef->filter_id] = [];

            $predefinedValues[$predef->filter_id][] = $predef->value;
        }

        $filters = array_replace($filters, $predefinedValues);

        $query = Product::select();
//        $query = \Product::select(DB::raw('MAX(group_id) as group_id, MAX(id) as id'));
        $query->groupBy(\DB::raw('IF(products.group_id IS NULL, products.id, products.group_id)'));

        $query->where('available', '>', 0);
        $query->where('price', '>', 0);
        $query->where('disabled', '=', 0);

        $useFilters = true;

        if ($useFilters) {
            foreach ($filters as $filterId => $values) {
                $filter = CatalogFilter::find($filterId);

                if (! $filter) {
                    throw new Exception('Unknown filter with id=[' . $filterId . ']');
                }

                if ($filter->type->is(CatalogFilterType::CLIST, CatalogFilterType::DROPDOWN)) {
                    if ($filter->value_type->is(CatalogFilterValueType::COLUMN)) {
                        $query->whereIn($filter->value_id, $values);
                    }
                    else if ($filter->value_type->is(CatalogFilterValueType::RELATION)) {
                        $key = Product::getModel()->getRelationForeignKey($filter->value_id);

                        $query->whereIn($key, $values);
                    }
                    else if ($filter->value_type->is(CatalogFilterValueType::ATTRIBUTE)) {
                        $attr = Attribute::findByXmlId($filter->value_id);
                        $attrConditions[$attr->id] = $values;
                    }
//SELECT DISTINCT product_id, attribute_id, COUNT(attribute_id) as attrCount
//
//FROM products_attributes
//WHERE (attribute_id = 9 AND value IN (182))
//OR (attribute_id = 10 AND value IN (187))
//GROUP BY product_id

                }
                else if ($filter->type_id == CatalogFilterType::RANGE){
                    list($from, $to) = explode('-', $values);

                    if ($from)
                        $query->where($filter->value_id, '>=' , $from);

                    if ($to)
                        $query->where($filter->value_id, '<=' , $to);

                    if ($to OR $from) {
                        $query->whereNotNull($filter->value_id);
                        $query->where($filter->value_id, '!=', '');
                    }
                }
            }

            if (! empty($attrConditions)) {
                $query->whereIn('id', function($query) use ($attrConditions) {
                    $query->select(['product_id'])
                        ->from(with(new ProductAttribute())->getTable())
                        ->groupBy('product_id')
                        ->distinct()
                        ->havingRaw('COUNT(attribute_id) >= ' . count($attrConditions))
                    ;

                    foreach ($attrConditions as $k => $vv) {
                        $query->orWhere(function($query) use ($k, $vv) {
                            $query->where('attribute_id', $k)
                                ->whereIn('value', $vv);
                        });
                    };
                });
            }
        }
        else {
            $query->where('category_id', $this->id);
        }

        return $query;
    }

    public function getProductCount()
    {
        return $this->products_count ? $this->products_count : $this->getProductSelector()->count();
    }

    /**
     * @return CategoryImage
     */
    public function getImageAttribute($value)
    {
        if (!$value)
            return;

        $img = new CategoryImage($this);
        return $img
            ->setPath($value)
            ;

    }
}