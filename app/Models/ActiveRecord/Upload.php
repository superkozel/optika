<?php
/**
 * Upload
 *
 * @author Зыков Дмитрий <aesleep@ya.ru>
 * @date 21.05.2015
 * @mixin \Eloquent
 * @property int $id
 * @property string $original_name
 * @property string $extension
 * @property string $size
 * @property int $user_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @method static \Illuminate\Database\Query\Builder|\Upload whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Upload whereOriginalName($value)
 * @method static \Illuminate\Database\Query\Builder|\Upload whereExtension($value)
 * @method static \Illuminate\Database\Query\Builder|\Upload whereSize($value)
 * @method static \Illuminate\Database\Query\Builder|\Upload whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Upload whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Upload whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Upload whereDeletedAt($value)
 */

class Upload extends Eloquent
{
	public function getPath()
	{
		return $this->id . '.' . $this->extension;
	}

	public function getSavePath()
	{
		return storage_path('uploads/' . $this->getPath());
	}

	public static function createFromUploadedFile(\Symfony\Component\HttpFoundation\File\UploadedFile $file)
	{
		$upload = new static;

		$upload->original_name = $file->getClientOriginalName();
		$upload->extension = $file->getClientOriginalExtension();
		$upload->size = $file->getClientSize();

		$upload->save();

		$file->move(storage_path('uploads'), $upload->getPath());
		chmod($upload->getSavePath(), 0777);

		return $upload;
	}

	public static function createFromUrl($url)
	{
		//урл должен ответить за одну секунду,
		//должен быть правильным mimeType

		//потом сделать реальную проверку, что это картинка
		$upload = new static;

		$url = file_url($url);
//		$headers = get_headers($url, 1);
		$string = file_get_contents($url);

		$upload->original_name = $url;
		$upload->extension = pathinfo($url, PATHINFO_EXTENSION);
		$upload->size = strlen($string);

		$upload->save();

		file_put_contents(storage_path('uploads/' . $upload->getPath()), $string);
		chmod($upload->getSavePath(), 0777);

		return $upload;
	}

	public function thumb()
	{
		$path = $this->getSavePath();

		$ip = new \Intervention\Image\ImageManagerStatic();

		$image = $ip->make($path);
		$image->fit(80, 80);

		return $image
			->encode('data-url')
			->getEncoded();
	}

//	public function makeFromUrl($url)
//	{
//		$url = file_url();
//	}

	public function delete()
	{
		$ok = unlink($this->path);

		if ($ok)
			return parent::delete();
		else
			throw new Exception('Unable to delete uploaded file');
	}
}