<?php

/**
 * Created by PhpStorm.
 * 
 * User: Superkozel
 * Date: 13.01.2017
 * Time: 23:59
 *
 * @property-read \MetroLine $line
 * @mixin \Eloquent
 * @property int $id
 * @property int $city_id
 * @property bool $line_id
 * @property string $name
 * @property string $slug
 * @property string $coordinates
 * @method static \Illuminate\Database\Query\Builder|\MetroStation whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\MetroStation whereCityId($value)
 * @method static \Illuminate\Database\Query\Builder|\MetroStation whereLineId($value)
 * @method static \Illuminate\Database\Query\Builder|\MetroStation whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\MetroStation whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\MetroStation whereCoordinates($value)
 */
class MetroStation extends \Illuminate\Database\Eloquent\Model
{
    public $timestamps = false;

    public function line()
    {
        return $this->belongsTo(MetroLine::class);
    }
}