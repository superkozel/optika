<?php

/**
 * Created by PhpStorm.
 * 
 * User: Superkozel
 * Date: 12.01.2017
 * Time: 17:46
 *
 * @property-read \Salon $salon
 * @mixin \Eloquent
 * @property int $id
 * @property int $salon_id
 * @property string $name
 * @property string $description
 * @property int $sort
 * @property string $path
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\SalonImage whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\SalonImage whereSalonId($value)
 * @method static \Illuminate\Database\Query\Builder|\SalonImage whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\SalonImage whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\SalonImage whereSort($value)
 * @method static \Illuminate\Database\Query\Builder|\SalonImage wherePath($value)
 * @method static \Illuminate\Database\Query\Builder|\SalonImage whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\SalonImage whereUpdatedAt($value)
 * @property string $from
 * @method static \Illuminate\Database\Query\Builder|\SalonImage whereFrom($value)
 */
class SalonImage extends EloquentImage
{
    public function salon()
    {
        return $this->belongsTo(Salon::class);
    }

    public $guarded = array();

    protected static $sizes = array(
        'full' => array(800),
		'large' => array(450, 355),
        'medium' => array(250, 250),
        'small' => array(100, 100),
        'mini' => array(69, 69),
    );

    public function getOwnerType()
    {
        return 'salon';
    }

    public function getPropertyName()
    {
        return 'images';
    }

    public function getSavePath()
    {
        $objectType = strtolower($this->getOwnerType());

        $milliseconds = round(microtime(true) * 1000);
        return 'storage/images/' . $objectType . '/' . $this->getOwner()->id . '_' . $this->getPropertyName() . '_' . $milliseconds;
    }
}