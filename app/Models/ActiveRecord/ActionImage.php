<?php

/**
 * Created by PhpStorm.
 * 
 * User: Superkozel
 * Date: 16.01.2017
 * Time: 1:43
 *
 * @property int $id
 * @property int $action_id
 * @property string $name
 * @property string $description
 * @property int $sort
 * @property string $path
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Action $action
 * @method static \Illuminate\Database\Query\Builder|\ActionImage whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\ActionImage whereActionId($value)
 * @method static \Illuminate\Database\Query\Builder|\ActionImage whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\ActionImage whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\ActionImage whereSort($value)
 * @method static \Illuminate\Database\Query\Builder|\ActionImage wherePath($value)
 * @method static \Illuminate\Database\Query\Builder|\ActionImage whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\ActionImage whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $from
 * @method static \Illuminate\Database\Query\Builder|\ActionImage whereFrom($value)
 */
class ActionImage extends EloquentImage
{
    public function action()
    {
        return $this->belongsTo(Action::class);
    }

    public $guarded = array();

    protected static $sizes = array(
        'full' => array(800),
        'medium' => array(300, null),
        'large' => array(450, null),
        'small' => array(220, null, true),
        'mini' => array(50, 50),
    );

    public function getOwnerType()
    {
        return 'action';
    }

    public function getPropertyName()
    {
        return 'image';
    }

    public function getSavePath()
    {
        $objectType = strtolower($this->getOwnerType());

        return 'storage/images/' . $objectType . '/' . $this->getOwner()->id . '_' . $this->getPropertyName();
    }
}