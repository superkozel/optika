<?php

/**
 * Created by PhpStorm.
 * 
 * User: Superkozel
 * Date: 09.02.2017
 * Time: 10:06
 *
 * @property int $category_id
 * @property int $filter_id
 * @property string $value
 * @method static \Illuminate\Database\Query\Builder|\CategoryPredefinedValue whereCategoryId($value)
 * @method static \Illuminate\Database\Query\Builder|\CategoryPredefinedValue whereFilterId($value)
 * @method static \Illuminate\Database\Query\Builder|\CategoryPredefinedValue whereValue($value)
 * @mixin \Eloquent
 */
class CategoryPredefinedValue extends \Illuminate\Database\Eloquent\Model
{
    public $timestamps = false;

}