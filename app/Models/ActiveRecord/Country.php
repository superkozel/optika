<?php

/**
 * Created by PhpStorm.
 * 
 * User: Superkozel
 * Date: 06.02.2017
 * Time: 11:02
 *
 * @property int $id
 * @property string $name
 * @property string $xml_id
 * @property string $alpha2
 * @property string $pattern
 * @property string $pattern_z
 * @property string $pattern_s
 * @property string $slug
 * @method static \Illuminate\Database\Query\Builder|\Country whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Country whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Country whereXmlId($value)
 * @method static \Illuminate\Database\Query\Builder|\Country whereAlpha2($value)
 * @method static \Illuminate\Database\Query\Builder|\Country wherePattern($value)
 * @method static \Illuminate\Database\Query\Builder|\Country wherePatternZ($value)
 * @method static \Illuminate\Database\Query\Builder|\Country wherePatternS($value)
 * @method static \Illuminate\Database\Query\Builder|\Country whereSlug($value)
 * @mixin \Eloquent
 */
class Country extends \Illuminate\Database\Eloquent\Model
{
    use MorphologyPatternTrait;
    use ActiveRecordSlugTrait;

    public $table = 'countries';
    public $timestamps = false;


}