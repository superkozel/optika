<?php

/**
 * Created by PhpStorm.
 * 
 * User: Superkozel
 * Date: 16.01.2017
 * Time: 1:19
 *
 * @property int $id
 * @property string $name
 * @property string $preview_text
 * @property string $content
 * @property \Carbon\Carbon $active_from
 * @property \Carbon\Carbon $active_to
 * @property string $slug
 * @property \Carbon\Carbon $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Action whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Action whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Action wherePreviewText($value)
 * @method static \Illuminate\Database\Query\Builder|\Action whereContent($value)
 * @method static \Illuminate\Database\Query\Builder|\Action whereActiveFrom($value)
 * @method static \Illuminate\Database\Query\Builder|\Action whereActiveTo($value)
 * @method static \Illuminate\Database\Query\Builder|\Action whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\Action whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Action whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Action whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \ActionImage $image
 * @property-read \Illuminate\Database\Eloquent\Collection|\Salon[] $salons
 */
class Action extends ActiveRecord
{

    use \Illuminate\Database\Eloquent\SoftDeletes;
    use ActiveRecordSlugTrait;

    public function image()
    {
        return $this->hasOne(ActionImage::class);
    }

    public function salons()
    {
        return $this->belongsToMany(Salon::class);
    }

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'active_from',
        'active_to',
    ];

    public function getUrl()
    {
        return actionts('ActionController@show', ['id' => $this->slug], false);
    }

    public function isActive()
    {
        $now = \Carbon\Carbon::createFromFormat('d.m.Y', date('d.m.Y'))->subDays(1);
        return $this->active_to->gte($now);
    }
}