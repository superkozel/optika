<?php

/**
 * Created by PhpStorm.
 *
 * User: Superkozel
 * Date: 12.02.2017
 * Time: 14:35
 *
 * @property int $id
 * @property int $order_id
 * @property bool $type_id
 * @property bool $status_id
 * @property bool $delivery_type_id
 * @property bool $paid
 * @property string $transaction_id
 * @property bool $courier_id
 * @property float $sum
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $additional_info
 * @property string $address
 * @property string $manager_comment
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Query\Builder|\Order whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereOrderId($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereTypeId($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereStatusId($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereDeliveryTypeId($value)
 * @method static \Illuminate\Database\Query\Builder|\Order wherePaid($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereTransactionId($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereCourierId($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereSum($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\Order wherePhone($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereAdditionalInfo($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereAddress($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereManagerComment($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $promocode
 * @property int $salon_id
 * @property string $comment
 * @property bool $payment_type_id
 * @property int $user_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\OrderItem[] $items
 * @property mixed $payment_type
 * @property-read mixed $status
 * @method static \Illuminate\Database\Query\Builder|\Order wherePromocode($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereSalonId($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereComment($value)
 * @method static \Illuminate\Database\Query\Builder|\Order wherePaymentTypeId($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereUserId($value)
 * @property float $paid_sum
 * @property float $payment_required_sum
 * @property int $bonus
 * @property string $gift_certificate
 * @property string $delivery_date
 * @property string $delivery_time
 * @property-read \Salon $salon
 * @property-read DeliveryType $delivery_type
 * @method static \Illuminate\Database\Query\Builder|\Order wherePaidSum($value)
 * @method static \Illuminate\Database\Query\Builder|\Order wherePaymentRequiredSum($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereBonus($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereGiftCertificate($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereDeliveryDate($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereDeliveryTime($value)
 * @property int $bonuses_used
 * @property-read mixed $type
 * @method static \Illuminate\Database\Query\Builder|\Order whereBonusesUsed($value)
 * @property int $manager_id
 * @property string $reason Причина отказа
 * @property string $token
 * @property-read \App\User $manager
 * @method static \Illuminate\Database\Query\Builder|\Order whereManagerId($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereReason($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereToken($value)
 */
class Order extends ActiveRecord
{
    protected $guarded = array();

    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }

    public function manager()
    {
        return $this->belongsTo(\App\User::class, 'manager_id');
    }

    /** @return \Illuminate\Database\Eloquent\Relations\HasMany */
    public function items()
    {
        return $this->hasMany(OrderItem::class);
    }

    public function salon()
    {
        return $this->belongsTo(Salon::class);
    }

    public function transactions()
    {
        return $this->hasMany(\ActiveRecord\PaymentTransaction::class);
    }

    public function getStatus()
    {
        return OrderStatus::find($this->status_id);
    }

    public function getStatusAttribute($value)
    {
        return OrderStatus::find($this->status_id);
    }

    public function getTypeAttribute($value)
    {
        return OrderType::find($this->type_id);
    }

    public static function boot()
    {
        static::creating(function($order){
            $order->token = $order->generateEmailToken();
        });

        parent::boot();
    }

    protected function generateEmailToken()
    {
        return md5($this->id . 'salt');
    }

    public function isPaymentRequired()
    {
        return $this->payment_required_sum > 0 && ($this->payment_required_sum > $this->paid_sum);
    }

    public function getRemainingPayment()
    {
        return $this->payment_required_sum - $this->paid_sum;
    }

    public function sendEmails() {
        $order = $this;
        try {
            $ok = Mail::send('emails.order.notify', array('order' => $this), function(\Illuminate\Mail\Message $message) use ($order)
            {
                $message
                    ->from(Config::get('mail.robot'), 'Робот ' . Config::get('contacts.site'))
                    ->to(Config::get('mail.notify'))
                    ->subject('Новый заказ №' . $order->getNumber() . ' - ' . Config::get('contacts.site'))
                ;

                if ($order->email)
                    $message->replyTo($order->email);
            });

//                if ($ok) {
//                    $order->notified = true;
//                    $order->save();
//                }

            if ($order->email) {
                Mail::send('emails.order.confirm', array('order' => $this), function(\Illuminate\Mail\Message $message) use ($order)
                {
                    $message
                        ->from(Config::get('mail.robot'), 'Интернет-магазин ' . Config::get('contacts.site'))
                        ->replyTo(Config::get('contacts.email'), 'Интернет-магазин ' . Config::get('contacts.site'))
                        ->to($order->email)
                        ->subject(Config::get('contacts.site') . ' - ваш заказ №' . $order->getNumber());
                });
            }
        }
        catch (Exception $e) {
            Log::error($e);
        }
    }

    /** @return Order */
    public static function makeFromBasket(Basket $basket, $data = [])
    {
        if ($basket->getOrderId()) {
            $order = Order::find($basket->getOrderId());
        }
        else {
            $order = new Order();
        }

        $order->fill($data);

        $items = array();

        foreach ($basket->getContents() as $basketItem) {
            $items[] = $order->createOrderItem($basketItem);
        }

        $order->promocode = $basket->getPromocode();

        $order->status_id = \OrderStatus::NEWS;

        $fitting = count($basket->getFittings());
        if ($fitting) {
            $order->payment_type_id = \Enum\PaymentType::KURIERU;
            $order->delivery_type_id = \DeliveryType::SAMOVYVOZ;
            $order->type_id = \OrderType::FITTING;
        }
        else {
            $order->type_id = \OrderType::NORMAL;
        }

        $order->sum = $basket->getTotalPrice();
//        $order->sum = 6;
        $order->bonus = $order->calculateTotalBonus();
        $order->sum -= (int)$order->bonuses_used;

        $deliveryPrice = $order->calculateDeliveryPrice();

        $order->delivery_price = $deliveryPrice;
        if (is_null($deliveryPrice)) {//неизвестная сумма доставки, нужно уточнить оператору
            $order->payment_allowed = 0;
        }
        else {
            $order->payment_allowed = 1;
            $order->sum += $deliveryPrice;
        }

        $order->payment_required_sum = ($order->payment_type && $order->payment_type->is(\Enum\PaymentType::KARTOY, \Enum\PaymentType::ONLINE)) ? $order->sum : $basket->calculateRequiredPaymentSum();

        return [$order, $items];
    }

    public function getTotalPrice()
    {
        return $this->sum;
    }

    public function calculateDeliveryPrice()
    {
        if ($this->getDeliveryType())
            return $this->getDeliveryType()->getOrderDeliveryPrice($this);
    }

    public function getNumber()
    {
        return self::numberFromId($this->id);
    }

    public static function numberFromId($id)
    {
        return $id;
    }

    /**
     * @return DeliveryType
     * @throws Exception
     */
    public function getDeliveryType()
    {
        if ($this->delivery_type_id)
            return DeliveryType::find($this->delivery_type_id);
    }

    public function getType()
    {
        if ($this->type_id) {
            return OrderType::find($this->type_id);
        }
        else
            return OrderType::find(OrderType::NORMAL);
    }

    public function getPaymentTypeAttribute()
    {
        if ($this->payment_type_id) {
            return \Enum\PaymentType::find($this->payment_type_id);
        }
    }

    public function setPaymentTypeAttribute($value)
    {
        $this->payment_type_id = $value->id;
    }

    public function getDeliveryTypeAttribute()
    {
        if ($this->delivery_type_id) {
            return DeliveryType::find($this->delivery_type_id);
        }
    }

    public function isEditable()
    {
        return $this->status->is(OrderStatus::NEWS);
    }

    public function validator($data)
    {
        $available = DeliveryType::availableForOrder($this);

        $ids = array();
        foreach ($available as $deliveryType) {
            $ids[] = $deliveryType->getId();
        }

        $deliveryString = join(',', $ids);
        $paymentString = join(',', array_keys(\Enum\PaymentType::getNames()));
        $rules = array(
            'name' => 'required|min:3',
            'phone' => 'required|min:10|phone',
            'email' => 'required|email',
            'payment_type_id' => ($this->sum > 0 ? 'required|' : '') . 'in:' . $paymentString,
            'delivery_type_id' => 'required|in:'.$deliveryString,
            'delivery_date' => 'date',
            'delivery_time' => '',
            'address' => 'required_if:delivery_type_id,' . DeliveryType::DOSTAVKA . ',' . DeliveryType::DOSTAVKA2 . '|min:10',
            'salon_id' => 'required_if:delivery_type_id,' . DeliveryType::SAMOVYVOZ . '|exists:salons,id',
            'manager_comment' => '',
            'status' => 'in:' . join(',', array_keys(OrderStatus::getNames())),
            'manager_id' => '',
            'payment_allowed' => ''
        );

        $v =  Validator::make(
            $data,
            $rules,
            array(
                'sum.min' => 'Минимальная сумма заказа с доставкой ' . Config::get('contacts.minOrder') . '  руб.',
                'payment_type_id.in' => 'Неверно указан способ оплаты',
                'delivery_type_id.in' => 'Неверно указан способ доставки',
                'personal_accept.required' => 'Необходимо согласие на передачу и обработку персональных данных',
            )
        );

        $niceNames = array(
            'name' => 'Имя',
            'phone' => 'Телефон',
            'email' => 'email',
            'address' => 'Адрес',
            'contacts' => 'Контакты',
            'delivery_type_id' => 'Способ доставки',
            'payment_type_id' => 'Способ оплаты',
            'comment' => 'Примечание к заказу',
            'manager_comment' => 'Комментарий менеджера',
        );
        $v->setAttributeNames($niceNames);

        $v->sometimes('sum', 'required|numeric|min:' . Config::get('contacts.minOrder'), function($input){
            return (! is_null($input->delivery) && $input->delivery != DeliveryType::SAMOVYVOZ);
        });

        return $v;
    }

    public function createOrderItem(BasketItem $basketItem) {
        $item = new OrderItem;
        $product = $basketItem->getProduct();

        OrderItem::unguard();
        $item->fill([
            'product_id' => $basketItem->getProductId(),
            'name' => $product->name,
            'count' => $basketItem->getCount(),
            'price' => $basketItem->getSellingPrice(),
//            'base_price' => $basketItem->getPrice(),
            'sum' => $basketItem->getSellingPrice($basketItem->getCount()),
            'bonus' => $product->getBonuses(Auth::user(), $basketItem->getCount()),
            'modifiers' => $basketItem->getModifiers(),
        ]);

        return $item;
    }

    public function calculateTotalBonus()
    {
        $bonus = 0;
        foreach ($this->items as $item) {
            $bonus += 6;
        }
        return $bonus;
    }

    /**
     * Сколько дней нужно на комплектовку заказа
     * @return int
     */
    public function calculateDeliveryDays()
    {
        $days = 1;

        foreach ($this->items as $item) {
            $days = min($days, $item->getDeliveryDays());
        }

        return $days;
    }

    public function calculateProductsSum()
    {
        $sum = 0;
        foreach ($this->items as $item) {
            $sum += $item->sum;
        }
        return $sum;
    }

    public function setPhoneAttribute($phone)
    {
        if ($phone) {
            $this->attributes['phone'] = filterPhoneFormat($phone);
        }
    }
}