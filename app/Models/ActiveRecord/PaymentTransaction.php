<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 25.04.2017
 * Time: 1:30
 */

namespace ActiveRecord;
use \Enum\PaymentTransactionStatus;

/**
 * ActiveRecord\PaymentTransaction
 *
 * @property int $id
 * @property int $order_id
 * @property string $transaction_id
 * @property string $payment_system_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property bool $status_id
 * @property float $sum
 * @property-read \Order $order
 * @method static \Illuminate\Database\Query\Builder|\ActiveRecord\PaymentTransaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\ActiveRecord\PaymentTransaction whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\ActiveRecord\PaymentTransaction whereOrderId($value)
 * @method static \Illuminate\Database\Query\Builder|\ActiveRecord\PaymentTransaction wherePaymentSystemId($value)
 * @method static \Illuminate\Database\Query\Builder|\ActiveRecord\PaymentTransaction whereStatusId($value)
 * @method static \Illuminate\Database\Query\Builder|\ActiveRecord\PaymentTransaction whereSum($value)
 * @method static \Illuminate\Database\Query\Builder|\ActiveRecord\PaymentTransaction whereTransactionId($value)
 * @method static \Illuminate\Database\Query\Builder|\ActiveRecord\PaymentTransaction whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PaymentTransaction extends \ActiveRecord
{
    public $timestamps = [
        'created_at',
        'updated_at',
        'paid_at'
    ];

    public function order()
    {
        return $this->belongsTo(\Order::class);
    }

    public function getStatusAttribute()
    {
        return PaymentTransactionStatus::find($this->status_id);
    }
}