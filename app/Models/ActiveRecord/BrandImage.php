<?php

/**
 * BrandImage
 *
 * @property int $id
 * @property int $brand_id
 * @property string $path
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Brand $brand
 * @method static \Illuminate\Database\Query\Builder|\BrandImage whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\BrandImage whereBrandId($value)
 * @method static \Illuminate\Database\Query\Builder|\BrandImage wherePath($value)
 * @method static \Illuminate\Database\Query\Builder|\BrandImage whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\BrandImage whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $from
 * @method static \Illuminate\Database\Query\Builder|\BrandImage whereFrom($value)
 */
class BrandImage extends EloquentImage
{
    function brand() {
        return $this->belongsTo('Brand');
    }

    public $guarded = array();

    protected static $sizes = array(
        'full' => array(800),
        'large' => array(450, 355, true),
        'medium' => array(250, 250, true),
        'small' => array(100, 100, true),
        'mini' => array(69, 69, true),
    );

    public function getOwnerType()
    {
        return 'brand';
    }

    public function getPropertyName()
    {
        return 'images';
    }

    public function getSavePath()
    {
        $objectType = strtolower($this->getOwnerType());

        $milliseconds = round(microtime(true) * 1000);
        return 'storage/images/' . $objectType . '/' . $this->getOwner()->id . '_' . $this->getPropertyName() . '_' . $milliseconds;
    }
}