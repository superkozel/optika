<?php

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 20.03.2017
 * Time: 20:29
 */
namespace ActiveRecord;
/**
 * ActiveRecord\Fitting
 *
 * @property int $user_id
 * @property int $product_id
 * @property string $session_id
 * @method static \Illuminate\Database\Query\Builder|\ActiveRecord\Fitting whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\ActiveRecord\Fitting whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\ActiveRecord\Fitting whereSessionId($value)
 * @mixin \Eloquent
 */
class Fitting extends \ActiveRecord
{
    public static function current()
    {
        $query = static::select();
        if (\Auth::user()) {
            $query->where('user_id', \Auth::user()->id);
        }
        else {
            $query->where('session_id', \Session::getId());
        }

        return $query;
    }
}