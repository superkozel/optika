<?php

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 03.03.2017
 * Time: 21:42
 */
namespace ActiveRecord;

use App\User;

/**
 * ActiveRecord\Favorite
 *
 * @property int $user_id
 * @property int $product_id
 * @property string $session_id
 * @property-read \Product $products
 * @method static \Illuminate\Database\Query\Builder|\ActiveRecord\Favorite whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\ActiveRecord\Favorite whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\ActiveRecord\Favorite whereSessionId($value)
 * @mixin \Eloquent
 */
class Favorite extends \ActiveRecord
{
    public $timestamps = false;

    public function products()
    {
        return $this->belongsTo(\Product::class);
    }

    public static function current()
    {
        $query = \Product::whereHas('favorites', function($query){
            if (\Auth::user()) {
                $query->where('user_id', \Auth::user()->id);
            }
            else {
                $query->where('session_id', \Session::getId());
            }
        });

        return $query;
    }

    public static function getForUser(User $user = null) {
        $query = \Product::whereHas('favorites', function($query){
            $query->where('user_id', $user->id);
        });

        return $query;
    }
}