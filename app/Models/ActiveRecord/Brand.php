<?php

/**
 * Created by PhpStorm.
 * 
 * User: Superkozel
 * Date: 09.01.2017
 * Time: 21:03
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $code
 * @property string $xml_id
 * @property string $name
 * @property string $alt_name
 * @property string $slug
 * @property string $description
 * @property string $short_description
 * @property int $product_count
 * @property string $logo
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Brand whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand whereXmlId($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand whereAltName($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand whereShortDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand whereProductCount($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand whereLogo($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand whereUpdatedAt($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\BrandImage[] $images
 * @property int $sort
 * @method static \Illuminate\Database\Query\Builder|\Brand whereSort($value)
 * @property-read \BrandImage $image
 * @property-read \Illuminate\Database\Eloquent\Collection|\Product[] $products
 * @property string $sections_ids
 * @property int $products_count
 * @method static \Illuminate\Database\Query\Builder|\Brand whereProductsCount($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand whereSectionsIds($value)
 */
class Brand extends ActiveRecord
{
    use ActiveRecordSlugTrait;

    public function applyPattern($word, $rod = 'm')
    {

        if ($word instanceof \Morphy\MorphyResult) {
            foreach ($word->getData() as $padezh => &$string) {
                $word->setForm($padezh, $string . ' ' . $this->name);
            }

            return $word;
        }
        else {
            return  $word . ' ' . $this->name;
        }
    }

    public $timestamps = false;

    public function image()
    {
        return $this->hasOne('BrandImage');
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function getImage()
    {
        return $this->image;
    }

    public function getUrl()
    {
        return \App\Http\Controllers\CatalogController::createUrl(CatalogCategory::find(1), [CatalogFilter::findBySlug('brand')->id => $this->id]);
    }
}