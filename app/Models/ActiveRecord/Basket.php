<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 15.02.2017
 * Time: 2:50
 */

namespace ActiveRecord;

/**
 * ActiveRecord\Basket
 *
 * @property int $id
 * @property int $user_id
 * @property string $session_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $promocode
 * @property-read \Illuminate\Database\Eloquent\Collection|\ActiveRecord\BasketItem[] $items
 * @method static \Illuminate\Database\Query\Builder|\ActiveRecord\Basket whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\ActiveRecord\Basket whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\ActiveRecord\Basket whereSessionId($value)
 * @method static \Illuminate\Database\Query\Builder|\ActiveRecord\Basket whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\ActiveRecord\Basket whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\ActiveRecord\Basket wherePromocode($value)
 * @mixin \Eloquent
 * @property int $order_id
 * @property string $gift_certificate
 * @property-read \Order $order
 * @method static \Illuminate\Database\Query\Builder|\ActiveRecord\Basket whereOrderId($value)
 * @method static \Illuminate\Database\Query\Builder|\ActiveRecord\Basket whereGiftCertificate($value)
 */
class Basket extends \ActiveRecord
{
    public function items()
    {
        return $this->hasMany(BasketItem::class);
    }

    public function order()
    {
        return $this->belongsTo(\Order::class);
    }
}