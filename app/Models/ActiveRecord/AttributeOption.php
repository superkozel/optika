<?php

/**
 * Created by PhpStorm.
 * 
 * User: Superkozel
 * Date: 07.01.2017
 * Time: 10:18
 *
 * @mixin \Eloquent
 * @property int $id
 * @property int $xml_id
 * @property int $1c_id
 * @property string $name
 * @property int $attribute_id
 * @property string $slug
 * @property string $pattern
 * @property string $pattern_z
 * @property string $pattern_o
 * @property int $sort
 * @method static \Illuminate\Database\Query\Builder|\AttributeOption whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\AttributeOption whereXmlId($value)
 * @method static \Illuminate\Database\Query\Builder|\AttributeOption where1cId($value)
 * @method static \Illuminate\Database\Query\Builder|\AttributeOption whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\AttributeOption whereAttributeId($value)
 * @method static \Illuminate\Database\Query\Builder|\AttributeOption whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\AttributeOption wherePattern($value)
 * @method static \Illuminate\Database\Query\Builder|\AttributeOption wherePatternZ($value)
 * @method static \Illuminate\Database\Query\Builder|\AttributeOption wherePatternO($value)
 * @method static \Illuminate\Database\Query\Builder|\AttributeOption whereSort($value)
 * @property string $description
 * @method static \Illuminate\Database\Query\Builder|\AttributeOption whereDescription($value)
 * @property string $pattern_s
 * @method static \Illuminate\Database\Query\Builder|\AttributeOption wherePatternS($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\ProductAttribute[] $productValues
 */
class AttributeOption extends ActiveRecord
{
    use ActiveRecordSlugTrait;
    use MorphologyPatternTrait;
    public $timestamps = false;

    public function productValues()
    {
        return $this->hasMany(ProductAttribute::class, 'value')->where('attribute_id', $this->attribute_id);
    }

    public static function generateSlug($name)
    {
        return preg_replace("/[-]{1,}/i", '-', preg_replace("/[^_+.0-9a-z]/i", "-", str_replace(',', '.', mb_strtolower(rus2lat(trim($name)), 'utf-8'))));
    }
}