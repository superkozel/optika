<?php

/**
 * Created by PhpStorm.
 * 
 * User: Superkozel
 * Date: 15.01.2017
 * Time: 22:35
 *
 * @property int $id
 * @property string $slug
 * @property string $path
 * @property int $parent_id
 * @property string $title
 * @property string $h1
 * @property string $metadesc
 * @property string $keywords
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Page $parent
 * @property-read \Illuminate\Database\Eloquent\Collection|\Page[] $children
 * @method static \Illuminate\Database\Query\Builder|\Page whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Page whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\Page wherePath($value)
 * @method static \Illuminate\Database\Query\Builder|\Page whereParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\Page whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\Page whereH1($value)
 * @method static \Illuminate\Database\Query\Builder|\Page whereMetadesc($value)
 * @method static \Illuminate\Database\Query\Builder|\Page whereKeywords($value)
 * @method static \Illuminate\Database\Query\Builder|\Page whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Page whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Page whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $layout
 * @method static \Illuminate\Database\Query\Builder|\Page whereLayout($value)
 */
class Page extends ActiveRecord
{
    public function parent()
    {
        return $this->belongsTo(Page::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(Page::class, 'parent_id');
    }

    use ActiveRecordSlugTrait;

    public function fullPath($path)
    {
        return resource_path('/views/front/pages/' . $path);
    }

    public function getUrl()
    {
        $url = '/' . $this->slug;
        if ($this->parent_id) {
            $url = rtrim($this->parent->getUrl(), '/') . $url;
        }

        return $url . '/';
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getH1()
    {
        return $this->h1;
    }


    public function getName()
    {
        return $this->getTitle();
    }

    public function getMetaDescription()
    {
        return $this->metadesc;
    }

    public function getBreadcrumbs()
    {
        if ($this->parent) {
            $breadcrumbs = $this->parent->getBreadcrumbs();
        }
        else {
            $breadcrumbs = Breadcrumbs::create();
        }

        return $breadcrumbs->add($this->getH1(), $this->getUrl());
    }

    public function getContent()
    {
        return view('front.pages.' . $this->path, ['page' => $this])->render();
    }

    public static function url($id)
    {
        $page = Page::find($id);

        return $page->getUrl();
    }

    public function isCurrent()
    {
        return Request::getRequestUri() == $this->getUrl();
    }

    public function getFullPath() {
        if (! $this->path) {
            $this->path = strtolower(slug($this->h1));

            if (strlen($this->path) == 0) {
                throw new Exception('path generation error for h1=[' . $this->h1.']');
            }
        }
        return resource_path('views/front/pages/' . $this->path . '.php');
    }

    public function setContentAttribute($value)
    {
        return file_put_contents($this->getFullPath(), $value);
    }

    public static function boot() {
        static::deleting(function(Page $page) {
            if (is_file($page->getFullPath())){
                unlink($page->getFullPath());
            }
        });
    }

}