<?php

/**
 * Created by PhpStorm.
 * 
 * User: Superkozel
 * Date: 28.02.2017
 * Time: 3:13
 *
 * @property int $id
 * @property int $product_type_id
 * @property string $name
 * @property bool $type_id
 * @property string $value_id
 * @property string $filters
 * @property string $sort
 * @property-read mixed $type
 * @method static \Illuminate\Database\Query\Builder|\ProductModifier whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductModifier whereProductTypeId($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductModifier whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductModifier whereTypeId($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductModifier whereValueId($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductModifier whereFilters($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductModifier whereSort($value)
 * @mixin \Eloquent
 */
class ProductModifier extends ActiveRecord
{
    const LENS_LEFT = 'lens_id_left';
    const LENS_RIGHT = 'lens_id_right';
    const FITTING = 'fitting';
    const MAKING = 'making';
    const TONING = 'toning';

    public $incrementing = false;
    public $timestamps = false;

    public function getTypeAttribute()
    {
        return ProductType::create($this->type_id);
    }

    public function setFiltersAttribute($value)
    {
        $this->attributes['filters'] = json_encode($value);
    }


    public function getFiltersAttribute($value)
    {
        return $value ? json_decode($value, true) : [];
    }

    public function getOptions(Product $product) {
        if ($this->id == 'toning') {
            return [
                ''
            ];
        }
    }

    public function getPrice(BasketItem $basketItem, $value)
    {
        switch ($this->id)
        {
            case (static::LENS_LEFT):
            case (static::LENS_RIGHT):
                return Product::find($value)->getSellingPrice();
                break;
            case (static::TONING):
                if ($value == 1) {
                    return 210;
                }
                else if ($value == 2) {
                    return 250;
                }
                break;
            case (static::MAKING):
                if ($value == 1)
                    return \ActiveRecord\SalonService::calculateIzgotovlenie($basketItem);
                break;
        }

        return null;
    }
}