<?php

/**
 * Created by PhpStorm.
 * 
 * User: Superkozel
 * Date: 12.01.2017
 * Time: 21:48
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\CatalogFilter[] $filters
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @method static \Illuminate\Database\Query\Builder|\CatalogFilterSet whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\CatalogFilterSet whereName($value)
 */
class CatalogFilterSet extends ActiveRecord
{
    public $timestamps = false;

    public function filters()
    {
        return $this->belongsToMany(CatalogFilter::class);
    }

}