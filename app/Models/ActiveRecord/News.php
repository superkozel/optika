<?php

/**
 * Created by PhpStorm.
 * 
 * User: Superkozel
 * Date: 15.01.2017
 * Time: 16:11
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $code
 * @property string $slug
 * @property string $active_from
 * @property string $active_to
 * @property string $preview_text
 * @property string $content
 * @property int $category_id
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\NewsImage[] $images
 * @method static \Illuminate\Database\Query\Builder|\News whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\News whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\News whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\News whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\News whereActiveFrom($value)
 * @method static \Illuminate\Database\Query\Builder|\News whereActiveTo($value)
 * @method static \Illuminate\Database\Query\Builder|\News wherePreviewText($value)
 * @method static \Illuminate\Database\Query\Builder|\News whereContent($value)
 * @method static \Illuminate\Database\Query\Builder|\News whereCategoryId($value)
 * @method static \Illuminate\Database\Query\Builder|\News whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\News whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\News whereUpdatedAt($value)
 * @property-read \NewsImage $image
 * @property-read mixed $category
 */
class News extends ActiveRecord
{
    use ActiveRecordSlugTrait;

    public $table = 'news';

    public function image()
    {
        return $this->hasOne(NewsImage::class);
    }

    public function getUrl()
    {
        return actionts('NewsController@show', ['id' => $this->slug], false);
    }

    public function getCategoryAttribute()
    {
        return NewsCategory::find($this->category_id);
    }
}