<?php

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 30.07.2017
 * Time: 3:09
 */
class BannerImage extends BaseImage
{
    public static $sizes = array(
        'mini' => array(50, 50, true),
    );

    public function getOwnerType()
    {
        return 'banner';
    }

    public function getSize($size = null)
    {
//        dd($this->getOwner());

        if ($size) {
            return static::$sizes[$size];
        }
    }

    public function getSizePath($size)
    {
        $sizeOpts = $this->getSize($size);
        return 'i/resize/' . strtolower($this->getOwnerType()) . '/' . $this->getOwner()->id . '_' . array_get($sizeOpts, 0) . 'x' .  array_get($sizeOpts, 1) . (! empty($sizeOpts2) ? 'f' : '');
    }
}