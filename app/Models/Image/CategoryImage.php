<?php

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 07.03.2017
 * Time: 11:06
 */
class CategoryImage extends BaseImage
{
    public static $sizes = array(
        'mini' => array(50, 50, true),
        'small' => array(150, 150, true),
    );

    public function getOwnerType()
    {
        return 'category';
    }
}