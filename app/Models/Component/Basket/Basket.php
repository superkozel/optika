<?php
/**
 * User: superk
 * Date: 24.09.14
 * Time: 19:19
 */

class Basket
{
    protected $storage;

    /** @var BasketItem[] */

    protected $contents = array();
    protected $promocode = array();
    protected $order_id;

    public static function create($storage)
    {
        $basket = new static;

        if ($storage)
            $basket->setStorage($storage);

        return $basket;
    }

    function add(Product $product, $count = 1, $modifiers = null)
    {
        if ($count == 0)
            return;

        $basketItem = $this->getBasketItem($product, $modifiers);
        if ($basketItem) {
            $basketItem->setCount($basketItem->getCount() + $count);

            return $basketItem;
        }
        else {
            $basketItem = BasketItem::create($this)
                ->setProductId($product->id)
                ->setCount($count)
                ->setModifiers($modifiers)
                ;

            $this->contents[] = $basketItem;
        }

        return $basketItem;
    }

    function getBasketItemById($id)
    {
        if (! $id)
            return;

        foreach ($this->contents as $basketItem) {
            if ($basketItem->getId() == $id) {
                return $basketItem;
            }
        }
    }

    function getBasketItem($productId, $modifiers)
    {
        if (is_object($productId))
            $productId = $productId->id;

        foreach ($this->contents as &$basketItem) {
            if (
                $basketItem->getProductId() == $productId
                && ((! $modifiers && ! $basketItem->getModifiers()) || ($basketItem->getModifiers() == $modifiers))
            ) {
                return $basketItem;
            }
        }
    }

    function update($basketItemId, $count, $modifiers = null)
    {
        $basketItem = $this->getBasketItemById($basketItemId);
        if ($basketItem) {
            $basketItem->setCount($count);
            if (! is_null($modifiers)) {
                $basketItem->setModifiers($modifiers);
            }
        }

        return $this;
    }


    public function remove($id)
    {
        if ($id instanceof BasketItem)
            $id = $id->getId();

        foreach ($this->contents as $k => $basketItem) {
            if ($basketItem->getId() == $id) {
                unset($this->contents[$k]);
                break;
            }
        }

        return $this;
    }

    public function clear()
    {
        $this->contents = array();

        return $this;
    }

    /** Сброс корзины */
    public function reset()
    {
        $this->promocode = null;
        $this->clear();
        $this->order_id = null;

        return $this;
    }

    function toArray()
    {
        $result =  [
            'sum' => $this->getTotalPrice(),
            'prepayment' => $this->calculateRequiredPaymentSum(),
            'count' => $this->getTotalCount(),
            'contents' => []
        ];

        foreach ($this->contents as $basketItem) {
            $result['contents'][] = $basketItem->toArray();
        }

        return $result;
    }

    /**
     * @param array $contents
     * @return $this
     */
    public function setContents($contents)
    {
        $this->contents = $contents;

        return $this;
    }

    /**
     * @return BasketItem[]
     */
    public function getContents()
    {
        return $this->contents;
    }

    public function getContentsCount()
    {
        return count($this->contents);
    }

    public function has($item)
    {
        return isset($this->contents[$item->id]);
    }

//    public function getCount(Product $item)
//    {
//        if (isset($this->contents[$item->id]))
//            return $this->contents[$item->id];
//        else
//            return 0;
//    }

    public function save()
    {
        $this->getStorage()->save($this);

        return $this;
    }

    /**
     * @param $storage
     */
    public function setStorage($storage)
    {
        $this->storage = $storage;

        $storage->load($this);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStorage()
    {
        return $this->storage;
    }

    /**
     * @param $id
     * @return Item
     */
    public function getItem($id)
    {
        return Product::find($id);
    }

    public function getTotalPrice()
    {
        $total = 0;

        foreach ($this->getContents() as $item) {
            $total += $item->getSellingPrice($item->getCount());
        }

        return $total;
    }

    public function getTotalCount()
    {
        $total = 0;

        foreach ($this->getContents() as $item) {
            $total += $item->getCount();
        }

        return $total;
    }

    public function getMaxUsedBonuses()
    {
        return 666;
    }


    public function isFitting()
    {
        $fittings = [];
        foreach ($this->getContents() as $item) {
            if ($item->getModifier(ProductModifier::FITTING))
                $fittings[] = $item->toArray();
        }

        return $fittings;
    }

    public function getFittings()
    {
        $fittings = [];
        foreach ($this->getContents() as $item) {
            if ($item->getModifier(ProductModifier::FITTING))
                $fittings[] = $item->toArray();
        }

        return $fittings;
    }

    public function getMakings()
    {
        $makings = [];
        foreach ($this->getContents() as $item) {
            if ($item->getModifier(ProductModifier::MAKING))
                $makings[] = $item;
        }

        return $makings;
    }

    public function getPurchases()
    {
        $purchases = [];
        foreach ($this->getContents() as $item) {
            if (! $item->getModifier(ProductModifier::FITTING))
                $purchases[] = $item;
        }

        return $purchases;
    }


    /**
     * @return array
     */
    public function getPromocode()
    {
        return $this->promocode;
    }

    /**
     * @param array $promocode
     */
    public function setPromocode($promocode)
    {
        $this->promocode = $promocode;

        return $this;
    }

    public function getItemCount(Product $product)
    {
        $total = 0;
        foreach ($this->getContents() as $basketItem) {
            if ($basketItem->getProductId() == $product->id) {
                $total += $basketItem->getTotalCount();
            }
        }
        return $total;
    }

    /**
     * @return mixed
     */
    public function getOrderId()
    {
        return $this->order_id;
    }

    /**
     * @param mixed $order_id
     */
    public function setOrderId($order_id)
    {
        $this->order_id = $order_id;

        return $this;
    }

    /**
     * @return Order $order
     */
    public function getOrder()
    {
        return Order::find($this->getOrderId());
    }

    public function loadOrder(Order $order)
    {
        $this
            ->reset()
            ->loadOrderContents($order)
            ->setPromocode($order->promocode)
            ->setOrderId($order->id)
        ;
        return $this;
    }

    public function loadOrderContents(Order $order) {
        $items = [];

        foreach ($order->items as $orderItem) {
            $basketItem = new BasketItem();
            $basketItem->setModifiers($orderItem->getModifiers());
            $basketItem->setProductId($orderItem->product_id);
            $basketItem->setCount($orderItem->count);
            $items[] = $basketItem;
        }

        $this->setContents($items);

        return $this;
    }

    protected $userId;

    public function setUser(\App\User $user)
    {
        $this->userId = $user->id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    public function calculateRequiredPaymentSum()
    {
        //предоплата стоимость линз+ изготовление;
        $required = 0;

        foreach ($this->getContents() as $item) {

            if ($item->getModifier(ProductModifier::MAKING)) {
                $required += $item->getSellingPrice($item->getCount());
            }
        }

        return $required;
    }
}