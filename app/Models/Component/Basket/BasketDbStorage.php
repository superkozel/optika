<?php

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 15.02.2017
 * Time: 2:26
 */
class BasketDbStorage
{
    /** @var \ActiveRecord\Basket */
    protected $record;

    public static function create($sessionId, \App\User $user = null)
    {
        $record = null;

        if ($user) {
            $record = \ActiveRecord\Basket::whereUserId(Auth::user()->id)->with(['items', 'order'])->first();
        }

        if (! $record) {
            $record = \ActiveRecord\Basket::whereSessionId($sessionId)->with(['items', 'order'])->first();
        }

        if (! $record) {
            $record = new \ActiveRecord\Basket();
        }

        if ($user)
            $record->user_id = $user->id;

        if (! $record->session_id)
            $record->session_id = $sessionId;

        $storage = new static;

        $storage->record = $record;

        return $storage;
    }


    /**
     * @return array
     */
    function load(Basket $basket)
    {
        $data = [];
        $basket->setUserId($this->record->user_id);
        $basket->setPromocode($this->record->promocode);
        $basket->setOrderId($this->record->order_id);

        foreach ($this->record->items as $basketItem) {
            $data[] = BasketItem::create($basket)
                ->setId($basketItem->id)
                ->setProductId($basketItem->product_id)
                ->setCount($basketItem->count)
                ->setPrice($basketItem->price)
                ->setModifiers($basketItem->modifiers ? json_decode($basketItem->modifiers, true) : null)
            ;
        }

        $basket->setContents($data);

        return true;
    }

    /**
     * @param array $data
     */
    function save(Basket $basket)
    {
        $basketRow = $this->record;

        DB::transaction(function() use ($basket, $basketRow){
            if (! $basketRow->id)
                $basketRow->save();

            $basketRow->order_id = $basket->getOrderId();
            $basketRow->user_id = $basket->getUserId();

            if ($basket->getUserId()) {
                \ActiveRecord\Basket::where('user_id', $basket->getUserId())->where('id', '!=', $basketRow->id)->delete();
            }

            $basketRow->promocode = $basket->getPromocode();

            $notDeleteIds = [];
            foreach ($basket->getContents() as $basketItemData) {
                $this->saveItem($basketItemData, $notDeleteIds);
            }

            $basketRow->items()->whereNotIn('id', $notDeleteIds)->delete();

            $basketRow->save();
        });
    }

    function saveItem(BasketItem $basketItemData, &$notDeleteIds)
    {
        $basketRow = $this->record;

        if ($basketItemData->getId()) {
            foreach ($basketRow->items as $bi) {
                if ($bi->id == $basketItemData->getId()) {
                    $basketItem = $bi;
                    break;
                }
            }
        }
        else
            $basketItem = new \ActiveRecord\BasketItem();

        $basketItem->product_id = $basketItemData->getProductId();
        $basketItem->count = $basketItemData->getCount();
        $basketItem->price = $basketItemData->getPrice();

        $modifiers = $basketItemData->getModifiers();
        if ($modifiers)
            $basketItem->modifiers = json_encode($modifiers);
        else {
            $basketItem->modifiers = null;
        }

        $basketRow->items()->save($basketItem);

        $basketItemData->setId($basketItem->id);
        $notDeleteIds[] = $basketItem->id;

        return $basketItemData;
    }
}