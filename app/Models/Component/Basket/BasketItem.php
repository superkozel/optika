<?php

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 14.03.2017
 * Time: 15:54
 */
class BasketItem
{
    /** @var integer */
    protected $id;
    /** @var integer */
    protected $productId;
    /** @var integer */
    protected $count;
    /** @var float */
    protected $price;
    /** @var array  */
    protected $modifiers;
    /** @var Basket */
    protected $basket;
    /** @var Product */
    protected $product;

    public static function create(Basket $basket)
    {
        $item = new static();
        $item->basket = $basket;
        return $item;
    }

    /**
     * @return array
     */
    public function getModifiers()
    {
        return $this->modifiers;
    }

    /**
     * @return string
     */
    public function getModifier($modifier)
    {
        return is_array($this->modifiers) ? array_get($this->modifiers, $modifier) : null;
    }

    /**
     * @param array $modifiers
     */
    public function setModifiers(array $modifiers = null)
    {
        $this->modifiers = $modifiers;

        return $this;
    }

    /**
     * @return string
     */
    public function setModifier($modifier, $value)
    {
        if (is_array($this->modifiers)) {
            if ($value == '' OR is_null($value)) {
                array_forget($this->modifiers, [$modifier]);
            }
            else {
                array_set($this->modifiers, $modifier, $value);
            }
        }
        else {
            $this->modifiers = [$modifier => $value];
        }
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    public function getSellingPrice($count = 1, $withoutModifiers = false)
    {
        if ($this->getModifier(ProductModifier::FITTING))
            return 0;

        $price =  $this->price ? $this->price : $this->getProduct()->getSellingPrice();

        if (! $withoutModifiers) {
            $product = $this->getProduct();
            foreach ($product->getModifiers()->get() as $modifier) {
                if ($value = $this->getModifier($modifier->id)) {
                    $price += $modifier->getPrice($this, $value);
                }
            }
        }

        return $price * $count;
    }

    public function getSellingPriceWithLenses($count = 1)
    {
        $sellingPrice = $this->getSellingPrice($count, true);

        if ($leftLensId = $this->getModifier(ProductModifier::LENS_LEFT)) {
            $sellingPrice += Product::find($leftLensId)->getSellingPrice(true);
        }

        if ($rightLensId = $this->getModifier(ProductModifier::LENS_RIGHT)) {
            $sellingPrice += Product::find($rightLensId)->getSellingPrice(true);
        }

        return $sellingPrice;
    }

    /**
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param int $count
     */
    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        if (! $this->product)
            $this->product = Product::find($this->productId);

        return $this->product;
    }

    /**
     * @param int $productId
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;

        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return Basket
     */
    public function getBasket()
    {
        return $this->basket;
    }

//    function getChildById($id)
//    {
//        foreach ($this->contents as $basketItem) {
//            if ($basketItem->getId() == $id && $basketItem->getParentId() == $this->getId()) {
//                return $basketItem;
//            }
//        }
//    }
//
//
//    function getChild($productId, $modifiers = null)
//    {
//        return $this->getBasket()->getBasketItem($productId, $modifiers, $this->getId());
//    }

    function getChildren()
    {
        $childModifiers = [ProductModifier::LENS_RIGHT, ProductModifier::LENS_LEFT];
        $children = [];
        foreach ($childModifiers as $value) {
            if ($this->getModifier($value))
                $children[] = Product::find($this->getModifier($value));
        }

        return $children;
    }

    function getTotalCount()
    {
        return count(1 + count($this->getChildren())) * $this->getCount();
    }

    public function toArray()
    {
        $content = array_except(get_object_vars($this), ['parent', 'basket']);

        /** @var Product $product */
        $product = $this->getProduct();
        $content['count'] = $this->getCount();
        $content['product_count'] = $this->getBasket()->getItemCount($product);
        $content['price'] = $this->getSellingPrice();
        $content['bonus'] = $product->getBonuses(Auth::user());
        $content['product'] = $product->toArray();

        return $content;
    }

    public function add(Product $product, $count = 1, $modifiers = null) {
        return $this->getBasket()->add($product, $count, $modifiers, $this);
    }

    /**
     * кол-во дней до доставки
     */
    public function getDeliveryDays()
    {
        if ($this->getModifier(ProductModifier::MAKING)) {
            return 14;
        }

        else return 0;
    }
}