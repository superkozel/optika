<?php

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 05.03.2017
 * Time: 7:57
 */
trait TreeRecordTrait
{
    public function getDescendants()
    {
        $children = $this->children;
        $descendants = clone($children);
        foreach ($children as $child) {
            $descendants->merge($child->getDescendants());
        }
        return $descendants;
    }

    public function getChildren()
    {
        return $this->children;
    }

    public function getAvailableParents()
    {
        $descendants = $this->getDescendants()->add($this);

        return static::whereNotIn('id', $descendants->pluck('id')->all())->get();
    }
}