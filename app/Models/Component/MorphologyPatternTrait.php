<?php

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 06.02.2017
 * Time: 12:01
 */
trait MorphologyPatternTrait
{
    public function applyPattern($word, $rod = 'm')
    {
        $column = ($rod == 'z') ? 'pattern_z' : (($rod == 's') ? 'pattern_s' : 'pattern');

        $pattern = $this->{$column} ? $this->{$column} : $this->pattern;

        if (! $pattern) {
            $pattern = '* ' . $this->name;
        }

        if ($word instanceof \Morphy\MorphyResult) {
            $patternMorph = \Morphy\Morphy::morph($pattern);

            foreach ($word->getData() as $padezh => &$string) {
                $word->setForm($padezh, str_replace('*', $string, $patternMorph->getForm($padezh)));
            }

            return $word;
        }
        else {
            return str_replace('*', $word, $pattern);
        }
    }
}