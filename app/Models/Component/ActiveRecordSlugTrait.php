<?php

/**
 * Created by PhpStorm.
 * User: LittlePoo
 * Date: 27.11.2015
 * Time: 6:43
 */
trait ActiveRecordSlugTrait
{
	protected static function slugAttribute()
	{
		return 'slug';
	}

	public static function bootActiveRecordSlugTrait()
	{
        static::creating(function($item){
            if (! $item->{static::slugAttribute()}) {
                $slug = static::generateSlug(static::getSlugedString($item));

                $item->{static::slugAttribute()} = $slug;
            }
        });
	}

	public static function generateSlug($name)
	{
		return slug($name);
	}

	public static function getSlugedString($item)
	{
		$name = $item->name;

		if (! $name)
			$name = $item->title;

		if (! $name)
			$name = $item->label;

		return $name;
	}

	/**
	 * @param $slug
	 * @return static
	 */
	public static function findBySlug($slug)
	{
		return static::where(static::slugAttribute(), '=', $slug)->first();
	}

	/**
	 * @return mixed
	 */
	public function getSlug()
	{
		return $this->{static::slugAttribute()};
	}
}