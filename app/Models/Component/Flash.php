<?php
/**
 * 
 * @author Зыков Дмитрий <aesleep@ya.ru>
 * @date 18.05.2015
 */

class Flash {

	protected static $sessionKey = 'flashMessage';

	public static function info($message)
	{
		static::set($message, 'info');
	}

	public static function success($message)
	{
		static::set($message, 'success');
	}

	public static function warning($message)
	{
		static::set($message, 'warning');
	}

	public static function error($message)
	{
		static::set($message, 'error');
	}

	public static function set($message, $level)
	{
		return Session::flash(static::$sessionKey, array(
			'text' => $message,
			'level' => $level
		));
	}

	public static function getMessage()
	{
		return (static::has() ? static::get()['text'] : null);
	}

	public static function getLevel()
	{
		return (static::has() ? static::get()['level'] : null);
	}

	public static function isSuccess()
	{
		return static::getLevel() == 'success';
	}

	public static function has()
	{
		return Session::has(static::$sessionKey);
	}

	public static function get()
	{
		return Session::get(static::$sessionKey);
	}
}