<?php

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 27.03.2017
 * Time: 23:13
 */
class Recent
{
    public static function add(Product $product)
    {
        if ($product->available == 0)
            return;

        $recent = Session::get('recent', []);

        $position = array_search($product->id, $recent);
        if ($position) {
            unset($recent[$position]);
        }
        array_push($recent, $product->id);

        if (count($recent) > 20) {
            array_shift($recent);
        }

        Session::set('recent', $recent);
    }

    public static function all()
    {
        return Session::get('recent', []);
    }
}