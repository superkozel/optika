<?php

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 02.03.2017
 * Time: 4:59
 */
class DataGrid
{
    protected $columns;
    protected $data;
    protected $pageSize = 50;

    public static function create()
    {
        return new static;
    }

    /**
     * @return mixed
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * @param mixed $columns
     */
    public function setColumns($columns)
    {
        $this->columns = $columns;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    public function getPageData()
    {
        if ($this->data instanceof \Illuminate\Database\Eloquent\Collection) {
            $this->data->slice($this->pageSize * ($this->getCurrentPage() - 1), $this->pageSize);
        }
        else if (is_array($this->data)) {
            return array_slice($this->data, $this->pageSize * ($this->getCurrentPage() - 1), $this->pageSize);
        }
        else if ($this->data instanceof Illuminate\Database\Eloquent\Builder
            || $this->data instanceof \Illuminate\Database\Eloquent\Relations\HasMany
            || $this->data instanceof \Illuminate\Database\Eloquent\Relations\BelongsToMany
        ){
            return $this->data->paginate()->items();
        }
    }

    public function getPaginator()
    {
        if ($this->data instanceof Illuminate\Database\Eloquent\Builder
            || $this->data instanceof \Illuminate\Database\Eloquent\Relations\HasMany
            || $this->data instanceof \Illuminate\Database\Eloquent\Relations\BelongsToMany
        ){
            return $this->data->paginate($this->pageSize);
        }
        else {
            return new \Illuminate\Pagination\LengthAwarePaginator($this->getPageData(), count($this->data), $this->pageSize, $this->getCurrentPage(), [
                'path' => \Illuminate\Pagination\Paginator::resolveCurrentPath(),
                'pageName' => 'page',
            ]);
        }
    }


    public function getCurrentPage()
    {
        return (int)Input::get('page', 1);
    }


    public function render()
    {
        return view('components.datagrid', ['grid' => $this])->render();
    }
}