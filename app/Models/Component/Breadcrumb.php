<?php

class Breadcrumb
{
	/**
	 * @var string $label
	 */
	protected $title;

	/**
	 * @var string $url
	 */
	protected $url;

	public static function create($label, $url)
	{
		$crumb = new static;

		$crumb->label = $label;
		$crumb->url = $url;

		return $crumb;
	}

	/**
	 * @return string
	 */
	public function getLabel()
	{
		return $this->label;
	}

	/**
	 * @return string
	 */
	public function getUrl()
	{
		return $this->url;
	}

}