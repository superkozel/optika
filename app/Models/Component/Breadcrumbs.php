<?php
/**
 * Created by PhpStorm.
 * User: Kirill
 * Date: 18.07.14
 * Time: 4:00
 */

class Breadcrumbs implements IteratorAggregate
{
	public static function create()
	{
		return new static;
	}

	protected $data = array();

	public function count()
	{
		return count($this->data);
	}


	/**
	 * @param $label
	 * @param $url
	 * @return $this
	 */
	public function add($label, $url = null)
	{
		$this->data[] = array(
			'label' => $label,
			'url' => $url
		);

		return $this;
	}

	public function getIterator()
	{
		return new ArrayIterator($this->data);
	}
}