<?php

class Bootstrap3Form extends Form
{
	CONST INPUT_CLASS = 'form-control';

	protected static $errors;
	protected static $model;

	public static function model($model, $options = array()){
		if (! empty($options['errors']))
			static::$errors = $options['errors'];
		unset($options['errors']);

//        if (empty($options['url']))
//            $options['url'] = $_SERVER["REQUEST_URI"];

		static::$model = $model;

		return parent::model($model, $options);
	}

	public static function open($options = array()){
		if (! empty($options['errors']))
			static::$errors = $options['errors'];
		unset($options['errors']);

		return parent::open($options);
	}

	public static function close(){
		static::$model = null;

		return parent::close();
	}


	public static function label($name, $value = null, $options = array())
	{
	    if (empty($options['class']))
		    $options['class'] = 'control-label';
	    else
	        $options['class'] .= ' control-label';

		return parent::label($name, $value, $options);
	}

	public static function text($name, $value = null, $options = array())
	{
		if (! empty($options['class']))
			$options['class'] .= ' ' . self::INPUT_CLASS;
		else
			$options['class'] = self::INPUT_CLASS;

		return parent::text($name, $value, $options);
	}

	public static function input($type, $name, $value = null, $options = array())
	{
		if ($type != 'file') {
			if (!empty($options['class']))
				$options['class'] .= ' ' . self::INPUT_CLASS;
			else
				$options['class'] = self::INPUT_CLASS;
		}

		return parent::input($type, $name, $value, $options);
	}

    public static function date($name, $value = null, $options = array())
    {
        if (!empty($options['class']))
            $options['class'] .= ' ' . self::INPUT_CLASS;
        else
            $options['class'] = self::INPUT_CLASS;

        if ($value instanceof DateTime) {
            $value = $value->format('Y-m-d');
        }

        return parent::input('date', $name, $value, $options);
    }

	public static function bagError($name, $messageBag)
	{
		if ($messageBag->has($name))
			return '<span class="help-block">' . $messageBag->first($name) . '</span>';
	}

	public static function error($errors)
	{
	    if (count($errors))
		    return '<span class="help-block">' . join('<br/>', $errors) . '</span>';
	    else
	        return '';
	}

	public static function textarea($name, $value = null, $options = array())
	{
		if (! empty($options['class']))
			$options['class'] .= ' ' . self::INPUT_CLASS;
		else
			$options['class'] = self::INPUT_CLASS;

		return parent::textarea($name, $value, $options);
	}

	public static function select($name, $list = array(), $selected = null, $options = array()){
		if (! empty($options['class']))
			$options['class'] .= ' ' . self::INPUT_CLASS;
		else
			$options['class'] = self::INPUT_CLASS;

		return parent::select($name, $list, $selected, $options);
	}

	public static function select2($name, $list = array(), $selected = null, $options = array()){
		if (! empty($options['class']))
			$options['class'] .= ' ' . self::INPUT_CLASS;
		else
			$options['class'] = self::INPUT_CLASS;

		$html = parent::select($name, $list, $selected, $options);

		$html .= "<script>$('[name=\"$name\"]').select2();</script>";

		return $html;
	}

	public static function checkbox($name, $label, $value = 1, $checked = null, $options = array())
    {
        $rowOptions = array_get($options, 'rowOptions', []);

		$str = '<div class="checkbox" ' . HTML::attributes($rowOptions) . '><label>' .parent::checkbox($name, $value, $checked, array_except($options, ['rowOptions', 'labelOptions', 'inputDivOptions'])) . ' ' . $label . '</label></div>';

		return $str;
	}

	public static function errorSummary($errors)
	{
		$str = '';
		if (count($errors) > 0) {
			$str .= '<div class="alert alert-danger">';
			$str .= '<strong>Ой!</strong> Пожалуйста, исправьте следующие ошибки:<br/><br/><ul>';
			foreach ($errors->all() as $error) {
				$str .= '<li>' . $error . '</li>';
			}
			$str .= '</ul>';
			$str .= '</div>';
		}

		return $str;
	}

	public static function checkboxList($name, $list = array(), $selected = null, $options = array()){
		if (! empty($options['class']))
			$options['class'] .= ' ' . self::INPUT_CLASS;
		else
			$options['class'] = self::INPUT_CLASS;

//        $model = static::getFacadeRoot()->getModelValueAttribute($name);
//
//        dd($model);
//
//        foreach (static::$model as $device) {
//            $values[] = $device->enum()[1];
//        }

		$str = '';
		if (old($name)) {
			$selected = old($name);
		}
		foreach($list as $k => $v) {
			$checked = ($selected && in_array($k, $selected));

			$options = array(
				'class' => 'checkbox'
			);

			if ($checked)
				$options['checked'] = $checked;


			$str .= '<div class="checkbox"><label>' .
				parent::input('checkbox', $name . '[]', $k, $options) .
//				parent::checkbox($name . '[]', $k, $checked, array('class' => 'checkbox')) .
				' ' . $v .
			'</label></div>';
		}

		return $str;
	}

	public static function formRow($name, $label, $input, $rowOptions = [], $labelOptions = [], $inputDivOptions = [])
	{
		$errors = static::$errors;

		$b3Class = 'form-group ' . ($errors && $errors->has($name) ? ' has-error' : '');
		if (! empty($rowOptions['class']))
            $rowOptions['class'] .= ' ' . $b3Class;
        else
            $rowOptions['class'] = $b3Class;

		return Html::tag('div', (($label ? Bootstrap3Form::label($name, $label, $labelOptions ? $labelOptions : []) : '')
            . Html::tag('div', (string)$input . (($errors && $errors->has($name)) ? Bootstrap3Form::error($errors->get($name)) : ''), $inputDivOptions ? $inputDivOptions : []))

            , $rowOptions ? $rowOptions : []);
	}

	public static function textRow($name, $label = null, $value = null, $options = array())
	{
        if (empty($options['placeholder'])) {
            $options['placeholder'] = rtrim($label, '*');
        }

        return Bootstrap3Form::formRow($name, $label, Bootstrap3Form::text($name, $value, array_except($options, ['rowOptions', 'labelOptions', 'inputDivOptions'])), array_get($options, 'rowOptions'), array_get($options, 'labelOptions'), array_get($options, 'inputDivOptions'));
	}

	public static function textareaRow($name, $label = null, $value = null, $options = array())
	{
		return Bootstrap3Form::formRow($name, $label, Bootstrap3Form::textarea($name, $value, array_except($options, ['rowOptions', 'labelOptions', 'inputDivOptions'])), array_get($options, 'rowOptions'), array_get($options, 'labelOptions'), array_get($options, 'inputDivOptions'));
	}

	public static function inputRow($type, $name, $label = null, $value = null, $options = array())
	{
        if (empty($options['placeholder'])) {
            $options['placeholder'] = rtrim($label, '*');
        }

		return Bootstrap3Form::formRow($name, $label, Bootstrap3Form::input($type, $name, $value, array_except($options, ['rowOptions', 'labelOptions', 'inputDivOptions'])), array_get($options, 'rowOptions'), array_get($options, 'labelOptions'), array_get($options, 'inputDivOptions'));
	}

    public static function dateRow($name, $label = null, $value = null, $options = array())
    {
        if (empty($options['placeholder'])) {
            $options['placeholder'] = rtrim($label, '*');
        }

        return Bootstrap3Form::formRow($name, $label, Bootstrap3Form::date($name, $value, array_except($options, ['rowOptions', 'labelOptions', 'inputDivOptions'])), array_get($options, 'rowOptions'), array_get($options, 'labelOptions'), array_get($options, 'inputDivOptions'));
    }

	public static function selectRow($name, $label, $list, $value = null, $options = array())
	{
		return Bootstrap3Form::formRow($name, $label, Bootstrap3Form::select($name, $list, $value, array_except($options, ['rowOptions', 'labelOptions', 'inputDivOptions'])), array_get($options, 'rowOptions'), array_get($options, 'labelOptions'), array_get($options, 'inputDivOptions'));
	}

	public static function select2Row($name, $label, $list, $value = null, $options = array())
	{
		return Bootstrap3Form::formRow($name, $label, Bootstrap3Form::select2($name, $list, $value, array_except($options, ['rowOptions', 'labelOptions', 'inputDivOptions'])), array_get($options, 'rowOptions'), array_get($options, 'labelOptions'), array_get($options, 'inputDivOptions'));
	}

	public static function checkboxListRow($name, $label, $list, $value = null, $options = array())
	{
		return Bootstrap3Form::formRow($name, $label, Bootstrap3Form::checkboxList($name, $list, $value, array_except($options, ['rowOptions', 'labelOptions', 'inputDivOptions'])), array_get($options, 'rowOptions'), array_get($options, 'labelOptions'), array_get($options, 'inputDivOptions'));
	}

	public static function submit($value = null, $types = null, $options = array())
	{
		$class = 'btn';

		if (! $types)
			$types = 'primary';

		if (is_array($types)){
			foreach ($types as $type) {
				$class .= ' btn-' . $type;
			}
		}
		else {
			$class .= ' btn-' . $types;
		}

		if (! empty($options['class']))
			$class .= ' ' . $options['class'];

		$options['class'] = $class;

		return parent::submit($value, $options);
	}

	public static function button($value = null, $types = null, $options = array())
	{
		$class = 'btn';

		if (! $types)
			$types = 'default';

		if (is_array($types)){
			foreach ($types as $type) {
				$class .= ' btn-' . $type;
			}
		}
		else {
			$class .= ' btn-' . $types;
		}

		if (! empty($options['class']))
			$class .= ' ' . $options['class'];

		$options['class'] = $class;

		return parent::submit($value, $options);
	}

	public static function imageUploadRow($name, $label)
	{
		return static::formRow($name , $label, View::make('front.upload.widget', array('model' => static::$model, 'property' => $name))->render());
	}

	/**
	 * @param $name
	 * @param Eloquent $value
	 * @return string
	 */
	public static function polymorphic($name, Eloquent $value) {
		return
		static::hidden($name . '_type', $value->getMorphClass()) . "\n" .
		static::hidden($name . '_id', $value->id);
	}
}