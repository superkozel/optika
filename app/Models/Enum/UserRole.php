<?php

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 10.04.2017
 * Time: 20:40
 */
class UserRole extends BaseEnum
{
    const CUSTOMER = 1;
    const MANAGER = 2;
    const ADMIN = 3;
    const SUPERADMIN = 4;

    public static $names = [
        UserRole::CUSTOMER => 'покупатель',
        UserRole::MANAGER => 'менеджер',
        UserRole::ADMIN => 'администратор',
        UserRole::SUPERADMIN => 'суперадмин',
    ];
}