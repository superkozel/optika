<?php

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 08.01.2017
 * Time: 14:34
 */
class AttributeType extends BaseEnum
{

    const BOOL = 1;
    const NUMBER = 2;
    const MODEL = 3;
    const SELECT = 4;
    const STRING = 5;

    public static $names = [
        AttributeType::BOOL => 'Да/Нет',
        AttributeType::NUMBER => 'Число',
        AttributeType::MODEL => 'Модель',
        AttributeType::SELECT => 'Список',
        AttributeType::STRING => 'Строка',
    ];
}