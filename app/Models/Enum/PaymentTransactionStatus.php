<?php
namespace Enum;

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 25.04.2017
 * Time: 0:12
 */
class PaymentTransactionStatus extends \BaseEnum
{
    const NEWS = 1;
    const COMPLETE = 2;
    const CANCELED = 3;

    public static $names = [
        PaymentTransactionStatus::NEWS => 'новая',
        PaymentTransactionStatus::COMPLETE => 'выполнен',
        PaymentTransactionStatus::CANCELED => 'отмена',
    ];
}