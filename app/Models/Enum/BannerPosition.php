<?php

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 29.07.2017
 * Time: 21:13
 */
class BannerPosition extends BaseEnum
{
    public static $names = [
        'mainpage-mainbanner' => 'Главная - главный баннер',
        'mainpage-novinki' => 'Главная - новинки',
        'catalogmenu-linzy' => 'Меню - линзы',
        'catalogmenu-opravy' => 'Меню - оправы',
        'catalogmenu-kontaktnye-linzy' => 'Меню - контактные линзы',
    ];

    public static $size = [
        'mainpage-mainbanner' => array(1185, 330, false),
        'mainpage-novinki' => array(null, 535, false),
        'catalogmenu-linzy' => 'Меню - линзы',
        'catalogmenu-opravy' => 'Меню - оправы',
        'catalogmenu-kontaktnye-linzy' => array(230, 300, false),
    ];

    public function getSize()
    {
        return static::$size[$this->getId()];
    }
}