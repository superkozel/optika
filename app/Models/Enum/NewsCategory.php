<?php

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 15.01.2017
 * Time: 21:28
 */
class NewsCategory extends BaseEnum
{
    public static $names = [
        1 => 'Интересное',
        2 => 'О компании',
    ];
}