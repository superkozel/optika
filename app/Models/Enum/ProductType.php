<?php

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 30.12.2016
 * Time: 3:40
 */
class ProductType extends BaseEnum
{
    const OPRAVA = 1;
    const SOLNECHNIE_OCHKI = 2;
    const KONTAKTNIE_LINZY = 3;
    const KAPLI = 4;
    const RASTVOR_DLYA_LINZ = 5;
    const SERTIFIKAT = 7;
    const KONTEYNER_DLYA_LINZ = 8;
    const PINCET_DLYA_LINZ = 9;
    const OCHKI_MEDICINSKIE = 10;
    const LINZI = 11;

    public static $names = [
        ProductType::OPRAVA => 'медицинская оправа',
        ProductType::LINZI => 'очковые линзы',
        ProductType::OCHKI_MEDICINSKIE => 'очки для зрения',
        ProductType::SOLNECHNIE_OCHKI => 'солнцезащитные очки',
        ProductType::KONTAKTNIE_LINZY => 'контактные линзы',
        ProductType::KAPLI => 'капли увлажняющие',
        ProductType::RASTVOR_DLYA_LINZ => 'раствор для контактных линз',
        ProductType::SERTIFIKAT => 'подарочный сертификат',
        ProductType::KONTEYNER_DLYA_LINZ => 'контейнер для линз',
        ProductType::PINCET_DLYA_LINZ => 'пинцет для контактных линз',
    ];

    public function getAttributeSet()
    {
        return AttributeSet::where('product_type_id', $this->id)->first();
    }

    public function modifiers()
    {
        return ProductModifier::whereProductTypeId($this->getId());
    }

    public static function findByUid($uid)
    {
        if ($uid == '2244d377-1a7a-11e1-93d2-00261858a6c5') {
            return static::create(ProductType::LINZI);
        }
        else if ($uid == 'a598fcea-dde6-11e0-a19e-f46d0405f555') {
            return static::create(ProductType::KONTAKTNIE_LINZY);
        }
        else if ($uid == '254f9532-5df7-11e1-8588-001517b9f1e1') {
            return static::create(ProductType::OCHKI_MEDICINSKIE);
        }
    }
}