<?php

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 28.02.2017
 * Time: 3:19
 */
class ProductModifierType extends BaseEnum
{
    const PRODUCT = 1;
    const SERVICE = 1;

    protected static $names = array(
        PaymentType::PRODUCT => 'товар',
        PaymentType::SERVICE => 'услуга',
    );
}