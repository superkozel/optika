<?php

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 01.01.2017
 * Time: 5:22
 */
class SalonSpecialization extends BaseEnum
{
    public static $names = [
        5 => 'Стоковая оптика',
        4 => 'Детская оптика',
        3 => 'Салон полного цикла',
    ];
}