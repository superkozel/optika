<?php

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 08.02.2017
 * Time: 12:16
 */
class CatalogFilterOptionSort extends BaseEnum
{
    const ALPHANUM = 1;//по алфавиту
    const COUNT = 2;//по количеству товаров в выборке
}