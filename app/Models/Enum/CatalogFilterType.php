<?php

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 12.01.2017
 * Time: 21:49
 */
class CatalogFilterType extends BaseEnum
{

    const CLIST = 1;
    const RANGE = 2;
    const COLOR = 3;
    const DROPDOWN = 4;

    public static $names = [
        CatalogFilterType::CLIST => 'Список',
        CatalogFilterType::RANGE => 'Промежуток',
        CatalogFilterType::COLOR => 'Цвет',
        CatalogFilterType::DROPDOWN => 'Выпадающий список',
    ];
}