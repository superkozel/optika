<?php
namespace Enum;

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 25.04.2017
 * Time: 1:37
 */
class PaymentSystem extends \BaseEnum
{
    const YANDEXKASSA = 'yandexkassa';

    public static $names = [
        PaymentSystem::YANDEXKASSA => 'Яндекс.Касса',
    ];
}