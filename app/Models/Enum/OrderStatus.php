<?php

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 12.02.2017
 * Time: 14:40
 */
class OrderStatus extends BaseEnum
{
    const NEWS = 1;
    const ACCEPTED = 2;
    const REJECTED = 3;
    const CANCELED = 4;
    const COMPLETE = 6;

    const OZHIDAETSA_POSTAVKA = 100;
    const IZGOTOVLENIE_ZAKAZA = 110;
    const IZGOTOVLEN = 120;
    const PEREDAN_NA_SKLAD = 130;
    const ZAKAZ_PROIZVODSTVO = 140;
    const SOBIRAETSA = 150;
    const SOBRAN = 160;
    const V_PUTI = 170;

    public static $names = [
        OrderStatus::NEWS => 'новый',
        OrderStatus::ACCEPTED => 'принят',
        OrderStatus::COMPLETE => 'выполнен',
        OrderStatus::REJECTED => 'не принят',
        OrderStatus::CANCELED => 'отменен',

        OrderStatus::OZHIDAETSA_POSTAVKA => 'ожидается поставка',
        OrderStatus::IZGOTOVLENIE_ZAKAZA => 'изготовление заказа',
        OrderStatus::IZGOTOVLEN => 'изготовлен',
        OrderStatus::PEREDAN_NA_SKLAD => 'передан на склад',
        OrderStatus::ZAKAZ_PROIZVODSTVO => 'заказ производство',
        OrderStatus::SOBIRAETSA => 'комплектуется',
        OrderStatus::SOBRAN => 'укомплектован',
        OrderStatus::V_PUTI => 'в пути',
    ];
}