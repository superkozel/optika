<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 16.02.2017
 * Time: 16:30
 */

namespace Enum;

class PaymentType extends \BaseEnum
{
    const KURIERU = 1;
    const KARTOY = 2;
    const KARTOY_KURIERU = 3;
    const ONLINE = 4;

    protected static $names = array(
        PaymentType::KURIERU => 'оплата наличными',
        PaymentType::KARTOY => 'оплата картой',
        PaymentType::KARTOY_KURIERU => 'оплата картой курьеру',
        PaymentType::ONLINE => 'оплата онлайн',
    );

    /**
     * @param Order $order
     */
    public static function availableForOrder(\Order $order)
    {
        $available = array();

        foreach (static::all() as $paymentType) {
            if ($paymentType->isAvailableForOrder($order))
                $available[] = $paymentType;
        }

        return $available;
    }

    public function isAvailableForOrder(\Order $order)
    {
        if ($this->is(PaymentType::ONLINE, PaymentType::KARTOY_KURIERU, PaymentType::KARTOY)) {
            if ($order->type && $order->type->is(\OrderType::FITTING)) {
                return false;
            }
        }

        return true;
    }
}