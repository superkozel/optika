<?php

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 12.01.2017
 * Time: 15:34
 */
class ProductAvailability extends BaseEnum
{
    public static $names = [
        0 => 'Не в наличии',
        1 => 'Под заказ',
        9 => 'В наличии',
    ];


    public function isAvailable()
    {
        return $this->id > 0;
    }
}