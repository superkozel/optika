<?php

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 10.02.2017
 * Time: 19:20
 */
class CatalogFilterSortType extends BaseEnum
{
    const NUM = 1;
    const ALPHANUM = 2;
    const COUNT = 3;
    const MANUAL = 4;

    public static $names = [
        CatalogFilterSortType::NUM => 'Цифровая',
        CatalogFilterSortType::ALPHANUM => 'Буквенно-цифровая',
        CatalogFilterSortType::COUNT => 'По количеству товаров',
        CatalogFilterSortType::MANUAL => 'Вручную',
    ];

}