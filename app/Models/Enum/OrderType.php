<?php

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 12.02.2017
 * Time: 17:52
 */
class OrderType extends BaseEnum
{
    const NORMAL = 1;
    const FITTING = 2;

    public static $names = [
        OrderType::NORMAL => 'Обычный',
        OrderType::FITTING => 'Примерка',
    ];
}