<?php

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 13.01.2017
 * Time: 13:37
 */
class Settings extends Config
{
    public static function get($key)
    {
        $options = [
            'phone' => '+7 (495) 231-46-72',
            'phone2' => '+7 (495) 730-60-58',
            'email' => 'info@optika-favorit.ru',
            'address' => 'Москва, ул. Вавилова, д.5, кор.3, офис 202',
            'vk' => '35567382',
            'twitter' => 'Optika_Favorit',
            'facebook' => 'optikaFavoritOfficial',
            'instagram' => 'optika_favorit',
            'bonus_program_active' => false,
        ];

        return $options[$key];
    }
}