<?php

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 13.01.2017
 * Time: 16:19
 */
class DeliveryType extends BaseEnum
{
    const DOSTAVKA = 1;
    const SAMOVYVOZ = 2;
    const POCHTA = 3;
    const DOSTAVKA2 = 4;
    const PVZ = 5;
    const REGIONALNAYA_KURIERSKAYA = 6;

    protected static $names = array(
        self::SAMOVYVOZ => 'получение в салоне',
        self::DOSTAVKA => 'доставка курьером по Москве',
        self::DOSTAVKA2 => 'доставка курьером в Московскую область',
//        self::PVZ => 'доставка по России в пункты выдачи заказов',
//        self::REGIONALNAYA_KURIERSKAYA => 'региональная курьерская доставка',
//        self::POCHTA => 'доставка почтой',
    );

    /**
     * @param Order $order
     */
    public static function availableForOrder(Order $order)
    {
        $available = array();

        foreach (static::all() as $deliveryType) {
            if ($deliveryType->isAvailableForOrder($order))
                $available[] = $deliveryType;
        }

        return $available;
    }

    public function isAvailableForOrder(Order $order)
    {
        if ($this->is(DeliveryType::SAMOVYVOZ) OR ! $order->type->is(OrderType::FITTING)) {
            return true;
        }

        return false;
    }

    public function getPriceText(Order $order)
    {
        if ($this->is(static::DOSTAVKA, static::DOSTAVKA2)) {
            $text = '';

            $text .= 'от 110 руб.';

            return $text;
        }
        else if ($this->getId() == static::SAMOVYVOZ) {
            return 'бесплатно';
        }
        else if ($this->is(static::POCHTA, static::REGIONALNAYA_KURIERSKAYA, static::PVZ)) {
            return 'по тарифу службы';
        }
    }

    public function getOrderDeliveryPrice(Order $order)
    {
        if ($this->getId() == static::DOSTAVKA) {
            return Config::get('contacts.deliveryPrice');
        }
        else if ($this->getId() == static::SAMOVYVOZ) {
            return 0;
        }
        else if ($this->is(static::DOSTAVKA2)) {//доставка по московской области
            return null;
        }
        else if ($this->is(static::POCHTA)) {
            return null;
        }
        else if ($this->is(static::PVZ)) {
            return null;
        }
        else if ($this->is(static::POCHTA)) {
            $addrData = DostavkaGuruApi::parseAddressString($order->address);

            $deliveryData = DostavkaGuruApi::calculateDeliveryCost('Почта', 1, $order->sum, $addrData['region'], $addrData['city']);
        }
    }

    public function getAvailableDatesForOrder(Order $order)
    {
        $orderDeliveryDays = $order->calculateDeliveryDays();
        $dates = [];

        $i = 0;

        while (count($dates) < 14) {
            $pause = (date('H') < 18) ? $orderDeliveryDays : $orderDeliveryDays + 1;

            $date = \Carbon\Carbon::createFromTimestamp(strtotime('+ ' . ($pause + $i) . ' day'));
            if (! $date->isSunday()) {//в воскренье нет доставки
                $dates[] = $date;
            }
            $i++;
        }

        return $dates;
    }

    public function timeIntervals()
    {
        if ($this->is(DeliveryType::DOSTAVKA2)) {
            return null;
        }
        else {
            return [
                '9:00-12:00',
                '12:00-15:00',
                '15:00-18:00',
                '18:00-21:00',
            ];
        }
    }
}