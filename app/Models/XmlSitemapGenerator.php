<?php
/***************************************************************************
 *   Copyright (C) 2014 by Dmitriy Zykov                                   *
 *                                                                         *
 ***************************************************************************/

class XmlSitemapGenerator
{
    const FREQ_WEEKLY = 'weekly';
    const FREQ_DAILY = 'daily';

    /**
     * @var string
     */
    private $host;
    /** @var XMLWriter $writer */
    protected $writer;
    /** @var  string */
    protected $path;

    /**
     * @return XmlSitemapGenerator
     */
    public static function create()
    {
        return new XmlSitemapGenerator();
    }


    /**
     * @param string $loc
     * @param float $priority
     * @param string $changefreq
     */
    public function add($loc, $priority = null, $changefreq = null)
    {
        $xml = $this->writer;

        $xml->startElement('url');

        $xml->writeElement('loc', 'http://' . $this->getHost() . $loc);

        if ($priority)
            $xml->writeElement('priority', $priority);

        if ($changefreq)
            $xml->writeElement('changefreq', $changefreq);

        $xml->endElement();

        return $this;
    }

    function start()
    {

        $this->writer = $xml = new XMLWriter();

        $xml->openUri($this->getPath());

        $xml->startDocument('1.0', 'UTF-8');

        $xml->setIndent(true);

        $xml->startElement('urlset');
        $xml->writeAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');

    }

    public function end()
    {
        $xml = $this->writer;

        $xml->endElement();

        $xml->endDocument();

        $xml->flush();
    }

    /**
     * @param string $host
     * @return $this
     */
    public function setHost($host)
    {
        $this->host = $host;

        return $this;
    }

    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }
}