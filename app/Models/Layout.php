 <?php
/**
 * 
 * @author Зыков Дмитрий <aesleep@ya.ru>
 * @date 17.04.2015
 */

class Layout
{
	public $view;

	public function __construct()
	{
		View::share('user', Auth::user());
		View::share('basket', \App\User::getBasket());
	}

	public static function make($viewName, $data = [])
	{
		$layout = new static;

		$layout->view = view($viewName, $data);

		return $layout;
	}

    public function setTitle($title)
    {
		$this->view->with('title', $title);
		View::share('title', $title);

        return $this;
    }

    public function setH1($h1)
    {
		$this->view->with('h1', $h1);
		View::share('h1', $h1);
        return $this;
    }

	public function setH1AndTitle($title)
	{
		$this->setTitle($title);
		$this->setH1($title);

		return $this;
	}

    public function setMetaDescription($metaDescription)
    {
		$this->view->with('metaDescription', $metaDescription);

        return $this;
    }

    public function setBreadcrumbs(Breadcrumbs $breadcrumbs)
    {
		View::share('breadcrumbs', $breadcrumbs);

        return $this;
    }

	public function addBreadcrumb($label, $url = null)
	{
		View::getShared()['breadcrumbs']->add($label, $url);

		return $this;
	}

    public function setContent($content)
    {
        $this->view->with('content', $content);

        return $this;
    }

	public function setCanonical($canonical)
	{
		View::share('canonical', $canonical);

		return $this;
	}

	public function setNoindex($noindex)
	{
		$this->view->with('noindex', $noindex);
		View::share('noindex', $noindex);
		return $this;
	}

	/**
	 * @return View
	 */
	public function getView()
	{
		View::share('layout', $this);

		return $this->view;
	}

	/**
	 * @param $view
	 * @return $this
	 */
	public function setView($view)
	{
		$this->view = $view;

		return $this;
	}

	/**
     * @return string
     */
	public function render()
	{
		return $this->getView()->render();
	}

	public function __toString()
	{
		return $this->render();
	}
}