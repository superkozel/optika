<?php
/**
 * Created by PhpStorm.
 * User: LittlePoo
 * Date: 14.12.2015
 * Time: 10:11
 */

namespace Morphy;

class MorphyLaravelAdapter
{
	/**
	 * @return MorphyLaravelAdapter
	 */
	public static function create()
	{
		return new MorphyLaravelAdapter();
	}

	/**
	 * @param string $sql
	 * @return array
	 */
	public function queryRow($sql)
	{
		return (array)\DB::selectOne(\DB::raw($sql));
	}

	/**
	 * @param string $sql
	 * @return array
	 */
	public function queryAll($sql)
	{
		return \DB::select(\DB::raw($sql));
	}

	/**
	 * @param string $sql
	 * @return mixed
	 */
	public function execute($sql)
	{
		return \DB::statement($sql);
	}

	public function getError()
	{
	}

	public function cacheSet($key, $value, $time)
	{
		return \Cache::add($key, $value, $time);
	}

	public function cacheGet($key)
	{
		return \Cache::has($key) ? \Cache::get($key) : false;
	}
}
