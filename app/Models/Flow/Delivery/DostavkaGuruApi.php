<?php

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 16.03.2017
 * Time: 12:31
 */
class DostavkaGuruApi
{
    public function getAvailableDates()
    {

    }

    public function parseAddress()
    {

    }

    public static function getPVZList()
    {
        $data = [
            'script' => 'all_list'
        ];

        $xml_string = static::request('pvz_list', $data);

        $xml = simplexml_load_string($xml_string);
        $json = json_encode($xml);
        return json_decode($json, true)['point'];
    }

    public static function request($action, $data)
    {
        $data['partner_id'] = 2194;
        $data['key'] = '15ca3f24c5c1523e65d6742b96d51659';

        $url = 'http://api.dostavka.guru/client/' . $action . '.php';

        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data)
            )
        );
        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);

        return $result;
    }

    public static function calculateDeliveryCost($deliveryType, $weight, $sum, $region, $city, $pvzId = null, $nalplat = null)
    {
        $data = array(
            'method'=>$deliveryType,
            'weight'=>$weight,
            'ocen_sum'=>$sum,
            'nal_plat'=>$sum,
            'region'=>$region,
            'city'=> $city,
        );
        //Курьер,Почта, ПВЗ
        if ($deliveryType == 'ПВЗ') {
            $data['point'] = $pvzId;
        }

        $response = static::request('/calc_guru_main_2_0', $data);

        return explode('::', $response);
    }


    public static function parseAddressString($address)
    {
        $data = [
            'addr' => $address,
        ];

        $xml_string = static::request('check_addr', $data);

        $xml = simplexml_load_string($xml_string);
        $json = json_encode($xml);

//        [source] => егорьевск 1-й мкн 35 34
// [result] => Россия, Московская обл, г Егорьевск,
//мкр 1-й, д 35, кв 34
// [postal_code] => 140304
// [country] => Россия
//    [region_type] => обл
//    [region_type_full] => область
//    [region] => Московская
//    [area_type] => р-н
//    [area_type_full] => район
//    [area] => Егорьевский
//    [city_type] => г
//    [city_type_full] => город
//41
// [city] => Егорьевск
//    [settlement_type] =>
// [settlement_type_full] =>
// [settlement] =>
// [city_district] =>
// [street_type] => мкр
//    [street_type_full] => микрорайон
//    [street] => 1-й
//    [house_type] => д
//    [house_type_full] => дом
//    [house] => 35
// [block_type] =>
// [block_type_full] =>
// [block] =>
// [flat_type] => кв
//    [flat] => 34
// [postal_box] =>
// [kladr_id] => 5000700100000010030
// [fias_id] => 31158fcd-b058-4b80-a43b-e8dcb3c0057c
//    [capital_marker] => 1
// [okato] => 46212501000
// [oktmo] => 46612101001
// [tax_office] => 5011
// [tax_office_legal] =>
// [flat_area] => 32.1
// [square_meter_price] =>
// [flat_price] =>
// [timezone] => UTC+3
// [geo_lat] => 55.3738931
// [geo_lon] => 39.062796
// [qc_geo] => 0
// [qc_complete] => 0
// [qc_house] => 2
// [qc] => 0
// [unparsed_parts] =>

        return json_decode($json, true)[0][0];
    }

}