<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 26.04.2017
 * Time: 17:57
 */

namespace Flow\Exchange;


use PhpParser\Serializer\XML;

class ExchangeParser
{
    public function parsePricesFile($path)
    {
        $reader = new \XMLReader();
        $reader->open($path); // указываем ридеру что будем парсить этот файл

        try {
            while ($reader->read()) {
                if($reader->nodeType == \XMLReader::ELEMENT && $reader->localName == 'Предложение') {
                    $el = $reader->expand();
                    $doc = new \DOMDocument();
                    $tovar = simplexml_import_dom($doc->importNode($el, true));

                    if ($product = \Product::whereExternalId((string)$tovar->Ид)->first()) {
                        foreach ($tovar->Цены->Цена as $tsena) {
                            if ((string)$tsena->ИдТипаЦены == '74968a3b-c75e-11e2-9f0a-0025902c4fba') {
                                $product->price = (string)$tsena->ЦенаЗаЕдиницу;
                            }
                            else if ((string)$tsena->ИдТипаЦены == '85758941-6868-11e6-9f8f-0025902c4fba') {
                                $product->old_price = (string)$tsena->ЦенаЗаЕдиницу;
                            }
                        }
                        $product->save();
                    }
                }
            }
        }
        catch (\Exception $e) {
            throw($e);
        }
    }

    public function parseRestsFile($path)
    {
        $reader = new \XMLReader();
        $reader->open($path); // указываем ридеру что будем парсить этот файл

        try {
            while ($reader->read()) {
                if($reader->nodeType == \XMLReader::ELEMENT && $reader->localName == 'Предложение') {
                    $el = $reader->expand();
                    $doc = new \DOMDocument();
                    $tovar = simplexml_import_dom($doc->importNode($el, true));

                    if ($product = \Product::whereExternalId((string)$tovar->Ид)->first()) {
                        $product->available = $tovar->Остатки->Остаток->Склад->Количество;
                        $product->save();
                    }
                }
            }
        }
        catch (\Exception $e) {
            throw($e);
        }
    }

    public function parseImportFile($path)
    {
        if (strpos($path, 'import1') !== false) {
            $productTypeId = \ProductType::OPRAVA;
        }
        else if (strpos($path, 'import2') !== false) {
            $productTypeId = \ProductType::LINZI;
        }
        elseif (strpos($path, 'import3') !== false) {
            $productTypeId = \ProductType::KONTAKTNIE_LINZY;
        }
        elseif (strpos($path, 'import_') !== false) {
            $productTypeId = \ProductType::SOLNECHNIE_OCHKI;
        }
        else {
            dd('Unknown Product Type');
        }
        $reader = new \XMLReader();
        $reader->open($path); // указываем ридеру что будем парсить этот файл

        try {
            while($reader->read()) {
//            if($reader->nodeType == \XMLReader::ELEMENT && $reader->localName == 'Классификатор') {
//                // если находим элемент <card>
//
//                $el = $reader->expand();
//                $doc = new DOMDocument();
//                $xml = simplexml_import_dom($doc->importNode($el, true));
//
//                foreach ($xml->Свойства->Свойство as $prop) {
//                    $propCode = (string)$prop->БитриксКод;
//                    echo(sprintf('impoting property[id=%s]' . "\n", (string)$propCode));
//
//                    if ($propCode == 'CML2_MANUFACTURER') {
//                        if (! Attribute::whereCode((string)$prop->БитриксКод)->exists()) {
//                            foreach ($prop->ВариантыЗначений->Вариант as $optionData) {
//                                if ($country = Country::whereName((string)$optionData->Значение)->first()) {
//                                    $country->xml_id = (string)$optionData->Ид;
//
//                                    $country->save();
//                                }
//                                else if ((string)$optionData->Значение != 'production'){
//                                    dd('Country not find [name=' . (string)$optionData->Значение . ']');
//                                }
//                            }
//
//
//                            $attr = new Attribute();
//                            $attr->name = (string)$prop->Наименование;
//
//                            $attr->multiple = (string)$prop->Множественное == 'false' ? 0 : 1;
//                            $attr->xml_id = (string)$prop->Ид;
//                            $attr->code = (string)$prop->БитриксКод;
//
//                            $attr->sort = (int)$prop->БитриксСортировка;
//
//                            $attr->type_id = AttributeType::MODEL;
//                            $attr->class = 'Country';
//                            $attr->save();
//                        }
//                    }
//
////                    if ($propCode == '44a008b2-d88d-11e0-80ad-f46d0405f555') {
////                        $attr = new Attribute();
////                        $attr->name = 'Принадлежность по возрасту';
////
////                        $attr->multiple = 1;
////
////                        $attr->sort = (int)$prop->БитриксСортировка;
////                    }
//
//                    if (! array_key_exists($propCode, $whitelist))
//                        continue;
//
//                    if ($attr = Attribute::where('code', $propCode)->first())
//                        continue;
//
//                    \DB::connection()->transaction(function() use ($prop, $whitelist, $propCode) {
//                        $attr = new Attribute();
//                        $attr->name = (string)$prop->Наименование;
//
//                        $attr->multiple = (string)$prop->Множественное == 'false' ? 0 : 1;
//                        $attr->xml_id = (string)$prop->Ид;
//                        $attr->code = (string)$prop->БитриксКод;
//
//                        $attr->sort = (int)$prop->БитриксСортировка;
//                        $attr->displayed = $whitelist[$propCode];
//
//                        $attrTypeMap = [
//                            'L' => AttributeType::SELECT,
//                            'N' => AttributeType::NUMBER,
//                            'S' => AttributeType::STRING,
//                        ];
//
//                        if (empty($attrTypeMap[(string)$prop->БитриксТипСвойства])) {
//                            dump($prop);
//                            dd('Unknown bitrix attribute type[' . (string)$prop->БитриксТипСвойства . ']');
//                        }
//
//                        $attr->type_id = $attrTypeMap[(string)$prop->БитриксТипСвойства];
//                        $attr->save();
//
//                        if ($attr->type_id == AttributeType::SELECT) {
//                            foreach ($prop->ВариантыЗначений->Вариант as $optionData) {
//                                $option = new AttributeOption();
//                                $option->name = (string)$optionData->Значение;
//                                $option->xml_id = (string)$optionData->Ид;
//                                $option->attribute_id = $attr->id;
//                                $option->sort = (int)$prop->Сортировка;
//
//                                $option->save();
//                            }
//                        }
//
//                    });
//                }
//            }

//            if($reader->depth < 4) {
//                dump($reader->name);
//            }
                $productCount = 0;
                if($reader->nodeType == \XMLReader::ELEMENT && $reader->localName == 'Товар') {
                    $el = $reader->expand();
                    $doc = new \DOMDocument();
                    $tovar = simplexml_import_dom($doc->importNode($el, true));

                    echo(sprintf('impoting product[id=%s]' . "\n", (string)$tovar->Ид));

                    if (! $product = \Product::whereExternalId((string)$tovar->Ид)->first()) {
                        $product = new \Product();
                        $product->external_id = (string)$tovar->Ид;
                        $product->available = 0;
                    }
                    $product->name = (string)$tovar->Наименование;

                    if ($tovar->Изготовитель) {
                        $attr = \Attribute::whereXmlId('CML2_MANUFACTURER')->first();
                        $country = \Country::whereXmlId((string)$tovar->Изготовитель->Ид)->first();

                        if ($country) {
                            $product->setProperty($attr->id, $country->id);
                        }
                    }

                    $brandId = null;
                    if ($tovar->Группы->Ид) {
                        $groupId = (string)$tovar->Группы->Ид;
                        $brand = \Brand::where('sections_ids', 'LIKE' , '%' . $groupId . '%')->first();
                        if (! empty($brand)) {
                            $product->brand_id = $brand->id;
                        }
                    }


                    $product->type_id = $productTypeId;
//                foreach ($tovar->Группы->Ид as $gr) {
//                    if (empty($groupMap[(string)$gr])){
//                        dd($tovar);
//                    }
//
//                    $product->type_id = $groupMap[(string)$gr];
//                    $product->external_section_id = (string)$gr;
//                }

                    /** @var \ProductImage $images */
                    $images = [];
                    if ((string)$tovar->Картинка) {
                        $path = '/exchange/' . (string)$tovar->Картинка;
                        $fullPath = base_path($path);

                        if (\ProductImage::whereFrom($path)->count() == 0) {
                            $image = \ProductImage::makeFromTemp($fullPath, $product);
                            $image->from = $path;
//                            $image->source = 'exchange';
                            if (! file_exists($fullPath)) {
                                throw new Exception('Image not found: [' . $fullPath . ']');
                            }

                            $images[] = $image;
                        }
                    }

                    $propValues = [];
                    foreach ($tovar->ЗначенияСвойств->ЗначенияСвойства as $prop) {
                        $propId = (string)$prop->Ид;
                        $value = (string)$prop->Значение;
                        switch ($propId) {
                            case 'ddd3e699-47f6-11e1-aac8-001517b9f1e1':
                                $product->model_name = $value;
                                break;
                            case '3f51e847-41a7-11e1-b6c3-001517b9f1e1'://скидка запрещена
                                $product->discount_restricted = 1;
                                break;
                            case '44a008ae-d88d-11e0-80ad-f46d0405f555':
                                if ($value && ($option = \AttributeOption::whereXmlId($value)->first())) {
                                    if (strpos($option->name, date('Y')) !== false) {
                                        $product->new = 1;
                                    }
                                }
                                break;
                        }

                        if ($value != '')
                            $propValues[$propId][] = $value;
                    }

                    foreach ($propValues as $propId => $values) {
                        if ($attr = \Attribute::whereXmlId($propId)->first()) {
                            $attrId = $attr->id;

                            if ($attr->type_id == \AttributeType::SELECT) {
                                $product->setProperty($attrId, $attr->options()->whereIn('xml_id', $values)->pluck('id', 'id')->all());
                            }
                            else if (
                                $attr->type_id == \AttributeType::STRING
                                OR $attr->type_id == \AttributeType::NUMBER
                            ) {
                                $product->setProperty($attrId, $values);
                            }
                        }
                    }

                    \DB::connection()->transaction(function() use ($product, $images) {
                        $product->save();
                        $product->saveProperties();

                        foreach ($images as $img) {
                            $img->product_id = $product->id;
                            $img->save();
                            $img->warmUp();
                        }

                        echo(sprintf('done, [url=%s]' . "\n", $product->getUrl(true)));
                    });
                }
            }
        }
        catch (\Exception $e) {
            dump($e->getMessage());
            return;
        }
    }
    public function parseFile($path)
    {
        if (strpos($path, 'import') !== false) {
            $this->parseImportFile($path);
        }
        if (strpos($path, 'rests') !== false) {
            $this->parseRestsFile($path);
        }
        if (strpos($path, 'prices') !== false) {
            $this->parsePricesFile($path);
        }
    }
}