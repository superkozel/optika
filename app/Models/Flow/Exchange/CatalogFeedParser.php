<?php

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 28.02.2017
 * Time: 20:49
 */
namespace Flow\Exchange;

class CatalogFeedParser
{
    public function start()
    {
        $path = 'exchange/data-lenses.xml';

        $this->parseFile($path);

        $path = 'exchange/data-contact.xml';

        $this->parseFile($path);
    }

    protected function parseFile($path)
    {
        $reader = new \XMLReader();
        $reader->open(base_path($path)); // указываем ридеру что будем парсить этот файл

        $whiteList = [
            'KOEFFICIENT_PRELOMLENIYA' => 'К.Преломления',
            'MATERIAL' => 'Материал',
            'POKRYTIE_LINZY' => 'ПокрытияЛинзы',
            'GEOMETRIYA_LINZY' => 'ГеометрияЛинзы',
            'OPTICHESKAYA_SILA_SFERA' => 'Оптическая сила (СФЕРА)',
            'TSILINDR' => 'Цилиндр',
            'STRAN' => 'Страна происхождения',
            'ADDIDACIYA' => 'Аддидация',
            'NAZNACHENIE' => 'Назначение',
            'TIP_LINZY' => 'Тип линзы',
            'KATEGORIYA' => 'Категория',
            'Режим замены',
            'Ось (ax)',
            'Срок оповещения (дн.)',
        ];
        $blackList = [
            'Скидка запрещена'
        ];
        $i = 0;
        $limit = 1000;
        while($reader->read()) {
            if ($i > 1000)
                break;
//            if ($reader->nodeType == XMLReader::ELEMENT && $reader->localName == 'asdasd') {
//                $el = $reader->expand();
//                $doc = new DOMDocument();
//                $xml = simplexml_import_dom($doc->importNode($el, true));
//
//                $attr = 123;
//            }

            if ($reader->nodeType == \XMLReader::ELEMENT && $reader->localName == 'goods') {
                $i++;
                $el = $reader->expand();
                $doc = new \DOMDocument();
                $xml = simplexml_import_dom($doc->importNode($el, true));

                /** @var \Product $product */
                $product = \Product::whereExternalId($xml->uid)->first();
                if (! $product) {
                    $product = new \Product();
                    $product->external_id = (string)$xml->uid;
                    $product->name = (string)$xml->name;

                    if (! $type = \ProductType::findByUid((string)$xml->type->uid)) {
                        dump('Unknown product type uid[' . (string)$xml->type->uid . '] name=[' . (string)$xml->type->name . '] product = [' . $product->name . ']');
                        continue;
                    }
                    $product->type_id = $type->getId();
                }

                $product->available = (int)$xml->balance + 1;
                $product->price = (float)$xml->price;
                $product->old_price = (float)$xml->old_price;

                $attributeSet = $product->type->getAttributeSet();
                if (! $attributeSet) {
                    $attributeSet = new \AttributeSet();
                    $attributeSet->name = $product->type->name;
                    $attributeSet->product_type_id = $product->type_id;
                    $attributeSet->save();
                }

                foreach ($xml->attributes_array->attribute as $xmlAttr) {
                    /** @var Attribute $attribute */
                    $attr = \Attribute::whereXmlId($xmlAttr->uid_attribute)->first();

                    if (! $attr) {
                        $uid = (string)$xmlAttr->uid_attribute;
                        if ($uid == "3f51e847-41a7-11e1-b6c3-001517b9f1e1" OR $uid ==  "3d759577-5d31-11e1-8588-001517b9f1e1") {//скидка запрещена
                            continue;
                        }
                        else if (in_array((string)$xmlAttr->name, $whiteList)) {
                            $attr = new \Attribute();
                            $attr->name = (string)$xmlAttr->name;
                            $attr->xml_id = (string)$xmlAttr->uid_attribute;
                            if ($xmlAttr->uid_attribute) {
                                $attr->type_id = \AttributeType::SELECT;
                            }
                            else {
                                $attr->type_id = \AttributeType::STRING;
                            }

                            $attr->save();
                        }
                        else {
                            dump('unknown attribute: ');
                            dump($xmlAttr);
//                            dd();
                            continue;
                        }
                    }

                    if ($attr->type_id == \AttributeType::SELECT) {
                        $option = $attr->options()->whereXmlId((string)$xmlAttr->uid_value)->first();
                        $valueName = (string)$xmlAttr->value;
                        if (! $option && $valueName) {
                            if ((string)$xmlAttr->uid_value){
                                $option = $attr->options()->create([
                                    'name' => $valueName,
                                    'xml_id' => (string)$xmlAttr->uid_value,
                                ]);
                            }
                            else {

                            }
                        }

                        if ($option)
                            $product->setProperty(
                                $attr->id,
                                $option->id
                            );
                    }
                    else if (
                        $attr->type_id == \AttributeType::STRING
                        OR $attr->type_id == \AttributeType::NUMBER
                    ) {
                        $product->setProperty($attr->id, (string)$xmlAttr->value);
                    }

                    if ($attributeSet)
                        $attributeSet->attrs()->syncWithoutDetaching([$attr->id]);
                }

                $images = [];
                \DB::connection()->transaction(function() use ($product, $images) {
                    $product->save();
                    $product->saveProperties();

                    foreach ($images as $img) {
                        $img->product_id = $product->id;
                        $img->save();
                    }
                });
            }
        }
    }
}