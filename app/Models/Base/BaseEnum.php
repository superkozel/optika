<?php
/**
 * Created by PhpStorm.
 * User: Kirill
 * Date: 16.07.14
 * Time: 3:35
 */

abstract class BaseEnum {

	/**
	 * @var string
	 */
	protected $id;

	/**
	 * @var
	 */
	protected static $names = array();

	/**
	 * @var array
	 */
	protected static $parents = array();

	/**
	 * @param $id
	 * @return static
	 */
	public static function create($id)
	{
		if (is_null($id)){
			throw new Exception('No id provided for enum ' . get_called_class());
		}

		$class = get_called_class();

		if (! isset($class::$names[$id])){
			throw new Exception('Unknown id ['.$class.'='.$id.']');
		}

		return (new static($id))->
		setId($id);
	}
	/**
	 * @param $id
	 * @return static
	 */
	public static function find($id)
	{
		if (isset(static::$names[$id])){
			return (new static($id))->
			setId($id);
		}
	}

	/**
	 * @param string $id
	 */
	public function setId($id)
	{
		$this->id = $id;

		return $this;
	}

	public function is($platformId) {
		$platformId = func_get_args();

		if (is_array($platformId[0])){
			$platformId = $platformId[0];
		}

		return in_array($this->getId(), $platformId);
	}

	/**
	 * @return string
	 */
	public function getId()
	{
		return $this->id;
	}

	public function getName()
	{
		return static::$names[$this->getId()];
	}

	/**
	 * @return array()
	 */
	public static function getNames()
	{
		return static::$names;
	}

	/**
	 * @return static[]
	 */
	public static function all()
	{
		return static::getByIds(array_keys(static::$names));
	}

	public function getNameMany()
	{
		return self::$namesMany[$this->getId()];
	}

	/**
	 * @return ProductType
	 */
	public function getParentId()
	{
		if (! empty(self::$parents[$this->getId()]))
			return self::$parents[$this->getId()];
	}

	/**
	 * @return ProductType
	 */
	public function getParent()
	{
		if (! empty(self::$parents[$this->getId()]))
			return static::create(self::$parents[$this->getId()]);
	}

	public function getDescendants()
	{

	}

	public function getChildren()
	{
		$parent = $this;

		return static::getByIds(array_keys(array_filter(self::$parents, function($v) use ($parent) {
			return $v == $parent->getId();
		})));
	}

	public static function getByIds($ids)
	{
		$class = get_called_class();

		return array_map(
			function($v) use ($class){
				return $class::create($v);
			},
			$ids
		);
	}

	public static function getRoots()
	{
		$roots = array();

		foreach (static::all() as $c) {
			if ($c->getParentId() == false)
				$roots[] = $c;
		}

		return $roots;
	}

	public function __get($name)
	{
		$method = 'get' . camel_case($name);

		if (method_exists($this, $method)) {
			return call_user_func([$this, $method]);
		}
	}

	public function __toString()
    {
        return (string)$this->getId();
    }
} 