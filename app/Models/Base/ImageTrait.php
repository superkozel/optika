<?php

use Intervention\Image\ImageManagerStatic;

trait ImageTrait
{
	public function getSavePath()
	{
		throw new Exception(__CLASS__. '::' . __METHOD__ . ' not implemented');
	}

	public function getSizePath($size)
	{
		throw new Exception(__CLASS__. '::' . __METHOD__ . ' not implemented');
	}

	public function getOwnerType()
	{
		throw new Exception(__CLASS__. '::' . __METHOD__ . ' not implemented');
	}

	public function getPropertyName()
	{
		throw new Exception(__CLASS__. '::' . __METHOD__ . ' not implemented');
	}

	/**
	 * @param null $image
	 * @param null Eloquent $owner
	 * @return static
	 */
	public static function make($image = null, Eloquent $owner = null)
	{
		$itemImage = new static();

		if ($owner)
			$itemImage->setOwner($owner);

		if ($image) {
			$image->resize(1300, 1300, function ($constraint) {
				$constraint->aspectRatio();
				$constraint->upsize();
			});

			$itemImage->_unsavedImage = $image;
		}

		return $itemImage;
	}


	/**
	 * Кодировать url для file_get_contents
	 * @param $url
	 * @return string
	 */
	public static function formatUrl($url)
	{
		$parts = parse_url($url);
		$path_parts = array_map('rawurldecode', explode('/', $parts['path']));

		$puny = new Punycode();

		return
			$parts['scheme'] . '://' .
			$puny->encode($parts['host']) .
			implode('/', array_map('rawurlencode', $path_parts))
			;
	}

	public static function makeFromUrl($url, $owner)
	{
		$image = ImageManagerStatic::make(static::formatUrl($url));

		$itemImage = static::make($image, $owner);

		return $itemImage;
	}

	public static function makeFromTemp($path, $owner)
	{
		$image = ImageManagerStatic::make($path);

		$itemImage = static::make($image, $owner);

		return $itemImage;
	}

	public static function makeFromUpload(Upload $upload, $owner)
	{
		$image = ImageManagerStatic::make($upload->getSavePath());

		$itemImage = static::make($image, $owner);

		return $itemImage;
	}

	public function stub()
	{
		return '/images/noimage.png';
	}

	public function getStubSizePath($size)
	{
		$props = array_get(static::$sizes, $size);

		$width = ! empty($props[0]) ? $props[0] : null;
		$height = ! empty($props[1]) ? $props[1] : null;
		$fill = ! empty($props[2]);

		$suffix = '';
		if ($width && $height) {
			$suffix = $width . 'x' . $height;
		}
		else if ($width) {
			$suffix = $width;
		}
		else if ($height) {
			$suffix = $height;
		}

		if ($fill)
			$suffix .= 'f';

		$stubImage = $this->stub();
		return 'i/resize/' . pathinfo($stubImage, PATHINFO_BASENAME) . '_' . $suffix . '.jpg';
	}

	public function thumb()
	{
		$path = $this->getFullPath();

		$ip = new \Intervention\Image\ImageManagerStatic();

		if (! is_readable($path)) {
            $path = public_path($this->stub());
        }
		$image = $ip->make($path);
		$image->fit(80, 80);

		return $image
			->encode('data-url')
			->getEncoded();
	}

	public function getUrl($size = null, $absolute = true)
	{
		$props = $this->getSize($size);

		$imageFullPath = $this->getFullPath();

		if (empty($this->path) OR ! file_exists($imageFullPath)) {
			$imageFullPath = public_path($this->stub());
			$savePath = $this->getStubSizePath($size);
		}
		else {
			$savePath = $this->getSizePath($size) . '.' . pathinfo($imageFullPath, PATHINFO_EXTENSION);
		}

		if (! file_exists(public_path($savePath)) OR filemtime(public_path($savePath)) < filemtime($imageFullPath)) {
			try {
				$dir = pathinfo(public_path($savePath), PATHINFO_DIRNAME);

				if (! file_exists($dir))
					mkdir($dir, 0777, true);

				$w = $props[0];
				$h = ! empty($props[1]) ? $props[1] : null;
				$fill = ! empty($props[2]);
				$quality = 100;

				$img = new \Imagick($imageFullPath);
				$img->setImageCompressionQuality($quality);
				if (! empty($w) && ! empty($h)) {
					if (! empty($fill))
						$img->thumbnailImage($w, $h, true, true);
					else
						$img->cropThumbnailImage($w, $h);
				}
				else if (! empty($w)) {
					$img->thumbnailImage($w, null);
				}
				else if (! empty($h)) {
					$img->thumbnailImage(null, $h);
				}
				else {
//					throw new Exception('cannot resize image with no options');
				}

				$img->writeImage(public_path($savePath));
				$img->clear();
				$img->destroy();

//				$manager = new \Intervention\Image\ImageManager();
//				$image = $manager->mxake($imageFullPath);
//				dd($image->getDriver());x
//
//				try {
//					if ($fill) {
//						$image->resize($width, $height, function ($constraint) {
//							$constraint->aspectRatio();
//						});
//					}
//					else {
//						if ($width && $height) {
//							$image->fit($width, $height);
//						}
//						else if ($width){
//							$image->widen($width);
//						}
//						else if ($height){
//							$image->heighten($height);
//						}
//					}
//
//					$image->resizeCanvas($width, $height)->
//					save(public_path($savePath))
//					;
//
//					$image->destroy();
//				}
//				catch (Exception $e) {
//					$image->destroy();
//				}

				unset($image);

				return '/' . $savePath;
			}
			catch (Exception $e) {
				throw ($e);
				return $this->stub();
			}
		}
		else {
			return URL::to('/' . $savePath);
		}
	}

	public function getSize($size)
	{
		return static::$sizes[$size];
	}

	public static function img($image, $size = null, $alt = null, $attrs = array())
	{
		$class = get_called_class();
		if (! $image) {
            $image = new $class();
        }

        $image->getSize($size);

		$sizeOptions = $image->getSize($size);
        $url = $image->getUrl($size);

//		$attrs['width'] = $sizeOptions[0];

//		if (! empty($sizeOptions[1]))
//			$attrs['height'] = $sizeOptions[1];

		return HTML::image($url, $alt, $attrs);
	}

	public function getFullPath()
	{
		return base_path($this->path);
	}

	/**
	 * @param string $path
	 * @return $this
	 */
	public function setPath($path)
	{
		$this->path = $path;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getPath()
	{
		return $this->path;
	}

	public function getOwner()
	{
		return $this->{$this->getOwnerType()};
	}

    public function storeImageFile()
    {
        $image = $this;
        $savePath = $image->getSavePath() . '.' . strtolower($image->_unsavedImage->extension);
        $fullSavePath = base_path($savePath);

        $dir = pathinfo($fullSavePath, PATHINFO_DIRNAME);

        if (! file_exists($dir))
            mkdir($dir, 0777, true);

        $image->_unsavedImage->save($fullSavePath);
        $image->_unsavedImage->destroy();

        $image->setPath($savePath);

        return $image;
    }

    public function deleteImageFile()
    {
        if (file_exists($this->getFullPath()))
            unlink($this->getFullPath());
    }
}