<?php
/**
 * Created by PhpStorm.
 * 
 * User: LittlePoo
 * Date: 03.11.2015
 * Time: 13:10
 *
 * @mixin \Eloquent
 */

use Intervention\Image\ImageManagerStatic;

class EloquentImage extends Eloquent
{
	use ImageTrait;

	/**
	 * @var \Intervention\Image\Image
	 */
	protected $_unsavedImage;

	/**
	 * удалить файл вместе с картинкой
	 */
	public static function boot()
	{
		static::deleting(function($image){
            $image->deleteImageFile();
		});

		static::saving(function(EloquentImage $image){
			if ($image->_unsavedImage) {
                $image->storeImageFile();
			}
		});

		parent::boot();
	}

	public function getSizePath($size)
	{
		$objectType = strtolower($this->getOwnerType());

		return 'i/resize/' . $objectType . '/' . $this->id . '_' . $this->getPropertyName() . '_' . $size;
	}

	public function toArray()
	{
		$data = [];

		foreach (static::$sizes as $sizeName => $size) {
			$data[$sizeName] = $this->getUrl($sizeName);
		}

		return $data;
	}

	public function setOwner($owner)
	{
		$this->{$this->getOwnerType()}()->associate($owner);

		return $this;
	}

	public function warmUp()
    {
        foreach (static::$sizes as $k => $v) {
            $this->getUrl($k);
        }
    }
}