<?php

/**
 * Created by PhpStorm.
 * User: LittlePoo
 * Date: 03.11.2015
 * Time: 13:22
 */
class BaseImage
{
	use ImageTrait;

	/**
	 * @var Eloquent
	 */
	protected $model;

	/**
	 * @var string
	 */
	protected $path;

	public function __construct($model = null)
	{
		return $this->setOwner($model);
	}

	public static function create($model)
	{
		return new static($model);
	}

	/**
	 * @param Item $model
	 * @param $url
	 */
	public static function createFromUrl($model, $url)
	{
		$image = ImageManagerStatic::make(str_replace(array("%3A", "%2F"),array(":", "/"),urlencode($url)));

		$image->resize(1300, 1300, function ($constraint) {
			$constraint->aspectRatio();
			$constraint->upsize();
		});

		$itemImage = static::create($model);

		$image->save(public_path($itemImage->getSavePath()));

		$itemImage->setPath($itemImage->getSavePath());

		return $itemImage;
	}

//	public static function createFromUpload($model, $path)
//	{
//		$image = ImageManagerStatic::make();
//	}

	public static function createFromTemp($model, $path)
	{
		$image = static::create($model);

		$fileName = '/i/item/item_' . $model->id;

		move_uploaded_file($path, public_path($fileName));

		return $image->setPath($fileName);
	}

	public function getPropertyName()
    {
        return 'image';
    }

    /**
	 * удалить картинку
	 * @return bool
	 */
	public function delete()
	{
		foreach(static::$sizes as $sizeName => $dimension) {
			$sizePath = public_path($this->getSizePath($sizeName));

			if (file_exists($sizePath))
				unlink($sizePath);
		}

		if (file_exists(public_path($this->path)))
			unlink(public_path($this->path));

		$this->getOwner()->image = null;

		$this->getOwner()->save();

		return true;
	}

	public function setOwner($owner)
	{
		$this->{$this->getOwnerType()} = $owner;
	}

	public function save()
    {
        $this->storeImageFile();
        $this->getOwner()->{$this->getPropertyName()} = $this->getPath();
        $this->getOwner()->save();

        return $this;
    }

    public function getSavePath()
    {
        return 'storage/images/' . strtolower($this->getOwnerType()) . '/' . $this->getOwner()->id;
    }

    public function getSizePath($size)
    {
        return 'i/resize/' . strtolower($this->getOwnerType()) . '/' . $this->getOwner()->id . '_' . $size;
    }
}