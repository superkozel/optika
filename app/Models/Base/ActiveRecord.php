<?php

/**
 * Created by PhpStorm.
 * 
 * User: Superkozel
 * Date: 09.02.2017
 * Time: 11:21
 *
 * @mixin \Eloquent
 */
class ActiveRecord extends Eloquent
{
    public function getRelationForeignKey($relation)
    {
        return (with(new static)->{$relation}())->getForeignKey();
    }

    public function getRelationModel($relation)
    {
        $className = (get_class(with(new static)->{$relation}()->getRelated()));
        return new $className;
    }

    public function getEditUrl($parameters = [], $absolute = false)
    {
        return actionts('Admin' . (new \ReflectionClass(static::class))->getShortName() .  'Controller@edit', array_replace($parameters, ['id' => $this->id]), $absolute);
    }

    public static function search($filters = [])
    {
        $select = static::select()->orderBy('id', 'DESC');
        $columns = Schema::getColumnListing(static::getModel()->getTable());
        foreach ($filters as $k => $v) {
            if ($v == '-')
                continue;

            if (in_array($k, $columns)) {
                $select->where($k, $v);
            }
        }

        return $select;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getDeleteUrl($parameters = [], $absolute = false)
    {
        return actionts('Admin' . (new \ReflectionClass(static::class))->getShortName() .  'Controller@delete', array_replace($parameters, ['id' => $this->id]), $absolute);
    }
}