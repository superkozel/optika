<?php

class StaticPage {

	/**
	 * @var int
	 */
	protected $id;

	/**
	 * @var string
	 */
	protected $title;

	/**
	 * @var string
	 */
	protected $h1;

	/**
	 * @var string
	 */
	protected $metaDescription;

	/**
	 * @var string
	 */
	protected $keywords;

	/**
	 * @var string
	 */
	protected $crumb;

	/**
	 * @var string
	 */
	protected $file;

	/**
	 * @var string
	 */
	protected $url;

	/**
	 * @var string
	 */
	protected $parentId;

	/**
	 * @var string
	 */
	protected $layout;

	/**
	 * @return \Breadcrumbs
	 */
	public function getBreadcrumbs()
	{
		$currentPage = $this;
		$breadcrumbs = new Breadcrumbs();

		$crumbs = array();
		while($currentPage->getParent()){
			$currentPage = $currentPage->getParent();

			$crumbs[] = array($currentPage->getCrumb(), url($currentPage->getUrl()));
		}

		$crumbs[] = array($this->getCrumb(), url($this->getUrl()));

		foreach ($crumbs as $crumb) {
			$breadcrumbs->add($crumb[0], $crumb[1]);
		}

		return $breadcrumbs;
	}

	/**
	 * @param string $file
	 */
	public function setFile($file)
	{
		$this->file = $file;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getFile()
	{
		return $this->file;
	}

	/**
	 * @param string $h1
	 */
	public function setH1($h1)
	{
		$this->h1 = $h1;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getH1()
	{
		return $this->h1;
	}

	/**
	 * @param string $keywords
	 */
	public function setKeywords($keywords)
	{
		$this->keywords = $keywords;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getKeywords()
	{
		return $this->keywords;
	}

	/**
	 * @param string $metaDescription
	 */
	public function setMetaDescription($metaDescription)
	{
		$this->metaDescription = $metaDescription;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getMetaDescription()
	{
		return $this->metaDescription;
	}

	/**
	 * @param string $title
	 */
	public function setTitle($title)
	{
		$this->title = $title;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * @var Page[]
	 */
	protected static $list;

	/**
	 * @param Page $page
	 */
	public static function addPage(StaticPage $page)
	{
		self::$list[] = $page;
	}

	/**
	 * @return static
	 */
	public static function create()
	{
		return new static;
	}

	/**
	 * @param string $crumb
	 */
	public function setCrumb($crumb)
	{
		$this->crumb = $crumb;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getCrumb()
	{
		return $this->crumb ? $this->crumb : $this->h1;
	}

	/**
	 * @param string $url
	 */
	public function setUrl($url)
	{
		$this->url = $url;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getUrl()
	{
		if ($this->url != '/')
			return '/' . $this->url;
		else
			return $this->url;
	}

	/**
	 * @return StaticPage[]
	 */
	public static function all()
	{
		return self::$list;
	}

	/**
	 * @param mixed $parentId
	 * @return Page
	 */
	public function setParentId($parentId)
	{
		$this->parentId = $parentId;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getParentId()
	{
		return $this->parentId;
	}

	/**
	 * @return mixed
	 */
	public function getParent()
	{
		return self::getById($this->getParentId());
	}

	/**
	 * @param $id
	 * @return StaticPage
	 */
	public static function getById($id){
		foreach (self::$list as $page) {
			if ($page->getId() == $id)
				return $page;
		}
	}

	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param string $layout
	 */
	public function setLayout($layout)
	{
		$this->layout = $layout;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getLayout()
	{
		return $this->layout;
	}

	/**
	 * @return Page[]
	 */
	public function getChildren()
	{
		$children = array();

		foreach ($this->all() as $p) {
			if ($p->getParentId() == $this->getId())
				$children[] = $p;
		}

		return $children;
	}

	/**
	 * @return Page[]
	 */
	public static function getRoots()
	{
		$roots = array();

		foreach (self::$list as $page) {
			if (! $page->getParentId()) {
				$roots[] = $page;
			}
		}

		return $roots;
	}

	public static function link($id, $htmlOptions = array(), $label = null)
	{
		if (! $page = StaticPage::getById($id))
			throw new Exception('page not found');

		if (! $label)
			$label = $page->getCrumb();

		return HTML::link($page->getUrl(), $label, $htmlOptions);
	}

	public static function url($id)
	{
		if (! $page = StaticPage::getById($id))
			throw new Exception('page not found');

		return $page->getUrl();
	}

	public function getContent()
	{
		return View::make('front.pages.' . $this->getFile(), array(
			'h1' => $this->getH1(),
			'page' => $this
		));
	}

	public static function findByUrl($url)
	{
		foreach (self::$list as $page) {
			if ($page->url == $url)
				return $page;
		}
	}
}


StaticPage::addPage(StaticPage::create()->
	setId('home')->
	setTitle('Сайт о доставке еды: рейтинг доставок, отзывы и обзоры')->
	setH1('Самые честные рейтинг доставки еды')->
	setFile('home')->
	setLayout('empty')->
	setUrl('/')
);

StaticPage::addPage(StaticPage::create()->
	setId('o-nas')->
	setTitle('О нас')->
	setH1('О нас')->
	setFile('o-nas')->
	setUrl('o-nas')->
	setLayout('page')
);

StaticPage::addPage(StaticPage::create()->
	setId('bonusi')->
	setTitle('Система бонусов')->
	setH1('Система бонусов')->
	setFile('bonusi')->
	setUrl('bonusnaya-sistema')->
	setLayout('page')
);

StaticPage::addPage(
	StaticPage::create()->
	setId('kontakty')->
	setTitle('Контакты')->
	setH1('Контакты')->
	setFile('kontakty')->
	setUrl('kontakty')->
	setLayout('page')
);

StaticPage::addPage(
	StaticPage::create()->
	setId('vladelcu')->
	setTitle('Владельцу доставки')->
	setH1('Владельцу доставки')->
	setFile('vladelcu')->
	setUrl('vladelcu-dostavki')->
	setLayout('page')
);

StaticPage::addPage(
	StaticPage::create()->
	setId('reklama-na-saite')->
	setTitle('Реклама на сайте')->
	setH1('Реклама на сайте')->
	setFile('reklama-na-saite')->
	setUrl('reklama-na-saite')->
	setLayout('page')
);

