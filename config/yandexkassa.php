<?php
return [
    'testmode' => env('YANDEXKASSA_TESTMODE'),
    'shopId' => env('YANDEXKASSA_SHOP_ID'),
    'scid' => env('YANDEXKASSA_SCID'),
    'password' => env('YANDEXKASSA_PASSWORD'),
];