<?php

IF ($_SERVER['HTTP_HOST'] == 'superk.optika-favorit.ru') {
    \App::abort(301, '', ['Location' => 'http://optika-favorit.ru']);
}

\Carbon\Carbon::setLocale(config('app.locale'));
setlocale(LC_TIME, 'ru_RU.utf8');

Auth::routes();
Route::any('/login/code/', 'Auth\LoginController@showOneOffCodeForm');

if (Schema::hasTable('brands')){
    foreach (Brand::all() as $brand) {
        Route::get('/' . $brand->slug . '/', function() use ($brand){
            return Redirect::to($brand->getUrl(), 301);
        });
    }
}

Route::get('robots.txt', function() {
    $data =
        "User-agent: *
" . (str_replace('www.', '', $_SERVER["HTTP_HOST"]) != 'optika-favorit.ru' ? "Disallow: *\n" : "") . "
Disallow: /admin
Host: optika-favorit.ru
Sitemap: http://optika-favorit.ru/sitemap.xml";
    return Response::make($data)->header('Content-type', 'text');
});

if (Schema::hasTable('pages') && Schema::hasColumn('pages', 'disabled')){
    foreach (Page::where('disabled', '0')->get() as $page) {
        Route::get($page->getUrl(), function() use ($page){
            $co = new \App\Http\Controllers\BaseFrontendController();

            return $co
                ->getLayout($page->layout ? $page->layout : 'page', ['page' => $page])
                ->setTitle($page->getTitle())
                ->setH1($page->getH1())
                ->setMetaDescription($page->getMetaDescription())
                ->setBreadcrumbs($page->getBreadcrumbs())
                ->setContent($page->getContent())
                ->render()
                ;
        });
    }
}

Route::get('/', 'IndexController@index');
Route::group(['prefix' => 'catalog'], function () {
    Route::any('/ajax', 'CatalogController@ajax');
    Route::get('{any?}', 'CatalogController@index')->where('any', '.*');
});

Route::get('/brands/', 'BrandController@index');

Route::get('/product/{id}/availability/', 'ProductController@availability');
Route::get('/product/{id}/configurate', 'ProductController@configurate');
Route::get('/product/{id}/', 'ProductController@show');

Route::get('/salons/', 'SalonController@index');
Route::get('/salons/{id}/', 'SalonController@show')->where('id', '[0-9]+');
Route::get('/salons/{id?}/', 'SalonController@index');
Route::get('/salons/{id}/favorite', 'SalonController@favorite');
Route::get('/salons/{id}/unfavorite', 'SalonController@unfavorite');

Route::get('/about_us/news/', 'NewsController@index');
Route::get('/about_us/news/ajax', 'NewsController@ajax');
Route::get('/about_us/news/{slug}/', 'NewsController@show');

Route::get('/shares/', 'ActionController@index');
Route::get('/shares_archive/', 'ActionController@archive');
Route::get('/shares/{id}/', 'ActionController@show');

Route::get('/brands/', 'BrandController@index');
Route::get('/brands/{id}/', 'BrandController@show');

Route::get('/uslugi/', 'ServiceController@index');
Route::get('/uslugi/{id}/', 'ServiceController@view');

Route::group(['prefix' => 'basket'], function () {
    Route::any('/', 'BasketController@index');
    Route::any('/add/', 'BasketController@add');
    Route::any('/add_lenses/', 'BasketController@addLenses');
    Route::any('/remove_lenses/', 'BasketController@removeLenses');
    Route::any('/update_modifier', 'BasketController@updateModifier');
    Route::any('/update/', 'BasketController@update');
    Route::any('/remove/', 'BasketController@remove');
    Route::get('/drop/', 'BasketController@drop');
    Route::get('/checkout/', 'BasketController@getCheckout');
    Route::post('/checkout/', 'BasketController@checkout');
});

Route::group(['prefix' => 'order'], function () {
    Route::get('/thanks/', 'OrderController@thanks');
    Route::get('/{id}/view/', 'OrderController@view')->where('id', '[0-9]+');
    Route::get('/{id}/load/', 'OrderController@load')->where('id', '[0-9]+');
    Route::get('/{id}/cancel/', 'OrderController@cancel')->where('id', '[0-9]+');
    Route::get('/{id}/renew/', 'OrderController@renew')->where('id', '[0-9]+');
    Route::get('/{id}/pay/', 'OrderController@pay')->where('id', '[0-9]+');
    Route::get('/{id}/repeat/', 'OrderController@repeat')->where('id', '[0-9]+');
});

Route::group(['prefix' => 'favorite'], function () {
    Route::get('/', 'FavoriteController@index');
    Route::post('/add/', 'FavoriteController@add');
    Route::post('/remove/', 'FavoriteController@remove');
});

Route::group(['prefix' => 'fitting'], function () {
    Route::get('/', 'FittingController@index');
    Route::post('/add/', 'FittingController@add');
    Route::post('/remove/', 'FittingController@remove');
});

Route::group(['prefix' => 'user'], function () {
    Route::post('/code/send/', 'UserController@sendSmsCode');
    Route::get('/confirm_email/', 'UserController@confirmEmail');
    Route::group(['middleware' => ['auth']], function () {
        Route::get('/', 'UserController@index');
        Route::get('/bonus/', 'UserController@bonus');
        Route::post('/bonus/activate/', 'UserController@bonusActivate');
        Route::post('/bonus/send/', 'UserController@sendBonuses');
        Route::get('/orders/', 'UserController@orders');
        Route::get('/orders/{id}', 'UserController@order');
        Route::get('/profile/', 'UserController@getProfile');
        Route::post('/profile/', 'UserController@updateProfile');
        Route::get('/password/', 'UserController@getPassword');
        Route::post('/password/', 'UserController@updatePassword');
        Route::get('/confirm_email/send/', 'UserController@sendEmailConfirmation');
        Route::any('/confirm_phone/', 'UserController@confirmPhone');
    });
});

Route::post('/feedback/', 'SiteController@feedback');//форма обратной связи
Route::any('/suggest/', 'SearchController@suggest');//подсказки при поиске по каталогу

Route::any('/app/', function(){
    return Redirect::to('http://old.optika-favorit.ru' . Request::server('REQUEST_URI'));
});

\App\Http\Controllers\PaymentYandexKassaController::routes();

Route::group(array('prefix' => 'admin', 'middleware' => ['admin']), function(){
    Route::get('/', 'AdminController@index');

    Route::group(array('prefix' => 'upload'), function(){
        Route::post('/', 'AdminUploadController@store');
    });

    \App\Http\Controllers\AdminBannerController::routes();
    \App\Http\Controllers\AdminPageController::routes();
    \App\Http\Controllers\AdminOrderController::routes();
    \App\Http\Controllers\AdminFileController::routes();
    \App\Http\Controllers\AdminNewsController::routes();
    \App\Http\Controllers\AdminBrandController::routes();
    \App\Http\Controllers\AdminAttributeController::routes();
    \App\Http\Controllers\AdminAttributeSetController::routes();
    \App\Http\Controllers\AdminSalonController::routes();
    \App\Http\Controllers\AdminActionController::routes();
    \App\Http\Controllers\AdminCatalogFilterSetController::routes();
    \App\Http\Controllers\AdminCatalogFilterController::routes();
    \App\Http\Controllers\AdminUserController::routes();

    Route::group(array('prefix' => 'product'), function(){
        $controller = 'AdminProductController';

        Route::get('/', $controller . '@index');
        Route::get('/create/', $controller . '@create');
        Route::post('/create/', $controller . '@store');
        Route::get('/edit/{id}/', $controller . '@edit');
        Route::post('/edit/{id}/', $controller . '@update');
        Route::get('/attributes/', $controller . '@getAttributes');
    });

    Route::group(array('prefix' => 'product-type'), function(){
        $controller = 'AdminProductTypeController';

        Route::get('/', $controller . '@index');
//        Route::get('/create', $controller . '@create');
//        Route::post('/create', $controller . '@store');
//        Route::get('/edit/{id}', $controller . '@edit');
//        Route::post('/edit/{id}', $controller . '@update');
    });

    Route::group(array('prefix' => 'category'), function(){
        $controller = 'AdminCatalogCategoryController';

        Route::get('/', $controller . '@index');
        Route::get('/create/', $controller . '@create');
        Route::post('/create/', $controller . '@store');
        Route::get('/edit/{id}/', $controller . '@edit');
        Route::post('/edit/{id}/', $controller . '@update');
    });
});

Route::any('/exchange/', 'ExchangeController@index');
Route::any('/exchange/callback/', 'CallbackController@list');

//Route::get('/', function(){
//    if (true OR ! Auth::user()) {
//        $co = new \App\Http\Controllers\BaseFrontendController();
//        $page = StaticPage::getById('home');
//
//        return $co
//            ->getLayout($page->getLayout())
//            ->setTitle($page->getTitle())
//            ->setH1($page->getH1())
//            ->setMetaDescription($page->getMetaDescription())
//            ->setBreadcrumbs($page->getBreadcrumbs())
//            ->setContent($page->getContent())
//            ->render()
//            ;
//    }
//    else {
//        $controller = new \App\Http\Controllers\UserController();
//        return $controller->callAction('home', array());
//    }
//});

Validator::extend('identifier', function($attribute, $value, $parameters){
    $className = $parameters['class'];

    if (is_array($value)) {
        foreach ($value as $v) {
            if (! $className::where('id', '=', $v)->exists()) {
                $this->addError('asdasdasd');
            }
        }
    }
    else {
        if (! $className::where('id', '=', $value)->exists()) {
            $this->addError('asdasdasd');
        }
    }
});

//Validator::extend('uniqueUserPhone', function($attribute, $value, $parameters){
//    if ($value) {
//        $
//    }
//});
//Validator::extend('uniqueUserEmail', function($attribute, $value, $parameters){
//
//});

Validator::extend('auth_password', function ($attribute, $value, $parameters)
{
    $user = \Auth::user();


    return ! $user->password || \Hash::check($value, $user->password);
},
    'Неверный пароль'
);

\Validator::extend('phone', function($attribute, $value, $parameters) {
    return mb_strlen(extract_numbers($value)) == 11;
}, 'Неверный номер телефона');

\Validator::extend('unique_user_phone', function($attribute, $value, $parameters){
    $userId = array_get($parameters,0);

    return DB::table(with(new \App\User())->getTable())->where('phone', filterPhoneFormat($value))->where('id', '!=', $userId)->count() == 0;
}, 'Такой телефон уже есть в базе');

\Validator::extend('unique_user_email', function($attribute, $value, $parameters){
    $userId = array_get($parameters,0);

    return DB::table(with(new \App\User())->getTable())->where('email', trim($value))->where('id', '!=', $userId)->count() == 0;
}, 'Данный адрес email уже используется');

\Validator::extend('array_in', function($attribute, $value, $parameters) {
    if (! is_array($value))
        $value = [$value];
    foreach ($value as $v) {
        if (! in_array($v, $parameters))
            return false;
    }

    return true;
}, 'В массиве нет элемента');

\Validator::extend('available_parent', function($attribute, $value, $parameters, $validator) {
    if (empty($parameters[0]))
        throw new \Illuminate\Validation\ValidationException('Available parent validator requires class name as first parameter');

    $class = $parameters[0];
    $id = $parameters[1];
    $data = $validator->getData();

    if (! empty($id)) {
        $available = $class::find($id)->getAvailableParents()->pluck('id');
        return in_array($data['parent_id'], $available->all());
    }
    else {
        return in_array($data['parent_id'], $class::pluck('id')->all()) ;
    }

}, 'Недопустимый родитель');

Html::macro('modelList', function ($models, $attribute = 'name', $idAttribute = 'id')
{
    $result = array();

    foreach ($models as $model) {
        $result[$model->{$idAttribute}] = is_callable($attribute) ? $attribute($model) : $model->$attribute;
    }

    return $result;
});
