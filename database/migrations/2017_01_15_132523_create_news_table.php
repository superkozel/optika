<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::dropIfExists('news');
        \Schema::create('news', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->string('code')->nullable();
            $table->string('slug')->nullable();

            $table->timestamp('active_from')->nullable();
            $table->timestamp('active_to')->nullable();

            $table->text('preview_text')->nullable();
            $table->text('content')->nullable();

            $table->integer('category_id')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });

        \Schema::dropIfExists('news_images');
        \Schema::create('news_images', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('news_id');

            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->string('from')->nullable();

            $table->integer('sort')->nullable();
            $table->string('path');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::dropIfExists('news');
        \Schema::dropIfExists('news_images');
    }
}
