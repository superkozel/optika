<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatalogFilterCatalogFilterSetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::create('catalog_filter_catalog_filter_set', function (Blueprint $table) {
            $table->integer('catalog_filter_set_id');
            $table->integer('catalog_filter_id');
            $table->tinyInteger('sort')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::drop('catalog_filter_catalog_filter_set');
    }
}
