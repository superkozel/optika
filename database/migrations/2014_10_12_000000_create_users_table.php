<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');

            $table->string('login')->nullable();

            $table->string('name')->nullable();
            $table->string('lastname')->nullable();
            $table->date('birthdate')->nullable();

            $table->string('email')->unique()->nullable();
            $table->string('email_confirmed')->nullable();

            $table->string('phone')->unique()->nullable();
            $table->string('phone_confirmed')->nullable();

            $table->string('phone_confirmation_code', 6)->nullable();
            $table->dateTime('phone_confirmation_sent_at')->nullable();

            $table->boolean('bonus_program_active')->default(0);
            $table->string('bonus_card_number')->nullable();

            $table->string('password')->nullable();
            $table->string('old_password')->nullable();
            $table->string('secret')->nullable();
            $table->timestamp('last_login_at')->nullable();

            $table->boolean('admin')->default(0);
            $table->tinyInteger('role_id')->default(UserRole::CUSTOMER);

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }

}
