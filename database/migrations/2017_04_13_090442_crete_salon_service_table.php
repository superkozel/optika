<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreteSalonServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::dropIfExists('salon_services');
        \Schema::create('salon_services', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->string('file_path')->nullable();
            $table->text('description')->nullable();
        });

        \ActiveRecord\SalonService::unguard();
        foreach ([
             67304 => 'Срочное изготовление очков',
             6 => 'Изготовление очков',
             7 => 'Ремонт очков',
             8 => 'Проверка зрения',
         ] as $id => $name) {
            \ActiveRecord\SalonService::create([
                'id' => $id,
                'name' => $name
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::dropIfExists('salon_services');
    }
}
