<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBasketTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::dropIfExists('baskets');
        \Schema::create('baskets', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->nullable();
            $table->integer('order_id')->nullable();
            $table->string('session_id');
            $table->string('promocode')->nullable();
            $table->string('gift_certificate')->nullable();

            $table->timestamps();
        });

        \Schema::dropIfExists('basket_items');
        \Schema::create('basket_items', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('basket_id')->nullable();

            $table->integer('product_id');
            $table->tinyInteger('count');
            $table->float('price')->nullable();

            $table->string('modifiers')->nullable();

            $table->foreign('basket_id')
                ->references('id')->on('baskets')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::dropIfExists('basket_items');
        \Schema::dropIfExists('baskets');
    }
}
