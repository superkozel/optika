<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMetroTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::dropIfExists('metro_stations');
        \Schema::create('metro_stations', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('city_id');
            $table->tinyInteger('line_id');

            $table->string('name');

            $table->string('slug')->nullable();

            $table->string('coordinates')->nullable();
        });

        \Schema::dropIfExists('metro_lines');
        \Schema::create('metro_lines', function (Blueprint $table) {
            $table->increments('id');

            $table->string('number');

            $table->integer('city_id');

            $table->string('name');
            $table->string('color');

            $table->string('slug')->nullable();
        });

        \Schema::dropIfExists('metro_station_salon');
        \Schema::create('metro_station_salon', function (Blueprint $table) {
            $table->integer('salon_id');
            $table->integer('metro_station_id');
            $table->integer('range')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::dropIfExists('metro_stations');
        \Schema::dropIfExists('metro_lines');
        \Schema::dropIfExists('metro_station_salon');
    }
}
