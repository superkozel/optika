<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttributeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::create('attributes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('type_id');

            $table->string('xml_id')->nullable()->unique();
            $table->string('code')->nullable();

            $table->boolean('displayed')->default(1);

            $table->boolean('required')->default(0);
            $table->boolean('multiple')->default(0);
            $table->string('defaultValue')->nullable();
            $table->string('description')->nullable();

            $table->string('class')->nullable();
            $table->string('column')->nullable();

            $table->integer('measure_id')->nullable();

            $table->unsignedSmallInteger('sort')->nullable();

            $table->timestamps();

            $table->unique('code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::drop('attributes');
    }
}
