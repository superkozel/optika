<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductImageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::create('product_images', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('product_id');

            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->string('from')->nullable();

            $table->integer('sort')->nullable();
            $table->string('path');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::drop('product_images');
    }
}
