<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFavoriteSalonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::dropIfExists('favorite_salons');
        \Schema::create('favorite_salons', function (Blueprint $table) {
            $table->unsignedInteger('user_id')->nullable();
            $table->integer('salon_id');

            $table->primary(['user_id', 'salon_id']);

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::dropIfExists('favorite_salons');
    }
}
