<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryPointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::create('delivery_points', function (Blueprint $table) {
            $table->increments('id');
            $table->string('external_id');
            $table->string('source_id');
            $table->string('name');
            $table->string('city_name');
            $table->text('description');
            $table->text('address');
            $table->string('metro');
            $table->string('work_time');
            $table->string('phone');
            $table->boolean('visa');
            $table->boolean('courier');
            $table->string('country');
            $table->tinyInteger('delivery_period');
            $table->string('type')->nullable();
            $table->string('small_address');
            $table->string('area');
            $table->boolean('only_pay');
            $table->string('coords_x');
            $table->string('coords_y');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::dropIfExists('delivery_points');
    }
}
