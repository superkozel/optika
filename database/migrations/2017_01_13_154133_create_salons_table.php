<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('code')->nullable();
            $table->string('slug')->nullable();

            $table->text('mode')->nullable();
            $table->string('address')->nullable();
            $table->text('description')->nullable();

            $table->string('service_ids')->nullable();

            $table->string('phone');

            $table->integer('specialization_id');
            $table->integer('city_id')->nullable();
            $table->string('coordinates')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salons');
    }
}
