<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreteProductModifiersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::dropIfExists('product_modifiers');
        \Schema::create('product_modifiers', function (Blueprint $table) {
            $table->string('id')->primary();

            $table->integer('product_type_id');
            $table->string('name');

            $table->tinyInteger('type_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::dropIfExists('product_modifiers');
    }
}
