<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttributeSetAttributeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::create('attribute_attribute_set', function (Blueprint $table) {
            $table->integer('attribute_set_id');
            $table->integer('attribute_id');
            $table->primary(['attribute_set_id', 'attribute_id']);

            $table->boolean('main')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::dropIfExists('attribute_attribute_set');
    }
}
