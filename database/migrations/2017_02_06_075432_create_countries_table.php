<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::create('countries', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->string('xml_id')->nullable();
            $table->string('alpha2');

            $table->string('pattern');
            $table->string('pattern_z')->nullable();
            $table->string('pattern_s')->nullable();

            $table->string('slug')->nullable();

            $table->unique('name');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::dropIfExists('countries');
    }
}
