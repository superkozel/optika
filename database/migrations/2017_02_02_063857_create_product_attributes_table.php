<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::dropIfExists('products_attributes');
        \Schema::create('products_attributes', function (Blueprint $table) {;
            $table->integer('product_id');
            $table->integer('attribute_id');
            $table->string('value');

            $table->primary(['product_id', 'attribute_id']);

            $table->index(['attribute_id', 'value']);
            $table->index('value');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::dropIfExists('products_attributes');
    }
}
