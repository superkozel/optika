<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::dropIfExists('pages');
        \Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');

            $table->string('slug')->nullable();

            $table->text('path')->nullable();
            $table->string('layout')->nullable();

            $table->integer('parent_id')->nullable();

            $table->string('title');
            $table->string('h1');
            $table->string('metadesc')->nullable();
            $table->string('keywords')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::dropIfExists('pages');
    }
}
