<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::dropIfExists('actions');
        \Schema::create('actions', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->string('preview_image');

            $table->text('preview_text')->nullable();
            $table->text('content')->nullable();

            $table->date('active_from')->nullable();
            $table->date('active_to')->nullable();

            $table->string('slug')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::dropIfExists('actions');
    }
}
