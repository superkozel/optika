<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::dropIfExists('orders');
        \Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');

            $table->tinyInteger('type_id');//обычный, быстрый заказ, примерка
            $table->tinyInteger('status_id');
            $table->tinyInteger('payment_type_id');
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('manager_id')->nullable();

            $table->double('paid_sum', 10, 2)->nullable();
            $table->double('payment_required_sum', 10, 2)->nullable();

            $table->tinyInteger('courier_id')->nullable();

            $table->float('sum');
            $table->integer('bonus')->default(0);
            $table->integer('bonuses_used')->default(0);

            $table->string('name');
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('comment')->nullable();

            $table->string('additional_info')->nullable()->comment('Дополнительная информация по доставке или заказу');
            $table->string('manager_comment')->nullable()->comment('Комментарий менеджера');
            $table->string('reason')->nullable()->comment('Причина отказа');

            $table->string('promocode')->nullable();
            $table->string('gift_certificate')->nullable();

            $table->tinyInteger('delivery_type_id');
            $table->date('delivery_date')->nullable();
            $table->string('delivery_time')->nullable();
            $table->string('address')->nullable();
            $table->unsignedInteger('salon_id')->nullable();
            $table->string('token')->nullable();

            $table->timestamps();
        });

        \Schema::dropIfExists('payment_transactions');
        \Schema::create('payment_transactions', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('order_id')->unsigned();//обычный, быстрый заказ, примерка
            $table->string('transaction_id');
            $table->string('type_id');
            $table->string('payment_system_id');

            $table->foreign('order_id')->references('id')->on('orders')->onDelete('restrict');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::dropIfExists('payment_transactions');
        \Schema::dropIfExists('orders');
    }
}
