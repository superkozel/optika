<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::create('brands', function (Blueprint $table) {
            $table->increments('id');
            $table->string('xml_id')->nullable();

            $table->string('name');
            $table->string('alt_name')->nullable();
            $table->string('slug')->nullable();

            $table->unsignedSmallInteger('sort')->nullable();

            $table->text('description')->nullable();
            $table->text('short_description')->nullable();
            $table->text('sections_ids')->nullable();

            $table->integer('products_count')->nullable();
            $table->string('logo')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::drop('brands');
    }
}
