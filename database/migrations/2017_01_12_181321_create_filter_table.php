<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::create('catalog_filters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('type_id');

            $table->string('slug');
            $table->string('name')->nullable();
            $table->text('description')->nullable();

            $table->tinyInteger('value_type');
            $table->tinyInteger('sort_type');
            $table->boolean('sort_desc')->default(0);

            $table->string('value_id');

            $table->text('options')->nullable();

            $table->unique('slug');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::drop('catalog_filters');
    }
}
