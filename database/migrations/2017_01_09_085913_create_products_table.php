<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('old_id')->nullable();//ид в битриксе для 301
            $table->string('external_id');
            $table->string('external_section_id')->nullable();

            $table->string('name');
            $table->string('model_name')->nullable();
            $table->string('article')->nullable();
            $table->text('description')->nullable();
            $table->text('short_description')->nullable();

            $table->unsignedInteger('group_id')->nullable();
            $table->unsignedTinyInteger('type_id');
            $table->unsignedInteger('brand_id')->nullable();
            $table->unsignedInteger('category_id')->nullable();
            $table->smallInteger('available');

            $table->float('price')->nullable();
            $table->float('old_price')->nullable();
            $table->boolean('discount_restricted')->default(0);

            $table->boolean('hit')->default(0);
            $table->boolean('trend')->default(0);
            $table->boolean('new')->default(0);

            $table->softDeletes();
            $table->timestamps();

            $table->index('type_id');
            $table->index('brand_id');
            $table->index('group_id');
            $table->index(['group_id', 'id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::drop('products');
    }
}
