<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::dropIfExists('order_items');
        \Schema::create('order_items', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('order_id');

            $table->integer('product_id')->nullable();

            $table->string('name');
            $table->tinyInteger('count');
            $table->float('price');
            $table->float('sum');
            $table->integer('bonus');

            $table->string('modifiers')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::dropIfExists('order_items');
    }
}
