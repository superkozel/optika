<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttributeOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::create('attribute_options', function (Blueprint $table) {
            $table->increments('id');
            $table->string('xml_id')->nullable();

            $table->string('name');
            $table->string('description')->nullable();


            $table->integer('attribute_id');

            $table->string('slug')->nullable();
            $table->string('pattern')->nullable();
            $table->string('pattern_z')->nullable();
            $table->string('pattern_s')->nullable();

            $table->unsignedSmallInteger('sort')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::drop('attribute_options');
    }
}
