<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatalogCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::create('catalog_categories', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->char('name_rod', 1)->nullable();

            $table->string('slug');
            $table->integer('parent_id')->nullable();

            $table->text('description')->nullable();
            $table->string('image')->nullable();

            $table->unsignedInteger('filter_set_id')->nullable();
            $table->foreign('filter_set_id')->references('id')->on('catalog_filter_sets')->onDelete('SET NULL');

            $table->boolean('visible')->default(1);

            $table->string('h1')->nullable();
            $table->string('title')->nullable();
            $table->text('seotext')->nullable();
            $table->string('metadesc')->nullable();

            $table->unsignedInteger('products_count')->nullable();

            $table->timestamps();

            $table->index('slug');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::drop('catalog_categories');
    }
}
