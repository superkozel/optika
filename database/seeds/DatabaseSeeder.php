<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeed::class);
//        $this->call(OrderSeed::class);

        $this->call(MetroSeed::class);
        $this->call(SalonSeed::class);

        $this->call(NewsSeed::class);

        $this->call(PageSeed::class);
        $this->call(ActionSeed::class);

        $this->call(CountrySeed::class);
        $this->call(ProductModifierSeed::class);
        $this->call(ProductSeed::class);
        $this->call(BrandSeed::class);

        $this->call(CatalogLensesSeed::class);//линзы

        $this->call(CatalogSeed::class);
    }
}
