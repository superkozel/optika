<?php

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 15.01.2017
 * Time: 16:54
 */
class ActionSeed extends \Illuminate\Database\Seeder
{
    public function run()
    {
        $path = 'import/akcii.xml';

        $this->importFile($path);
    }

    public function importFile($path)
    {
        $string = file_get_contents(base_path($path));

        $string = utf8_for_xml($string);
        $xml = simplexml_load_string($string);

        foreach ($xml->Каталог->Товары->Товар as $tovar) {

            echo (sprintf('impoting action[id=%s]' . "\n", (string)$tovar->Ид));

            if (! $action = Action::find($tovar->Ид)) {
                $action = new Action();
                $action->id = (string)$tovar->Ид;
            }

            $imageSrc = null;
            $action->name = (string)$tovar->Наименование;
            foreach ($tovar->ЗначенияСвойств->ЗначенияСвойства as $prop) {
                $value = (string)$prop->Значение;
                switch ($prop->Ид) {
                    case 'CML2_CODE':
                        $action->slug = $value;
                        break;
                    case 'CML2_ACTIVE':
                        if ($value != 'true') {
                            $action->delete();
                        }
                        break;
                    case 'CML2_PREVIEW_PICTURE'://картинки
                        if ($value) {
                            $imageSrc1 = $imageSrc = 'import/' . $value;
                        }
                        break;
                    case 'CML2_ACTIVE_FROM':
                        if ($value) {
                            $action->setCreatedAt($value);
                            $action->active_from = $value;
                        }
                        break;
                    case 'CML2_ACTIVE_TO':
                        if ($value)
                            $action->active_to = $value;
                        break;
                    case 'CML2_PREVIEW_TEXT':
                        $action->preview_text = $value;
                        break;
                    case 'CML2_DETAIL_TEXT':
                        $action->content = $value;
                        break;
                    case '13':
                        $action->salons()->sync((array)$prop->Значение);
                        break;
                }
            }

//            $doc = phpQuery::newDocumentHTML($action->content);
//            $jImg = $doc->find('img');
//            if ($jImg->length > 0) {
//                $imageSrc = 'public' . parse_url($jImg->attr('src'), PHP_URL_PATH);
//            }

            if ($action->image) {
                if (! $imageSrc || ($action->image->from != $imageSrc))
                    $action->image->delete();
            }

            if ($imageSrc) {
                $imageSrc1251 = mb_convert_encoding($imageSrc, 'windows-1251', 'utf-8');
                try {
                    $image = ActionImage::makeFromTemp(base_path($imageSrc), $action);
                }
                catch (Exception $e) {
                    if (! empty($imageSrc1))
                        $image = ActionImage::makeFromTemp(base_path($imageSrc1), $action);
                    else
                        throw new Exception('image[' . $imageSrc . ']: ' . $e->getMessage());
                }
                $image->from = $imageSrc;
                $image->save();
            }

            $action->save();
        }
    }

}