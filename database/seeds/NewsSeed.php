<?php

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 15.01.2017
 * Time: 16:54
 */
class NewsSeed extends \Illuminate\Database\Seeder
{
    public function run()
    {
        $path = 'import/novosti2.xml';
        $path2 = 'import/novosti1.xml';

        $this->importFile($path, 2);
        $this->importFile($path2, 1);
    }

    public function importFile($path, $categoryId)
    {
        $string = file_get_contents(base_path($path));

        $string = utf8_for_xml($string);
        $xml = simplexml_load_string($string);

        foreach ($xml->Каталог->Товары->Товар as $tovar) {

            echo (sprintf('impoting news[id=%s]' . "\n", (string)$tovar->Ид));

            if ($news = News::find($tovar->Ид))
                continue;

            $news = new News();
            $news->name = (string)$tovar->Наименование;
            $news->id = (string)$tovar->Ид;

            foreach ($tovar->ЗначенияСвойств->ЗначенияСвойства as $prop) {
                $value = (string)$prop->Значение;
                switch ($prop->Ид) {
                    case 'CML2_CODE':
                        $news->code = $value;
                        $news->slug = $value;
                        break;
                    case 'CML2_ACTIVE':
                        if ($value != 'true') {
                            $news->delete();
                        }
                        break;
                    case 'CML2_PREVIEW_PICTURE'://картинки
                        $images = [];
                        if ($value) {
                            $fullPath = base_path('/import/' . $value);

                            $image = NewsImage::makeFromTemp(base_path('/import/' . $value), $news);
                            if (! file_exists($fullPath)) {
                                throw new Exception('Image not found: [' . $fullPath . ']');
                            }
                            $images[] = $image;
                        }
                        break;
                    case 'CML2_ACTIVE_FROM':
                        if ($value) {
                            $news->setCreatedAt($value);
                            $news->active_from = $value;
                        }
                        break;
                    case 'CML2_ACTIVE_TO':
                        if ($value)
                            $news->active_to = $value;
                        break;
                    case 'CML2_PREVIEW_TEXT':
                        $news->preview_text = $value;
                        break;
                    case 'CML2_DETAIL_TEXT':
                        $news->content = $value;
                        break;
                }
            }

            $news->category_id = $categoryId;
            $news->save();

            foreach ($images as $img) {
                $img->save();
            }
        }
    }

}