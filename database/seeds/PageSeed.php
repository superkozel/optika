<?php

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 15.01.2017
 * Time: 22:51
 */
class PageSeed extends \Illuminate\Database\Seeder
{
    public function run()
    {
        $pages = [
            [1, 'О компании', 'О компании', 'about_us', null, 'about_us'],
            [2, 'Публикации и отзывы в СМИ об "Оптика Фаворит"', 'СМИ о нас', 'smi', 1, 'smi'],
            [3, 'Группы "Оптика Фаворит" в соцсетях вконтакте, фейсбук, инстаграм', 'Мы в соцсетях', 'social', 1, 'social'],
            [4, 'Вакансии "Оптика Фаворит"', 'Вакансии', 'jobs', 1, 'jobs'],
            [5, 'Контакты "Оптика Фаворит"', 'Контакты', 'contacts', 1, 'contacts'],
            [6, 'Информация покупателю', 'Информация', 'informaciya', null, 'informaciya'],
            [7, 'Доставка', 'Доставка', 'dostavka', 6, 'dostavka'],
            [8, 'Гарантийный отдел', 'Гарантия', 'garantiya', 6, 'garantiya'],
            [9, 'Бонусная программа', 'Бонусная программа', 'bonusnaya-programma', 6, 'bonusnaya-programma'],
            [10, 'Договор-оферта держателя бонусной карты', 'Договор-оферта', 'oferta', 9, 'oferta'],
            [11, 'Проверка зрения онлайн: тест для самостоятельной проверки зрения, контраста и на дальтонизм', 'Проверка зрения онлайн', 'check', null, 'proverka-zrenija-online', 'empty'],

            [12, 'Статьи оптика', 'Статьи', 'special', null, 'special'],
            [13, 'Консультация специалиста: как ухаживать за очками', 'Как ухаживать за очками', 'special2', 12, 'konsultatsiya_spetsialista_kak_ukhazhivat_za_ochkami'],
            [14, 'Выбор очков для работа за компьютером', 'Очки для работы за компьютером', 'special3', 12, 'konsultatsiya_spetsialista_ochki_dlya_raboty_za_kompyuterom'],
            [15, 'Как правильно выбирать очки', 'Как правильно выбирать очки', 'special4', 12, 'konsultatsiya_spetsialista_kak_pravilno_vybirat_ochki'],
            [16, 'Консультация специалиста: как ухаживать за очками', 'Ультразвуковая чистка очков, замена винтов и носоупоров - бесплатно', 'special5', 12, 'ultrazvukovaya_chistka_ochkov_zamena_vintov_i_nosouporov_besplatno'],

            [17, 'Способы оплаты', 'Способы оплаты', 'oplata', 6, 'oplata'],
            [18, 'Обмен и возврат товара', 'Обмен и возврат', 'vozvrat', 6, 'vozvrat'],
            [19, 'Как подобрать очки', 'Как подобрать очки', 'kak-podobrat-ochki', 12, 'kak-podobrat-ochki'],
            [20, 'Макияж для тех, кто носит очки', 'Макияж для тех, кто носит очки', 'makiyazh', 12, 'makiyazh-dlya-teh-kto-nosit-ochki'],
            [21, 'Примерка оправ в салоне Оптика Фаворит', 'Примерка', 'try_on', null, 'try_on'],
            [22, 'Поиск по сайту', 'Поиск по сайту', 'search', null, 'search'],
            [23, 'Карта сайта', 'Карта сайта', 'sitemap', null, 'sitemap'],
            [24, 'Пункты выдачи заказов', 'Пункты выдачи заказов', 'pvz', 7, 'pvz'],
            [25, 'Политика конфиденциальности', 'Политика конфиденциальности', 'politika-konfidencialnosti', null, 'politika-konfidencialnosti']
        ];

        foreach ($pages as $pageData) {
            echo 'seeding page[' . $pageData[3] . ']' . "\n";
            if (Page::find($pageData[0]))
                continue;

            $page = new Page();
            $page->id = $pageData[0];
            $page->title = $pageData[1];
            $page->h1 = $pageData[2];
            $page->path = $pageData[3];
            $page->parent_id = $pageData[4];
            $page->slug = $pageData[5];
            $page->layout = array_get($pageData, 6);

            $page->save();
        }
    }
}