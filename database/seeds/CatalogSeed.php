<?php

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 04.01.2017
 * Time: 12:14
 */
class CatalogSeed extends \Illuminate\Database\Seeder
{


    public function run()
    {
        $this->attributeSets();
        $this->catalogCategories();
    }

    public function catalogCategories()
    {
        $filtersData = [
            ['Цена', CatalogFilterValueType::COLUMN, 'price',  CatalogFilterType::RANGE, 'price'],
            ['Бренд', CatalogFilterValueType::RELATION, 'brand',  CatalogFilterType::CLIST, 'brand', CatalogFilterSortType::ALPHANUM],
            ['Тип товара', CatalogFilterValueType::COLUMN, 'type_id',  CatalogFilterType::CLIST, 'type', CatalogFilterSortType::COUNT],
            ['Материал', CatalogFilterValueType::ATTRIBUTE, 'MATERIAL',  CatalogFilterType::CLIST, 'material', CatalogFilterSortType::COUNT],
            ['Цвет', CatalogFilterValueType::ATTRIBUTE, 'TSVET',  CatalogFilterType::CLIST, 'tsvet', CatalogFilterSortType::ALPHANUM],
            ['Принадлежность', CatalogFilterValueType::ATTRIBUTE, 'PRINADLEZHNOST',  CatalogFilterType::CLIST, 'prinadlezhnost', CatalogFilterSortType::COUNT],
            ['Конструкция', CatalogFilterValueType::ATTRIBUTE, 'KONSTRUKTSIYA',  CatalogFilterType::CLIST, 'kontrukciya', CatalogFilterSortType::COUNT],

            ['Размер рамки', CatalogFilterValueType::ATTRIBUTE, 'RAZMER_RAMKI_',  CatalogFilterType::CLIST, 'razmer-ramki', CatalogFilterSortType::NUM],
            ['Длина заушника', CatalogFilterValueType::ATTRIBUTE, 'DLINA_ZAUSHNIKA',  CatalogFilterType::CLIST, 'dlina-zaushnika', CatalogFilterSortType::NUM],
//                ['Межцентровое расстояние', CatalogFilterValueType::ATTRIBUTE, 'MEZHTSENTROVOE_RASSTOYANIE',  CatalogFilterType::CLIST, 'mezhcentrovoe-rasstoyanie', CatalogFilterSortType::NUM],
            ['Высота рамки', CatalogFilterValueType::ATTRIBUTE, 'VYSOTA_RAMKI',  CatalogFilterType::CLIST, 'visota-ramki', CatalogFilterSortType::NUM],
            ['Размер переносицы', CatalogFilterValueType::ATTRIBUTE, 'RAZMER',  CatalogFilterType::CLIST, 'razmer-perenositsi', CatalogFilterSortType::NUM],
            ['Ширина оправы', CatalogFilterValueType::ATTRIBUTE, 'SHIRINA_OPRAVY_V_MM',  CatalogFilterType::CLIST, 'shirina-opravy', CatalogFilterSortType::NUM],


            ['Коэффициент преломления', CatalogFilterValueType::ATTRIBUTE, '723fd356-1a7e-11e1-93d2-00261858a6c5',  CatalogFilterType::CLIST, null, CatalogFilterSortType::NUM],
            ['Покрытие линзы', CatalogFilterValueType::ATTRIBUTE, '723fd358-1a7e-11e1-93d2-00261858a6c5',  CatalogFilterType::CLIST, null, CatalogFilterSortType::COUNT],
            ['Геометрия линзы', CatalogFilterValueType::ATTRIBUTE, '723fd354-1a7e-11e1-93d2-00261858a6c5',  CatalogFilterType::CLIST, null, CatalogFilterSortType::COUNT],
            ['Оптическая сила(Сфера)', CatalogFilterValueType::ATTRIBUTE, 'a598fced-dde6-11e0-a19e-f46d0405f555',  CatalogFilterType::DROPDOWN, null, CatalogFilterSortType::NUM],
            ['Цилиндр', CatalogFilterValueType::ATTRIBUTE, 'a598fcf3-dde6-11e0-a19e-f46d0405f555',  CatalogFilterType::DROPDOWN, null, CatalogFilterSortType::NUM],
            ['Страна происхождения', CatalogFilterValueType::ATTRIBUTE, '7c6026c5-fe10-11e0-a774-f46d0405f555',  CatalogFilterType::CLIST, null, CatalogFilterSortType::COUNT],
            ['Аддидация', CatalogFilterValueType::ATTRIBUTE, 'ba380d42-0ac9-11e6-9f8f-0025902c4fba',  CatalogFilterType::DROPDOWN, null, CatalogFilterSortType::NUM],
            ['Назначение', CatalogFilterValueType::ATTRIBUTE, '08f2bed5-5a20-11e6-9f8f-0025902c4fba',  CatalogFilterType::CLIST, null, CatalogFilterSortType::NUM],
            ['Тип линзы', CatalogFilterValueType::ATTRIBUTE, 'd8d42ee6-0ac9-11e6-9f8f-0025902c4fba',  CatalogFilterType::CLIST, null, CatalogFilterSortType::NUM],
            ['Категория', CatalogFilterValueType::ATTRIBUTE, '723fd355-1a7e-11e1-93d2-00261858a6c5',  CatalogFilterType::CLIST, null, CatalogFilterSortType::NUM],
            ['Режим замены', CatalogFilterValueType::ATTRIBUTE, '3120a59a-2183-11e6-9f8f-0025902c4fba',  CatalogFilterType::CLIST, null, CatalogFilterSortType::COUNT],
        ];

         foreach ($filtersData as $filterData) {
            $slug = ! empty($filterData[4]) ? $filterData[4] :  ActiveRecordSlugTrait::generateSlug($filterData[0]);
            if ($filter = CatalogFilter::whereSlug($slug)->exists())
                continue;

            $valueId = $filterData[2];
            if ($filterData[1] == CatalogFilterValueType::ATTRIBUTE) {
                if ($attr = Attribute::findByCode($filterData[2]))
                    $valueId = $attr->xml_id;
                else if (! Attribute::whereXmlId($filterData[2])->exists()) {
                    dd('Attribute not found for filter id[' . $filterData[2] . ']');
                }
            }


            CatalogFilter::forceCreate([
                'name' => $filterData[0],
                'value_type' => $filterData[1],
                'value_id' => $valueId,
                'type_id' => $filterData[3],
                'slug' => $slug,
                'sort_type' => ! empty($filterData[5]) ? $filterData[5] : CatalogFilterSortType::COUNT,
            ]);
        }

        $filterSets = [

            [1, 'Базовые фильтры', [
                'price',
                'brand',
                'tsvet',
            ]],

            [2, 'Без фильтров', []],

            [3, 'Медицинские оправы', [
                'price',
                'brand',
                'tsvet',

                'material',
                'prinadlezhnost',
                'kontrukciya',

                'razmer-ramki',
                'dlina-zaushnika',
                'visota-ramki',
                'razmer-perenositsi',
                'shirina-opravy'

            ]],

            [4, 'Солнцезащитные очки', [
                'price',
                'brand',
                'tsvet',

                'material',
                'prinadlezhnost',
                'kontrukciya',
            ]],

            [5, 'Очковые линзы', [
                'price',

                'opticheskaya-sila-sfera',
                'cilindr',
                'addidaciya',

                'tsvet',
                'material',
                'strana-proishozhdeniya',
                'tip-linzy',
                'koefficient-prelomleniya',
                'pokrytie-linzy',
                'geometriya-linzy',
            ]],

            [6, 'Контактные линзы', [
                'price',

                'opticheskaya-sila-sfera',
                'cilindr',
                'addidaciya',

                'tsvet',
                'material',
                'strana-proishozhdeniya',
                'tip-linzy',
                'koefficient-prelomleniya',
                'pokrytie-linzy',
                'geometriya-linzy',
            ]],

        ];

        foreach ($filterSets as $filterSetData) {
            Db::transaction(function() use ($filterSetData){
                if (! $filterSet = CatalogFilterSet::find($filterSetData[0])) {
                    $filterSet = CatalogFilterSet::forceCreate([
                        'id' => $filterSetData[0],
                        'name' => $filterSetData[1]
                    ]);
                }

                $filterSet->filters()->detach();
                foreach ($filterSetData[2] as $filterSlug) {
                    if ($filterExist = CatalogFilter::whereSlug($filterSlug)->first()) {
                        $filterSet->filters()->attach($filterExist->id);
                    }
                }
            });
        }

        $categories = [
            ['medical_frames', 'Медицинские оправы', null, 3, ['type' => ProductType::OPRAVA]],
            ['sunglasses', 'Солнцезащитные очки', null, 4, ['type' => ProductType::SOLNECHNIE_OCHKI]],
            ['finished_glasses', 'Готовые очки', null, 1, ['type' => ProductType::OCHKI_MEDICINSKIE]],
            ['contact_lenses', 'Контактные линзы', null, 6, ['type' => ProductType::KONTAKTNIE_LINZY]],
                ['one_day_lenses', 'Однодневные', 'contact_lenses', 6, ['type' => ProductType::KONTAKTNIE_LINZY], 'Однодневные контактные линзы'],
                ['replacement', 'Плановой замены', 'contact_lenses', 6, ['type' => ProductType::KONTAKTNIE_LINZY], 'Контактные линзы плановой замены'],
                ['color', 'Цветные и оттеночные', 'contact_lenses', 6, ['type' => ProductType::KONTAKTNIE_LINZY], 'Цветные контактные линзы'],
                ['toric_lenses', 'Торические', 'contact_lenses', 6, ['type' => ProductType::KONTAKTNIE_LINZY, 'rezhim-zameny' => []], 'Торические контактные линзы'],
                ['solutions_and_eye_drops', 'Растворы и капли', 'contact_lenses', 1, ['type' => [ProductType::KAPLI, ProductType::RASTVOR_DLYA_LINZ]], 'Растворы и капли для глаз'],
                ['accessories', 'Аксессуары', 'contact_lenses', 1, ['type' => [ProductType::PINCET_DLYA_LINZ, ProductType::KONTEYNER_DLYA_LINZ]], 'Аксессуары для контактных линз'],
            ['lenses', 'Очковые линзы', null, 5, ['type' => [ProductType::LINZI]]],
            ['glasses_accessories', 'Аксессуары для очков', null, 1, ['type' => [ProductType::KONTEYNER_DLYA_LINZ]]],
            ['gift_certificates', 'Подарочные сертификаты', null, 2, ['type' => ProductType::SERTIFIKAT]],
        ];


        if (! $root = CatalogCategory::find(1)) {
            $root = CatalogCategory::forceCreate([
                'id' => 1,
                'slug' => 'catalog',
                'filter_set_id' => 1,
                'name' => 'Каталог'
            ]);
        }

        foreach ($categories as $catData) {
            /** @var CatalogCategory $cat */
            if (! $cat = CatalogCategory::whereSlug($catData[0])->first()) {
                $cat = CatalogCategory::forceCreate([
                    'slug' => $catData[0],
                    'name' => $catData[1],
                    'filter_set_id' => ! empty($catData[3]) ? $catData[3] : 1,
                    'parent_id' => ! empty($catData[2]) ? CatalogCategory::whereSlug($catData[2])->first()->id : $root->id,
                    'h1' => ! empty($catData[5]) ? $catData[5] : null
                ]);
            }
            else {
                $cat->fill([
                    'name' => $catData[1],
                    'filter_set_id' => ! empty($catData[3]) ? $catData[3] : 1,
                    'parent_id' => ! empty($catData[2]) ? CatalogCategory::whereSlug($catData[2])->first()->id : $root->id,
                    'h1' => ! empty($catData[5]) ? $catData[5] : null
                ]);
                $cat->save();
            }

            if (! empty($catData[4])) {
                foreach ($catData[4] as $k => $v) {
                    if (! is_array($v))
                        $v = [$v];

                    $cat->predefinedValues()->delete();
                    $filterId = CatalogFilter::findBySlug($k)->id;
                    foreach ($v as $vv) {
                        $cat->predefinedValues()->create([
                            'filter_id' => $filterId,
                            'value' => $vv
                        ]);
                    }
                }
            }
        }
    }

    public function attributeSets()
    {
        $setsData = [
            ProductType::OPRAVA => [
                'STRANA_DLYA_ETIKETKI_ROZNITSY',
                'KONSTRUKTSIYA',
                'MATERIAL',
                'PRINADLEZHNOST',

                'GOD_VYPUSKA_KOLLEKTSII',

                'RAZMER_RAMKI_',
                'DLINA_ZAUSHNIKA',
                'MEZHTSENTROVOE_RASSTOYANIE',
                'VYSOTA_RAMKI',
                'RAZMER',
                'SHIRINA_OPRAVY_V_MM',

                'KOMPLEKTATSIYA_FUTLYAROM'
            ],

            ProductType::SOLNECHNIE_OCHKI => [
                'STRANA_DLYA_ETIKETKI_ROZNITSY',
                'KONSTRUKTSIYA',
                'MATERIAL',
                'PRINADLEZHNOST',

                'GOD_VYPUSKA_KOLLEKTSII',

                'RAZMER_RAMKI_',
                'DLINA_ZAUSHNIKA',
                'MEZHTSENTROVOE_RASSTOYANIE',
                'VYSOTA_RAMKI',
                'RAZMER',
                'SHIRINA_OPRAVY_V_MM',

                'KOMPLEKTATSIYA_FUTLYAROM'
            ],

//            ProductType::LINZI => [
//                'a598fced-dde6-11e0-a19e-f46d0405f555',
//                '723fd354-1a7e-11e1-93d2-00261858a6c5',
//                '723fd356-1a7e-11e1-93d2-00261858a6c5',
//                '723fd358-1a7e-11e1-93d2-00261858a6c5',
//                'a598fcf3-dde6-11e0-a19e-f46d0405f555',
//                'd8d42ee6-0ac9-11e6-9f8f-0025902c4fba' => 1
//            ],
        ];

        foreach ($setsData as $typeId => $setData) {
            DB::transaction(function() use ($setData, $typeId){
                if (! $set = AttributeSet::whereProductTypeId($typeId)->first())
                    $set = new AttributeSet();

                $set->name = ProductType::find($typeId)->getName();
                $set->product_type_id = $typeId;

                $set->save();

                $set->attrs()->detach();
                foreach ($setData as $k => $v) {
                    if (! is_numeric($k)) {
                        list($k, $v) = array($v, $k);
                        $main = 1;
                    }
                    else {
                        $main = 0;
                    }

                    if (($attr = Attribute::findByCode($v)) OR ($attr = Attribute::findByXmlId($v))) {
                        $set->attrs()->attach($attr->id, ['main' => $main]);
                    }
                }
            });
        }
    }
}