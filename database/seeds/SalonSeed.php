<?php

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 04.01.2017
 * Time: 12:14
 */

function mb_unserialize($string) {
    $string = preg_replace('!s:(\d+):"(.*?)";!se', "'s:'.strlen('$2').':\"$2\";'", $string);
    return unserialize($string);
}
class SalonSeed extends \Illuminate\Database\Seeder
{
    public function run()
    {
        $path = '/import/asdasd.xml';

        $xml = simplexml_load_file(base_path($path));

        foreach ($xml->Классификатор->Группы->Группа as $group) {
            if ($city = City::find($group->Ид))
                continue;

            $city = new City();
            $city->id = (string)$group->Ид;
            $city->name = str_replace('г. ', '', (string)$group->Наименование);
            $city->save();
        }

        foreach ($xml->Каталог->Товары->Товар as $tovar) {

            if ($salon = Salon::find($tovar->Ид))
                continue;

            $salon = new Salon();
            $salon->name = (string)$tovar->Наименование;
            $salon->id = (string)$tovar->Ид;

            foreach ($tovar->Группы->Ид as $cityId) {
                $salon->city_id = $cityId;
            }

            foreach ($tovar->ЗначенияСвойств->ЗначенияСвойства as $prop) {
                $value = (string)$prop->Значение;
                switch ($prop->Ид) {
                    case 5:
                        $data = explode(':"', $value);
                        $data = explode('";', $data[2])[0];
                        $data = trim($data);
                        $salon->mode = $data;
                        break;
                    case 6://специализация салона
                        $salon->specialization_id = $value;
                        break;
                    case 2://телефоны
                        $salon->phone = $value;
                        break;
                    case 1://адрес
                        $salon->address = $value;
                        break;
                    case 10://описание
                        $ids = [];
                        foreach ($prop->ЗначениеСвойства as $propValue) {
                            $ids[] = (string)$propValue->Значение;
                        }
                        $salon->service_ids = count($ids) > 0 ? join(',', $ids) : null;
                        break;
                    case 9://Доп информация(услуги салона)
                        $salon->description = $value;
                        break;
                    case 4://картинки
                        $images = [];
                        foreach ($prop->Значение as $imgPath) {
                            $image = SalonImage::makeFromTemp(base_path('/import/' . $imgPath), $salon);
                            $images[] = $image;
                        }
                        break;
                    case 3://координаты
                        $salon->coordinates = $value;
                        break;
                    case 'CML2_CODE':
                        $salon->code = $value;
                        $salon->slug = $value;
                        break;
                    case 'CML2_ACTIVE':
                        if ($value != 'true') {
                            $salon->delete();
                        }
                        break;
                }
            }

            $salon->save();


            //проверка московских салонов
            if ($salon->city_id == 8) {
                $metros = [];
                foreach (MetroStation::all() as $metro) {
                    if (mb_strpos($salon->name, $metro->name) !== false) {
                        $metros[] = $metro->id;
                    }

                    if (count($metros))
                        $salon->metros()->sync($metros);
                }
            }

            foreach ($images as $img) {
                $img->salon_id = $salon->id;
                $img->save();
            }
        }
    }
}