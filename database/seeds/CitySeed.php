<?php

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 03.01.2017
 * Time: 0:55
 */
class CitySeed extends Seeder
{
    public function run()
    {
        $cities = [
            'Москва',
            'Дзержинский',
            'Балашиха',
            'Долгопрудный',
            'Железнодорожный',
            'Жуковский',
            'Ивантеевка',
            'Красногорск',
            'Лыткарино',
            'Люберцы',
            'Мытищи',
            'Одинцово',
            'Химки',
        ];
    }
}