<?php

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 04.01.2017
 * Time: 12:14
 */
class ProductSeed extends \Illuminate\Database\Seeder
{
    public function run()
    {
//        $this->createAutoAttributes();

        $path = '/import/katalog1.xml';

        $reader = new XMLReader();
        $reader->open(base_path($path)); // указываем ридеру что будем парсить этот файл

        $whitelist = [
            'FABRIKA' => 1,
            'SKIDKA_V_ROZNITSA' => 0,
//            'STRANA_PROISKHOZHDENIYA' => 1,
            'DLINA_ZAUSHNIKA' => 1,
            'STRANA_DLYA_ETIKETKI_ROZNITSY' => 1,
            'REGISTRATSIONNOE_UDOSTOVERENIE' => 0,
            'RAZMER_RAMKI_' => 1,
            'KONSTRUKTSIYA' => 1,
            'DIAMETR' => 1,
            'RADIUS_KRIVIZNY' => 1,
            'BREND' => 1,//скорее всего нет, т.к. там только один бренд указан
            'MEZHTSENTROVOE_RASSTOYANIE' => 0,
            'VYSOTA_RAMKI' => 1,
            'RAZMER' => 1,//ширина переносицы
            'SHIRINA_OPRAVY_V_MM' => 1,
            'GOD_VYPUSKA_KOLLEKTSII' => 1,
            'MATERIAL' => 1,
            'TSVET' => 1,
            'PRINADLEZHNOST' => 1,
            'KOMPLEKTATSIYA_FUTLYAROM' => 1,
        ];

        // циклическое чтение документа

        $groupMap = [];
        $productCount = 0;
        while($reader->read()) {
            if($reader->nodeType == XMLReader::ELEMENT && $reader->localName == 'Классификатор') {
                // если находим элемент <card>

                $el = $reader->expand();
                $doc = new DOMDocument();
                $xml = simplexml_import_dom($doc->importNode($el, true));

                foreach ($xml->Свойства->Свойство as $prop) {

                    $propCode = (string)$prop->БитриксКод;
                    echo(sprintf('impoting property[id=%s]' . "\n", (string)$propCode));

                    if ($propCode == 'CML2_MANUFACTURER') {
                        if (! Attribute::whereCode((string)$prop->БитриксКод)->exists()) {
                            foreach ($prop->ВариантыЗначений->Вариант as $optionData) {
                                if ($country = Country::whereName((string)$optionData->Значение)->first()) {
                                    $country->xml_id = (string)$optionData->Ид;

                                    $country->save();
                                }
                                else if ((string)$optionData->Значение != 'production'){
                                    dd('Country not find [name=' . (string)$optionData->Значение . ']');
                                }
                            }


                            $attr = new Attribute();
                            $attr->name = (string)$prop->Наименование;

                            $attr->multiple = (string)$prop->Множественное == 'false' ? 0 : 1;
                            $attr->xml_id = (string)$prop->Ид;
                            $attr->code = (string)$prop->БитриксКод;

                            $attr->sort = (int)$prop->БитриксСортировка;

                            $attr->type_id = AttributeType::MODEL;
                            $attr->class = 'Country';
                            $attr->save();
                        }
                    }

//                    if ($propCode == '44a008b2-d88d-11e0-80ad-f46d0405f555') {
//                        $attr = new Attribute();
//                        $attr->name = 'Принадлежность по возрасту';
//
//                        $attr->multiple = 1;
//
//                        $attr->sort = (int)$prop->БитриксСортировка;
//                    }

                    if (! array_key_exists($propCode, $whitelist))
                        continue;

                    \DB::connection()->transaction(function() use ($prop, $whitelist, $propCode) {
                        if (! ($attr = Attribute::where('code', $propCode)->first())) {
                            $attr = new Attribute();
                            $attr->name = (string)$prop->Наименование;

                            $attr->multiple = (string)$prop->Множественное == 'false' ? 0 : 1;
                            $attr->xml_id = (string)$prop->Ид;
                            $attr->code = (string)$prop->БитриксКод;

                            $attr->sort = (int)$prop->БитриксСортировка;
                            $attr->displayed = $whitelist[$propCode];

    //    <option value="S">Строка</option>
    //	<option value="N">Число</option>
    //	<option value="L" selected="">Список</option>
    //	<option value="F">Файл</option>
    //	<option value="G">Привязка к разделам</option>
    //	<option value="E">Привязка к элементам</option>
                            $attrTypeMap = [
                                'L' => AttributeType::SELECT,
                                'N' => AttributeType::NUMBER,
                                'S' => AttributeType::STRING,
                            ];

                            if (empty($attrTypeMap[(string)$prop->БитриксТипСвойства])) {
                                dump($prop);
                                dd('Unknown bitrix attribute type[' . (string)$prop->БитриксТипСвойства . ']');
                            }

                            $attr->type_id = $attrTypeMap[(string)$prop->БитриксТипСвойства];
                            $attr->save();
                        }

                        if ($attr->type_id == AttributeType::SELECT) {
                            foreach ($prop->ВариантыЗначений->Вариант as $optionData) {
                                if (! $attr->options()->whereXmlId((string)$optionData->Ид)->first()){
                                    $option = new AttributeOption();
                                    $option->name = (string)$optionData->Значение;
                                    $option->xml_id = (string)$optionData->Ид;
                                    $option->attribute_id = $attr->id;
                                    $option->sort = (int)$prop->Сортировка;

                                    $option->save();
                                }
                            }
                        }
                    });
                }

                foreach ($xml->Группы->Группа as $group) {
                    dump((string)$group->Ид);
                    dump((string)$group->Наименование);

                    if ((string)$group->Ид == '44a00892-d88d-11e0-80ad-f46d0405f555') {
                        $curTypeId = ProductType::OPRAVA;
                    }
                    else if ((string)$group->Ид == '44a00893-d88d-11e0-80ad-f46d0405f555'){
                        $curTypeId = ProductType::SOLNECHNIE_OCHKI;
                    }
                    else{
                        dump($group);
                        continue;
//                        throw new ErrorException('Unknown group');
                    }
                    $groupMap[(string)$group->Ид] = $curTypeId;


                    $expandGroup = function($group) use ($curTypeId, &$expandGroup, &$groupMap) {
                        if (count($group->Группы->Группа) > 0) {
                            foreach ($group->Группы->Группа as $subgroup) {
                                $groupMap[(string)$subgroup->Ид] = $curTypeId;

                                $expandGroup($subgroup);
                            }
                        }
                    };

                    $expandGroup($group);
                }
            }


//            if($reader->depth < 4) {
//                dump($reader->name);
//            }
            if($reader->nodeType == XMLReader::ELEMENT && $reader->localName == 'Предложение') {
                $productCount++;
                if (env('APP_DEBUG') && $productCount > 100) {
                    break;
                }

                $el = $reader->expand();
                $doc = new DOMDocument();
                $tovar = simplexml_import_dom($doc->importNode($el, true));

                echo(sprintf('impoting product[id=%s]' . "\n", (string)$tovar->Ид));

                if (! ($product = Product::whereExternalId((string)$tovar->Ид)->first())) {
                    $product = new Product();
                    $product->external_id = (string)$tovar->Ид;
                }
                else {
                    continue;
                }

                $product->name = (string)$tovar->Наименование;

                foreach ($tovar->Группы->Ид as $gr) {
                    if (empty($groupMap[(string)$gr])){
                        dd($tovar);
                    }

                    $product->type_id = $groupMap[(string)$gr];
                    $product->external_section_id = (string)$gr;
                }

                $images = [];
                if ((string)$tovar->Картинка) {
                    $fullPath = base_path('/import/' . (string)$tovar->Картинка);

                    $image = ProductImage::makeFromTemp(base_path('/import/' . (string)$tovar->Картинка), $product);
                    if (! file_exists($fullPath)) {
                        throw new Exception('Image not found: [' . $fullPath . ']');
                    }

                    $images[] = $image;
                }

                foreach ($tovar->ЗначенияСвойств->ЗначенияСвойства as $prop) {
                    $propId = (string)$prop->Ид;
                    $value = (string)$prop->Значение;
                    switch ($propId) {
                        case 'MORE_PHOTO':
                            dd($value);
                            break;
                        case 'CML2_ACTIVE':
                            if ($value != 'true') {
                                $product->delete();
                            }
                            break;
                        case 'CML2_ARTICLE':
                            $product->article = $value;
                            break;
//                        case 'CML2_PREVIEW_PICTURE'://картинки
//                            if ($value) {
//                                $fullPath = base_path('/import/' . $value);
//
//                                $image = ProductImage::makeFromTemp(base_path('/import/' . $value), $product);
//                                if (!file_exists($fullPath)) {
//                                    throw new Exception('Image not found: [' . $fullPath . ']');
//                                }
//                                $images[] = $image;
//                            }
//                            break;
                        case 'CML2_ACTIVE_FROM':
                            if ($value) {
                                $product->setCreatedAt($value);
                            }
                            break;
                        case 'CML2_ACTIVE_TO':
                            if ($value)
                            break;
                        case 'CML2_PREVIEW_TEXT':
                            $product->short_description = $value;
                            break;
                        case 'CML2_DETAIL_TEXT':
                            $product->description = $value;
                            break;
                        case 'CML2_MANUFACTURER':
                            $attr = Attribute::whereXmlId((string)$prop->Ид)->first();
                            $country = Country::whereXmlId($value)->first();

                            if ($country) {
                                $product->setProperty($attr->id, $country->id);
                            }
                            break;
                        case 'ddd3e699-47f6-11e1-aac8-001517b9f1e1':
                            $product->model_name = $value;
                            break;
                        case '3f51e847-41a7-11e1-b6c3-001517b9f1e1'://скидка запрещена
                            $product->discount_restricted = 1;
                            break;
                        case 'GOD_VYPUSKA_KOLLEKTSII':
                            if ($value && ($option = AttributeOption::whereXmlId($value)->first())) {
                                if (strpos($option->name, date('Y')) !== false) {
                                    $product->new = 1;
                                }
                            }
                            break;
//                        case '44a008b2-d88d-11e0-80ad-f46d0405f555'://принадлежность по полу и возраству(пока нафиг)
//                            $product->setProperty();
//                            switch($value) {
//                                case 'e793e427-deae-11e0-a19e-f46d0405f555':
//                                    $pol = [1,2];
//                                    break;
//                                case 'a0f3f297-dea7-11e0-a19e-f46d0405f555':
//                                    $pol = [1];
//                                    $vozrast = 2;
//                                    break;
//                                case '9eaa41f0-dead-11e0-a19e-f46d0405f555':
//                                    $pol = [1];
//                                    $vozrast = 2;
//                                    break;
//                                case 'e793e427-deae-11e0-a19e-f46d0405f555':
//                                    break;
//                                case 'e793e427-deae-11e0-a19e-f46d0405f555':
//                                    break;
//                            }
//                            break;
                    }

                    if ($attr = Attribute::whereXmlId((string)$prop->Ид)->first()) {
                        $attrId = $attr->id;

                        foreach ($prop->ЗначениеСвойства as $propValue) {
                            if ($attr->type_id == AttributeType::SELECT) {
                                $option = $attr->options()->whereXmlId((string)$propValue->Значение)->first();
                                if ($option)
                                    $product->setProperty($attrId, $option->id);
                            }
                            else if (
                                $attr->type_id == AttributeType::STRING
                                OR $attr->type_id == AttributeType::NUMBER
                            ) {
                                $product->setProperty($attrId, (string)$propValue->Значение);
                            }
                        }
                    }

                    if ($tovar->Цены->Цена) {
                        $product->price = (string)$tovar->Цены->Цена->ЦенаЗаЕдиницу;
                    }

                    $product->available = (int)$tovar->Количество;
                }

                \DB::connection()->transaction(function() use ($product, $images) {
                    $product->save();
                    $product->saveProperties();

                    foreach ($images as $img) {
                        $img->product_id = $product->id;
                        $img->save();
                    }
                });
            }
        }

        $this->importOldIds();
    }

    protected function importOldIds()
    {
        $file = fopen(base_path('import/export_file_983412.csv'), 'r');

        $k = 0;
        while ($line = fgetcsv($file, null, ';')) {
            $k++;
            if ($k == 1) continue;

            Product::where('external_id', $line[0])->update(['old_id' => $line[1]]);
        }
    }

    protected function createAutoAttributes()
    {
        //бренд, тип товара

        $attr = new Attribute();
        $attr->name = 'Бренд';

        $attr->multiple = 0;
        $attr->xml_id = (string)$prop->Ид;
        $attr->code = (string)$prop->БитриксКод;

        $attr->sort = (int)$prop->БитриксСортировка;

        $attr->type_id = AttributeType::MODEL;
        $attr->class = 'Country';
        $attr->save();
    }
}