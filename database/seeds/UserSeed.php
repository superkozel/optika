<?php

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 14.01.2017
 * Time: 5:25
 */
class UserSeed extends \Illuminate\Database\Seeder
{
    public function run()
    {
        $file = fopen(base_path('import/b_user.csv'), 'r');

        $i = 0;
        $timeNormalize = function($time)
        {
            $date = date_create_from_format('j.n.Y H:i:s', $time);
            if (! $date)
                return null;
            return $date->format('Y-m-d H:i:s');
        };
        while ($line = fgetcsv($file)) {
            $i++;
            if ($i == 1){
                $columns = $line;
                continue;
            }

            $line = array_combine($columns, $line);

            if (! $user = \App\User::find($line['ID'])) {
                $user = new \App\User();

                $user->email = $line['EMAIL'];
                $user->email_confirmed = $line['EMAIL'];
            }

            $user->id = $line['ID'];
            if ($user->id == 1)
            {
                $user->role_id = UserRole::SUPERADMIN;
            }
            elseif ($user->id == 2)
            {
                $user->role_id = UserRole::ADMIN;
            }

            $user->login = $line['LOGIN'];
            $user->password = '';
            $user->old_password = $line['PASSWORD'];
            $user->secret = $line['CHECKWORD'];

            $user->name = $line['NAME'];
            $user->lastname = $line['LAST_NAME'];

            $user->last_login_at = $timeNormalize($line['LAST_LOGIN']);
            $user->created_at = $timeNormalize($line['DATE_REGISTER']);
            $user->updated_at = $timeNormalize($line['DATE_REGISTER']);

            if (\App\User::whereEmail($user->email)->exists()) {
                continue;
            }

            $user->save();
        }

        fclose($file);
    }
}