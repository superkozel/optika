<?php

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 18.04.2017
 * Time: 18:24
 */
class ProductModifierSeed extends \Illuminate\Database\Seeder
{
    public function run()
    {
        ProductModifier::unguard();

        ProductModifier::updateOrCreate([
            'id' => ProductModifier::LENS_LEFT,
            'name' => 'Левая линзы',
            'product_type_id' => ProductType::OPRAVA,
            'type_id' => ProductModifierType::PRODUCT,
        ]);

        ProductModifier::updateOrCreate([
            'id' => ProductModifier::LENS_RIGHT,
            'name' => 'Правая линзы',
            'product_type_id' => ProductType::OPRAVA,
            'type_id' => ProductModifierType::PRODUCT,
        ]);

        ProductModifier::updateOrCreate([
            'id' => ProductModifier::MAKING,
            'name' => 'Изготовление',
            'product_type_id' => ProductType::OPRAVA,
            'type_id' => ProductModifierType::SERVICE,
        ]);

        ProductModifier::updateOrCreate([
            'id' => ProductModifier::TONING,
            'name' => 'Тонирование линз',
            'product_type_id' => ProductType::OPRAVA,
            'type_id' => ProductModifierType::SERVICE,
        ]);
    }
}