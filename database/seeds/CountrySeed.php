<?php

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 06.02.2017
 * Time: 11:54
 */
class CountrySeed extends \Illuminate\Database\Seeder
{
    public function run()
    {
        $data = <<<HERE
1	Россия	ru	российский *	rossiya	6
2	Польша	pl	польский *	polsha	4
3	Испания	es	испанский *	ispaniya	1
4	Италия	it	итальянский *	italiya	2
5	Китай	cn	китайский *	kitay	8
6	Германия	de	* германия	germaniya	3
7	Хорватия	hr	* хорватия	horvatiya	5
8	Украина	ua	украинский *	ukraina	7
9	Беларусь	by	белорусский *	belorussiya
10	Япония	jp	японский *	yaponiya
10	Корея	kp	корейский *	koreya
HERE;
        foreach (explode("\n", $data) as $line) {
            $line = explode("\t", $line);
            if (Country::whereName($line[1])->count() > 0) {
                continue;
            }

            $c = new Country;
            $c->name = $line[1];
            $c->alpha2 = $line[2];
            $c->pattern = $line[3];
            $c->slug = $line[4];
            $c->save();
        }

    }
}