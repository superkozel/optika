<?php

/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 06.02.2017
 * Time: 18:46
 */
class BrandSeed extends \Illuminate\Database\Seeder
{
    public function run()
    {
        $path = 'import/brands.xml';

        $this->importFile($path);
//        $this->updateProductBrands();
    }

    public function importFile($path)
    {
        $string = file_get_contents(base_path($path));

        $string = utf8_for_xml($string);
        $xml = simplexml_load_string($string);

        foreach ($xml->Каталог->Товары->Товар as $tovar) {

            echo (sprintf('impoting brand[id=%s]' . "\n", (string)$tovar->Ид));

            $images = [];
            if (! $brand = Brand::find($tovar->Ид)) {
                $brand = new Brand();
                $brand->name = (string)$tovar->Наименование;
                $brand->id = (string)$tovar->Ид;

                if ((string)$tovar->Картинка) {
                    $fullPath = base_path('import/' . (string)$tovar->Картинка);

                    if (! file_exists($fullPath)) {
                        throw new Exception('Image not found: [' . $fullPath . ']');
                    }

                    $image = BrandImage::makeFromTemp($fullPath, $brand);
                    $images[] = $image;
                }
            }


            foreach ($tovar->ЗначенияСвойств->ЗначенияСвойства as $prop) {
                $value = (string)$prop->Значение;
                switch ($prop->Ид) {
                    case 'CML2_CODE':
                        $brand->slug = $value;
                        break;
                    case 'CML2_ACTIVE':
                        if ($value != 'true') {
                            $brand->delete();
                        }
                        break;
                    case 'CML2_PREVIEW_TEXT':
                        $brand->short_description = $value;
                        break;
                    case 'CML2_DETAIL_TEXT':
                        $brand->description = $value;
                        break;
                    case 32:
                        $brand->sections_ids = join(';', (array)$prop->Значение);
                        Product::whereIn('external_section_id', (array)$prop->Значение)->update(['brand_id' => $brand->id]);
                        break;
                }
            }

            $brand->save();

            foreach ($images as $img) {
                $img->save();
            }
        }
    }
}